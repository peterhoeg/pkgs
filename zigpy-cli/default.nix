{
  stdenv,
  lib,
  python3Packages,
  makeWrapper,
}:

let
  pypkgs = python3Packages;
  py = pypkgs.python.withPackages (p: with p; [ zigpy-znp ]);

  tools = [
    "energy_scan"
    "flash_read"
    "flash_write"
    "form_network"
    "network_backup"
    "network_restore"
    "network_scan"
    "nvram_read"
    "nvram_reset"
    "nvram_write"
  ];

in
stdenv.mkDerivation rec {
  pname = "zigpy-cli";
  inherit (pypkgs.zigpy-znp) version;

  nativeBuildInputs = [ makeWrapper ];

  buildCommand =
    ''
      mkdir -p $out/bin

      install -Dm444 ${pypkgs.zigpy-znp.src}/TOOLS.md $out/share/doc/${pname}/README.md
    ''
    + lib.concatMapStringsSep "\n" (e: ''
      makeWrapper ${py}/bin/python3 $out/bin/zigpy-${e} \
        --add-flags "-m zigpy_znp.tools.${e}"
    '') tools;

  meta = with lib; {
    description = "Zigpy wrapper";
    inherit (pypkgs.zigpy-znp.meta) license;
  };
}
