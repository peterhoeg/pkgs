{
  stdenv,
  lib,
  fetchFromGitHub,
  cmake,
  glew,
  glfw3,
  libogg,
  libopus,
  opusfile,
  libsndfile,
  mpg123,
  libGL,
  libGLU,
  openal,
  platform ? "GL3",
}:

let

in
stdenv.mkDerivation rec {
  pname = "smhasher";
  version = "20220822";

  src = fetchFromGitHub {
    owner = "rurban";
    repo = pname;
    rev = "7db446a0b8d2e29cd648fb5bf4224db9aed30905";
    hash = "sha256-w7iC/ioz9zl8IhTww1RTaqDvR0WovM5KsytcM0xVP1s=";
    fetchSubmodules = true;
  };

  nativeBuildInputs = [ cmake ];

  # buildInputs = [ glew glfw3 libGL libGLU ];

  # cmakeFlags = [
  #   "-DLIBRW_INSTALL=ON"
  #   "-DLIBRW_PLATFORM=${platform}"
  # ];

  meta = {
    broken = true; # doesn't compile
  };
}
