{
  stdenv,
  lib,
  mkDerivation,
  fetchFromGitHub,
  qmake,
  pkg-config,
}:

mkDerivation rec {
  pname = "traykeys";
  version = "1.5";

  src = fetchFromGitHub {
    owner = "z80-ro";
    repo = "TrayKeys";
    rev = "v${version}";
    hash = "sha256-DDDHAYNyxl7Q780nOZMeQ/NKLjREhGz0cnlxJwZ374w=";
  };

  nativeBuildInputs = [ qmake ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin tray
    install -Dm444 -t $out/share/doc/${pname} *.md

    runHook postInstall
  '';

  meta = with lib; {
    description = "Display pressed keys and mouse events in a nice fashion in Linux";
    license = licenses.mit;
  };
}
