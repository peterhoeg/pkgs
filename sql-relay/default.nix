{
  stdenv,
  lib,
  fetchurl,
  autoreconfHook,
  pkg-config,
  openssl,
  zlib,
}:

let
  version = "2.0.0";

  rudiments = stdenv.mkDerivation (finalAttrs: {
    pname = "rudiments";
    inherit version;

    src = fetchurl {
      url = "http://downloads.sourceforge.net/rudiments/rudiments-${version}.tar.gz";
      hash = "sha256-XAfR1+ANSo7CPM6S0HroLz6V5wPbMDBbyrwfKzF6Wp4=";
    };

    buildInputs = [
      openssl
      zlib
    ];

    nativeBuildInputs = [
      autoreconfHook
      pkg-config
    ];

    enableParallelBuilding = true;

    meta = {
      description = "Library providing base classes for things such as daemons, clients and servers, and wrapper classes for the standard C functions for things like regular expressions, semaphores and signal handling.";
      license = lib.licenses.free;
      maintainers = with lib.maintainers; [ peterhoeg ];
      platforms = lib.platforms.all;
    };
  });

in
stdenv.mkDerivation (finalAttrs: {
  pname = "sql-relay";
  inherit version;

  src = fetchurl {
    url = "http://downloads.sourceforge.net/sqlrelay/sqlrelay-${version}.tar.gz";
    hash = "sha256-keoUsedsE85a6pxFFevJBqsn2ycWwwHtaZk1Y6HHtNc=";
  };

  buildInputs = [
    rudiments
    openssl
    zlib
  ];

  nativeBuildInputs = [
    pkg-config
    rudiments
  ];

  enableParallelBuilding = true;

  meta = {
    description = "SQL Relay";
    mainProgram = "sql-relay";
    license = lib.licenses.free;
    maintainers = with lib.maintainers; [ peterhoeg ];
    platforms = lib.platforms.all;
  };
})
