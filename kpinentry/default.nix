{
  mkDerivation,
  lib,
  fetchFromGitHub,
  extra-cmake-modules,
  pkg-config,
  kwallet,
  libgpg-error,
  libassuan,
}:

mkDerivation rec {
  pname = "kpinentry";
  version = "0.0.0.20200614";

  src = fetchFromGitHub {
    owner = "dmgk";
    repo = pname;
    rev = "765ceb08d73cb556b70d7efe16ab1d777407570b";
    hash = "sha256-+UglbTYJR6YK8GIv9dhw0ADkt2pVlhSlS+YuBbWNVwk=";
  };

  buildInputs = [
    libgpg-error
    kwallet
    libassuan
  ];

  nativeBuildInputs = [
    extra-cmake-modules
    pkg-config
  ];

  meta = with lib; {
    description = "Pinetry with KDE support";
    license = licenses.gpl2Only;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
