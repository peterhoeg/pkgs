{
  stdenv,
  lib,
  fetchFromGitHub,
  makeWrapper,
  brotli,
  coreutils,
  gawk,
  git,
  gnugrep,
  python3,
  yad,
}:

let
  inherit (lib) getExe;

in
# move to resholve?
stdenv.mkDerivation (finalAttrs: {
  pname = "salt";
  version = "3.27";

  src = fetchFromGitHub {
    owner = "steadfasterX";
    repo = finalAttrs.pname;
    rev = "${finalAttrs.version}-latest";
    hash = "sha256-p93BUnFCIT2KkB/IM7yT4FImNwHmEvT/vEulrBo6ZcY=";
  };

  postPatch = ''
    for f in salt salt.func reboot; do
      sed -i $f \
        -e "s@FUNCS=.*@FUNCS=$out/share/${finalAttrs.pname}/salt.func@" \
        -e "s@HASHES=.*@HASHES=$out/share/${finalAttrs.pname}/salt.func@" \
        -e "s@VARS=.*@VARS=$out/share/${finalAttrs.pname}/salt.vars@"
    done

    sed -i salt.vars \
      -e "s@SALTPATH=.*@SALTPATH=$out/share/${finalAttrs.pname}@"

    substituteInPlace salt.vars \
      --replace-fail '$SALTPATH/kdzmanager.sh' $out/libexec/kdzmanager.sh \
      --replace-fail /bin/awk ${getExe gawk} \
      --replace-fail /usr/bin/brotli ${getExe brotli} \
      --replace-fail /usr/bin/git ${git}/bin/git \
      --replace-fail /bin/grep ${getExe gnugrep} \
      --replace-fail /bin/egrep ${getExe gnugrep} \
      --replace-fail '/usr/bin/env python2' ${python3.interpreter} \
      --replace-fail '/usr/bin/env python3' ${python3.interpreter} \
      --replace-fail /usr/bin/yad ${getExe yad}
  '';

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin salt
    install -Dm555 -t $out/libexec kdzmanager.sh reboot
    install -Dm444 -t $out/share/${finalAttrs.pname} salt.{func,hashes,vars}
    install -Dm444 -t $out/share/doc/${finalAttrs.pname} LICENSE README*

    cp -r {foreign,icons} $out/share/${finalAttrs.pname}

    wrapProgram $out/bin/salt \
      --prefix PATH : ${lib.makeBinPath [ coreutils ]}

    runHook postInstall
  '';

  nativeBuildInputs = [ makeWrapper ];

  meta = {
    description = "All-in-one LG tool";
  };
})
