{
  stdenv,
  lib,
  fetchgit,
  autoreconfHook,
  help2man,
  texinfo,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "ccd2cue";
  version = "0.5";

  src = fetchgit {
    url = "https://git.savannah.gnu.org/git/ccd2cue.git";
    rev = finalAttrs.version;
    hash = "sha256-LzNGNQHSRk52o57sCuAeUGMWclcn6+SVASibRID8bFw=";
  };

  buildInputs = [ ];

  nativeBuildInputs = [
    autoreconfHook
    help2man
    texinfo
  ];

  enableParallelBuilding = true;

  meta = with lib; {
    description = "CCD to CUE converter.";
    mainProgram = "ccd2cue";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
})
