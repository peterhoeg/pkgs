{
  lib,
  stdenv,
  resholve,
  fetchurl,
  dosbox,
  dosbox-staging,
  dosbox-x,
  coreutils,
  findutils,
  imagemagick,
  runtimeShell,
  unzip,
}:

let
  # which do we use?
  # dosbox' = dosbox;
  dosbox' = dosbox-x;
  # dosbox' = dosbox-staging;

  # Game wants to write in the current directory, but of course we can't
  # let it write in the Nix store.  So create symlinks to the game files
  # in $XDG_DATA_HOME/keen4 and execute game from there.
  script =
    resholve.writeScriptBin "keen4"
      {
        interpreter = runtimeShell;
        inputs = [
          coreutils
          dosbox'
          findutils
        ];
        execer = map (e: "cannot:${if builtins.isString e then e else lib.getExe e}") [
          dosbox'
        ];
      }
      ''
        set -eEuo pipefail

        KEEN_DIR=''${XDG_DATA_HOME:-~/.local/share}/keen4

        mkdir -p $KEEN_DIR
        pushd $KEEN_DIR > /dev/null
        ln -sf @dir@/share/keen4/* .

        ${dosbox'.meta.mainProgram or "dosbox"} ./KEEN4E.EXE -fullscreen -exit || true

        find $KEEN_DIR -type l -delete
        popd > /dev/null
      '';

  icon = fetchurl {
    url = "https://gremlin.hu/system/covers/big/covers_1624.jpg?1488793166";
    hash = "sha256-1+FRZe3PFB+XaD/eiP8jgUKJppdlSL5QAwUXtH1rEMY=";
    name = "keen4.jpg";
  };

in
stdenv.mkDerivation {
  pname = "keen4";
  version = "1.0";

  src = fetchurl {
    url = "https://archive.org/download/msdos_Commander_Keen_4_-_Secret_of_the_Oracle_1991/Commander_Keen_4_-_Secret_of_the_Oracle_1991.zip";
    hash = "sha256-gCmtg4ccZ1hbGHqmeeV6BX/o/QCX3EGGtVjwi5NnvE4=";
  };

  nativeBuildInputs = [
    imagemagick
    unzip
  ];

  installPhase = ''
    mkdir -p $out/bin $out/share/keen4/icons
    mv *.{CK4,EXE} $out/share/keen4
    install -Dm555 -t $out/bin ${lib.getExe script}
    substituteInPlace $out/bin/keen4 \
      --replace-fail @dir@ ${placeholder "out"}
    magick ${icon} $out/share/keen4/icons/keen4.png
  '';

  meta = {
    description = "Commander Keen Episode 4: Secret of the Oracle";
    license = lib.licenses.free; # I know it isn't
    maintainers = with lib.maintainers; [ peterhoeg ];
    mainProgram = "keen4";
  };
}
