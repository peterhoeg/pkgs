{
  buildGoModule,
  lib,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "flexlm_exporter";
  version = "0.0.8";

  src = fetchFromGitHub {
    owner = "mjtrangoni";
    repo = pname;
    rev = "v" + version;
    hash = "sha256-icEnvSJZ0aQGuAytVYUVFuVvsa7LNzGP1PDYucnZ+zQ=";
  };

  vendorHash = "sha256-ISJOILRyL//tc6ujXKSfb9koRYrIYeewxdV8u+RCzg8=";

  meta = with lib; {
    description = "Prometheus FlexLM Client";
    license = licenses.asl20;
    mainProgram = "flexlm_exporter";
  };
}
