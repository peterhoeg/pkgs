{
  stdenv,
  lib,
  fetchFromGitHub,
  perl,
}:

stdenv.mkDerivation rec {
  pname = "fzf-marks";
  version = "1.1.20220701";

  src = fetchFromGitHub {
    owner = "urbainvaes";
    repo = pname;
    rev = "ff3307287bba5a41bf077ac94ce636a34ed56d32";
    hash = "sha256-e6z0ePN0SrMQw/jqTJHPfFSwcLJpd2ZA6kTaj++wdIk=";
  };

  buildCommand = ''
    mkdir -p $out
    cp -r $src/* $out

    for f in $out/fzf-marks.plugin.*; do
      substituteInPlace $f \
        --replace-fail ' perl ' ' ${perl}/bin/perl '
    done
  '';

  meta = with lib; {
    description = "fzf-marks";
    license = licenses.mit;
  };
}
