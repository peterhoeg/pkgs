{
  fetchFromGitHub,
  fetchurl,
  lib,
  perlPackages,
}:

# http://h.foofus.net/?page_id=218
let
  inherit (perlPackages) buildPerlPackage;

  ClassErrorHandler = buildPerlPackage rec {
    pname = "Class-ErrorHandler";
    version = "0.04";
    src = fetchurl {
      url = "mirror://cpan/authors/id/T/TO/TOKUHIROM/Class-ErrorHandler-${version}.tar.gz";
      sha256 = "sha256-NC0tz8eXogvugXmxuWuFwK56W0iCc1lSPNjHTD5wRQI=";
    };
    meta = {
      description = "Base class for error handling";
      license = with lib.licenses; [
        artistic1
        gpl1Plus
      ];
    };
  };

  HTMLTagParser = buildPerlPackage rec {
    pname = "HTML-TagParser";
    version = "0.20";
    src = fetchurl {
      url = "mirror://cpan/authors/id/K/KA/KAWASAKI/HTML-TagParser-${version}.tar.gz";
      sha256 = "sha256-fxB4FfG2FfTAXWjeuV0/XFo5EseaiMOes2mb98/RXzc=";
    };
    meta = {
      description = "HTML Tag Parser";
      license = with lib.licenses; [
        artistic1
        gpl1Plus
      ];
    };
  };

  URIFetch = buildPerlPackage rec {
    pname = "URI-Fetch";
    version = "0.15";
    src = fetchurl {
      url = "mirror://cpan/authors/id/N/NE/NEILB/URI-Fetch-${version}.tar.gz";
      hash = "sha256-N5858kxtrlxTYzKxeXn9kHmdq8zf6OeS5+6tPrjNpQw=";
    };
    meta = {
      description = "Smart URI fetching/caching";
    };
    # need internet
    doCheck = false;
  };

in
perlPackages.buildPerlPackage rec {
  pname = "praeda";
  version = "0.02.3.118b";

  src = fetchFromGitHub {
    owner = "percx";
    repo = "Praeda";
    rev = "1dc222055767fbb80c0d3e7d7ec38de370476f1d";
    sha256 = "sha256-EPZ86X6imFPsVIWdpKC96ghyculUGnOzeOetQ+UIass=";
  };

  outputs = [ "out" ];

  postPatch = ''
    touch Makefile.PL

    substituteInPlace praeda.pl \
      --replace-fail '$dirpath ="."' "\$dirpath = \"$out/share/${pname}\""
  '';

  buildInputs = with perlPackages; [
    ClassErrorHandler
    CryptSSLeay
    HTMLTableExtract
    HTMLTagParser
    LWP
    NetAddrIP
    NetSNMP
    URIFetch
  ];

  installPhase = ''
    install -Dm555 praeda.pl $out/bin/praeda
    install -d $out/share/${pname}
    cp -r data jobs $out/share/${pname}
  '';

  meta = with lib; {
    description = "Automated Printer Data Harvesting Tool";
    maintainers = with maintainers; [ peterhoeg ];
  };
}
