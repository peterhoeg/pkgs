{
  stdenv,
  lib,
  fetchurl,
  makeWrapper,
  unzip,
  jdk17,
}:

let
  jre = jdk17;

in
stdenv.mkDerivation rec {
  pname = "cloveretl";
  version = "5.1.1";

  src = fetchurl {
    url = "mirror://sourceforge/${pname}/cloveretl.engine/rel-${version}/cloverETL.rel-${
      lib.replaceStrings [ "." ] [ "-" ] version
    }.zip";
    # https://sourceforge.net/projects/cloveretl/files/cloveretl.engine/rel-5.1.1/cloverETL.rel-5-1-1.zip/download
    sha256 = "0v1zqdbvmbpyqdfgnf615qwmmwimp3nscjkwmpa8xsxvzcv7d69b";
  };

  nativeBuildInputs = [
    makeWrapper
    unzip
  ];

  dontBuild = true;

  installPhase = ''
    dir=$out/share/${pname}

    mkdir -p $dir $out/bin
    cp -r * $dir/
  '';

  postFixup = ''
    libs="$dir/lib"
    for f in $libs/*.{jar,zip}; do
      libs="$f:$libs"
    done
    makeWrapper ${jre}/bin/java $out/bin/clover \
      --set JAVA_HOME ${jre} \
      --set CLOVER_HOME $dir \
      --prefix CLASSPATH : $libs \
      --add-flags "-Dclover.home=$dir org.jetel.main.runGraph -plugins $dir/plugins"
  '';

  meta = with lib; {
    description = "cloverETL/CloverDX ETL tool";
  };
}
