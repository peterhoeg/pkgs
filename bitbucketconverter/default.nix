{
  lib,
  fetchFromGitHub,
  fetchpatch,
  python3,
}:

let
  pypkgs = python3.pkgs;

in
pypkgs.buildPythonApplication rec {
  pname = "bitbucketconverter";
  version = "20190326"; # technically called version 0.4 which is unrelated to version of the repo in which this is hosted

  format = "other";

  src = fetchFromGitHub {
    owner = "Portisch";
    repo = "RF-Bridge-EFM8BB1";
    rev = "af1bddb3d81c79d67063184219ec21f8249dffd0";
    hash = "sha256-rRSSxyz+QtPpLM8sVXeE1C4U+Nq85G9GozjtaeG49bc=";
  };

  patches = [
    (fetchpatch {
      url = "https://patch-diff.githubusercontent.com/raw/Portisch/RF-Bridge-EFM8BB1/pull/142.patch";
      hash = "sha256-av21dzB5M2XuhOiPRb8c/N/5dx63IhQAPcxEhQgGtE8=";
    })
  ];

  # raw_input is called input on py3
  postPatch = ''
    substituteInPlace BitBucketConverter.py \
      --replace-fail raw_input input
  '';

  installPhase = ''
    install -Dm555 BitBucketConverter.py $out/bin/${pname}
  '';

  propagatedBuildInputs = with pypkgs; [
    pycurl
    pillow
  ];

  checkInputs = with pypkgs; [ pytest ];

  # no tests
  doCheck = false;

  meta = with lib; {
    description = "BitBucket Converter for B0 -> B1 for RF codes";
  };
}
