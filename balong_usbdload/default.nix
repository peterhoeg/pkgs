{
  stdenv,
  lib,
  fetchFromGitHub,
}:

stdenv.mkDerivation rec {
  pname = "balong-usbdload";
  version = "20180211";

  src = fetchFromGitHub {
    owner = "forth32";
    repo = "balong-usbdload";
    rev = "752a2de73b944ee16d5eb0f263928cf72b0f6851";
    sha256 = "1i20k98s9khh88bi6025a2kf778p6awz413njphaq52my38biabp";
  };

  enableParallelBuilding = true;

  installPhase = ''
    runHook preInstall

    install -Dm755 -t $out/bin balong-usbdload loader-patch ptable-injector ptable-list

    runHook postInstall
  '';
}
