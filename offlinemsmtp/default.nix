{
  lib,
  gobject-introspection,
  python3Packages,
  wrapGAppsHook,
  libnotify,
  msmtp,
}:

let
  inherit (python3Packages) buildPythonApplication fetchPypi;

in
buildPythonApplication rec {
  pname = "offlinemsmtp";
  version = "0.3.8";

  src = fetchPypi {
    inherit pname version;
    sha256 = "sha256-hqN0flHWHghnlTLyRiXQjzbeEITwpsHOwHZ15sSXWxo=";
  };

  buildInputs = [
    gobject-introspection
    libnotify
    msmtp
  ];

  # I don't know why is needed, but without we get:
  # ValueError: Namespace Notify not available
  nativeBuildInputs = [ wrapGAppsHook ];

  propagatedBuildInputs = with python3Packages; [
    pycairo
    pygobject3
    watchdog
  ];

  pythonImportsCheck = [ "offlinemsmtp" ];

  dontWrapGApps = true;

  preFixup = ''
    makeWrapperArgs+=("''${gappsWrapperArgs[@]}")
  '';

  # No tests
  doCheck = false;

  # Needed by gobject-introspection
  strictDeps = false;

  meta = with lib; {
    description = "msmtp wrapper allowing for offline use";
    license = licenses.agpl3Only;
  };
}
