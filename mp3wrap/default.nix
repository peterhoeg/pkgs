{
  stdenv,
  lib,
  fetchurl,
  autoreconfHook,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "mp3wrap";
  version = "0.5";

  src = fetchurl {
    url = "mirror://sourceforge/mp3wrap/mp3wrap-${finalAttrs.version}-src.tar.gz";
    hash = "sha256-G0ZE9rcJncq4iwhSHVnW9zD6IRtfrx+IvQO/Yf7cBOc=";
  };

  postPatch = ''
    substituteInPlace mp3wrap.c \
      --replace-fail HOME XDG_CONFIG_HOME
    substituteInPlace mp3wrap.h \
      --replace-fail 'CONFFILE ".mp3wrap"' 'CONFFILE "mp3wrap"'
    substituteInPlace mp3wrap.1 \
      --replace-fail '~/.mp3wrap' '$XDG_CONFIG_HOME/mp3wrap' \
      --replace-fail ' .mp3wrap'  ' mp3wrap'
  '';

  nativeBuildInputs = [ autoreconfHook ];

  # gcc14 is a lot stricter
  env.NIX_CFLAGS_COMPILE = toString [
    "-Wno-error=implicit-function-declaration"
  ];

  enableParallelBuilding = true;

  postInstall = ''
    install -Dm444 -t $out/share/doc/mp3wrap {ChangeLog,README,doc/*.html}
  '';

  meta = with lib; {
    description = "Merge MP3 files";
    license = licenses.gpl2Only;
    mainProgram = "mp3wrap";
  };
})
