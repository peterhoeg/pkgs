{
  lib,
  makeBinaryWrapper,
  llama-cpp,
  curl,
  ...
}:

(llama-cpp.override { vulkanSupport = true; }).overrideAttrs (old: {
  pname = "llama-cpp-vulkan";

  buildInputs = old.buildInputs ++ [ curl ];

  nativeBuildInputs = old.nativeBuildInputs ++ [ makeBinaryWrapper ];

  cmakeFlags = old.cmakeFlags ++ [
    (lib.cmakeBool "LLAMA_CURL" true)
  ];

  preFixup = ''
    mkdir -p $out/libexec
    mv $out/bin/{llama-*,vulkan-shaders-gen} $out/libexec
    for f in $out/libexec/llama*; do
      name="$(basename "$f")"
      makeWrapper $out/libexec/$name $out/bin/$name \
        --prefix PATH : $out/libexec
    done
  '';
})
