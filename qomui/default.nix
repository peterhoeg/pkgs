{
  lib,
  fetchFromGitHub,
  python3Packages,
  coreutils,
  iproute,
  nettools,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "qomui";
  version = "0.8.2.20191201"; # develop branch

  src = fetchFromGitHub {
    owner = "corrad1nho";
    repo = pname;
    # rev = "v${version}";
    rev = "09773afd7e4f4240f3a6e5abb95ad14d235b8c41";
    sha256 = "03k73sm0p5rv9bwvhhsvxliir677x4labdahj6jpv1ynaymsxwwc";
  };

  postPatch = ''
    substituteInPlace setup.py \
      --replace-fail /usr/share/qomui/scripts ./libexec \
      --replace-fail /usr ./

    for f in qomui/*.py; do
      substituteInPlace $f \
        --replace-fail /usr/share $out/share \
        --replace-fail /scripts/ /libexec/
    done

    substituteInPlace systemd/org.qomui.service \
      --replace-fail /bin/false ${coreutils}/bin/false \
      --replace-fail org.qomui.service org.qomui

    substituteInPlace systemd/qomui.service \
      --replace-fail '/usr/bin/env ' $out/bin/

    patchShebangs ./scripts
    for f in scripts/*.sh; do
      substituteInPlace $f \
        --replace-fail '(ip '      '(${iproute}/bin/ip ' \
        --replace-fail /sbin/ip    ${iproute}/bin/ip \
        --replace-fail /sbin/route ${nettools}/bin/route
    done
  '';

  propagatedBuildInputs = with pypkgs; [
    beautifulsoup4
    lxml
    pexpect
    psutil
    pydbus
    pyqt5
    requests
  ];

  doCheck = false;

  meta = with lib; {
    description = "Qomui (Qt OpenVPN/WireGuard Management UI)";
    license = licenses.gpl3;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
  };
}
