{
  stdenv,
  lib,
  fetchFromGitHub,
  installShellFiles,
  pkg-config,
  lz4,
  openssl,
  zlib,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "lz4json";
  version = "2-unstable-2019-12-29";

  src = fetchFromGitHub {
    owner = "andikleen";
    repo = "lz4json";
    rev = "c44c51005c505de2636cc1e59cde764490de7632";
    hash = "sha256-rLjJ7qy7Tx0htW1VxrfCCqVbC6jNCr9H2vdDAfosxCA=";
  };

  buildInputs = [
    lz4
    openssl
    zlib
  ];

  nativeBuildInputs = [
    installShellFiles
    pkg-config
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin lz4jsoncat
    install -Dm444 -t $out/share/doc/lz4json README
    installManPage *.1

    runHook postInstall
  '';

  meta = {
    description = "C decompress tool for mozilla lz4json format";
    mainProgram = "lz4jsoncat";
    license = lib.licenses.free;
    maintainers = with lib.maintainers; [ peterhoeg ];
    platforms = lib.platforms.all;
  };
})
