{
  stdenv,
  lib,
  fetchFromGitHub,
  cmake,
  python3Packages,
}:

let
  version = "1.5.0";

  src = fetchFromGitHub {
    owner = "kimci86";
    repo = "bkcrack";
    rev = "v" + version;
    hash = "sha256-iyx4mOTr6MHECk9S9zrIAE5pt+cxWnOKS7iQPUyWfzs=";
  };

  pyTools = python3Packages.buildPythonApplication rec {
    pname = "bkcrack-tools";
    inherit version src;

    format = "other";

    installPhase = ''
      for f in tools/*.py; do
        install -Dm555 $f $out/bin/$(basename $f .py)
      done
    '';
  };

in
stdenv.mkDerivation rec {
  pname = "bkcrack";

  inherit version src;

  nativeBuildInputs = [ cmake ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin src/bkcrack
    ln -s ${pyTools}/bin/* $out/bin/
    install -Dm444 -t $out/share/doc/${pname} ../*.{md,txt}
    cp -r ../example $out/share/doc/${pname}

    runHook postInstall
  '';

  meta = with lib; {
    description = "Crack legacy zip encryption with Biham and Kocher's known plaintext attack";
    license = licenses.zlib;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
