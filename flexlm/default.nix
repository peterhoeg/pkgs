{
  stdenv,
  lib,
  fetchurl,
  unzip,
  autoPatchelfHook,
}:

stdenv.mkDerivation rec {
  pname = "flexlm";
  version = "11.18.1.0";

  src = fetchurl {
    url = "https://ssd.mathworks.com/supportfiles/downloads/R2022a/license_manager/R2022a/daemons/glnxa64/mathworks_network_license_manager_glnxa64.zip";
    hash = "sha256-1va6Y13xySZLaqDehhiBpEdTsX/zQYXA4JHoTrLXU2Q=";
  };

  nativeBuildInputs = [
    autoPatchelfHook
    unzip
  ];

  # there are libraries provided in sys/os/glnxa64, but we use the ones from nixpkgs instead
  buildInputs = [ stdenv.cc.cc.lib ];

  sourceRoot = ".";

  dontBuild = true;

  # bin/{lmgrd,MLM} are specific to mathworks but we install anyway
  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin etc/glnxa64/*

    runHook postInstall
  '';

  # no tests
  doCheck = false;

  doInstallCheck = true;

  installCheckPhase = ''
    $out/bin/lmutil lmver $out/bin/lmutil | grep v${version}
  '';

  meta = with lib; {
    description = "FlexLM";
    mainProgram = "lmutil";
  };
}
