{
  stdenv,
  lib,
  fetchFromGitHub,
  makeWrapper,
  jdk,
  jre_minimal,
  ant,
}:

let
  # we should be able to a minimal JRE for running the application instead of
  # the full JDK but I need to figure out how to specify all the dependencies
  # first
  jre = jre_minimal.override {
    modules = [
      "java.base"
      "java.logging"
      # need to add awt somehow
    ];
  };

  jre' = jdk;

in
stdenv.mkDerivation {
  pname = "nearinfinity";
  version = "2.2.20210501";

  src = fetchFromGitHub {
    owner = "NearInfinityBrowser";
    repo = "NearInfinity";
    rev = "6d0d1a587866bfba790820f15a807e81740d031c";
    sha256 = "sha256-j0AZ3Wqi49o19v24uVe0+7zALlDv4XfoUFIjHNgVtAM=";
  };

  nativeBuildInputs = [
    ant
    jdk
    makeWrapper
  ];

  dontConfigure = true;

  buildPhase = "ant";

  installPhase = ''
    runHook preInstall

    install -Dm444 -t $out/share/java *.jar
    mkdir -p $out/bin
    makeWrapper ${jre'}/bin/java $out/bin/near-infinity \
      --set JAVA_HOME ${jre'.home} \
      --add-flags "-jar $out/share/java/NearInfinity.jar"

    runHook postInstall
  '';

  meta = with lib; {
    description = "NearInfinity Editor";
    maintainer = with maintainers; [ peterhoeg ];
    inherit (jre.meta) platform;
  };
}
