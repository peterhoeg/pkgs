{
  stdenv,
  lib,
  fetchurl,
  unzip,
}:

let
  bins = {
    OpenShell = "shellx64";
    TpmInfo = "tpminfo";
  };

in
stdenv.mkDerivation rec {
  pname = "uefi-shell";
  version = "0.7.8";

  src = fetchurl {
    url = "https://github.com/acidanthera/OpenCorePkg/releases/download/${version}/OpenCore-${version}-RELEASE.zip";
    sha256 = "sha256-oqreYAVtUAG7p1A8x0/ZztsupKPYqAvUkJuc0lNTWZ0=";
  };

  sourceRoot = "X64/EFI/OC/Tools";

  nativeBuildInputs = [ unzip ];

  dontConfigure = true;

  dontBuild = true;

  installPhase =
    ''
      runHook preInstall

    ''
    + lib.concatStringsSep "\n" (
      lib.mapAttrsToList (n: v: ''
        install -Dm444 ${n}.efi $out/${v}.efi
      '') bins
    )
    + ''

      runHook postInstall
    '';

  dontFixup = true;

  meta = with lib; {
    description = "UEFI Shell";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
  };
}
