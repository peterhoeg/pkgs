{
  lib,
  fetchFromGitHub,
  python3Packages,
}:

let
  pypkgs = python3Packages;

  bk7231tools' = pypkgs.buildPythonPackage rec {
    pname = "bk7231tools";
    version = "2.0.2";
    pyproject = true;
    src = pypkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-gyUSOuJbOJrl9w3JKwE2nlVOpml93cMonQnrb9N157g=";
    };
    buildInputs = with pypkgs; [
      py-datastruct'
      poetry-core
      pyserial
    ];
  };

  py-datastruct' = pypkgs.buildPythonPackage rec {
    pname = "py-datastruct";
    version = "1.0.0";
    pyproject = true;
    src = pypkgs.fetchPypi {
      inherit version;
      pname = "py_datastruct";
      hash = "sha256-tgbmwrg+i7Rjq0LDddAf9FfMJVK0MmzXSJfn87lvG2s=";
    };
    buildInputs = with pypkgs; [ poetry-core ];
  };

in
pypkgs.buildPythonApplication rec {
  pname = "ltchiptool";
  version = "4.11.1";

  pyproject = true;

  src = fetchFromGitHub {
    owner = "libretiny-eu";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-VHdFt+KlhU8HGdGcewgFhCQkAjVu6qL1X2ZxfHkNz5c=";
  };

  buildInputs = with pypkgs; [
    bk7231tools'
    py-datastruct'

    bitstruct
    click
    colorama
    hexdump
    importlib-metadata
    poetry-core
    prettytable
    pycryptodome
    requests
    semantic-version
    xmodem
  ];

  meta = with lib; {
    description = "Universal, easy-to-use GUI flashing/dumping tool for BK7231, RTL8710B and RTL8720C.";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
}
