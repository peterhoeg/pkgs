{
  stdenv,
  lib,
  python3Packages,
  fetchurl,
  pandoc,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "mimembellish";
  version = "0.0.1";

  format = "other";

  src = fetchurl {
    url = "https://raw.githubusercontent.com/tshirtman/.mutt/master/MIMEmbellish";
    sha256 = "03i2nvxbxrfyvg07qzqid6xzzy39l33z4ww7mfa42vzjjsh0jn8s";
  };

  dontUnpack = true;

  # darwin's filesystem is case-insensitive so we cannot symlink the binary
  installPhase = ''
    runHook preInstall

    dir=$out/libexec

    mkdir -p $dir
    substitute ${src} $dir/${lib.toLower src.name} \
      --replace-fail "'pandoc " "'${lib.getBin pandoc}/bin/pandoc "
    ${lib.optionalString stdenv.isLinux ''
      ln -s $dir/${lib.toLower src.name} $dir/${src.name}
    ''}
    chmod 555 $dir/*

    runHook postInstall
  '';

  meta = with lib; {
    description = "MIME handler";
  };
}
