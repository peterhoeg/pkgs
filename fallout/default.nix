{
  stdenv,
  lib,
  fetchFromGitHub,
  innoextract,
  requireFile,
  xorg,
  coreutils,
  findutils,
  resholve,
  runtimeShell,
  writeShellScriptBin,
  fallout-ce,
  fallout2-ce,
}:

let

  generic =
    {
      pname,
      gameVersion,
      version,
      fileName,
      hash,
      binary,
    }:
    let
      name = "fallout${lib.optionalString (gameVersion == 2) "2"}-ce";

      lowerCase = [
        "CRITTER.DAT"
        "MASTER.DAT"
        "DATA"
        "data/SOUND"
        "data/sound/MUSIC"
      ];

      assets = stdenv.mkDerivation {
        pname = "fallout-${toString gameVersion}-assets";
        inherit version;

        src = requireFile {
          name = fileName;
          inherit hash;
          message = ''
            1. Download from GOG.com
            2. nix-prefetch-url --type sha256 file://\$(pwd)/${fileName}
          '';
        };

        nativeBuildInputs = [ (innoextract.override { withGog = true; }) ];

        buildCommand =
          ''
            dir=$out/share/${name}

            mkdir -p $out/etc $dir $out/share/doc/${name}

            innoextract \
              --extract \
              --output-dir $dir \
              --no-gog-galaxy \
              --exclude-temp \
              $src

            mv $dir/app/* $dir
            rm -rf $dir/{__support,app,commonappdata,goggame*} $dir/*.{dll,exe}
            mv $dir/*.{cfg,ini} $out/etc
            mv $dir/*.{pdf,rtf,txt} $out/share/doc/${name}
          ''
          + lib.concatMapStringsSep "\n" (e: ''
            upper=$dir/${e}
            lower=$dir/${lib.strings.toLower e}
            if [ -f "$upper" ]; then
              mv "$upper" "$lower"
            fi
          '') lowerCase;

        passthru.baseDir = "share/${name}";

        meta = with lib; {
          description = "Fallout ${toString gameVersion} game assets for use by ${name}.";
          license = licenses.unfree;
          maintainers = with maintainers; [ peterhoeg ];
          platforms = platforms.all;
        };
      };

      runner = writeShellScriptBin "run-${name}" ''
        set -eEuo pipefail

        DIR="''${XDG_DATA_HOME}/${name}"

        if [ -d "$DIR" ]; then
          find "$DIR" -type l -delete
        else
          mkdir -p "$DIR"
        fi

        lndir -silent "${assets}/${assets.baseDir}" "$DIR"

        pushd "$DIR" >/dev/null

        for f in ${assets}/etc/*; do
          b="$(basename "$f")"
          test -f "$b" && continue
          install -Dm644 "$f" "$b"
        done

        ${name}

        popd >/dev/null
      '';

    in
    resholve.mkDerivation {
      inherit pname version;

      nativeBuildInputs = [ xorg.lndir ];

      src = "";

      dontUnpack = true;
      dontConfigure = true;
      dontBuild = true;

      installPhase = ''
        runHook preInstall

        install -Dm555 ${lib.getExe runner} $out/bin/${runner.name}

        runHook postInstall
      '';

      solutions.default = {
        scripts = [ "bin/${runner.name}" ];
        interpreter = runtimeShell;
        inputs = [
          binary
          coreutils
          findutils
          xorg.lndir
        ];
        execer = map (e: "cannot:${e}") [ ];
      };

      meta = with lib; {
        license = licenses.unfree;
        hydraPlatforms = platforms.none;
      };
    };

in
{
  fallout1 = generic rec {
    pname = "fallout${toString gameVersion}";
    gameVersion = 1;
    version = "2.1.0.18";
    fileName = "setup_fallout_${version}.exe";
    hash = "sha256-vp3UAl7zQRfd+0/ooBUyCynA/J2jPZ8Po9vLGoRMfLk=";
    binary = fallout-ce.overrideAttrs (old: {
      version = "1.0.0.20231031";
      src = fetchFromGitHub {
        owner = "alexbatalov";
        repo = "fallout1-ce";
        rev = "f33143d0db9066d4c654464f66aba58871e4c81e";
        hash = "sha256-5gRuUrO5/5pKor3fuKY/cXMkkbPvkhICrR2fPKRTaTU=";
      };
    });
  };

  fallout2 = generic rec {
    pname = "fallout${toString gameVersion}";
    gameVersion = 2;
    version = "2.1.0.17";
    fileName = "setup_fallout2_${version}.exe";
    hash = "sha256-lXCADNegxT9HsIg6hh01TjPja5jur7xxaHR4FQNiUig=";
    binary = fallout2-ce;
  };
}
