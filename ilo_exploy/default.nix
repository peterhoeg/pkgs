{ lib, python3Packages }:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "ilo_exploy";
  version = "0.1";
  format = "other";

  src = ./.;

  propagatedBuildInputs = with pypkgs; [ requests ];

  installPhase = ''
    install -Dm555 -t $out/bin ./ilo_exploy.py
  '';

  meta = with lib; {
    description = "Exploit ILO";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
