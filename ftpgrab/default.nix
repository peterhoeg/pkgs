{
  lib,
  buildGoModule,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "ftpgrab";
  version = "7.1.0";

  src = fetchFromGitHub {
    owner = "crazy-max";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-oAEhxT8nuoagGhiPh5KQY4BGona5KB1dlExI9CeJmkg=";
  };

  vendorHash = "sha256-RFWu9w50qWaYsCx+hq8AeKwgRFvCFe261sa4o0XgNsA=";

  ldflags = [ "-X main.version=${version}" ];

  postInstall = ''
    mv $out/bin/cmd $out/bin/${pname}
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md
  '';

  meta = with lib; {
    description = "Grab files from FTP and SFTP";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
