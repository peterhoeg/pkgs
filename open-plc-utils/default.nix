{
  stdenv,
  lib,
  fetchFromGitHub,
}:

let
  dirs = [
    "ether"
    "key"
    "mdio"
    "mme"
    "nvm"
    "pib"
    "plc"
    "ram"
    "serial"
    "slac"
    "tools"
  ];

in
stdenv.mkDerivation (finalAttrs: {
  pname = "open-plc-utils";
  version = "0.0.4.20230430"; # 0.0.4 comes from docbook/index.html

  src = fetchFromGitHub {
    owner = "qca";
    repo = "open-plc-utils";
    rev = "1ba7d5a042e4e8ff6858b08e113eec5dc4e89cf2";
    hash = "sha256-dJAUj9EimZSwxx2/r/FmnsWIO4IPKAjcvvnCrgMDeSo=";
  };

  postPatch =
    ''
      substituteInPlace make.def \
        --replace-fail /usr/local/bin $out/bin \
        --replace-fail /usr/local/man $out/share/man \
        --replace-fail /home/www/software $out/share/doc/${finalAttrs.pname} \
        --replace-fail 'SUID_PERM=4555' 'SUID_PERM=555'
    ''
    + lib.concatMapStringsSep "\n" (e: ''
      substituteInPlace ${e}/Makefile \
        --replace-fail '-o ''${OWNER}' "" \
        --replace-fail '-g ''${GROUP}' ""
    '') dirs;

  postInstall = ''
    make manuals

    mkdir -p $out/share/doc/${finalAttrs.pname}
    cp -r docbook/* $out/share/doc/${finalAttrs.pname}
  '';

  enableParallelBuilding = true;

  meta = with lib; {
    description = "Qualcomm Atheros Open Powerline Toolkit.";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
})
