{
  stdenv,
  fetchurl,
  lib,
  pkg-config,
  ncurses,
}:

stdenv.mkDerivation rec {
  pname = "star-trader";
  version = "7.18";

  src = fetchurl {
    url = "https://ftp.zap.org.au/pub/trader/unix/trader-${version}.tar.xz";
    hash = "sha256-NzCy/t0zmt/DTBZAswmzQTsQs9eJadzXHwf9drRRToU=";
  };

  buildInputs = [ ncurses ];

  nativeBuildInputs = [ pkg-config ];

  enableParallelBuilding = true;

  meta = with lib; {
    description = "Star Trader game";
    license = licenses.gpl3Plus;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
