{
  stdenv,
  lib,
  fetchurl,
  unzip,
  dpkg,
  autoPatchelfHook,
  makeWrapper,
}:

let
  version = "2.1";
  rev = "352";

in
stdenv.mkDerivation rec {
  pname = "intel-mas-tool";
  inherit version;

  src = fetchurl {
    url = "https://downloadmirror.intel.com/737923/Intel_MAS_CLI_Tool_Linux_${version}.zip";
    hash = "sha256-JJ2tXlZJvCIHtwM7RGVu5FwHvCW89z5b7RFO9RPoPEE=";
  };

  unpackPhase = ''
    unzip -q $src -d .
  '';

  installPhase = ''
    runHook preInstall

    mkdir -p $out
    dpkg --extract intelmas_${version}.${rev}-0_amd64.deb $out
    mv $out/usr/* $out/
    rmdir $out/usr

    runHook postInstall
  '';

  # runtimeDependencies = [ (placeholder "out") ];

  preFixup = ''
    addAutoPatchelfSearchPath $out/lib/intelmas
  '';

  nativeBuildInputs = [
    autoPatchelfHook
    dpkg
    makeWrapper
    unzip
  ];

  meta = with lib; {
    description = "Drive management CLI tool for Intel® Optane™ SSDs and Intel® Optane™ Memory devices";
    broken = true; # hunt for new upstream
  };
}
