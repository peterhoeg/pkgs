{
  lib,
  buildGoModule,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "gitbatch";
  version = "0.6.1";

  src = fetchFromGitHub {
    owner = "isacikgoz";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-ovmdbyPRSebwmW6AW55jBgBKaNdY6w5/wrpUF2cMKw8=";
  };

  vendorHash = "sha256-wwpaJO5cXMsvqFXj+qGiIm4zg/SL4YCm2mNnG/qdilw=";

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} LICENSE README*
  '';

  # needs internet access
  doCheck = false;

  meta = with lib; {
    description = "Manage your git repositories in one place";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
    inherit (src.meta) homepage;
  };
}
