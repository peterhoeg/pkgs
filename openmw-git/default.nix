{
  stdenv,
  lib,
  fetchFromGitHub,
  fetchFromGitLab,
  fetchpatch,
  wrapQtAppsHook,

  openmw,
  luajit,

  bullet,

  SDL2,
  boost,
  ffmpeg,
  libXt,
  lz4,
  mygui,
  openal,
  openscenegraph,
  recastnavigation,
  unshield,
  yaml-cpp,
  VideoDecodeAcceleration ? null,
}:

let
  ver = "48-rc9";
  opengl = "GLVND";

  osg' = (openscenegraph.override { colladaSupport = true; }).overrideDerivation (old: {
    pname = "openmw-openscenegraph";
    src = fetchFromGitHub {
      owner = "OpenMW";
      repo = "osg";
      rev = "69cfecebfb6dc703b42e8de39eed750a84a87489";
      hash = "sha256-gq8P1DGRzo+D96++yivb+YRjdneSNZC03h9VOp+YXuE=";
    };
    patches = [
      (fetchpatch {
        # For Darwin, OSG doesn't build some plugins as they're redundant with QuickTime.
        # OpenMW doesn't like this, and expects them to be there. Apply their patch for it.
        name = "darwin-osg-plugins-fix.patch";
        url = "https://gitlab.com/OpenMW/openmw-dep/-/raw/0abe3c9c3858211028d881d7706813d606335f72/macos/osg.patch";
        sha256 = "sha256-/CLRZofZHot8juH78VG1/qhTHPhy5DoPMN+oH8hC58U=";
      })
    ];
    cmakeFlags = (old.cmakeFlags or [ ]) ++ [
      "-Wno-dev"
      "-DOpenGL_GL_PREFERENCE=${opengl}"
    ];
  });

  bullet' = bullet.overrideAttrs (old: {
    pname = "openmw-bullet";
    cmakeFlags = (old.cmakeFlags or [ ]) ++ [
      "-DOpenGL_GL_PREFERENCE=${opengl}"

      "-DUSE_DOUBLE_PRECISION=ON"
      "-DBULLET2_MULTITHREADING=ON"
    ];
  });

in
openmw
