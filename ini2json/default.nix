{
  stdenv,
  lib,
  fetchFromGitHub,
  cmake,
}:

stdenv.mkDerivation rec {
  pname = "ini2json";
  version = "0.0.1";

  src = fetchFromGitHub {
    owner = "Anubisss";
    repo = pname;
    rev = "5504b9857af3de5e92f4e00d13c1d1694a905be0";
    sha256 = "sha256-Im7Cin8erDgDNKdAdRnH7dzCiE7hnIlonpTMReadUaI=";
  };

  nativeBuildInputs = [ cmake ];

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname}/examples ../example.*
    install -Dm444 -t $out/share/doc/${pname}          ../{COPYING,README}
  '';

  meta = with lib; {
    description = "Convert INI to JSON";
  };
}
