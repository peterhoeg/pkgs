{
  stdenv,
  lib,
  fetchFromGitHub,
  libxkbcommon,
  xorg,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "evdoublebind";
  version = "unstable-2019-06-28";

  src = fetchFromGitHub {
    owner = "exrok";
    repo = "evdoublebind";
    rev = "3b2f3da2e02a50f8d576add00d020b9e56590f0c";
    hash = "sha256-em1hPNZ6XVXiBeqsZ/amX8WlsJbKVKehm6Sd3Z/+aLY=";
  };

  postPatch = ''
    substituteInPlace makefile \
      --replace-fail /usr ${placeholder "out"} \
      --replace-fail "'g+s'" 0555

    for f in src/make_config.cc install_xkb_rule.sh; do
      substituteInPlace $f \
        --replace-fail '/.xkb/' '/.config/xkb/' \
        --replace-fail /usr/share/X11 ${xorg.xkeyboardconfig}/share/X11
    done
  '';

  buildInputs = [ libxkbcommon ];

  postInstall = ''
    install -Dm555 -t $out/libexec install_xkb_rule.sh
  '';

  enableParallelBuilding = true;

  meta = with lib; {
    description = "Key mapper";
    inherit (src.meta) homepage;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
    mainProgram = "evdoublebind";
  };
})
