{
  stdenv,
  lib,
  fetchFromGitHub,
  go,
}:

stdenv.mkDerivation rec {
  pname = "filter-prometheus";
  version = "0.0.0.20200520";

  src = fetchFromGitHub {
    owner = "poolpOrg";
    repo = pname;
    rev = "db858a88ef9ad0396706abba3a5513457150e7e7";
    sha256 = "sha256-FdpkxVS4I8zuYLwBoAbZWrZs65RCuMwXmUFBxvn6mr8=";
  };

  nativeBuildInputs = [ go ];

  buildPhase = ''
    runHook preBuild

    export HOME=/tmp
    go build ${pname}.go

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/libexec ${pname}
    install -Dm444 -t $out/share/doc/${pname} *.md

    runHook postInstall
  '';

  meta = with lib; {
    description = "Prometheus OpenSMTPd Client";
  };
}
