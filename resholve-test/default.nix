{
  stdenv,
  writeShellScriptBin,
  lib,
  resholve,
  bash,
  coreutils,
  gnused,
}:

let
  inherit (lib) getExe;

in
resholve.mkDerivation rec {
  pname = "resholve-test";
  version = "0.1";

  src = writeShellScriptBin "resholve-test" ''
    _check_bin() {
      local bin=$1

      echo -n "Checking for $bin: "

      if command -v "$bin" >/dev/null; then
        echo "found it!"
      else
        echo "no dice :-("
      fi
    }

    _check_bin git
    _check_bin mpv
    _check_bin foobar
  '';

  solutions.default = {
    scripts = [ "bin/resholve-test" ];
    interpreter = getExe bash;
    inputs = [
      coreutils
      gnused
    ];
    fake = {
      external = [ "foobar" ];
      function = [ "git" ];
    };
  };

  dontBuild = true;

  installPhase = ''
    install -Dm555 -t $out/bin ${src}/bin/*
  '';

  meta = with lib; {
    description = "Resholve test space";
    license = licenses.gpl3Only;
  };
}
