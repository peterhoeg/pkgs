{
  stdenv,
  lib,
  fetchFromGitHub,
  pkg-config,
  avahi,
  openssl,
  pam,
  poppler,
  zlib,
}:

let
in
stdenv.mkDerivation (finalAttrs: {
  pname = "ippsample";
  version = "2023.09";

  # src = fetchurl {
  # src = fetchFromGitLab {
  src = fetchFromGitHub {
    owner = "istopwg";
    repo = "ippsample";
    rev = "v${finalAttrs.version}";
    hash = "sha256-rzY5GmnfzsStHWL1wUUMVhfHyAX9v59AL/2CXKvUExw=";
    fetchSubmodules = true;
  };

  buildInputs = [
    avahi
    # cups
    openssl
    pam
    poppler
    zlib
  ];

  nativeBuildInputs = [
    pkg-config
  ];

  enableParallelBuilding = true;

  meta = with lib; {
    description = "";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
})
