{
  stdenv,
  lib,
  fetchFromGitHub,
  fetchurl,
  perlPackages,
  makeWrapper,
  bzip2,
  gzip,
  xz,
}:

let
  localPkgs = {
    dontBuild = true;
    installPhase = "make install";
    doCheck = false;
  };

  csv_xs = perlPackages.buildPerlModule (
    localPkgs
    // rec {
      pname = "Text-CSV_XS";
      version = "1.40";

      src = fetchurl {
        url = "mirror://cpan/authors/id/H/HM/HMBRAND/${pname}-${version}.tgz";
        sha256 = "0x9n0wrih2cdxqgmsm9dl8z3zanadcjgplhwxifzls37yvhqli3a";
      };
    }
  );

  canary_stability = perlPackages.buildPerlModule (
    localPkgs
    // rec {
      pname = "Canary-Stability";
      version = "2013";

      src = fetchurl {
        url = "mirror://cpan/authors/id/M/ML/MLEHMANN/${pname}-${version}.tar.gz";
        sha256 = "1smnsx371x9zrqmylgq145991xh8561mraqfyrlbiz4mrxi1rjd5";
      };
    }
  );

  json_xs = perlPackages.buildPerlModule (
    localPkgs
    // rec {
      pname = "JSON-XS";
      version = "4.02";
      src = fetchurl {
        url = "mirror://cpan/authors/id/M/ML/MLEHMANN/${pname}-${version}.tar.gz";
        sha256 = "05ngmpc0smlfzgyhyagd5gza8g93r8hik858kmr186h770higbd5";
      };

      buildInputs = [ canary_stability ];
    }
  );

  # I don't know why the tests are broken
  PathTools' = perlPackages.PathTools.overrideAttrs (old: {
    doCheck = false;
  });

in

perlPackages.buildPerlPackage rec {
  pname = "pgbadger";
  version = "12.1";

  src = fetchFromGitHub {
    owner = "darold";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-+EFMgEk9S9ks8giPNArc5k1DSIKFaJ4TSOVn6fc6a/o=";
  };

  buildInputs = with perlPackages; [
    csv_xs
    json_xs
    JSONXSVersionOneAndTwo
    PathTools'
  ];

  nativeBuildInputs = [
    bzip2
    makeWrapper
  ];

  DESTDIR = placeholder "out";

  installFlags = [ "INSTALLDIRS=perl" ];

  outputs = [ "out" ];

  postPatch = ''
    patchShebangs .
  '';

  postInstall = ''
    mv $out/nix/store/*perl*/* $out/
    rm -rf $out/nix
  '';

  postFixup = ''
    wrapProgram $out/bin/pgbadger \
      --prefix PATH : ${
        lib.makeBinPath [
          bzip2
          gzip
          xz
        ]
      }
  '';

  # tests need fixing
  doCheck = false;
  doInstallCheck = false;

  meta = with lib; {
    description = "A fast PostgreSQL Log Analyzer";
    homepage = "http://pgbadger.darold.net/";
    license = licenses.postgresql;
  };
}
