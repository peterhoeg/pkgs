{
  lib,
  fetchFromGitHub,
  python3,
}:

let
  pypkgs = python3.pkgs;

in
pypkgs.buildPythonApplication rec {
  pname = "multicast-relay";
  version = "1.3.1";

  format = "other";

  src = fetchFromGitHub {
    owner = "alsmith";
    repo = pname;
    rev = "v" + version;
    hash = "sha256-55LaNDk6WPKNNOCm+uKI0PWrfe0y6CN4cVALw0LGdVo=";
  };

  propagatedBuildInputs = with pypkgs; [ netifaces ];

  installPhase = ''
    runHook preInstall

    install -Dm555    ${pname}.py $out/bin/${pname}
    install -Dm444 -t $out/${python3.sitePackages} ssdpDiscover.py

    install -Dm444 -t $out/share/doc/${pname} *.md
    install -Dm444 -t $out/share/doc/${pname}/examples *.json

    runHook postInstall
  '';

  meta = with lib; {
    description = "SSDP multicast relay";
    license = licenses.gpl3Only;
    maintainers = with maintainers; [ peterhoeg ];
    mainProgram = pname;
  };
}
