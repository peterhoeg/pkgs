{
  pkgsi686Linux,
  lib,
  fetchFromGitHub,
  cmake,
}:

let
  # https://github.com/XboxDev/extract-xiso/issues/29
  stdenv' = pkgsi686Linux.stdenv;

in
stdenv'.mkDerivation (finalAttrs: {
  pname = "extract-xiso";
  version = "2.7.1";

  src = fetchFromGitHub {
    owner = "XboxDev";
    repo = "extract-xiso";
    rev = "build-202303040307";
    hash = "sha256-nPkdyxI2gxxCpE4sG9sax+mbKkhbe3zZENvlO7JSUEQ=";
  };

  nativeBuildInputs = [ cmake ];

  postInstall = ''
    install -Dm444 -t $out/share/doc/extract-xiso ../*.md
  '';

  meta = with lib; {
    description = "Xbox ISO Creation/Extraction utility";
    mainProgram = "extract-xiso";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
})
