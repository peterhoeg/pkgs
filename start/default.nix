{
  stdenvNoCC,
  lib,
  tree, # for troubleshooting
  zola,
  debug ? false,
  port ? 31337, # zola needs to know where we will eventually listen
}:

let
  inherit (lib) concatStringsSep optionals optionalString;
  inherit (lib.importJSON ./repo.json) rev version;

  baseDir = "share/start";
  outDir = concatStringsSep "/" [
    (placeholder "out")
    baseDir
  ];

in
stdenvNoCC.mkDerivation (finalAttrs: {
  pname = "start-page";
  inherit version;

  src = builtins.fetchGit {
    url = "ssh://git@gitlab.com/peterhoeg/start-zola.git";
    ref = "main";
    inherit rev;
  };

  env.ADDITIONAL_ARGS = concatStringsSep " " [
    "--base-url"
    "http://localhost:${toString port}"
    "--output-dir"
    outDir
  ];

  nativeBuildInputs = [ zola ] ++ optionals debug [ tree ];

  dontInstall = true;

  doInstallCheck = true;

  preInstallCheck = optionalString debug ''
    tree $out
  '';

  installCheckPhase = ''
    runHook preInstallCheck

    test -e ${outDir}/index.html

    runHook postInstallCheck
  '';

  passthru = { inherit baseDir; };

  meta = {
    description = "Browser start page";
  };
})
