{ lib, python3Packages }:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "ff-containers-sort";
  version = "1.5.2";

  src = pypkgs.fetchPypi {
    inherit pname version;
    hash = "sha256-eFeSgXXe0H8ltyweLgaa8RSg9O1hnm4igft1mJgWx8w=";
  };

  # no tests
  doCheck = false;

  meta = with lib; {
    description = "Sort firefox containers";
    license = licenses.mpl20;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
