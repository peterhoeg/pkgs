{
  buildGoModule,
  lib,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "syncthing_exporter";
  version = "0.3.4";

  src = fetchFromGitHub {
    owner = "f100024";
    repo = pname;
    rev = "v" + version;
    hash = "sha256-vT/c3BnYC1VH39asdTFjIgGNCGkgSBi4MLClQ7j5Jxk=";
  };

  vendorHash = "sha256-SbphuRNAslQq8uEW4E3NDNdDb91+G1KB83xN+ScOlMc=";

  meta = with lib; {
    description = "Prometheus Syncthing Client";
    license = licenses.mit;
  };
}
