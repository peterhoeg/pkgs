{
  stdenv,
  lib,
  fetchurl,
  unzip,
  libjpeg,
  zlib,
}:

stdenv.mkDerivation rec {
  pname = "tispack";
  version = "0.91";

  src = fetchurl {
    url = "http://mods.pocketplane.net/${pname}-${version}.zip";
    sha256 = "sha256-PjmjsDAVF/eNS0c14L7PKrU2FoRz5VJx2YoBD/KmXEE=";
  };

  postPatch = ''
    cp makefile.unix Makefile
  '';

  sourceRoot = "${pname}-${version}/source";

  nativeBuildInputs = [ unzip ];

  buildInputs = [
    libjpeg
    zlib
  ];

  enableParallelBuilding = true;

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin tispack tisunpack tizparse
    install -Dm555 -t $out/share/doc/${pname} *.txt

    runHook postInstall
  '';

  meta = with lib; {
    description = "TIS pack/unpack";
    license = licenses.free;
    platforms = platforms.linux;
  };
}
