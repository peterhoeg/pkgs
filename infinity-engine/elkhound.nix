{
  stdenv,
  lib,
  fetchFromGitHub,
  bison,
  cmake,
  flex,
  perl,
}:

stdenv.mkDerivation rec {
  pname = "elkhound";
  version = "2020-04-13";

  src = fetchFromGitHub {
    owner = "WeiDUorg";
    repo = pname;
    rev = "a7eb4bb2151c00cc080613a770d37560f62a285c";
    sha256 = "sha256-Y96OFpBNrD3vrKoEZ4KdJuI1Q4RmYANsu7H3ZzfaA6g=";
  };

  postPatch = ''
    patchShebangs scripts
  '';

  sourceRoot = "source/src";

  nativeBuildInputs = [
    bison
    cmake
    flex
    perl
  ];

  installPhase = ''
    install -Dm555 -t $out/bin ast/astgen elkhound/elkhound
    install -Dm444 -t $out/lib {ast,elkhound,smbase}/*.a
    for d in ast elkhound smbase; do
      install -Dm444 -t $out/include/$d $src/src/$d/*.h
    done
    install -Dm444 -t $out/share/doc/${pname} $src/src/elkhound/*.txt
  '';

  meta = with lib; {
    description = "A parser generator which emits GLR parsers, either in OCaml or C++.";
    license = licenses.bsd3;
  };
}
