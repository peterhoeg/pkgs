{
  stdenv,
  lib,
  fetchurl,
}:

# https://code.google.com/archive/p/libsquish/
stdenv.mkDerivation rec {
  pname = "libsquish";
  version = "1.15";

  src = fetchurl {
    urls = [
      "mirror://sourceforge/project/libsquish/libsquish-${version}.tgz"
      # Google Code only has up to v1.11
      "https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/libsquish/squish-${version}.zip"
    ];
    sha256 = "sha256-YoeW7rpgiGYYOmHQgNRpZ8ndpnI7wKPsUjJMhdIUcmk=";
  };

  sourceRoot = ".";

  INSTALL_DIR = placeholder "out";
  USE_SHARED = 1;
  USE_SSE = 1;

  enableParallelBuilding = true;
}
