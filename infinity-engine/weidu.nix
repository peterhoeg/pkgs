{
  stdenv,
  lib,
  fetchFromGitHub,
  bison,
  cmake,
  flex,
  ocaml-ng,
  perl,
  which,

  callPackage,
}:

let
  inherit (lib.versions) major;

  # needs ocaml >= 4.04 and <= 4.11. Just happened to only have tried with 4.10
  ocaml' = ocaml-ng.ocamlPackages_4_10.ocaml.overrideAttrs (old: {
    configureFlags = old.configureFlags ++ [
      # https://github.com/WeiDUorg/weidu/issues/197
      "--disable-force-safe-string"
    ];
  });

  elkhound = callPackage ./elkhound.nix { };

in
stdenv.mkDerivation rec {
  pname = "weidu";
  version = "247.00";

  src = fetchFromGitHub {
    owner = "WeiDUorg";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-vAIIYn0urQnnL82mdfwJtahrS3uWPFferm+0F13TKcw=";
  };

  postPatch = ''
    substitute sample.Configuration Configuration \
      --replace-fail /usr/bin ${lib.makeBinPath [ ocaml' ]} \
      --replace-fail elkhound ${elkhound}/bin/elkhound

    mkdir -p obj/{.depend,x86_LINUX}
  '';

  nativeBuildInputs = [
    elkhound
    ocaml'
    perl
    which
  ];

  buildFlags = "weidu weinstall tolower";

  installPhase = ''
    runHook preInstall

    for b in tolower weidu weinstall; do
      strip $b.asm.exe
      install -Dm555 $b.asm.exe $out/bin/$b
    done

    install -Dm444 -t $out/share/doc/weidu README* COPYING

    runHook postInstall
  '';

  meta = with lib; {
    description = "InfinityEngine Modding Engine";
    license = licenses.gpl2Only;
    platforms = platforms.linux;
  };
}
