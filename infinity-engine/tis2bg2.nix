{
  stdenv,
  lib,
  fetchFromGitHub,
  cmake,
}:

let
  bgtVersion = "1.23.1";

in
stdenv.mkDerivation {
  pname = "tig2bg2";
  # last release of tis2bg2 was v1.0 in 2005
  # but there is of course no harm in bumping it every now and then to see if there is anything new at:
  # https://github.com/SpellholdStudios/BGT-WeiDU/blob/master/bgt/install/tis2bg2/tis2bg2.cpp#L255
  version = "1.0";

  src = fetchFromGitHub {
    owner = "SpellholdStudios";
    repo = "BGT-WeiDU";
    rev = "v${bgtVersion}";
    hash = "sha256-uuBhDbQ/OruG5meAjwVKmYfhMoc3R9WKtpAOZPoaLBE=";
  };

  sourceRoot = "source/bgt/install/tis2bg2";

  nativeBuildInputs = [ cmake ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin ../bin/tis2bg2

    runHook postInstall
  '';

  meta = with lib; {
    description = "BG1 -> BG2 conversion tool";
    license = licenses.gpl2Only;
    platforms = platforms.linux;
  };
}
