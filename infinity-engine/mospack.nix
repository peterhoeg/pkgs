{
  stdenv,
  lib,
  fetchFromGitLab,
  libjpeg,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "mospack";
  version = "0.92";

  src = fetchFromGitLab {
    owner = "peterhoeg";
    repo = "mospack";
    rev = "v" + finalAttrs.version;
    hash = "sha256-vvg1arXSoLpinRKi2a7nxgqE45iVGdn7ZtLGRYPDHdE=";
  };

  sourceRoot = "source/source";

  prePatch = ''
    cp makefile.unix Makefile
  '';

  buildInputs = [ libjpeg ];

  enableParallelBuilding = true;

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin jpgextract mospack mosunpack mozparse
    install -Dm555 -t $out/share/doc/${finalAttrs.pname} ../*.txt ../doc/*.html

    runHook postInstall
  '';

  doInstallCheck = true;

  installCheckPhase = ''
    $out/bin/mosunpack -V
  '';

  meta = with lib; {
    description = "MOS pack/unpack";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.unix;
  };
})
