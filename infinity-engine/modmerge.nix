{
  buildGoModule,
  fetchFromGitHub,
  lib,
}:

buildGoModule rec {
  pname = "modmerge";
  version = "1.1.20200408";

  src = fetchFromGitHub {
    owner = "ScottBrooks";
    repo = pname;
    rev = "98b1d865d4af1558d73d8217b5f1ef69c8807a85";
    sha256 = "sha256-wrrbQqIuGcysuquyiv6Ay+5DXY9P2bregLNMhv6HOmo=";
  };

  vendorHash = null;

  # doCheck = false;

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md
  '';

  meta = with lib; {
    description = "Merge v2 mod zip files back into the main game.";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
