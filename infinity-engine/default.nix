{
  stdenv,
  lib,
  libsForQt5,
  gemrb,

  callPackage,
  callPackages,
}:

let
  gog = (callPackage ./gog.nix { }).generic;

in
{
  demosrc = stdenv.mkDerivation rec {
    pname = "gemrb-demo";
    inherit (gemrb) version;
    buildCommand = ''
      dir=$out/share/${pname}
      mkdir -p $dir
      cp -r ${gemrb}/share/gemrb/demo/* $dir
    '';
    passthru.baseDir = "share/${pname}";
  };

  bg1src = gog rec {
    pname = "baldurs_gate_1";
    version = "23532";
    fileName = "baldur_s_gate_the_original_saga_gog_3_${version}.sh";
    sha256 = "1pd54ngvr8kmpk44gna18dvj2vlb7an48x1hzbyw5zyh9ckv5xb4";
    directory = "Baldur's Gate";
  };

  bg2src = gog rec {
    pname = "baldurs_gate_2";
    version = "23651";
    fileName = "baldur_s_gate_2_complete_gog_3_${version}.sh";
    sha256 = "0rpg1116l4rq5wsy9w4j9223mcrbsawbfws35i08j7jk58y91bva";
    directory = "Baldur's Gate 2";
  };

  pstsrc = gog rec {
    pname = "planescape_torment";
    version = "23483";
    fileName = "planescape_torment_gog_3_${version}.sh";
    sha256 = "0cj0c5yc7wccs1dm9zipwy89j22acqx4dzbb4698jh5h4613m24r";
    directory = "Planescape Torment";
  };

  mods = callPackages ./mod.nix { };

  modmerge = callPackage ./modmerge.nix { };

  mospack = callPackage ./mospack.nix { };

  tis2bg2 = callPackage ./tis2bg2.nix { };

  zeitgeist = libsForQt5.callPackage ./zeitgeist.nix { };
}
