{
  stdenv,
  lib,
  fetchFromGitHub,
  autoPatchelfHook,

  callPackage,
  pkgs,
}:

let
  inherit (pkgs.local) prename;

  generic =
    {
      pname,
      version,
      src,
      directory ? null,
      binDir ? null,
    }:
    let
      pname' = lib.toLower pname;
      dir = if builtins.isNull directory then pname' else directory;
      baseDir = "share/infinity-engine/${dir}";
    in
    stdenv.mkDerivation rec {
      name = "infinity-engine-mod-${pname'}";
      inherit src version;

      # the binaries from here are all mapped in
      buildInputs = [
        (callPackage ./modmerge.nix { }) # for EE games
        (callPackage ./mospack.nix { })
        # (callPackage ./tile2ee.nix { }) # doesn't compile
        # (callPackage ./tileconv.nix { }) # doesn't compile
        (callPackage ./tis2bg2.nix { })
        (callPackage ./tispack.nix { })
      ];

      nativeBuildInputs = [
        # autoPatchelfHook
        prename
      ];

      dontConfigure = true;
      dontBuild = true;

      postPatch = ''
        patchShebangs .
      '';

      passthru = {
        inherit baseDir;
        modName = pname';
      };

      # rename here is prename aka perl-rename, not the one from util-linux
      installPhase =
        ''
          runHook preInstall

          dir=$out/${baseDir}
          mkdir -p $dir
          cp -r ${dir}/* $dir/

          find $out -depth -exec rename 's/(.*)\/([^\/]*)/$1\/\L$2/' {} \;

          pushd "$dir" >/dev/null
        ''
        + (
          if (builtins.isNull binDir) then
            ''
              for d in bin install; do
                if [ -d $d ]; then
                  echo "Found binary directory, but none was specified: $d"
                  exit 1
                fi
              done
            ''
          else
            (
              ''
                if [ -d ${binDir} ]; then
                  rm -r "${binDir}"
                  mkdir -p "${binDir}"
              ''
              + (lib.concatMapStringsSep "\n" (e: ''
                for b in ${lib.getBin e}/bin/*; do
                  ln -s "$b" "${binDir}/"
                done
              '') buildInputs)
            )
            + ''
              else
                echo "Specified bin dir doesn't exist: ${binDir}"
                exit 1
              fi

              popd >/dev/null

              runHook postInstall
            ''
        );
      meta = {
        description = "Infinity Engine Mod - ${pname}";
      };
    };

  gibberling =
    {
      pname,
      version,
      hash,
      tag ? null,
      directory ? null,
      repo ? null,
      binDir ? null,
    }:
    let
      rev = if builtins.isNull tag then "v" + version else tag;
    in
    generic {
      inherit
        pname
        directory
        version
        binDir
        ;

      src = fetchFromGitHub {
        owner = "Gibberlings3";
        repo = if builtins.isNull repo then pname else repo;
        inherit rev hash;
      };
    };

  ppg =
    {
      pname,
      version,
      hash,
      directory ? null,
      repo ? null,
      binDir ? null,
    }:
    generic {
      inherit
        pname
        directory
        version
        binDir
        ;

      src = fetchFromGitHub {
        owner = "Pocket-Plane-Group";
        repo = if builtins.isNull repo then pname else repo;
        rev = "v" + version;
        inherit hash;
      };
    };

  shs =
    {
      pname,
      version,
      hash,
      tag ? null,
      directory ? null,
      repo ? null,
      binDir ? null,
    }:
    let
      rev = if builtins.isNull tag then "v" + version else tag;
    in
    generic {
      inherit
        pname
        directory
        version
        binDir
        ;

      src = fetchFromGitHub {
        owner = "SpellholdStudios";
        repo = if builtins.isNull repo then pname else repo;
        inherit rev hash;
      };
    };

in
{
  bgt = generic rec {
    pname = "bgt";
    version = "1.23.1";
    # patching = "rm -r bgt/install";

    src = fetchFromGitHub {
      owner = "SpellholdStudios";
      repo = "BGT-WeiDU";
      rev = "v${version}";
      hash = "sha256-uuBhDbQ/OruG5meAjwVKmYfhMoc3R9WKtpAOZPoaLBE=";
    };

    binDir = "install/unix/amd64";
  };

  bg1ub = ppg {
    pname = "bg1ub";
    version = "16.4";
    hash = "sha256-pKMgU6Mo1/vMfdlLlttjtYktzHPKEaceGBz1r8xJBYM=";
  };

  bg2ub = ppg {
    pname = "bg2ub";
    version = "28";
    hash = "sha256-jYlYZPLHJ1P1kRFnn4k1WoEId3twHAgp8ykz7+R80cY=";
    repo = "UnfinishedBusiness";
    directory = "ub";
  };

  bg2fixpack = gibberling {
    pname = "bg2fixpack";
    version = "13";
    hash = "sha256-2gHbgiWXxCC5qnEzhhTDf6XBUfzC4mUjnbAOgdZBO9Y=";
    repo = "BG2-Fixpack";
  };

  bolsa = shs {
    pname = "Bolsa";
    version = "6.0.0";
    hash = "sha256-e9hSW7W0p0NMzSsOdPLQtKI+TAJzuXco4GNvdf1TJ6I=";
  };

  # https://github.com/SpellholdStudios/generalized_biffing
  generalized_biffing = shs {
    pname = "generalized_biffing";
    version = "2.5";
    hash = "sha256-oaxDCW3q3x2TEtKU2RtRSaoIqDVtUCkb0Pkr+nR9knE=";
  };

  # this seems to require tobex which patches the executable in memory and thus
  # works only on Windows with the official binaries
  iwdification = gibberling rec {
    pname = "iwdification";
    version = "Beta_6";
    tag = version;
    hash = "sha256-mV8RmAUQavTtJgQAw9eF1CAGZUXiQQucblvvKFjptRE=";
  };

  # doesn't work as we cannot compile tileconv and tile2ee and we cannot use the
  # 32 bit executables vendored in ntotsc
  # ntotsc = shs {
  #   pname = "NTotSC";
  #   version = "4.2.0";
  #   hash = "sha256-DunNjVxONQF4Mzn8iHKOhfzSViPgoZb+mei1wGg43OE=";
  #   binDir = "bin/unix";
  # };

  stuff-of-the-magi = shs {
    pname = "StuffOfTheMagi";
    version = "6.0.0";
    hash = "sha256-6cXxCsLCEgqZ0v0CBRAnUrdTfhKutCuDmxd+7VH7l/M=";
  };

  tweaks-anthology = gibberling {
    pname = "tweaksanthology";
    version = "9.20201031";
    repo = "Tweaks-Anthology";
    tag = "6bfc0ca4703b13abbe51d952ab42de57432da7c3";
    hash = "sha256-7ZvC+LGXXiGDbU9ZTbPBi/2E2i6+QOQb2s/QSFPzYFM=";
    directory = "cdtweaks";
  };

  widescreen = gibberling {
    pname = "widescreen";
    version = "3.07";
    hash = "sha256-lV/Cfq4rl2X1mvzWLzZ8HJN046Bz/qhYKvJCNZSdTX4=";
  };

  # https://github.com/SpellholdStudios/BP-BGT-Worldmap
  worldmap = shs {
    pname = "BP-BGT-Worldmap";
    version = "12.0.0";
    hash = "sha256-+ZZifA5hpK9W01Fpj6e7UT4yjScb2ICpURXFd3wCjNY=";
    binDir = "bin/unix";
  };
}
