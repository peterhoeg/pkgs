{
  stdenv,
  lib,
  requireFile,
  p7zip,
  unzip,
}:

{
  generic =
    {
      pname,
      version,
      fileName,
      sha256,
      directory,
    }:
    stdenv.mkDerivation (
      let
        baseDir = "share/${pname}";

      in
      rec {
        inherit pname version;

        src = requireFile rec {
          name = fileName;
          inherit sha256;
          message = ''
            1. Download from GOG.com
            2. nix-prefetch-url --type sha256 file://\$(pwd)/${name}
          '';
        };

        nativeBuildInputs = [ unzip ]; # p7zip

        # use -LL to force everything to lower case
        buildCommand =
          let
            srcDir = lib.toLower "data/noarch/prefix/drive_c/GOG Games/${directory}";

          in
          ''
            dir=$out/${baseDir}

            unzip -LL -q "$src" || true

            find "${srcDir}" -type l -delete

            mkdir -p $dir
            mv "${srcDir}"/* $dir
            mkdir -p $out/nix-support
            echo $src > $out/nix-support/_dependencies.txt
          '';

        passthru = {
          inherit baseDir;
        };

        meta = with lib; {
          license = licenses.unfree;
          hydraPlatforms = platforms.none;
        };
      }
    );
}
