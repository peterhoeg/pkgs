{
  stdenv,
  lib,
  fetchurl,
  unzip,
  libimagequant,
  libjpeg_turbo,
  pngquant,
  zlib,

  callPackage,
}:

let
  squish = callPackage ./squish.nix { };

in
stdenv.mkDerivation rec {
  pname = "tile2ee";
  version = "0.3b";

  # github is outdated
  src = fetchurl {
    url = "http://www.shsforums.net/files/download/1134-tile2ee-a-mos-and-tis-converter/";
    sha256 = "sha256-oHiFBVOYpcvvgQgHyV3lWM6mGtWRW+UhBjlK+R/sCrQ=";
    name = "${pname}-${version}.zip";
  };

  postPatch = ''
    substituteInPlace ColorQuant.hpp \
      --replace-fail lib/libimagequant.h libimagequant.h
  '';

  sourceRoot = "src";

  buildInputs = [
    libimagequant
    squish
    zlib
  ];

  nativeBuildInputs = [ unzip ];

  enableParallelBuilding = true;

  meta = with lib; {
    description = "Tile2EE";
    license = licenses.free;
    platforms = platforms.linux;
  };
}
