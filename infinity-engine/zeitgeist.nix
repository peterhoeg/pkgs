{
  lib,
  fetchFromGitHub,
  mkDerivation,
  qmake,
  qtbase,
  quazip,

  callPackage,
}:

let
  weidu = callPackage ./weidu.nix { };

in
mkDerivation rec {
  pname = "zeitgeist";
  version = "2020-01-28";

  src = fetchFromGitHub {
    owner = "WeiDUorg";
    repo = pname;
    rev = "74c89528e0503f4ade9d139f8b8072d971cb4c1e";
    sha256 = "sha256-SudVzeXZNAafHBR6D0shR7b++wDt9RjULJZEqReZEhw=";
  };

  patches = [ ./zeitgeist_binary_path.patch ];

  postPatch = ''
    substituteInPlace zeitgeist.pro \
      --replace-fail 'CONFIG += debug'      'CONFIG += release' \
      --replace-fail /usr/include/quazip5   ${lib.getDev quazip}/include/QuaZip-Qt5-${quazip.version}/quazip \
      --replace-fail '/usr/lib64 -lquazip5' '${lib.getLib quazip}/lib -lquazip1-qt5'

    substituteInPlace src/platform.h \
      --replace-fail '"weidu"' '"${weidu}/bin/weidu"'
  '';

  nativeBuildInputs = [ qmake ];

  buildInputs = [
    quazip
    weidu
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin zeitgeist
    install -Dm444 -t $out/share/doc/${pname} README* COPYING

    runHook postInstall
  '';

  meta = with lib; {
    description = "GUI for the InfinityEngine Modding Engine";
    license = licenses.gpl2Only;
    platforms = platforms.linux;
  };
}
