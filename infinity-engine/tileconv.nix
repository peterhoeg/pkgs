{
  stdenv,
  lib,
  fetchFromGitHub,
  fetchurl,
  libimagequant,
  libjpeg_turbo,
  unzip,
  zlib,

  callPackage,
}:

let
  squish = callPackage ./squish.nix { };

in
stdenv.mkDerivation rec {
  pname = "tileconv";
  version = "0.6d";

  src = fetchurl {
    url = "http://www.shsforums.net/files/download/1094-tileconv-a-mostis-compressor/";
    sha256 = "sha256-29KHntlC3G78dD3YlKwofCdo400k1X7PazFRS5uKm4Q=";
    name = "${pname}-${version}.zip";
  };

  sourceRoot = "src";

  buildInputs = [
    libimagequant
    libjpeg_turbo
    zlib
  ];

  nativeBuildInputs = [ unzip ];

  enableParallelBuilding = true;

  meta = with lib; {
    description = "tileconv";
    license = licenses.free;
    platforms = platforms.linux;
  };
}
