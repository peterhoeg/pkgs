{
  stdenv,
  lib,
  fetchFromGitHub,
  fetchurl,
  # , fetchpatch
  cmake,
  pkg-config,
  unzip,
  glew,
  glfw3,
  libogg,
  libopus,
  # , opusfile # see cmakeFlags
  libsndfile,
  mpg123,
  libGL,
  openal,
  platform ? "GL3",
}:

let
  bins = {
    RE3 = "re3";
    RELCS = "re3LCS";
    REVC = "re3VC";
  };

  librw = stdenv.mkDerivation {
    pname = "librw";
    version = "unstable-2021-08-19";

    src = fetchFromGitHub {
      owner = "aap";
      repo = "librw";
      rev = "5501c4fdc7425ff926be59369a13593bb6c81b54";
      sha256 = "sha256-z3hQ0PAlwfPfgM5oFwX8rMVc/IKLTo2fgFw1/nSH87I=";
    };

    nativeBuildInputs = [ cmake ];

    buildInputs = [
      glew
      glfw3
      libGL
    ];

    cmakeFlags = [
      "-DLIBRW_INSTALL=ON"
      "-DLIBRW_PLATFORM=${platform}"
    ];
  };

  generic =
    {
      pname,
      id,
      branch,
      hash,
    }:
    let
      bin = bins.${id};
    in
    stdenv.mkDerivation {
      inherit pname;
      version = "0-unstable-2021-09-03";

      src = fetchurl {
        url = "https://web.archive.org/web/20210903220036/https://codeload.github.com/GTAmodding/re3/zip/refs/heads/${branch}";
        # 20210903153549
        inherit hash;
      };

      # doesn't work
      patches = [
        # (fetchpatch {
        #   # for id_, see https://briangrinstead.com/blog/fetch-latest-copy-from-wayback/
        #   url = "https://web.archive.org/web/20210903151250id_/https://patch-diff.githubusercontent.com/raw/GTAmodding/re3/pull/967.patch";
        #   hash = "sha256-dM43dvy80B8mKiCCm/Wc+QGSodcQzdEHOwBiu7EIoDw=";
        #   name = "re3_relocatable.patch";
        # })
      ];

      nativeBuildInputs = [
        cmake
        pkg-config
        unzip
      ];

      buildInputs = [
        librw
        glew
        glfw3
        mpg123
        libogg
        libopus
        libsndfile
        openal
      ];

      cmakeFlags =
        let
          inherit (lib) cmakeBool;
        in
        [
          (cmakeBool "LIBRW_PLATFORM_${platform}" true)
          (cmakeBool "${id}_INSTALL" true)
          (cmakeBool "${id}_VENDORED_LIBRW" false)
          (cmakeBool "${id}_WITH_OPUS" false) # build is broken when using openfile
          (cmakeBool "${id}_WITH_LIBSNDFILE" true)
        ];

      unpackPhase = ''
        unzip -d. $src
      '';

      sourceRoot = "re3-${branch}";

      # this is ugly, but until https://github.com/GTAmodding/re3/pull/967 is fixed, it's needed
      postInstall = ''
        mkdir -p $out/.tmp
        mv $out/* $out/.tmp

        dir=$out/share/${pname}
        mkdir -p $dir
        mv $out/.tmp/* $dir/ || true

        rmdir $out/.tmp
      '';

      meta = with lib; {
        description = "GTA3 (${id}), reverse engineered";
        license = licenses.free;
        maintainers = with maintainers; [ peterhoeg ];
        mainProgram = bin;
      };
    };

in
{
  re3 = generic {
    pname = "re3";
    id = "RE3";
    branch = "master";
    hash = "sha256-WsNKWAMDUuHmPs+zPUZjo+VlUnlrsYHHkW8KfHjyG7o=";
  };

  re3lcs = generic {
    pname = "re3lcs";
    id = "RELCS";
    branch = "lcs";
    hash = "sha256-9jnffUqCRjz0kTAVCjCejAmvoECwAld3Xr5WZoywflQ=";
  };

  re3vc = generic {
    pname = "re3vc";
    id = "REVC";
    branch = "miami";
    hash = "sha256-QcoxfJ27GPylCW/dk75eAcmeq8axT3QUz6swjwmqslk=";
  };
}
