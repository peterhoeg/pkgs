{
  stdenv,
  lib,
  bundlerEnv,
  ruby,
  writeText,
  runtimeShell,
}:

let
  env = bundlerEnv {
    pname = "geminabox";
    gemdir = ./.;

    meta = with lib; {
      description = "Local gem cache";
      license = licenses.mit;
      maintainers = with maintainers; [ peterhoeg ];
      platforms = platforms.unix;
    };
  };

  bin = writeText "geminabox" ''
    #!${runtimeShell}

    rackup "@out@"/etc/${configRu.name}
  '';

  configRu = writeText "config.ru" ''
    require 'rubygems'
    require 'geminabox'

    Geminabox.rubygems_proxy = true
    Geminabox.allow_remote_failure = true
    Geminabox.data = './var/gems'

    run Geminabox::Server
  '';

in
stdenv.mkDerivation rec {
  name = "geminabox";

  buildCommand = ''
    mkdir -p $out/{bin,etc}

    install -Dm444 ${configRu} $out/etc/${configRu.name}
    install -Dm555 ${bin}      $out/bin/${bin.name}
    substituteInPlace $out/bin/${bin.name} \
      --replace-fail @out@ $out
  '';
}
