{
  lib,
  fetchFromGitHub,
  python3Packages,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "python-onvif-zeep";
  version = "0.2.12.20190417";

  src = fetchFromGitHub {
    owner = "FalkTannhaeuser";
    repo = pname;
    rev = "d549eed77d9dd5fa23100db4090b89817d280d4c";
    hash = "sha256-31VYxyY6VXBUjMmGNTWs4iFxPSFAkjuJqSB9sJYW0m4=";
  };

  propagatedBuildInputs = with pypkgs; [ zeep ];

  doCheck = false;

  meta = with lib; {
    description = "Python ONVIF library and client";
    mainProgram = "onvif-cli";
    platform = platforms.unix;
  };
}
