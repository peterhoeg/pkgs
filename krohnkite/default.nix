{
  lib,
  stdenv,
  fetchFromGitHub,
  qt6Packages,
  kdePackages,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "krohnkite";
  version = "0.9.5-unstable-20240612";

  src = fetchFromGitHub {
    owner = "anametologin";
    repo = "krohnkite";
    # rev = "v${finalAttrs.version}";
    rev = "9ad9550d0551e95189a37bab920e9f99026b1040";
    hash = "sha256-opcf2UA8sskzG0+gXnOEivOhf+wZ2oDXg22TX5jYOl4=";
  };

  postPatch = ''
    for f in res/metadata.*; do
      substituteInPlace $f \
        --replace-fail '$REV' ${finalAttrs.version} \
        --replace-fail '$VER' ${finalAttrs.version}
    done
  '';

  env.LANG = "C.UTF-8";

  buildInputs = with kdePackages; [
    qt6Packages.qtbase
    kcoreaddons
    kpackage
    kwindowsystem
    systemsettings
  ];

  nativeBuildInputs = [ qt6Packages.wrapQtAppsHook ];

  dontBuild = true;

  # 1. --global still installs to $HOME/.local/share so we use --packageroot
  # 2. plasmapkg2 doesn't copy metadata.desktop into place, so we do that manually
  installPhase = ''
    runHook preInstall

    kpackagetool6 \
      --install res/ \
      --packageroot $out/share/kwin/scripts
    install -Dm444 res/metadata.desktop $out/share/kservices5/krohnkite.desktop

    runHook postInstall
  '';

  meta = with lib; {
    description = "A dynamic tiling extension for KWin";
    license = licenses.mit;
    maintainers = with maintainers; [ seqizz ];
    inherit (src.meta) homepage;
    inherit (kdePackages.kwindowsystem.meta) platforms;
  };
})
