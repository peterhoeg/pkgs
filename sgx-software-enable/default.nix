{
  stdenv,
  lib,
  fetchFromGitHub,
  makeWrapper,
  pkg-config,
  openssl,
  zlib,
}:

let

in
stdenv.mkDerivation rec {
  pname = "sgx-software-enable";
  version = "1.0.20230107";

  src = fetchFromGitHub {
    owner = "intel";
    repo = pname;
    rev = "7977d6dd373f3a14a615ee9be6f24ecd37c0b43d";
    hash = "sha256-xBmFCrnNQq0xKwv7irJFN8YRfBCLmSxtak5dtHFv/xk=";
  };

  buildInputs = [
    # openssl
    # zlib
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin sgx_enable
    install -Dm444 -t $out/share/doc/${pname} *.md

    runHook postInstall
  '';

  meta = with lib; {
    description = "Enable SGX on software-controlled only machines";
    license = licenses.bsd3;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
}
