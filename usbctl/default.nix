{
  stdenv,
  lib,
  fetchFromGitHub,
  diffutils,
  gnugrep,
  procps,
  util-linux,
}:

stdenv.mkDerivation rec {
  pname = "usbctl";
  version = "1.2";

  src = fetchFromGitHub {
    owner = "anthraxx";
    repo = pname;
    rev = version;
    sha256 = "sha256-oegrBo1xRb/8WXo9q+Rq+3xEdFe5DQiVmepjmubyE88=";
  };

  postPatch = ''
    substituteInPlace contrib/systemd/deny-new-usb.service \
      --replace-fail /usr/bin/sysctl ${procps}/bin/sysctl

    substituteInPlace ${pname} \
      --replace-fail '$(basename "$0")' ${pname} \
      --replace-fail '=dmesg'  '=${util-linux}/bin/dmesg' \
      --replace-fail 'diff '   '${diffutils}/bin/diff ' \
      --replace-fail 'grep '   '${gnugrep}/bin/grep ' \
      --replace-fail 'sysctl ' '${procps}/bin/sysctl '
  '';

  dontBuild = true;

  installFlags = [
    "PREFIX=${placeholder "out"}"
  ];

  meta = with lib; {
    description = "Linux-hardened deny_new_usb control";
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
  };
}
