{
  lib,
  fetchFromGitHub,
  python3Packages,
  passage,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "bitwarden-to-pass";
  version = "20211026";

  format = "other";

  src = fetchFromGitHub {
    owner = "Shuta4";
    repo = pname;
    rev = "f50a9ab3e96980247a46de19b88eccad5cb4a47b";
    hash = "sha256-HSv+rULXoKdocrIdx1S0EHE4QJXVCSep7Y74o6WAGaw=";
  };

  postPatch = ''
    substituteInPlace ${pname} \
      --replace-fail '"pass"' '"${lib.getExe passage}"'
  '';

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin ${pname}
    install -Dm444 -t $out/share/doc/${pname} *.md LICENSE

    runHook postInstall
  '';

  # no tests
  doCheck = false;

  meta = with lib; {
    description = "Bitwarden to pass";
    license = licenses.gpl3Only;
  };
}
