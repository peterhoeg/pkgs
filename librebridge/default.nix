{
  mkDerivation,
  fetchFromGitHub,
  qmake,
  lib,
  pkg-config,
}:

mkDerivation rec {
  pname = "librebridge";
  version = "0.0.1.20170727";

  src = fetchFromGitHub {
    owner = "juef17";
    repo = "LibreBridge";
    rev = "85d220b8bfce39b13adac0d5fbd6b978d40856d4";
    sha256 = "0lx1j8s7xw9ak8dji1kl0fw895kkfibd7plsfqbv08inpazyw961";
  };

  nativeBuildInputs = [
    pkg-config
    qmake
  ];

  enableParallelBuilding = true;

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin LibreBridge
    install -d $out/share/LibreBridge
    cp -r images $out/share/LibreBridge

    runHook postInstall
  '';

  meta = with lib; {
    description = "Play bridge";
    maintainers = with maintainers; [ peterhoeg ];
  };
}
