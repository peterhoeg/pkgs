{
  buildGoModule,
  lib,
  fetchFromGitHub,
  go-md2man,
  installShellFiles,
}:

buildGoModule rec {
  pname = "unpoller";
  version = "2.1.3";

  src = fetchFromGitHub {
    owner = "unpoller";
    repo = pname;
    rev = "v" + version;
    hash = "sha256-xh9s1xAhIeEmeDprl7iPdE6pxmxZjzgMvilobiIoJp0=";
  };

  nativeBuildInputs = [
    go-md2man
    installShellFiles
  ];

  vendorHash = "sha256-HoYgBKTl9HIMVzzzNYtRrfmqb7HCpPHVPeR4gUXneWk=";

  postInstall = ''
    dir=$out/share/${pname}
    mkdir -p $dir
    cp -r init/webserver $dir/

    go-md2man -in examples/MANUAL.md -out ${pname}.1
    installManPage *.1
    install -Dm444 -t $out/share/doc/${pname}          LICENSE *.md
    install -Dm444 -t $out/share/doc/${pname}/examples examples/*
  '';

  meta = with lib; {
    description = "Prometheus UniFi Poller";
    license = licenses.mit;
    mainProgram = "unifi-poller";
  };
}
