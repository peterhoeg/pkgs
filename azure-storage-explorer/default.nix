{
  lib,
  stdenv,
  fetchurl,
  autoPatchelfHook,
  wrapGAppsHook,
  makeWrapper,
  atomEnv,
  libuuid,
  pulseaudio,
  at-spi2-atk,
  coreutils,
  gawk,
  libsecret,
  xdg-utils,
  systemd,
}:

stdenv.mkDerivation rec {
  pname = "azure-storage-explorer";
  version = "0.0.1";

  src = fetchurl {
    url = "https://download.microsoft.com/download/A/E/3/AE32C485-B62B-4437-92F7-8B6B2C48CB40/StorageExplorer-linux-x64.tar.gz";
    hash = "sha256-IV2MY41CuyuIBgStIjcNoWr7TgS5qNdgz+7sQvlKMSg=";
  };

  sourceRoot = ".";

  nativeBuildInputs = [
    autoPatchelfHook
    makeWrapper
    wrapGAppsHook
  ];

  buildInputs = atomEnv.packages ++ [
    at-spi2-atk
    libsecret
    libuuid
  ];

  runtimeDependencies = [
    pulseaudio
    (lib.getLib systemd)
  ];

  preFixup = ''
    gappsWrapperArgs+=(--prefix PATH : "${coreutils}/bin:${gawk}/bin:${xdg-utils}/bin")
    gappsWrapperArgs+=(--add-flags --disable-namespace-sandbox)
    gappsWrapperArgs+=(--add-flags --disable-setuid-sandbox)
  '';

  installPhase = ''
    runHook preInstall

    mkdir -p $out/{opt,bin}

    mv * $out/opt

    # substituteInPlace $out/share/applications/teams.desktop \
    #   --replace-fail /usr/bin/ ""

    # ln -s $out/opt/teams/teams $out/bin/

    # Work-around screen sharing bug
    # https://docs.microsoft.com/en-us/answers/questions/42095/sharing-screen-not-working-anymore-bug.html
    # rm $out/opt/teams/resources/app.asar.unpacked/node_modules/slimcore/bin/rect-overlay

    runHook postInstall
  '';

  dontAutoPatchelf = true;

  # Includes runtimeDependencies in the RPATH of the included Node modules
  # so that dynamic loading works. We cannot use directly runtimeDependencies
  # here, since the libraries from runtimeDependencies are not propagated
  # to the dynamically loadable node modules because of a condition in
  # autoPatchElfHook since *.node modules have Type: DYN (Shared object file)
  # instead of EXEC or INTERP it expects.
  # Fixes: https://github.com/NixOS/nixpkgs/issues/85449
  postFixup = ''
    autoPatchelf "$out"

    runtime_rpath="${lib.makeLibraryPath runtimeDependencies}"

    for mod in $(find "$out/opt" -type f -name '*.node'); do
      mod_rpath="$(patchelf --print-rpath "$mod")"

      echo "Adding runtime dependencies to RPATH of Node module $mod"
      patchelf --set-rpath "$runtime_rpath:$mod_rpath" "$mod"
    done;

    makeWrapper $out/opt/StorageExplorerExe $out/bin/storage-explorer
  '';

  meta = with lib; {
    description = "Azure Storage Explorer";
    homepage = "https://microsoft.com";
    license = licenses.unfree;
    platforms = [ "x86_64-linux" ];
    broken = true; # due to atom dependency
  };
}
