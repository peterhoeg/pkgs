{
  buildGoModule,
  lib,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "script_exporter";
  version = "2.5.2";

  src = fetchFromGitHub {
    owner = "ricoberger";
    repo = pname;
    rev = "v" + version;
    hash = "sha256-Sc707StdLmQeulEDYzHcY9lxrwx8NpjYWKcZSO3xJLE=";
  };

  vendorHash = "sha256-CWOdflrWqnMzbj+UqaJ4n3/SFJqudkE3loXqfci8xNI=";

  ldflags = [
    "-w"
    "-s"
    "-X github.com/prometheus/common/version.Branch=master"
    "-X github.com/prometheus/common/version.BuildDate=unknown"
    "-X github.com/prometheus/common/version.Revision=${src.rev}"
    "-X github.com/prometheus/common/version.Version=${version}-0"
  ];

  meta = with lib; {
    description = "Prometheus Script Exporter";
  };
}
