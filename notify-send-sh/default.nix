{
  stdenv,
  lib,
  resholve,
  fetchFromGitHub,
  bash,
  coreutils,
  glib,
  gnused,
}:

# NOTE: notify-action.sh uses an array to store a command and its args, so we
# have to patch it to get the full path as resholve cannot handle that.

let
  inherit (lib) getExe;

in
resholve.mkDerivation rec {
  pname = "notify-send.sh";
  version = "1.2";

  src = fetchFromGitHub {
    owner = "vlevit";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-v52+hQC9USv0iUg5K3il75sjYvNGeTuK28efXWAu89A=";
  };

  postPatch = ''
    sed -i notify-send.sh \
      -e "s@local notify_action=.*@local notify_action=$out/libexec/notify-action.sh@"

    sed -i notify-action.sh -E \
      -e "s@(GDBUS_MONITOR)=\(gdbus (.*)\)@\1=\"${lib.getBin glib}/bin/gdbus \2\"@"
  '';

  solutions = {
    main = {
      scripts = [ "bin/notify-send" ];
      interpreter = getExe bash;
      inputs = [
        coreutils
        glib
        gnused
        "${placeholder "out"}/libexec"
      ];
      keep = {
        "$notify_action" = true;
      };
    };

    action = {
      scripts = [ "libexec/notify-action.sh" ];
      interpreter = getExe bash;
      inputs = [
        bash
        coreutils
        glib
        gnused
      ];
      keep = {
        "$GDBUS_MONITOR" = true;
        "bash:$cmd" = true;
      };
    };
  };

  dontBuild = true;

  installPhase = ''
    install -Dm555 notify-send.sh $out/bin/notify-send
    install -Dm555 notify-action.sh -t $out/libexec
    install -Dm444 AUTHORS* LICENSE* README* -t $out/share/doc/${pname}
  '';

  meta = with lib; {
    description = "Send notifications";
    license = licenses.gpl3;
  };
}
