{
  resholve,
  lib,
  fetchFromGitHub,
  runtimeShell,
  coreutils,
  findutils,
  gawk,
  gnugrep,
  gnused,
  smartmontools,
}:

resholve.mkDerivation rec {
  pname = "smartmon";
  version = "20240509";

  src = fetchFromGitHub {
    owner = "prometheus-community";
    repo = "node-exporter-textfile-collector-scripts";
    # no releases
    rev = "18fcb1c3754673480a1ed9f70e3834ef05635332";
    hash = "sha256-RJ1HXTOIliOWHc0uaElmy/hgl2wVTxfYipOkU5kVwsE=";
  };

  postPatch = ''
    substituteInPlace smartmon.sh \
      --replace-fail /usr/sbin/ ""
  '';

  dontConfigure = true;

  dontBuild = true;

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin smartmon.sh
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md

    runHook postInstall
  '';

  solutions.smartmon = {
    scripts = [ "bin/smartmon.sh" ];
    interpreter = runtimeShell;
    inputs = [
      coreutils
      findutils
      gawk
      gnugrep
      gnused
      smartmontools
    ];
  };

  meta = with lib; {
    description = "Prometheus Node Exporter SMART monitoring via textfile_collector";
    license = licenses.asl20;
    mainProgram = "smartmon.sh";
  };
}
