{
  stdenv,
  lib,
  fetchFromGitHub,
  qmake,
  pkg-config,
  wrapQtAppsHook,
  qtbase,
  qtwebengine,
}:

stdenv.mkDerivation rec {
  pname = "openfortivpn-webview";
  version = "1.1.0";

  src = fetchFromGitHub {
    owner = "gm-vm";
    repo = "openfortivpn-webview";
    rev = "v${version}-electron";
    hash = "sha256-SZaC5bN2cYaMIOhqxisd3AXqKO4P/kmBcbg0IaEflr4=";
  };

  sourceRoot = "source/${pname}-qt";

  buildInputs = [
    qtbase
    qtwebengine
  ];

  nativeBuildInputs = [
    qmake
    pkg-config
    wrapQtAppsHook
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin ${pname}
    install -Dm444 -t $out/share/doc/${pname} ../*.{md,txt}

    runHook postInstall
  '';

  meta = with lib; {
    description = "Perform SAML single sign-on and easily retrieve the SVPNCOOKIE needed by openfortivpn";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
}
