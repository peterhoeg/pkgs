{
  stdenv,
  lib,
  fetchurl,
  wxGTK32,
}:

stdenv.mkDerivation rec {
  pname = "wxedid";
  version = "0.0.31";

  src = fetchurl {
    url = "mirror://sourceforge/${pname}/${pname}-${version}.tar.gz";
    hash = "sha256-r8JbP+Y5iDhl3EbOP2yqh1iFhVZuDcbrvtencWsnV98=";
  };

  postPatch = ''
    patchShebangs src/rcode
  '';

  buildInputs = [ wxGTK32 ];

  enableParallelBuilding = true;

  meta = with lib; {
    description = "Extended Display Identification Data (EDID/DisplayID) editor";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
}
