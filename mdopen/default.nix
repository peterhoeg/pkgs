{
  lib,
  rustPlatform,
  fetchFromGitHub,
}:

rustPlatform.buildRustPackage rec {
  pname = "mdopen";
  version = "0.5.0";

  src = fetchFromGitHub {
    owner = "immanelg";
    repo = "mdopen";
    rev = "94e238d2a77316847c23032e10fe54dbc4d4588e";
    hash = "sha256-L5PU8mECUOHLok8wpT0eiJIksS9U1ov+kbPazsS3zW4=";
  };

  cargoHash = "sha256-ZnhWN2gmrluZ9AkdbD2zgKAE2rSmB7rmq9LIVAs5/Qs=";

  postInstall = ''
    install -Dm444 -t $out/share/doc/mdopen *.md
  '';

  doCheck = false; # no tests

  meta = with lib; {
    description = "Preview Markdown Files";
    license = licenses.bsd3;
    maintainers = with maintainers; [ peterhoeg ];
    mainProgram = "mdopen";
    inherit (src.meta) homepage;
  };
}
