{
  lib,
  buildGoModule,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "stoml";
  version = "0.7.1";

  src = fetchFromGitHub {
    owner = "freshautomations";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-Adjag1/Hd2wrar2/anD6jQEMDvUc2TOIG7DlEgxpTXc=";
  };

  vendorHash = "sha256-i5m2I0IApTwD9XIjsDwU4dpNtwGI0EGeSkY6VbXDOAM=";

  # tests expect to be run from a checkout
  doCheck = true;

  meta = with lib; {
    description = "Simple TOML/INI parser for bash";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
