{
  stdenv,
  lib,
  python3Packages,
  fetchurl,
  requireFile,
  dos2unix,
  unrar,
}:
let
  pypkgs = python3Packages;

  pname = "gopupd";

  sources = {
    # moved to level1techs.com
    original = rec {
      version = "1.9.6.5";
      sourceRoot = ".";
      src = fetchurl {
        name = "${pname}-${version}.rar";
        url = "https://files.homepagemodules.de/b602300/f16t892p15730n2_UixHgnbP.rar";
        sha256 = "sha256-Pm1aQY57rkq11D2erPe9SEz+Zq3Pjnb2BZvscHJasNc=";
      };
    };

    # https://winraid.level1techs.com/t/amd-and-nvidia-gop-update-no-requests-diy/30917/1495
    modded = rec {
      version = "1.9.6.5.k_mod_v0.4.9";
      sourceRoot = "GOP_Updater_v${version}";

      src = requireFile rec {
        name = "${pname}-${version}.rar";
        # nix-prefile-url file:///home/peter/Downloads/blalbalba --name [whatever is set above]
        sha256 = "1qlv4fn8pi5vyym27r782jjpvqc4hqg4461rl2xmcsbspg385bcf";
        message = ''
          Download from https://mega.nz/folder/VINlTSTL#Ua6djpZb_zZtO7E6R2hcAg

          nix-prefetch-url file://\$PWD/${name}
        '';
      };
    };
  };

in
pypkgs.buildPythonApplication rec {
  inherit pname;
  inherit (sources.modded) version src sourceRoot;

  postPatch = ''
    dos2unix GOPupd.py

    substituteInPlace GOPupd.py \
      --replace-fail '#GOP_Files\\' $out/share/${pname}/ \
      --replace-fail '\\'           '/' \
      --replace-fail "'r+'"         "'r'"
  '';

  nativeBuildInputs = [
    dos2unix
    unrar
  ];

  propagatedBuildInputs = with pypkgs; [ colorama ];

  format = "other";

  unpackPhase = ''
    unrar x $src
  '';

  installPhase = ''
    runHook preInstall

    install -Dm555 GOPupd.py $out/bin/gopupd
    install -Dm444 -t $out/share/${pname} '#GOP_Files'/*

    runHook postInstall
  '';

  meta = with lib; {
    description = "GOP updater for AMD and nVidia GPUs";
    hydraPlatforms = platforms.none;
  };
}
