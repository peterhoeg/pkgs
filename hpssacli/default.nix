{
  stdenv,
  lib,
  fetchurl,
  dpkg,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "hpssacli";
  version = "2.40-13.0";

  src =
    let
      file = "${finalAttrs.pname}-${finalAttrs.version}_amd64.deb";
    in
    fetchurl {
      urls = [
        "https://downloads.linux.hpe.com/SDR/downloads/MCP/Ubuntu/pool/non-free/${file}"
        "http://apt.netangels.net/pool/main/h/hpssacli/${file}"
      ];
      hash = "sha256-XBMaJfk3PGOCwloEYnrHNcpXG5dXEuU94K7SkSZ3h4c=";
    };

  nativeBuildInputs = [ dpkg ];

  unpackPhase = "dpkg -x $src ./";

  installPhase = ''
    mkdir -p $out/bin $out/share/doc $out/share/man
    mv opt/hp/hpssacli/bld/{hpssascripting,hprmstr,hpssacli} $out/bin/
    mv opt/hp/hpssacli/bld/*.{license,txt}                   $out/share/doc/
    mv usr/man                                               $out/share/

    for file in $out/bin/*; do
      chmod +w $file
      patchelf --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" \
               --set-rpath ${lib.makeLibraryPath [ stdenv.cc.cc ]} \
               $file
    done
  '';

  dontStrip = true;

  meta = with lib; {
    description = "HP Smart Array CLI";
    homepage = "https://downloads.linux.hpe.com/SDR/downloads/MCP/Ubuntu/pool/non-free/";
    license = licenses.unfreeRedistributable;
    platforms = [ "x86_64-linux" ];
    maintainers = with maintainers; [ ];
  };
})
