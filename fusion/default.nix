{
  buildGoModule,
  lib,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "fusion";
  version = "0.1.2";

  src = fetchFromGitHub {
    owner = "edgelaboratories";
    repo = "fusion";
    rev = "v" + version;
    hash = "sha256-4T4NG1Xd61rYRloA97wLmYwRMeDbw2V8BvwRqWIyr/0=";
  };

  vendorHash = "sha256-r66K3Y+gTgkjILKCLJ72XpjRXmu5xbH2R1gSVZOlEcY=";

  postInstall = ''
    install -Dm444 -t $out/share/doc/fusion *.md
  '';

  meta = {
    description = "CLI tool to merge JSON, YAML or TOML files";
    license = lib.licenses.free;
    mainProgram = "fusion";
  };
}
