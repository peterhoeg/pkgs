{
  stdenv,
  lib,
  fetchurl,
  unzip,
  pages ? [ ],
}:

let

in
stdenv.mkDerivation rec {
  pname = "homer";
  version = "22.07.2";

  src = fetchurl {
    url = "https://github.com/bastienwirtz/homer/releases/download/v${version}/homer.zip";
    hash = "sha256-DZLQQQMSrJ+e8VCiBQRYtFtKuyxZffvSyjFKfZWl410=";
  };

  buildInputs = [ ];

  nativeBuildInputs = [ unzip ];

  sourceRoot = ".";

  dontUnpack = true;

  # we copy in config.yml to at leave have *something* but you should override with pages
  buildCommand =
    ''
      dir=$out/share/${pname}
      mkdir -p $dir
      unzip -q ${src} -d $dir
      cp $dir/assets/config.yml.dist $dir/assets/config.yml
    ''
    + lib.concatMapStringsSep "\n" (e: ''
      install -Dm444 ${e} $dir/assets/${e.name}
    '') pages;

  meta = with lib; {
    description = "Static start page";
  };
}
