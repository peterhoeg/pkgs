{
  lib,
  buildGoModule,
  fetchFromGitHub,
  pkg-config,
  libusb1,
}:

buildGoModule {
  pname = "vga2usb";
  version = "0-unstable-20211201";

  src = fetchFromGitHub {
    owner = "benjojo";
    repo = "userspace-vga2usb";
    rev = "581b9e8d1d5201a7b46ff6ad435d63be90ccc252";
    hash = "sha256-AoCTxWn3JgDuHvm6qPnTmQR3oMAve7ilzn+Cwu4K1gg=";
  };

  vendorHash = null;

  nativeBuildInputs = [ pkg-config ];

  buildInputs = [ libusb1 ];

  meta = with lib; {
    description = "A userspace driver implementation of the Epiphan VGA2USB LR";
    homepage = "https://blog.benjojo.co.uk/";
    maintainers = with maintainers; [ peterhoeg ];
  };
}
