{
  mkDerivation,
  fetchFromGitHub,
  cmake,
  extra-cmake-modules,
  lib,
  pkg-config,
  qtbase,
  qtsensors,
  kcoreaddons,
  kdbusaddons,
  kded,
  gawk,
  gnugrep,
  xinput,
  xrandr,
}:

let
  timerStart = 25;

in
mkDerivation rec {
  pname = "kded_rotation";
  version = "20201002";

  nativeBuildInputs = [
    cmake
    extra-cmake-modules
    pkg-config
  ];

  src = fetchFromGitHub {
    owner = "dos1";
    repo = pname;
    rev = "fac175b05892dda77a696e80aadfc3069df88648";
    hash = "sha256-VszOJMBAsPcHb5VBLmWCHfOpbLLAlshxFISJtYwgBgU=";
  };

  buildInputs = [
    qtbase
    qtsensors
    kcoreaddons
    kded
  ];

  postPatch = ''
    substituteInPlace orientation-helper \
      --replace-fail 'awk '    '${gawk}/bin/awk ' \
      --replace-fail 'grep '   '${gnugrep}/bin/grep ' \
      --replace-fail 'xinput ' '${xinput}/bin/xinput ' \
      --replace-fail 'xrandr ' '${xrandr}/bin/xrandr '

    substituteInPlace screenrotator.cpp \
      --replace-fail '"orientation-helper"' '"${placeholder "out"}/bin/orientation-helper"' \
      --replace-fail 'start(25)' 'start(${toString timerStart})'
  '';

  cmakeFlags = [
    "-Wno-dev"
    "-DLIB_INSTALL_DIR=lib"
    "-DLIBEXEC_INSTALL_DIR=lib"
    "-DKDE_INSTALL_USE_QT_SYS_PATHS=ON"
  ];

  postInstall = ''
    chmod 555 $out/bin/*
  '';

  meta = with lib; {
    description = "A small, hacky KDED module for handling automatic screen rotation on tablets and hybrid laptops.";
    license = licenses.gpl2;
    maintainers = with maintainers; [ peterhoeg ];
    inherit (kcoreaddons.meta) platforms;
  };
}
