{
  lib,
  rustPlatform,
  fetchFromGitHub,
  installShellFiles,
}:

rustPlatform.buildRustPackage rec {
  pname = "kls";
  version = "0.11.10";

  src = fetchFromGitHub {
    owner = "rszyma";
    repo = "vscode-kanata";
    rev = "v" + version;
    hash = "sha256-kQhHB0MA6dG3RPHfJ6xfyHgqtgImfUeKGSjJCVNpTg4=";
    fetchSubmodules = true;
  };

  sourceRoot = "source/kls";

  cargoLock.lockFile = ./Cargo.lock;

  cargoHash = "sha256-22222lW+T1Byj8H4qju6n0mkz2etTgQCFheq6nuMtcc=";

  postPatch = ''
    cp ${cargoLock.lockFile} Cargo.lock
  '';

  meta = with lib; {
    description = "Language Server for kanata files";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    inherit (src.meta) homepage;
  };
}
