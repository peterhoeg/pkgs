{
  stdenv,
  lib,
  fetchurl,
  libpaper,
  libiconv,
  qpdf,
  asciidoc,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "paperjam";
  version = "1.2";

  src = fetchurl {
    url = "https://mj.ucw.cz/download/linux/${finalAttrs.pname}-${finalAttrs.version}.tar.gz";
    hash = "sha256-0AziT7ROICTEPKaA4Ub1B8NtIfLmxRXriW7coRxDpQ0=";
  };

  postPatch = ''
    substituteInPlace Makefile \
      --replace-fail '-std=gnu++11' '-std=c++17 -Wno-cpp'
  '';

  enableParallelBuilding = true;

  buildInputs = [
    libiconv
    qpdf
    libpaper
  ];

  nativeBuildInputs = [ asciidoc ];

  installFlags = [ "PREFIX=$(out)" ];

  postInstall = ''
    install -Dm444 -t $out/share/doc/${finalAttrs.pname} LICENSE NEWS *.md
  '';

  meta = with lib; {
    homepage = "https://mj.ucw.cz/sw/paperjam/";
    description = "Paperjam";
    platforms = platforms.unix;
    license = with licenses; [
      bsd3
      gpl2
    ];
  };
})
