{
  lib,
  rustPlatform,
  fetchFromGitHub,
  git,
}:

rustPlatform.buildRustPackage rec {
  pname = "git-bonsai";
  version = "0.3.0";

  src = fetchFromGitHub {
    owner = "agateau";
    repo = pname;
    rev = version;
    hash = "sha256-P9zSO5nGtw/iFl7i91eGep/PEKJcavUbsuRVqAAea4o=";
  };

  cargoSha256 = "sha256-iy59PdM5H9QNC8FvTvQTOgsfH3RpShXjfJaH6bid8Sg=";

  nativeBuildInputs = [ git ]; # for the tests

  meta = with lib; {
    description = "Tend the branches of your git garden";
    license = licenses.gpl3Only;
    maintainers = with maintainers; [ peterhoeg ];
    inherit (src.meta) homepage;
  };
}
