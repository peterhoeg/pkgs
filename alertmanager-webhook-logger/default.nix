{
  buildGoModule,
  lib,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "alertmanager-webhook-logger";
  version = "1.0";

  src = fetchFromGitHub {
    owner = "tomtom-international";
    repo = pname;
    rev = version;
    hash = "sha256-mJbpDiTwUsFm0lDKz8UE/YF6sBvcSSR6WWLrfKvtri4=";
  };

  vendorHash = "sha256-gKtOoM9TuEIHgvSjZhqWmdexG2zDjlPuM0HjjP52DOI=";

  ldflags = [
    "-w"
    "-s"
    "-X github.com/prometheus/common/version.Branch=master"
    "-X github.com/prometheus/common/version.BuildDate=unknown"
    "-X github.com/prometheus/common/version.Revision=${src.rev}"
    "-X github.com/prometheus/common/version.Version=${version}-0"
  ];

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md
  '';

  meta = with lib; {
    description = "Matrix notifier/manager for Prometheus AlertManager";
  };
}
