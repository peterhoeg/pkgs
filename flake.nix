{
  description = "Local package repository";

  inputs = {
    # crystal = {
    #   url = "github:manveru/crystal-flake";
    #   inputs.nixpkgs.follows = "nixpkgs";
    # };
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs =
    { self, nixpkgs, ... }:
    let
      # we don't really care about Darwin or ARM, so just use x86_64-linux for now
      system = "x86_64-linux";

      pkgs = import nixpkgs {
        inherit system;
        overlays = [
          # crystal.overlays.default
          self.outputs.overlays.default
        ];
        config = {
          allowUnfree = true;
        };
      };

      specFile = (pkgs.formats.json { }).generate "hydra.json" {
        main = {
          enabled = 1;
          type = 1;
          hidden = false;
          description = "Local pkgs";
          checkinterval = 600;
          schedulingshares = 1;
          enableemail = true;
          emailoverride = "";
          keepnr = 3;
          flake = "gitlab:peterhoeg/pkgs?ref=main";
        };
      };

    in
    {
      overlays.default =
        final: prev:
        let
          inherit (prev) callPackage callPackages recurseIntoAttrs;
          qt5' = prev.libsForQt5;
          qt6' = prev.qt6Packages;
          qt' = qt5';

        in
        {
          local = rec {
            test = callPackage ./test { };

            _3cx = callPackage ./3cx { };
            a_to_b = callPackage ./a_to_b { };
            acer-acpi-s3 = callPackage ./acer-acpi-s3 { };
            acme-dns = callPackage ./acme-dns { };
            additional-wallpapers = callPackage ./additional-wallpapers { };
            adtool = callPackage ./adtool { };
            age-plugin-tpm = callPackage ./age-plugin-tpm { };
            amber = callPackage ./amber { };
            api7 = callPackage ./api7 { };
            alertmanager-webhook-logger = callPackage ./alertmanager-webhook-logger { };
            amd_s2idle = callPackage ./amd_s2idle { };
            amdvbflash = callPackage ./amdvbflash { };
            android-otp-extractor = callPackage ./android-otp-extractor { };
            arigi = callPackage ./arigi { };
            arkmanager = callPackage ./arkmanager { };
            astaro-mibs = callPackage ./astaro-mibs { };
            awatcher = callPackage ./awatcher { };
            azure-storage-explorer = callPackage ./azure-storage-explorer { };
            # bacap = callPackage ./bacap { };
            balong_usbdload = callPackage ./balong_usbdload { };
            balongflash = callPackage ./balongflash { };
            basemark-gpu = qt'.callPackage ./basemark-gpu { };
            bd_prochot = callPackage ./bd_prochot { };
            bit = callPackage ./bit { };
            bitbucketconverter = callPackage ./bitbucketconverter { };
            bitwarden-to-pass = callPackage ./bitwarden-to-pass { };
            bkcrack = callPackage ./bkcrack { };
            black-bean-control = callPackage ./black-bean-control { };
            breeze-chameleon-icons = callPackage ./breeze-chameleon-icons { };
            broadlink-mqtt = callPackage ./broadlink-mqtt { };
            broadlinkmanager = callPackage ./broadlinkmanager { };
            brother_printer_fwupd = callPackage ./brother_printer_fwupd { };
            bsnes = callPackage ./bsnes { };
            calibre-breeze-icon-theme = callPackage ./calibre-breeze-icon-theme { };
            cc-tool = callPackage ./cc-tool { };
            cc2538-bsl = callPackage ./cc2538-bsl { };
            ccd2cue = callPackage ./ccd2cue { };
            cdctl = callPackage ./cdctl { };
            ceedling = callPackage ./ceedling { };
            check-dell-emc = callPackage ./check-dell-emc { };
            check-idrac = callPackage ./check-idrac { };
            citrix-adc-metrics-exporter = callPackage ./citrix-adc-metrics-exporter { };
            cleanScriptsHook = callPackage ./cleanScriptsHook { };
            cloudmapper = callPackage ./cloudmapper { };
            cloudprober = callPackage ./cloudprober { };
            cloveretl = callPackage ./cloveretl { };
            cncgold-assets = callPackage ./cncgold-assets { };
            concourse = callPackage ./concourse { };
            controlvault2-nfc-enable = callPackage ./controlvault2-nfc-enable { };
            cpu_features = callPackage ./cpu_features { };
            csharpier = callPackage ./csharpier { };
            davmail = callPackage ./davmail { };
            dbxtool = callPackage ./dbxtool { };
            descent3 = callPackage ./descent3 { };
            ddns-updater = callPackage ./ddns-updater { };
            droplet-agent = callPackage ./droplet-agent { };
            drsprinto = callPackage ./drsprinto { };
            dune2-assets = callPackage ./dune2-assets { };
            dunedynasty = callPackage ./dunedynasty { };
            dynamic-wallpapers = callPackage ./dynamic-wallpapers { };
            edid-checksum = callPackage ./edid-checksum { };
            egt = recurseIntoAttrs (callPackages ./egt { });
            emacs-fancy-logos = callPackage ./emacs-fancy-logos { };
            emacsql-sqlite = callPackage ./emacsql-sqlite { };
            email-oauth2-proxy = callPackage ./email-oauth2-proxy { };
            espack = callPackage ./espack { };
            espota = callPackage ./espota { };
            espota-server = callPackage ./espota-server { };
            inherit (callPackages ./etlegacy { })
              etlegacy
              ;
            evdoublebind = callPackage ./evdoublebind { };
            evilginx = callPackage ./evilginx { };
            exe-info = callPackage ./exe-info { };
            extract-msg = callPackage ./extract-msg { };
            extract-xiso = callPackage ./extract-xiso { };
            inherit (callPackages ./fallout { })
              fallout1
              fallout2
              ;
            feed2exec = callPackage ./feed2exec { };
            ferretdb = callPackage ./ferretdb { };
            ff-containers-sort = callPackage ./ff-containers-sort { };
            flexlm = callPackage ./flexlm { };
            flexlm_exporter = callPackage ./flexlm_exporter { };
            filter-prometheus = callPackage ./filter-prometheus { };
            freeotp-migrator = callPackage ./freeotp-migrator { };
            freeotp-to-andotp-migrator = callPackage ./freeotp-to-andotp-migrator { };
            ftpgrab = callPackage ./ftpgrab { };
            fusion = callPackage ./fusion { };
            fzf-marks = callPackage ./fzf-marks { };
            gauth = callPackage ./gauth { };
            gang-beasts = callPackage ./gang-beasts { };
            geminabox = callPackage ./geminabox { };
            geo-nft = callPackage ./geo-nft { };
            geowifi = callPackage ./geowifi { };
            gio-test = callPackage ./gio-test { };
            git-bonsai = callPackage ./git-bonsai { };
            git-hooks = callPackage ./git-hooks { };
            gitbatch = callPackage ./gitbatch { };
            gk6x = callPackage ./gk6x { };
            inherit (callPackages ./gog { })
              baldurs_gate_enhanced_edition
              eotb
              ;
            gogextract = callPackage ./gogextract { };
            gopupd = callPackage ./gopupd { };
            habapp = callPackage ./habapp { };
            hamclock = callPackage ./hamclock { };
            hatch = callPackage ./hatch { };
            hoegdotcom = callPackage ./hoegdotcom { };
            hoeg-icc = callPackage ./hoeg-icc { };
            homer = callPackage ./homer { };
            homie-monitor = callPackage ./homie-monitor { };
            hp-ams = (callPackages ./hp-ams { }).fromSource; # binaryDebian binaryRhel
            hp-health = callPackage ./hp-health { };
            hpssacli = callPackage ./hpssacli { };
            # huginn = callPackage ./huginn { }; #  BROKEN, doesn't eval
            icantbelieveitsnotvaletudo = callPackage ./icantbelieveitsnotvaletudo { };
            idractools = callPackage ./idractools { };
            ilo_exploy = callPackage ./ilo_exploy { };
            ilorest = callPackage ./ilorest { };
            immortal = callPackage ./immortal { };
            infinityEngine = recurseIntoAttrs (callPackages ./infinity-engine { });
            ini2json = callPackage ./ini2json { };
            initool = callPackage ./initool { };
            intel-mas-tool = callPackage ./intel-mas-tool { };
            ip6words = callPackage ./ip6words/package.nix { };
            iperf3_exporter = callPackage ./iperf3_exporter { };
            ipmi_exporter = callPackage ./ipmi_exporter { };
            ippsample = callPackage ./ippsample { };
            isochronous = callPackage ./isochronous { };
            janet-lsp = callPackage ./janet-lsp { };
            json2xml = callPackage ./json2xml { };
            # karma = callPackage ./karma { };
            kde-control-station = qt6'.callPackage ./kde-control-station { };
            kde-shader-wallpaper = qt6'.callPackage ./kde-shader-wallpaper { };
            kded_rotation = qt'.callPackage ./kded_rotation { };
            keen4 = callPackage ./keen4 { };
            keydbcfg = callPackage ./keydbcfg { };
            kls = callPackage ./kls { };
            kodi-netflix-auth = callPackage ./kodi-netflix-auth { };
            kodi-plymouth-theme = callPackage ./kodi-plymouth-theme { };
            kodi-texturecache = callPackage ./kodi-texturecache { };
            # kpinentry = qt'.callPackage ./kpinentry { }; #  BROKEN on 2022-11-07
            krohnkite = qt6'.callPackage ./krohnkite { };
            kshift = callPackage ./kshift { };
            lac = callPackage ./lac { };
            lemminx = callPackage ./lemminx { };
            libchdr = callPackage ./libchdr { };
            librebridge = qt'.callPackage ./librebridge { };
            libsigrok-udev = callPackage ./libsigrok-udev { };
            llama-bsl = callPackage ./llama-bsl { };
            llama-cpp = callPackage ./llama-cpp { };
            luanti-addons = recurseIntoAttrs (callPackages ./luanti-addons { });
            ltchiptool = callPackage ./ltchiptool { };
            lz4json = callPackage ./lz4json { };
            maildir2mbox = callPackage ./maildir2mbox { };
            maildirwatch = callPackage ./maildirwatch { };
            mailnotify = callPackage ./mailnotify { };
            mailtest = callPackage ./mailtest { };
            marten = callPackage ./marten { };
            mdopen = callPackage ./mdopen { };
            memreserver = callPackage ./memreserver { };
            mimembellish = callPackage ./mimembellish { };
            minetest-addons = luanti-addons;
            minimalistic-wallpaper-collection = callPackage ./minimalistic-wallpaper-collection { };
            mkeosimg = callPackage ./mkeosimg { };
            motioneye = callPackage ./motioneye { };
            mp3wrap = callPackage ./mp3wrap { };
            mqtt-explorer = callPackage ./mqtt-explorer { };
            mqttx = callPackage ./mqttx { };
            multicast-relay = callPackage ./multicast-relay { };
            multi-git-status = callPackage ./multi-git-status { };
            mutt-octet-filter = callPackage ./mutt-octet-filter { };
            mutt-scripts = callPackage ./mutt-scripts { };
            mutt-vcard-filter = callPackage ./mutt-vcard-filter { };
            mutt_ics = callPackage ./mutt_ics { };
            muttprint = callPackage ./muttprint { };
            inherit (callPackages ./naemon { })
              naemon
              naemon-core
              ;
            nearinfinity = callPackage ./nearinfinity { };
            niet = callPackage ./niet { };
            nix-upgrade-scripts = callPackage ./nix-upgrade-scripts { };
            nixos-shell = callPackage ./nixos-shell { };
            non-free = callPackage ./non-free { };
            notify-send-py = callPackage ./notify-send-py { };
            notify-send-sh = callPackage ./notify-send-sh { };
            nuget2nix = callPackage ./nuget2nix { };
            nyquist = callPackage ./nyquist { };
            oauth2l = callPackage ./oauth2l { };
            oda-file-converter = qt'.callPackage ./oda-file-converter { };
            odf2txt = callPackage ./odf2txt { };
            offlinemsmtp = callPackage ./offlinemsmtp { };
            ogmrip = callPackage ./ogmrip { };
            oh-brother = callPackage ./oh-brother/package.nix { };
            onvifviewer = qt'.callPackage ./onvifviewer { };
            opencv-face-recognition = callPackage ./opencv-face-recognition { };
            inherit (callPackages ./openssl { })
              openssl_1_0_1
              openssl_1_0_2
              ;
            openfortivpn-webview = qt'.callPackage ./openfortivpn-webview { };
            open-plc-utils = qt'.callPackage ./open-plc-utils { };
            openmw-git = qt'.callPackage ./openmw-git { };
            ops = callPackage ./ops { };
            osticket = callPackage ./osticket { };
            pandoc-templates = recurseIntoAttrs (callPackages ./pandoc-templates { });
            paperjam = callPackage ./paperjam { };
            paq9a = callPackage ./paq9a { };
            perccli = callPackage ./perccli { };
            pg_catcheck = callPackage ./pg_catcheck { };
            pgbadger = callPackage ./pgbadger { };
            pixelserv = callPackage ./pixelserv { };
            plasma-applet-ddcci-plasma5 = qt5'.callPackage ./plasma-applet-ddcci { };
            plasma-applet-ddcci-plasma6 = qt6'.callPackage ./plasma-applet-ddcci { };
            plasma-applet-ddcci = plasma-applet-ddcci-plasma5;
            plasma-applet-focus = qt'.callPackage ./plasma-applet-focus { };
            plasma-theme-switcher = qt'.callPackage ./plasma-theme-switcher { };
            playground = callPackage ./playground { };
            polonium-git = qt5'.callPackage ./polonium-git { };
            polonium-plasma6 = qt6'.callPackage ./polonium-git { };
            postgres-checkup = callPackage ./postgres-checkup { }; # BROKEN
            postgresqltuner = callPackage ./postgresqltuner { };
            praeda = callPackage ./praeda { };
            prename = callPackage ./prename { };
            pret = callPackage ./pret { };
            procmon = callPackage ./procmon { };
            projectlibre = callPackage ./projectlibre { };
            prometheus-msteams = callPackage ./prometheus-msteams { };
            pscoverdl = callPackage ./pscoverdl { };
            psscriptanalyzer = callPackage ./psscriptanalyzer { };
            inherit (callPackages ./psx-bios { })
              ps1-bios
              ps2-bios
              ;
            pushover-cli = callPackage ./pushover-cli { };
            pushprox = callPackage ./pushprox { };
            pykmip = callPackage ./pykmip { };
            pykodi-cli = callPackage ./pykodi-cli { };
            pymedusa = callPackage ./pymedusa { };
            python-evtx = callPackage ./python-evtx { };
            python-onvif = callPackage ./python-onvif { }; # BROKEN
            python-onvif-zeep = callPackage ./python-onvif-zeep { };
            qomui = callPackage ./qomui { };
            quickemu = callPackage ./quickemu { };
            qmkfmt = callPackage ./qmkfmt/package.nix { };
            rancid = callPackage ./rancid { };
            inherit (callPackages ./re3 { })
              re3
              re3lcs
              re3vc
              ;
            redka = callPackage ./redka { };
            repostat = callPackage ./repostat { };
            resholve-test = callPackage ./resholve-test { };
            rfetch = callPackage ./rfetch { };
            roller-coaster-tycoon-collection = callPackage ./roller-coaster-tycoon-collection { };
            ros = callPackage ./ros { };
            rss2maildir = callPackage ./rss2maildir { };
            s0ixselftesttool = callPackage ./s0ixselftesttool { };
            s25client = callPackage ./s25client { };
            salt = callPackage ./salt { };
            samsung-magician = callPackage ./samsung-magician { };
            samsung-toolkit = callPackage ./samsung-toolkit { };
            san-francisco-fonts = callPackage ./san-francisco-fonts { };
            sasl2-oauth = callPackage ./sasl2-oauth { };
            script_exporter = callPackage ./script_exporter { };
            scummvm-games = recurseIntoAttrs (callPackages ./scummvm-games { });
            sdcv = callPackage ./sdcv { };
            sdl2-gamepad-mapper = callPackage ./sdl2-gamepad-mapper { };
            semantic-prompt = callPackage ./semantic-prompt { };
            set_conf = callPackage ./set_conf { };
            sf-mono = san-francisco-fonts;
            sfarkxtc = callPackage ./sfarkxtc { };
            sgx-software-enable = callPackage ./sgx-software-enable { };
            shadow-warrior = callPackage ./shadow-warrior { };
            shyaml = callPackage ./shyaml { };
            smartmon = callPackage ./smartmon { };
            smhasher = callPackage ./smhasher { };
            sparrow-wifi = qt'.callPackage ./sparrow-wifi { };
            sping = callPackage ./sping { };
            splashtop = qt5'.callPackage ./splashtop { }; # doesn't support qt6
            # sql-relay = callPackage ./sql-relay { }; # BROKEN on 2024-11-26
            ssacli = callPackage ./ssacli { };
            stable-diffusion-cpp = callPackage ./stable-diffusion-cpp { };
            standardrb = callPackage ./standardrb { };
            start = callPackage ./start { };
            star-trader = callPackage ./star-trader { };
            stoml = callPackage ./stoml { };
            story_branch = callPackage ./story_branch { };
            # suitecrm = callPackage ./suitecrm { };
            synapse-purge = callPackage ./synapse-purge { };
            syncthing_exporter = callPackage ./syncthing_exporter { };
            sys2mqtt = callPackage ./sys2mqtt { };
            sysit = callPackage ./sysit { };
            systemd-swap = callPackage ./systemd-swap { };
            teensy-loader-udev = callPackage ./teensy-loader-udev { };
            toml-merge = callPackage ./toml-merge { };
            toolbox = callPackage ./toolbox { };
            totp-cli = callPackage ./totp-cli { };
            traykeys = qt'.callPackage ./traykeys { };
            uavs3 = recurseIntoAttrs (callPackages ./uavs3 { });
            ubports-installer = callPackage ./ubports-installer { };
            uefi-shell = callPackage ./uefi-shell { };
            umskt-rs = callPackage ./umskt-rs { };
            unarc = callPackage ./unarc { };
            unicode = callPackage ./unicode { };
            unpoller = callPackage ./unpoller { };
            uresourced = callPackage ./uresourced { };
            usbctl = callPackage ./usbctl { };
            vga2usb = callPackage ./vga2usb { };
            virt-v2v = callPackage ./virt-v2v { };
            vkquake2 = callPackage ./vkquake2 { };
            vmware_exporter = callPackage ./vmware_exporter { };
            vvvvvv = callPackage ./vvvvvv { };
            wait-for-dns = callPackage ./wait-for-dns { };
            warble = callPackage ./warble { };
            wd-unlock = callPackage ./wd-unlock { };
            webster = callPackage ./webster { };
            whatmask = callPackage ./whatmask { };
            wordle = callPackage ./wordle { };
            wtwitch = callPackage ./wtwitch { };
            wxedid = callPackage ./wxedid { };
            xash3d = callPackage ./xash3d { };
            xbows-driver = callPackage ./xbows-driver { };
            xdg-mr = callPackage ./xdg-mr { };
            xdpi = callPackage ./xdpi { };
            xdpiqt = qt'.callPackage ./xdpiqt { inherit xdpi; };
            xpkey = umskt-rs;
            xroach = callPackage ./xroach { };
            xtrkcad = callPackage ./xtrkcad { };
            yaml-merge = callPackage ./yaml-merge { };
            zigpy-cli = callPackage ./zigpy-cli { };
            zram-generator = callPackage ./zram-generator { };
            zsh-history-filter = callPackage ./zsh-history-filter { };
          };
        };

      packages."${system}" = pkgs.local;

      checks."${system}" = {
        lint =
          pkgs.runCommandLocal "pkgs-lint"
            {
              nativeBuildInputs = with pkgs; [
                # nix-linter #  BROKEN on 2023-11-02
                statix
              ];
            }
            ''
              report=$out/lint.txt

              mkdir -p $out

              # nix-linter -r . > $report
              statix check . > $report
            '';
      };

      # we need either a default devShell or a default package and the latter makes no sense
      devShells."${system}".default = pkgs.mkShell {
        nativeBuildInputs = with pkgs; [
          crystal
          crystal2nix
          shfmt
          treefmt2
        ];
        shellHook = ''
          install -Dm644 ${specFile} $(git rev-parse --show-toplevel)/.ci/${specFile.name}
        '';
      };

      # ignore all jobs that either have set hydraPlatforms = [] or are broken
      hydraJobs = pkgs.lib.filterAttrs (
        _: v:
        ((v.meta.hydraPlatforms or [ 1 ]) != pkgs.lib.platforms.none) && ((v.meta.broken or false) == false)
      ) pkgs.local;
    };
}
