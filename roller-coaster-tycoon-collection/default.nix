{
  stdenvNoCC,
  lib,
  fetchurl,
  _7zz,
  unshield,
}:

let
  rct1 = fetchurl {
    url = "https://archive.org/download/roller-coaster-tycoon-cd-rom-iso/RCTYCOON.ISO";
    hash = "sha256-HgldE1YjvUPk2xVYg+Ku6ckOD4ncTaFrK9GYLVEgJKk=";
    name = "roller-coaster-tycoon1.iso";
  };

  rct2 = fetchurl {
    url = "https://archive.org/download/roller-coaster-tycoon-2-triple-thrill-pack/RollerCoaster%20Tycoon%202%20Triple%20Thrill%20Pack.iso";
    hash = "sha256-VYmWFyjInBXHvkcCrzX0qxoA5Y1ttRirDE1U+fJFWsA=";
    name = "roller-coaster-tycoon2.iso";
  };

in
stdenvNoCC.mkDerivation (finalAttrs: {
  pname = "roller-coaster-tycoon-collection";
  version = "1.0";

  nativeBuildInputs = [
    _7zz
    unshield
  ];

  buildCommand = lib.concatLines (
    lib.imap1
      (i: e: ''
        mkdir rct${toString i}; cd rct${toString i}

        dir="$out/${finalAttrs.passthru.baseDir}/roller-coaster-tycoon${toString i}"
        mkdir -p "$dir"
        7zz x ${e}
        unshield -d . x data1.cab
        for d in Minimum MINIMUM Default_File_Group ; do
          if [ -d "$d" ]; then
            mv "$d"/* $dir/
          fi
        done

        cd ..
      '')
      [
        rct1
        rct2
      ]
  );

  passthru.baseDir = "share/${finalAttrs.pname}";
})
