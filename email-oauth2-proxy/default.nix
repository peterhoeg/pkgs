{
  lib,
  python3,
  fetchFromGitHub,
  writeShellScript,
  withGui ? true,
}:

let
  pypkgs = python3.pkgs;

  inherit (lib) optionals;

in
pypkgs.buildPythonApplication rec {
  pname = "email-oauth2-proxy";
  version = "2022-10-10";

  format = "other";

  src = fetchFromGitHub {
    owner = "simonrob";
    repo = pname;
    rev = version;
    hash = "sha256-h9Mev6UAxUdg7api71FnADpNxazTc5H52onY2SGGAak=";
  };

  postPatch = ''
    sed -i emailproxy.py \
      -e '1i#!/usr/bin/env python3' \
      -e "s@^CONFIG_FILE_PATH.*@CONFIG_FILE_PATH = '%s/%s.config' % (os.environ['XDG_CONFIG_HOME'], APP_SHORT_NAME)@g"
  '';

  propagatedBuildInputs =
    with pypkgs;
    [
      cryptography
      setuptools # needs pkg_resources at runtime
    ]
    ++ optionals withGui [
      pystray
      timeago
      pywebview
    ];

  installPhase = ''
    install -Dm555 emailproxy.py $out/bin/emailproxy
    install -Dm444 -t $out/share/doc/${pname} *.config *.md
  '';

  meta = with lib; {
    description = "ESPOTA tool";
    license = licenses.free;
  };
}
