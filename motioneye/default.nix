{
  fetchFromGitHub,
  resholve,
  coreutils,
  curl,
  gnugrep,
  gnused,
  ffmpeg,
  lib,
  lsb-release,
  motion,
  python3Packages,
  runtimeShell,
  v4l-utils,
  which,
}:

let
  pypkgs = python3Packages;

  v4l-utils' = v4l-utils.override { withGUI = false; };

  pname = "motioneye";
  version = "0.43.1b2";

  src = fetchFromGitHub {
    owner = "motioneye-project";
    repo = pname;
    rev = version;
    hash = "sha256-YRGOPLwyiTRZafzoi8CKn2ad4JtQqiXcVvlnO83fobI=";
  };

  scripts = resholve.mkDerivation {
    pname = "motioneye-scripts";
    inherit version src;
    sourceRoot = "source/motioneye/scripts";

    dontConfigure = true;
    dontBuild = true;

    installPhase = ''
      runHook preInstall

      install -Dm555 -t $out/bin *.sh

      runHook postInstall
    '';

    solutions = {
      migrate = {
        scripts = [ "bin/migrateconf.sh" ];
        interpreter = runtimeShell;
        inputs = [
          coreutils
          gnugrep
          gnused
        ];
      };
      relay = {
        scripts = [ "bin/relayevent.sh" ];
        interpreter = runtimeShell;
        inputs = [
          coreutils
          curl
          gnugrep
          gnused
        ];
      };
    };
  };

in
pypkgs.buildPythonApplication rec {
  inherit pname version src;

  format = "pyproject";

  disabled = pypkgs.pythonOlder "3.7";

  buildInputs = [
    ffmpeg
    lsb-release
    motion
    v4l-utils'
    which
  ];

  postInstall = ''
    rm -rf $out/${pypkgs.python.sitePackages}/motioneye/scripts
    ln -s -t $out/bin ${scripts}/bin/*
    install -Dm444 motioneye/extra/motioneye.systemd $out/lib/systemd/system/motioneye.service
    substituteInPlace $out/lib/systemd/system/motioneye.service \
      --replace-fail /usr/local $out
  '';

  makeWrapperArgs = [
    "--prefix PATH : ${lib.makeBinPath buildInputs}"
  ];

  propagatedBuildInputs = with pypkgs; [
    babel
    boto3
    jinja2
    numpy
    pillow
    pycurl
    pytz
    # I don't remember *why* this is needed, but some changes in our packaging
    # means it is now needed
    setuptools
    six
    tornado
  ];

  doCheck = false;

  meta = with lib; {
    description = "MotionEye - a web frontend for the motion daemon";
    licence = licenses.gpl3Only;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
