{
  stdenv,
  lib,
  fetchFromGitHub,
  resholve,
  bash,
  coreutils,
  dosfstools,
  e2fsprogs,
  gawk,
  gnugrep,
  gnused,
  gnutar,
  parted,
  util-linux,
}:

resholve.mkDerivation rec {
  pname = "mkeosimg";
  version = "2021-11-24";

  src = fetchFromGitHub {
    owner = "sowbug";
    repo = pname;
    rev = "0063c66e4c3f9e87c2eb9f9cf5a0739dac2a3020";
    hash = "sha256-RU53qBeZed9goa42P0ohY20taeoujDEvIAp7mPt4ZRA=";
  };

  installPhase = ''
    install -Dm555 -t $out/bin mkeos*
    install -Dm444 -t $out/share/doc/${pname} *.md
  '';

  solutions.default = {
    scripts = [
      "bin/mkeosdrive"
      "bin/mkeosimg"
    ];
    interpreter = lib.getExe bash;
    inputs = [
      coreutils
      dosfstools
      e2fsprogs
      gawk
      gnugrep
      gnutar
      parted
      util-linux
    ];
    execer = [
      "cannot:${e2fsprogs}/bin/mkfs.ext3"
    ];
    fake.external = [
      "mount"
      "umount"
    ];
  };

  meta = with lib; {
    description = "Unbrick EdgeRouters";
    license = licenses.free;
  };
}
