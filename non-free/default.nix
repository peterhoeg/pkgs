{
  stdenv,
  lib,
  fetchurl,
}:

let
  fix = url: lib.replaceStrings [ " " ] [ "%20" ] url;

  sources = [
    rec {
      version = "4.4";
      src = fetchurl {
        url = fix "http://cdn.speartail.com/DisplayLink USB Graphics Software for Ubuntu ${version}.zip";
        sha256 = "0c02mg7vbijpfpk9imh0hmls1yiglc216zfllw5ar86r1slhd5y0";
        name = "displaylink.zip";
      };
    }
    rec {
      version = "5.2";
      src = fetchurl {
        url = fix "http://cdn.speartail.com/DisplayLink USB Graphics Software for Ubuntu ${version}.zip";
        sha256 = "03b176y95f04rg3lcnjps9llsjbvd8yksh1fpvjwaciz48mnxh2i";
        name = "displaylink.zip";
      };
    }
  ];

in
stdenv.mkDerivation rec {
  pname = "non-free-software";
  version = "0.0.1";

  buildCommand =
    ''
      dir=$out/share/non-free-software

      mkdir -p $dir
    ''
    + lib.concatMapStringsSep "\n" (e: ''
      ln -s ${e.src} $dir/${e.version}-${e.src.name}
    '') sources;

  meta = with lib; {
    description = "Non-free software that needs to be pre-fetched";
  };
}
