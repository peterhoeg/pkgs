{
  buildGoModule,
  lib,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "cloudprober";
  version = "0.12.7";

  src = fetchFromGitHub {
    owner = "cloudprober";
    repo = pname;
    rev = "v" + version;
    hash = "sha256-OZEzPOGgnWk8i3mb1LWhWm9/EgTICCsawY3CKCa1YwM=";
  };

  vendorHash = "sha256-wGDEfVNPiFSvq0+gOFrVVgUrsFjgusCH5VsY/Mvca2c=";

  subPackages = [ "cmd" ];

  ldflags = [ "-X main.version=${version}" ];

  postInstall = ''
    mv $out/bin/cmd $out/bin/${meta.mainProgram}

    mkdir -p $out/share/doc/${pname}
    cp -r docs examples $out/share/doc/${pname}
  '';

  meta = with lib; {
    description = "CloudProber";
    mainProgram = "cloudprober";
  };
}
