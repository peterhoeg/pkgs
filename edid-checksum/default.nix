{
  lib,
  python3Packages,
  fetchFromGitHub,
  makeWrapper,
}:

let
  pypkgs = python3Packages;

  pname = "edid-checksum";

in
pypkgs.buildPythonApplication {
  inherit pname;
  version = "20171013";

  format = "other";

  src = fetchFromGitHub {
    owner = "mochikun";
    repo = "EDID_checksum";
    rev = "f1a1e69983cde7a7d16c0cd5956e33e4e636a99f";
    hash = "sha256-5T4UXVH1IUXVXoDaa8NxQ6py/yg6KbVgaMRE28e3pIk=";
  };

  nativeBuildInputs = [ makeWrapper ];

  installPhase = ''
    runHook preInstall

    install -Dm555 edid_checksum.py $out/libexec/edid-checksum.py
    install -Dm444 -t $out/share/doc/${pname} *.md *.txt

    makeWrapper ${pypkgs.python.interpreter} $out/bin/edid-checksum \
      --add-flags '--' \
      --add-flags $out/libexec/edid-checksum.py

    runHook postInstall
  '';

  meta = with lib; {
    description = "Calculate and verify EDID checksum";
    license = licenses.mit;
    mainProgram = "edid-checksum";
  };
}
