{
  stdenv,
  lib,
  requireFile,
}:

stdenv.mkDerivation rec {
  pname = "arigi";
  version = "1.1.6";

  src = requireFile {
    name = "arigi-${version}-linux-amd64.tar.gz";
    sha256 = "1z6sbq41kpw4y5hi40vp4pixdkdbh7sjzxhbyxgz30b2f81xpbd8";
    message = ''
      Register and download from https://arigi.kastelo.net/downloads/
    '';
  };

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin bin/*
    install -Dm444 -t $out/share/doc/${pname} README* *.pdf

    mkdir -p $out/nix-support
    echo $src > $out/nix-support/_dependencies.txt

    runHook postInstall
  '';

  meta = with lib; {
    description = "Syncthing management server";
    license = licenses.unfree;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
    hydraPlatforms = platforms.none;
  };
}
