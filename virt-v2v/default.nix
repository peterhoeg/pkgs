{
  stdenv,
  lib,
  fetchFromGitHub,
  autoreconfHook,
  ocaml-ng,
  pkg-config,
  cdrkit,
  gobject-introspection,
  jansson,
  libguestfs,
  libnbd,
  libosinfo,
  libvirt,
  libxml2,
  pcre2,
}:

# TODO: this doesn't work because we don't have ocaml bindings for a few
# dependencies (libnbd, libguestfs and probably more)

let
  ocaml' = with ocaml-ng.ocamlPackages; [
    ocaml
    findlib
    # libguestfs
    # libnbd
    ocaml_libvirt
  ];

in
stdenv.mkDerivation rec {
  pname = "virt-v2v";
  version = "2.3.2";

  src = fetchFromGitHub {
    owner = "libguestfs";
    repo = pname;
    rev = "v" + version;
    hash = "sha256-PFNKjW4u9Un38hSaDi8UjDc1HiNDq3FY8vGsKA7VS7I=";
    fetchSubmodules = true;
  };

  nativeBuildInputs = [
    autoreconfHook
    ocaml'
    pkg-config
  ];

  buildInputs = [
    cdrkit
    gobject-introspection
    jansson
    libguestfs
    libnbd
    libosinfo
    libvirt
    libxml2
    pcre2
  ];

  enableParallelBuilding = true;

  meta = with lib; {
    description = "Convert guests from foreign hypervisors to run on KVM";
    license = licenses.gpl2Only;
    broken = true; # wip
  };
}
