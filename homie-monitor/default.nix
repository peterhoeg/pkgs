{
  stdenv,
  lib,
  writeShellScript,
  bundlerEnv,
  fetchFromGitHub,
  nodejs,
  v8,
  pkg-config,
  zlib,
  libxml2,
  openssl,
  ruby,
}:

let
  ruby' = ruby;

  pname = "homie-monitor";
  env = bundlerEnv {
    name = "${pname}-env";
    ruby = ruby';
    gemdir = ./.;
  };
  server = writeShellScript "server" ''
    set -eEuo pipefail

    exec bundle exec puma config.ru -v
  '';
in
stdenv.mkDerivation rec {
  inherit pname;
  version = "0.9.1";

  src = fetchFromGitHub {
    owner = "skoona";
    repo = "HomieMonitor";
    rev = version;
    hash = "sha256-pVjhoJoGXt5YTEvAN0f/qotSL8H7fyuSeoiwF8UDFnM=";
  };

  buildInputs = [
    env.wrappedRuby
    libxml2
    nodejs
    openssl
    v8
    zlib
  ];

  nativeBuildInputs = [ pkg-config ];

  postInstall = ''
    install -Dm555 ${server} $out/bin/${server.name}
  '';

  meta = with lib.lib; {
    description = "Homie 3.0 Convention Administrative Monitor for ESP-8266 Devices";
    # doesn't build due to an issue with nokogiri
    broken = true;
  };
}
