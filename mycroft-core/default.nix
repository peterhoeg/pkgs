{
  lib,
  python3,
  fetchFromGitHub,
  formats,
  writeShellScript,
}:

let
  packageOverrides = [
    (mkOverride "pillow" "8.3.2" "sha256-3ePz7Y0AxyYxvBnL//itO2IVBipe7UAjga02X4LwwYw=")
    (mkOverride "python-dateutil" "2.6.0" "sha256-YqL43z1m+Hg3P9AHLqz07lIZS6MC4ACCgo4NJjsEGNI=")
    (mkOverride "websocket-client" "1.2.1" "sha256-jftxXYqZL1cS//jIQ62ulOIrIqmbLF5rDsShqYHMTg0=")
  ];

  lingua-franca = pypkgs.buildPythonPackage rec {
    pname = "lingua-franca";
    version = "0.4.2";
    src = fetchFromGitHub {
      owner = "MycroftAI";
      repo = pname;
      rev = "v" + version;
      hash = "sha256-VpoPKopz61/XusKqtyT9wBvNerB+hEDOLyWkKRRiofM=";
    };
    propagatedBuildInputs = with pypkgs; [
      python-dateutil
    ];
    pythonImportsCheck = null;
  };

  mkOverride = attrName: version: hash: self: super: {
    ${attrName} = super.${attrName}.overridePythonAttrs (oldAttrs: {
      inherit version;
      src = oldAttrs.src.override {
        inherit version hash;
      };
      doCheck = false;
    });
  };

  python = python3.override {
    # Put packageOverrides at the start so they are applied after defaultOverrides
    packageOverrides = lib.foldr lib.composeExtensions (self: super: { }) packageOverrides;
  };

  pypkgs = python.pkgs;

  mkSystemdUnit =
    name:
    (formats.toINI { }).generate "${name}.unit" {
      Unit = {
        Description = "MyCroftAI - ${name}";
      };

      Service = {
        Type = "exec";
        ExecStart = "@out@/bin/mycroft ${name}";
        DynamicUser = true;
      };

      WantedBy = "mycroftai.target";
    };

in
pypkgs.buildPythonApplication rec {
  pname = "mycroft-core";
  version = "21.2.2";

  src = fetchFromGitHub {
    owner = "MycroftAI";
    repo = pname;
    rev = "tags/release/v" + version;
    hash = "sha256-SCz4rQYTHB1wKeQnJwjmc/jJr/aOWB73tjBqCHjfIAs=";
  };

  propagatedBuildInputs = with pypkgs; [
    # fann2
    fasteners
    lingua-franca
    # msk
    # msm
    # padaos
    # petact
    pillow
    # precise-runner
    pyee
    pyserial
    pyxdg
    requests
    # tornado6
    websocket-client
  ];

  # installPhase = ''
  #   runHook preInstall

  #   install -Dm555 ${wrapper} $out/bin/${wrapper.name}
  #   substituteInPlace $out/bin/${wrapper.name} \
  #     --subst-var out
  #   install -Dm555 -t $out/libexec *.py
  #   install -Dm444 -t $out/share/doc/${pname} *.md

  #   runHook postInstall
  # '';

  meta = with lib; {
    description = "MycroftAI";
    license = licenses.free;
  };
}
