{
  lib,
  buildRubyGem,
  ruby,
}:

buildRubyGem rec {
  name = "ceedling-${version}";
  inherit ruby;
  gemName = "ceedling";
  version = "0.31.1";

  source.sha256 = "sha256-Ma9Rr/ziR9YDSIiEoqyfxh2cUTBzd5UJQ7ug10j4W8o=";

  meta = with lib; {
    description = "build system for C projects";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.unix;
  };
}
