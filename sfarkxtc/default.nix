{
  lib,
  stdenv,
  fetchFromGitHub,
  zlib,
}:

let
  postPatch = ''
    substituteInPlace Makefile \
      --replace-fail '$(DESTDIR)/usr/local' $out
  '';

  postInstall = ''
    install -Dm444 -t $out/share/doc/$pname *.md
  '';

  sfarklib = stdenv.mkDerivation rec {
    pname = "sfarklib";
    version = "2.24.2019-06-14";

    src = fetchFromGitHub {
      owner = "raboof";
      repo = pname;
      rev = "c13233a979856d762246e6856654c51b310edc9b";
      sha256 = "1ggg6h31jz88pmzdbha1ndv62mwbylbx2731j49vk00dy3bhq84r";
    };

    inherit postPatch postInstall;

    buildInputs = [ zlib ];
  };

in
stdenv.mkDerivation rec {
  pname = "sfarkxtc";
  version = "unstable-2018-12-08";

  src = fetchFromGitHub {
    owner = "raboof";
    repo = pname;
    rev = "4ed577d5779a68422b816f96a40e8e4abe65894a";
    sha256 = "1n2b2q97nmz7hi943wgdrvmfpm5ahpm2vdkgfjkmc8122x4bynpz";
  };

  inherit postPatch postInstall;

  buildInputs = [
    zlib
    sfarklib
  ];

  meta = with lib; {
    description = "Converts soundfonts in the legacy sfArk v2 file format to sf2";
    maintainers = with maintainers; [ peterhoeg ];
  };
}
