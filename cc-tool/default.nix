{
  stdenv,
  lib,
  fetchFromGitHub,
  cmake,
  pkg-config,
  boost,
  libusb1,
}:

stdenv.mkDerivation rec {
  pname = "cc-tool";
  version = "0.26.20211112";

  # seems to be the most recent upstream although it just adds cmake
  src = fetchFromGitHub {
    owner = "MikeColeGuru";
    repo = pname;
    rev = "08328400f4d538b88738a23dd5b378917c59ec03";
    hash = "sha256-HZo/bv7V/ys480R8I6Hx6zyhW+bRCH150zoK9lN/OX0=";
  };

  nativeBuildInputs = [
    cmake
    pkg-config
  ];

  buildInputs = [
    boost
    libusb1
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin cc-tool
    install -Dm444 -t $out/lib/udev/rules.d ../udev/*.rules

    runHook postInstall
  '';

  meta = with lib; {
    description = "Tools for Texas Instruments CC Debugger";
    license = licenses.gpl2Only;
    maintainers = with maintainers; [ peterhoeg ];
    inherit (src.meta) homepage;
  };
}
