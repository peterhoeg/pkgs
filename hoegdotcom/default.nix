{
  stdenv,
  lib,
  fetchFromGitHub,
  hugo,
  sassc,
}:

let
  theme = stdenv.mkDerivation {
    pname = "keepit";
    version = "2022-08-17";
    src = fetchFromGitHub {
      owner = "peterhoeg";
      repo = "KeepIt";
      rev = "e5525b765f4e20deabd057e169bec2d40d227c9b";
      hash = "sha256-MBZzqNpLRTp9NIoojWeiiXbYhCgwBBxHVEndMWXMarU=";
    };
    dontBuild = true;
    installPhase = ''
      mkdir -p $out
      cp -r * $out
    '';
  };

in
stdenv.mkDerivation (finalAttrs: {
  pname = "hoeg.com";
  version = "1.0";

  src = builtins.fetchGit {
    url = "ssh://git@gitlab.com/peterhoeg/hoeg.com.git";
    ref = "main";
    rev = "65382178b6bb019d605d613459d7ff7d19564b8b";
  };

  buildCommand = ''
    cp -r --no-preserve=all $src/* .
    rm -rf themes/keepit
    ln -s ${theme} themes/keepit
    make build
    cp -r public $out
  '';

  buildTargets = [ "build" ];

  nativeBuildInputs = [
    hugo
    sassc
  ];

  meta = with lib; {
    description = "hoeg.com";
    hydraPlatforms = platforms.none; # in a private repo
  };
})
