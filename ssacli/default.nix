{
  stdenv,
  lib,
  fetchurl,
  autoPatchelfHook,
  dpkg,
  installShellFiles,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "ssacli";
  version = "6.15-11.0";

  src = fetchurl {
    url = "https://downloads.linux.hpe.com/SDR/downloads/MCP/Ubuntu/pool/non-free/ssacli-${finalAttrs.version}_amd64.deb";
    hash = "sha256-qjkUhyj9r7FR9z/6vYebLP9GvG6ehzidd9hxDFjltYk=";
  };

  buildInputs = [ stdenv.cc.cc.lib ];

  nativeBuildInputs = [
    autoPatchelfHook
    dpkg
    installShellFiles
  ];

  unpackPhase = "dpkg -x $src .";

  dontConfigure = true;

  dontBuild = true;

  installPhase = ''
    install -Dm555 -t $out/bin opt/smartstorageadmin/ssacli/bin/{rmstr,ssacli,ssascripting}
    install -Dm444 -t $out/share/doc/${finalAttrs.pname} opt/smartstorageadmin/ssacli/bin/*.{license,txt}
    installManPage usr/man/man?/ssacli.?.gz
  '';

  dontStrip = true;

  meta = with lib; {
    description = "HP Smart Array CLI";
    license = licenses.unfreeRedistributable;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = [ "x86_64-linux" ];
    hydraPlatforms = platforms.none;
  };
})
