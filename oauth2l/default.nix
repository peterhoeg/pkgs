{
  lib,
  buildGoModule,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "oauth2l";
  version = "1.3.0";

  src = fetchFromGitHub {
    owner = "google";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-bL1bys/CBo/P9VfWc/FB8JHW/aBwC521V8DB1sFBIAA=";
  };

  vendorHash = null;

  subPackages = [ "." ];

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md
  '';

  meta = with lib; {
    description = "Simple CLI for interacting with Google oauth tokens";
    license = licenses.asl20;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.unix;
  };
}
