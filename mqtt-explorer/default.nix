{
  lib,
  appimageTools,
  fetchurl,
  makeDesktopItem,
}:

let
  pname = "mqtt-explorer";

  icon = fetchurl {
    url = "https://mqtt-explorer.com/icon.png";
    hash = "sha256-yBYLcV998ZQ9Y/Wh9t1zN6D4X2OGWDf1bf707DmvY1s=";
  };

  desktopItems = [
    (makeDesktopItem {
      name = pname;
      desktopName = "MQTT Explorer";
      genericName = "GUI for viewing and manipulating MQTT bus";
      exec = "@out@/bin/mqtt-explorer";
      icon = icon;
      comment = "MQTT Explorer for Linux";
      categories = [ "Network" ];
      startupNotify = false;
      keywords = [
        "mqtt"
        "network"
      ];
    })
  ];

in
appimageTools.wrapType2 rec {
  inherit pname;
  # when bumping this, check that the format hasn't changed (why we have the urls)
  version = "0.4.0-beta.6";

  src = fetchurl {
    urls = [
      "https://github.com/thomasnordquist/MQTT-Explorer/releases/download/v${version}/MQTT-Explorer-${version}.AppImage"
      "https://github.com/thomasnordquist/MQTT-Explorer/releases/download/${version}/MQTT-Explorer-${version}.AppImage"
      "https://github.com/thomasnordquist/MQTT-Explorer/releases/download/0.0.0-${version}/MQTT-Explorer-${version}.AppImage"
    ];
    hash = "sha256-zEosMda2vtq+U+Lrvl6DExvT5cGPbDz0eJo7GRlVzVA=";
  };

  # we should no longer need to do the version move as of 24.05
  extraInstallCommands =
    ''
      if [ -e $out/bin/${pname}-${version} ]; then
        mv $out/bin/${pname}-${version} $out/bin/${pname}
      fi
    ''
    + lib.concatMapStringsSep "\n" (e: ''
      install -Dm444 -t $out/share/applications ${e}/share/applications/*.desktop
    '') desktopItems
    + ''
      for f in $out/share/applications/*.desktop; do
        substituteInPlace $f --subst-var out
      done
    '';

  meta = with lib; {
    description = "MQTT Explorer";
    license = licenses.free;
    mainProgram = "mqtt-explorer";
  };
}
