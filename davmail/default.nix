{
  stdenv,
  lib,
  fetchFromGitHub,
  davmail,
  ant,
  graalvm11-ce,
  graalvm17-ce,
  graalvm19-ce,
  jdk11,
  jdk17,
  jdk19,
  makeWrapper,
  freetype,
  nativeImage ? false, # something wrong with the classpath
}:

let
  # AWT only works with JDK11 on linux:
  # https://github.com/oracle/graal/issues/4807#issuecomment-1211260039
  jdk' = (if nativeImage then jdk11 else jdk11).override { enableJavaFX = true; };
  graalvm' =
    {
      "11" = graalvm11-ce;
      "17" = graalvm17-ce;
      "19" = graalvm19-ce;
    }
    ."${builtins.head (builtins.splitVersion jdk'.version)}";

  inherit (lib) optionals optionalString;

in
stdenv.mkDerivation rec {
  pname = "davmail";
  version = "6.1.0";

  src = fetchFromGitHub {
    owner = "mguessan";
    repo = pname;
    rev = version;
    hash = "sha256-S1HBYZOZKlkmNWdSQOYbZBrvn/dj/TeNChaXzpiYz0c=";
  };

  postPatch = ''
    substituteInPlace build.xml \
      --replace-fail '<antcall target="download-jre"/>' ""
  '';

  buildInputs = optionals nativeImage [ freetype ];

  nativeBuildInputs = [
    ant
    jdk'
  ] ++ [ (if nativeImage then graalvm' else makeWrapper) ];

  buildPhase = ''
    runHook preBuild

    ANT_OPTS=-Dfile.encoding=UTF-8 ant
    ${optionalString nativeImage ''
      native-image --verbose --no-fallback -cp dist/lib -jar dist/davmail.jar davmail
    ''}

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

    install -Dm444 -t $out/share/icons/apps dist/davmail.png
    ${
      if nativeImage then
        ''
          install -Dm555 -t $out/bin davmail
        ''
      else
        ''
          install -Dm444 -t $out/share/java dist/davmail.jar dist/lib/*.jar

          makeWrapper ${jdk'}/bin/java $out/bin/davmail \
            --set JAVA_HOME ${jdk'.home} \
            --set SWT_GTK3 1 \
            --add-flags "\$JAVA_PATH" \
            --add-flags "-cp $out/share/java/\* davmail.DavGateway"
        ''
    }

    runHook postInstall
  '';

  inherit (davmail) meta;
}
