{
  lib,
  stdenvNoCC,
  fetchFromGitHub,
}:

let
  fonts = [
    "New York"
    "SF Compact"
    "SF Compact Rounded"
    "SF Mono"
    "SF Pro"
  ];

in
stdenvNoCC.mkDerivation (finalAttrs: {
  pname = "san-francisco-fonts";
  version = "1.4";

  src = fetchFromGitHub {
    owner = "thelioncape";
    repo = "San-Francisco-Family";
    rev = finalAttrs.version;
    hash = "sha256-xGdW1D/rtpagC3bDQiE71rE7QvDTIfTneAItlh7jkR4=";
  };

  installPhase = ''
    runHook preInstall

    dir=$out/share/fonts/san-francisco

    rm -rf **/Windows*

    mkdir -p $dir
    ${lib.concatMapStringsSep "\n" (e: ''
      cp -r "${e}" $dir/
    '') fonts}

    runHook postInstall
  '';

  meta = with lib; {
    description = "San Francisco Font Family";
    homepage = "http://developer.apple.com/";
    license = licenses.free;
    platforms = platforms.all;
    maintainers = with maintainers; [ peterhoeg ];
  };
})
