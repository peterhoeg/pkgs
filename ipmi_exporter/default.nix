{
  buildGoModule,
  lib,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "ipmi_exporter";
  version = "1.6.1";

  src = fetchFromGitHub {
    owner = "prometheus-community";
    repo = pname;
    rev = "v" + version;
    hash = "sha256-hifG1lpFUVLoy7Ol3N6h+s+hZjnQxja5svpY4lFFsxw=";
  };

  vendorHash = "sha256-UuPZmxoKVj7FusOS6H1gn6SAzQIZAKyX+m+QS657yXw=";

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} *.md docs/*.md
  '';

  meta = with lib; {
    description = "Prometheus IPMI exporter";
  };
}
