{
  stdenv,
  lib,
  fetchFromGitHub,
  writeText,
  makeWrapper,
  runtimeShell,
  installShellFiles,
  bzip2,
  coreutils,
  curl,
  findutils,
  gnugrep,
  gnused,
  gnutar,
  lsof,
  perl,
  rsync,
  steamcmd,
}:

stdenv.mkDerivation rec {
  pname = "arkmanager";
  version = "1.6.62";

  src = fetchFromGitHub {
    owner = "arkmanager";
    repo = "ark-server-tools";
    rev = "v" + version;
    hash = "sha256-auZpQxyMGgRJUJvN0d9kCBfBtHOOB881qYx+Qgclch8=";
  };

  sourceRoot = "source/tools";

  postPatch = ''
    patchShebangs .
  '';

  dontConfigure = true;

  dontBuild = true;

  buildInputs = [
    bzip2
    coreutils
    curl
    findutils
    gnugrep
    gnused
    gnutar
    lsof
    perl
    rsync
    steamcmd
  ];

  nativeBuildInputs = [
    installShellFiles
    makeWrapper
  ];

  installPhase = ''
    install -Dm555 -t $out/bin arkmanager
    install -Dm444 -t $out/share/doc/arkmanager/examples *.cfg
    install -Dm444 -t $out/lib/systemd/system systemd/*

    installShellCompletion  \
      --cmd arkmanager \
      --bash bash_completion/arkmanager

    wrapProgram $out/bin/arkmanager \
      --prefix PATH : ${lib.makeBinPath buildInputs}
  '';

  meta = with lib; {
    description = "ARK Manager";
    license = licenses.free;
  };
}
