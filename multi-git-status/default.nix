{
  stdenv,
  lib,
  fetchFromGitHub,
}:

stdenv.mkDerivation rec {
  pname = "multi-git-status";
  version = "2.2";

  src = fetchFromGitHub {
    owner = "fboender";
    repo = "multi-git-status";
    rev = version;
    hash = "sha256-jzoX7Efq9+1UdXQdhLRqBlhU3cBrk5AZblg9AYetItg=";
  };

  dontConfigure = true;
  dontBuild = true;

  makeFlags = [ "PREFIX=${placeholder "out"}" ];

  meta = with lib; {
    description = "Multi Git Status";
    license = licenses.mpl20;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
