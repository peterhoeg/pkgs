{
  lib,
  resholve,
  fetchFromGitHub,
  runtimeShell,
  coreutils,
  rsync,
}:

resholve.mkDerivation rec {
  pname = "bacap";
  # no tagged releases
  version = "0-unstable-2024-11-10";

  src = fetchFromGitHub {
    owner = "llucax";
    repo = "bacap";
    rev = "270e57f33eac055fb7f5b8d89069d03be5c10861";
    hash = "sha256-rPIgFeGoaN2MExY1E3sTtM/Xv/IiEpfGKya44vP4DsE=";
  };

  solutions.default = {
    scripts = [ "bin/bacap" ];
    interpreter = runtimeShell;
    inputs = [
      coreutils
      rsync
    ];
    fake = {
      source = [
        "/etc/bacaprc"
      ];
    };
  };

  dontBuild = true;

  installPhase = ''
    runHook preInstall

    install -Dm555 bacap -t $out/bin
    install -Dm444 README* -t $out/share/doc/${pname}

    runHook postInstall
  '';

  meta = with lib; {
    description = "Very simple backup tool using rsync and hardlinks";
    license = licenses.gpl3;
    mainProgram = "bacap";
  };
}
