{
  lib,
  fetchFromGitHub,
  python3Packages,
  git,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication {
  pname = "cc2538-bsl";
  version = "2.1.0.20240318";

  format = "other";

  disabled = !pypkgs.isPy3k;

  # the version on pypi only supports py2
  src = fetchFromGitHub {
    owner = "JelmerT";
    repo = "cc2538-bsl";
    rev = "0142853bc41fa80d754a47b3e1d886072d88b8da";
    # hash = "sha256-Vz6FbbifgM4wAhp861U/MAdqTL9Gj0GDSR5MsQQPVB8=";
    hash = "sha256-k+wqGny6Uo0caxVV2PzfQ7AldlI+dQ1obNSNvX2L7i0=";
    # leaveDotGit = true;
  };

  nativeBuildInputs = [
    git
    pypkgs.setuptools_scm
  ];

  propagatedBuildInputs = with pypkgs; [
    intelhex
    pyserial
    python_magic
  ];

  checkInputs = with pypkgs; [
    pytestCheckHook
    scripttest
  ];

  postPatch = ''
    substituteInPlace cc2538-bsl.py \
      --replace-fail 'sys.argv[0]' '"cc2538-bsl"'
  '';

  installPhase = ''
    runHook preInstall

    install -Dm555 cc2538-bsl.py $out/bin/cc2538-bsl
    install -Dm444 -t $out/share/doc/cc2538-bsl *.md

    runHook postInstall
  '';

  meta = {
    description = "Upload firmware via the serial boot loader onto the CC13xx, CC2538 and CC26xx SoC.";
    license = lib.licenses.bsd3;
    mainProgram = "cc2538-bsl";
  };
}
