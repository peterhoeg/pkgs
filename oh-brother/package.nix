{
  lib,
  fetchFromGitHub,
  python3Packages,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "oh-brother";
  version = "0-unstable-2024-08-10";

  pyproject = false;

  src = fetchFromGitHub {
    owner = "CauldronDevelopmentLLC";
    repo = "oh-brother";
    rev = "078f8baa8eabbe719ad75574a7b846478d1d07b4";
    hash = "sha256-1FIso+CisPorrbybFgN/2wTTNTnnbgeMcmlGcSBrEjI=";
  };

  postPatch = ''
    sed -i '16i import importlib.util' oh-brother.py
    sed -i '17i import importlib.machinery' oh-brother.py
  '';

  dependencies = with pypkgs; [
    pyasyncore
    pysnmplib # *MUST* come before pysnmp as it shares the pysnmp python namespace
    pysnmp
    pysnmp-pyasn1
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 oh-brother.py $out/bin/${meta.mainProgram}
    install -Dm444 -t $out/share/doc/${pname} *.md

    runHook postInstall
  '';

  # no tests
  doCheck = true;

  meta = with lib; {
    description = "Update Brother Printer Firmware";
    license = licenses.gpl2Only;
    mainProgram = "oh-brother";
  };
}
