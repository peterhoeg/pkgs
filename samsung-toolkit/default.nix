{
  stdenv,
  lib,
  fetchurl,
  autoPatchelfHook,
}:

stdenv.mkDerivation rec {
  pname = "samsung-toolkit";
  version = "2.1";

  src = fetchurl {
    url = "https://www.samsung.com/semiconductor/global.semi.static/Samsung_SSD_DC_Toolkit_for_Linux_V${version}";
    sha256 = "sha256-mkz16/Rta3PK59e0gG1s/+rnxHFl/fLtkaZT2xIGR/A=";
  };

  dontUnpack = true;

  sourceRoot = ".";

  nativeBuildInputs = [ autoPatchelfHook ];

  buildInputs = [ stdenv.cc.cc.lib ];

  installPhase = ''
    install -Dm555 ${src} $out/bin/samsung-toolkit
  '';

  meta = with lib; {
    description = "Samsung SSD Toolkit DC";
  };
}
