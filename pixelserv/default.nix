{
  stdenv,
  lib,
  fetchurl,
  writeText,
  perl,
}:

let
  service = writeText "pixelserv.service" ''
    [Unit]
    Description = 1x1 pixel server
    After = network.target

    [Service]
    DynamicUser = true
    ExecStart = @out@/bin/pixelserv
    AmbientCapabilities = CAP_NET_BIND_SERVICE
  '';

in
stdenv.mkDerivation rec {
  pname = "pixelserv";
  version = "unstable";

  src = fetchurl {
    url = "http://proxytunnel.sourceforge.net/files/pixelserv.pl.txt";
    sha256 = "sha256-j2kDcSNg8qdP0kF0u7gE4fYehh86+RlQxAgsGt/3NSg=";
  };

  buildInputs = [ perl ];

  dontUnpack = true;

  installPhase = ''
    runHook preInstall

    mkdir -p $out/lib/systemd/system

    install -Dm555 ${src} $out/bin/pixelserv

    substitute ${service} $out/lib/systemd/system/pixelserv.service \
      --subst-var out

    runHook postInstall
  '';

  meta = { };
}
