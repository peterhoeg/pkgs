{
  stdenv,
  lib,
  fetchurl,
  unzip,
}:

stdenv.mkDerivation rec {
  pname = "amdvbflash";
  version = "4.71";

  # https://www.techpowerup.com/download/ati-atiflash/
  src = fetchurl {
    urls = [
      "https://sg1-dl.techpowerup.com/files/_49lR-Ga0FYW0UKFriUwcw/1652191104/amdvbflash_linux_${version}.zip"
      "https://sg1-dl.techpowerup.com/files/3NknlTKZnawXSeyYm_gmZg/1647441666/amdvbflash_linux_${version}.zip"
      "https://us5-dl.techpowerup.com/files/-N6opXJoCL9ugeILh2IuCA/1601986025/amdvbflash_linux_${version}.zip"
      "https://us2-dl.techpowerup.com/files/SRsHLK0rvsxPU-yf_Vr7hA/1601997231/amdvbflash_linux_${version}.zip"
    ];
    hash = "sha256-IYbWDKlbvxWSamRLHg6d54e/0Jtfw1cPt2ETnfm37tU=";
  };

  sourceRoot = ".";

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin ${pname}

    runHook postInstall
  '';

  meta = with lib; {
    description = "AMD GPU firmware flash tool";
    homepage = "https://www.techpowerup.com/download/ati-atiflash/";
    maintainers = with maintainers; [ peterhoeg ];
    hydraPlatforms = platforms.none;
  };
}
