{
  stdenv,
  lib,
  fetchFromGitHub,
  autoreconfHook,
  cyrus_sasl,
}:

stdenv.mkDerivation rec {
  pname = "sasl2-oauth";
  version = "unstable-2015-05-23";

  src = fetchFromGitHub {
    owner = "robn";
    repo = pname;
    rev = "c1d7cd0719c233c89307b7406f92a01602a85993";
    sha256 = "03xz40qjsznwb2z83ngm27yrvpg26z4jdzzafgclaqv1q9w0nkzd";
  };

  nativeBuildInputs = [ autoreconfHook ];

  buildInputs = [ cyrus_sasl ];

  enableParallelBuilding = true;

  meta = with lib; {
    description = "OAuth plugin for libsasl2";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
