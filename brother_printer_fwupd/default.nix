{
  lib,
  fetchFromGitHub,
  python3Packages,
}:

let
  pypkgs = python3Packages;

  depOverrides = {
    # lxml = "^4.9.0";
  };

in
pypkgs.buildPythonApplication rec {
  pname = "brother_printer_fwupd";
  version = "0.8.0";

  pyproject = true;

  src = fetchFromGitHub {
    owner = "sedrubal";
    repo = "brother_printer_fwupd";
    rev = version;
    hash = "sha256-CL9JAy9HLyGu2JMlXZvGzPOA47LK+81DypfP7imACk8=";
  };

  postPatch = lib.concatStringsSep "\n" (
    lib.mapAttrsToList (n: v: ''
      substituteInPlace pyproject.toml \
        --replace-fail '${n} = "${v}"' '${n} = "${pypkgs.${n}.version}"'
    '') depOverrides
  );

  propagatedBuildInputs = with pypkgs; [
    beautifulsoup4
    lxml
    pdm-backend
    poetry-core
    pysnmp
    requests
    termcolor
    zeroconf
  ];

  # no tests
  doCheck = false;

  meta = with lib; {
    description = "Update Brother Printer Firmware";
    license = licenses.gpl3Only;
  };
}
