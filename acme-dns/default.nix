{
  lib,
  buildGoModule,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "acme-dns";
  version = "1.0";

  src = fetchFromGitHub {
    owner = "joohoi";
    repo = pname;
    rev = "v" + version;
    hash = "sha256-qQwvhouqzkChWeu65epgoeMNqZyAD18T+xqEMgdMbhA=";
  };

  vendorHash = "sha256-q/P+cH2OihvPxPj2XWeLsTBHzQQABp0zjnof+Ys/qKo=";

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} *.md
  '';

  # tests require a live internet connection
  doCheck = true;

  meta = with lib; {
    description = "Limited DNS server with RESTful HTTP API to handle ACME DNS challenges easily and securely.";
    homepage = "https://github.com/joohoi/acme-dns";
    maintainers = with maintainers; [ peterhoeg ];
  };
}
