{
  lib,
  fetchurl,
  appimageTools,
  makeDesktopItem,
}:

let
  pname = "oda-file-converter";

  desktopItems = [
    (makeDesktopItem rec {
      name = pname;
      desktopName = "ODA DWG-DXF Converter";
      genericName = desktopName;
      exec = "@out@/bin/${pname}";
      icon = "nix-snowflake";
      # categories = [ "Network" ];
      startupNotify = false;
      # keywords = [ "mqtt" "network" ];
    })
  ];

in
appimageTools.wrapType2 rec {
  inherit pname;
  version = "23.6";

  src = fetchurl {
    url = "https://download.opendesign.com/guestfiles/Demo/ODAFileConverter_QT5_lnxX64_8.3dll_${version}.AppImage";
    hash = "sha256-x0xuo+XnpFG3y0PRQP407EOBZ6ay+iQRK5l8Ja/IXZE=";
  };

  extraInstallCommands =
    ''
      mv $out/bin/${pname}-${version} $out/bin/${pname}
    ''
    + lib.concatMapStringsSep "\n" (e: ''
      install -Dm444 -t $out/share/applications ${e}/share/applications/*.desktop
    '') desktopItems
    + ''
      for f in $out/share/applications/*.desktop; do
        substituteInPlace $f --subst-var out
      done
    '';

  meta = with lib; {
    description = "ODA DWG-DXF Converter";
    license = licenses.free;
  };
}
