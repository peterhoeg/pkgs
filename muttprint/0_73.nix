{
  stdenv,
  lib,
  fetchurl,
  makeWrapper,
  dialog,
  perlPackages,
  recode,
  autoreconfHook,

}:

perlPackages.buildPerlPackage rec {
  pname = "muttprint";
  version = "0.73";

  src = fetchurl {
    url = "mirror://sourceforge/muttprint/${pname}-${version}.tar.gz";
    sha256 = "1dny4niyibfgazwlzfcnb37jy6k140rs6baaj629z12rmahfdavw";
  };

  postPatch = ''
    substituteInPlace Makefile.am \
      --replace-fail ' pics ' ' '

    for d in doc/manpages/*; do
      test -d $d || continue

      substituteInPlace $d/Makefile.am \
        --replace-fail 'pod2man ' 'pod2man --utf8 --release=${version}'

      sed -i -e '1i=encoding utf8' $d/muttprint.pod
    done

    # This allows us to use the standard builder
    touch Makefile.PL
  '';

  makeFlags = [
    "prefix=${placeholder "out"}"
  ];

  doCheck = false;

  outputs = [ "out" ];

  buildInputs = with perlPackages; [
    DateTimeFormatDateParse
    TextIconv
  ];

  nativeBuildInputs = [
    autoreconfHook
    makeWrapper
  ];

  postInstall = ''
    wrapProgram $out/bin/muttprint \
      --prefix PATH : ${
        lib.makeBinPath [
          dialog
          recode
        ]
      }
  '';

  meta = with lib; {
    description = "mutt print formatter";
    # the perl package TextIconv needs libiconv on darwin
    broken = stdenv.isDarwin;
  };
}
