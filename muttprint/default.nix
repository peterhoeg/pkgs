{
  lib,
  fetchurl,
  perlPackages,
}:

perlPackages.buildPerlPackage rec {
  pname = "muttprint";
  version = "0.72d";

  src = fetchurl {
    url = "mirror://sourceforge/${pname}/${pname}-${version}.tar.gz";
    sha256 = "1j1b7kab82nrw8afqll5vnyfhig100sjx15nfwvwh9lwfd82jrx2";
  };

  postPatch = ''
    # We don't have css files because we don't process the sgml files, so these
    # changes to html, just makes it not error out
    for f in Makefile doc/manual/Makefile doc/manual/**/Makefile ; do \
      substituteInPlace $f \
        --replace-warn /usr/local $out \
        --replace-warn /usr       $out \
        --replace-warn /package   "" \
        --replace-warn .css       .html
    done

    # needed for buildPerlPackage
    touch Makefile.PL
  '';

  # no checks
  doCheck = false;

  # buildPerlPackage defaults to out and devdoc
  outputs = [ "out" ];

  buildInputs = with perlPackages; [
    perl
    DateTimeFormatDateParse
    TextIconv
  ];

  meta = with lib; {
    description = "mutt print";
  };
}
