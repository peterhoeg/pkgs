{
  lib,
  python3Packages,
  fetchFromGitHub,
  makeBinaryWrapper,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "cloudmapper";
  version = "2.10.0";

  format = "other";

  src = fetchFromGitHub {
    owner = "duo-labs";
    repo = pname;
    rev = version;
    hash = "sha256-J1rU03Ecu1PAModzfb2n0HSqL2EnMAuGpY0/334I1AE=";
  };

  buildInputs = with pypkgs; [
    astroid
    boto3
    pytz
    pyyaml
    requests
  ];

  nativeBuildInputs = [ makeBinaryWrapper ];

  dontBuild = true;

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/libexec cloudmapper.py
    mkdir -p $out/${pypkgs.python.sitePackages} $out/share/${pname}
    cp -r auditor commands shared utils $out/${pypkgs.python.sitePackages}
    cp -r web $out/share/${pname}

    mkdir -p $out/bin
    ln -s $out/libexec/cloudmapper.py $out/bin/cloudmapper

    runHook postInstall

    echo "Not ready yet!!!"
    exit 77
  '';

  meta = with lib; {
    description = "Map AWS environments";
    license = licenses.bsd3;
    mainProgram = pname;
    broken = true;
  };
}
