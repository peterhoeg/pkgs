{
  lib,
  appimageTools,
  fetchurl,
  makeDesktopItem,
}:

let
  pname = "mqttx";

  icon = fetchurl {
    url = "https://mqtt-explorer.com/icon.png";
    hash = "sha256-yBYLcV998ZQ9Y/Wh9t1zN6D4X2OGWDf1bf707DmvY1s=";
  };

  desktopItems = [
    (makeDesktopItem {
      name = pname;
      desktopName = "MQTTX";
      genericName = "GUI for viewing and manipulating MQTT bus";
      exec = "@out@/bin/mqttx";
      icon = icon;
      comment = "MQTTX for Linux";
      categories = [ "Network" ];
      startupNotify = false;
      keywords = [
        "mqtt"
        "network"
      ];
    })
  ];

in
appimageTools.wrapType2 rec {
  inherit pname;
  version = "1.9.6";

  src = fetchurl {
    url = "https://www.emqx.com/en/downloads/MQTTX/v${version}/MQTTX-${version}.AppImage";
    hash = "sha256-cYrw/PcfIFAgF3jmCBBIH70Y/8i3nO3Nrmjsj/MbiaA=";
  };

  extraInstallCommands =
    ''
      mv $out/bin/${pname}-${version} $out/bin/${pname}
    ''
    + lib.concatMapStringsSep "\n" (e: ''
      install -Dm444 -t $out/share/applications ${e}/share/applications/*.desktop
    '') desktopItems
    + ''
      for f in $out/share/applications/*.desktop; do
        substituteInPlace $f --subst-var out
      done
    '';

  meta = with lib; {
    description = "MQTTX";
    license = licenses.free;
  };
}
