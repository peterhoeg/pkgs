{
  lib,
  fetchFromGitHub,
  python3Packages,
  gobject-introspection,
  gtk3,
  libnotify,
  wrapGAppsHook,
}:

python3Packages.buildPythonApplication rec {
  pname = "maildirwatch";
  version = "0.2.0";

  # it's also on pypi but I have needed to try specific commits as it was being
  # developed, so let's just leave it at GH for now
  src = fetchFromGitHub {
    owner = "mkcms";
    repo = "maildirwatch";
    rev = "v${version}";
    hash = "sha256-ugydKmGkajMSZBbis0t2HFJ/nssRlSrFBiWRsRwsjuI=";
  };

  buildInputs = [
    gobject-introspection
    gtk3
    libnotify
  ];

  # for some reason, gobject-introspection is needed in *BOTH* build and nativeBuild inputs.
  nativeBuildInputs = [
    gobject-introspection
    wrapGAppsHook
  ];

  propagatedBuildInputs = with python3Packages; [ pygobject3 ];

  pythonImportsCheck = [ "maildirwatch" ];

  dontWrapGApps = true;

  preFixup = ''
    makeWrapperArgs+=("''${gappsWrapperArgs[@]}")
  '';

  # No tests
  doCheck = false;

  meta = with lib; {
    description = "Watch a maildir directory for new mails and notify";
    license = licenses.gpl3Only;
    mainProgram = "maildirwatch";
  };
}
