{
  lib,
  python3Packages,
  fetchFromGitLab,
}:

python3Packages.buildPythonApplication {
  pname = "freeotp-migrator";
  version = "20190322";

  format = "other";

  src = fetchFromGitLab {
    owner = "stavros";
    repo = "freeotp-to-andotp-migrator";
    rev = "acab711e67ddd3e66ea3b3a5ba24dec98ab2b5ad";
    sha256 = "1prnfanhb3s70bnmr538jjkn877f91d5bnnp1lp756cnqav9grlm";
  };

  installPhase = ''
    runHook preInstall

    install -Dm755 -t $out/bin freeotp_migrate.py

    runHook postInstall
  '';

  meta = with lib; {
    description = "FreeOTP to andOTP migrator";
    maintainer = with maintainers; [ peterhoeg ];
  };
}
