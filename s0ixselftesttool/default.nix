{
  resholve,
  lib,
  fetchFromGitHub,
  runtimeShell,
  acpica-tools,
  bc,
  coreutils,
  gawk,
  gnugrep,
  gnused,
  pciutils,
  powertop,
  linuxPackages,
  util-linux,
  xset,
  xxd,
}:

let
  inherit (lib) getBin getExe;

in
resholve.mkDerivation rec {
  pname = "s0ixselftesttool";
  version = "2024-02-07";

  src = fetchFromGitHub {
    owner = "intel";
    repo = "s0ixselftesttool";
    rev = "c12ae3ea611812547e09bb755dd015dd969b664c";
    hash = "sha256-9O72TxlLrkQbt80izWdbLQt9OW/4Aq1p4RuQoD2yQ5E=";
  };

  postPatch = ''
    substituteInPlace *.sh \
      --replace-fail '"$DIR"/turbostat' turbostat
  '';

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin                *.sh
    install -Dm444 -t $out/share/doc/${pname} *.md

    runHook postInstall
  '';

  solutions.default = {
    scripts = [ "bin/s0ix-selftest-tool.sh" ];
    interpreter = runtimeShell;
    inputs = [
      acpica-tools
      bc
      coreutils
      gawk
      gnugrep
      gnused
      pciutils
      powertop
      linuxPackages.turbostat
      util-linux
      xxd
      xset
    ];
    execer = map (e: "cannot:${if builtins.isString e then e else getExe e}") [
      powertop
      linuxPackages.turbostat
      "${getBin util-linux}/bin/dmesg"
      "${getBin util-linux}/bin/rtcwake"
    ];
  };

  meta = with lib; {
    description = "S0ix Self Test Tool";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
}
