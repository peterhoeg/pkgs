{
  lib,
  buildGoModule,
  fetchFromGitHub,
  installShellFiles,
  pkg-config,
  glib-networking,
  json-glib,
  withGUI ? true,
  webkitgtk,
  wrapGAppsHook,
}:

buildGoModule rec {
  pname = "ddns-updater";
  version = "2.5.0";

  src = fetchFromGitHub {
    owner = "qdm12";
    repo = "ddns-updater";
    rev = "v${version}";
    hash = "sha256-DQv6WG5qvwMCLU2yrR/Fa/81C1EAFq2lDMdo0+C8ono=";
  };

  vendorHash = "sha256-hRcmaz5iG9sWRzzIsMTW6qchlP84jhwOD3PjoeWIW5g=";

  ldflags = [ "-X main.version=${version}" ];

  meta = with lib; {
    description = "Dynamic DNS updater";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
    mainProgram = "updater";
  };
}
