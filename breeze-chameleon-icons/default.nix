{
  lib,
  stdenvNoCC,
  fetchFromGitHub,
}:

let
  dirs = {
    breeze-chameleon-dark = "Breeze Chameleon Dark";
    breeze-chameleon-light = "Breeze Chameleon Light";
    breeze-round-chameleon-dark = "Breeze-Round-Chameleon Dark Icons";
    breeze-round-chameleon-light = "Breeze-Round-Chameleon Light Icons";
  };

in
stdenvNoCC.mkDerivation rec {
  pname = "breeze-chameleon-icons";
  version = "2024-02-27";

  src = fetchFromGitHub {
    owner = "L4ki";
    repo = "Breeze-Chameleon-Icons";
    rev = "a65fc7a866734f9eeb8e9566b090d694e2221e91";
    hash = "sha256-ZNM0uw2pV6wIrmRirxCA0fREzznssRd/OmWBokSf9+Y=";
  };

  installPhase =
    ''
      dir=$out/share/icons
      install -d $dir

      install -Dm444 -t $out/share/doc/${pname} LICENSE *.md
    ''
    + lib.concatStringsSep "\n" (
      lib.mapAttrsToList (n: v: ''
        cp -r "${v}" $dir/${n}
      '') dirs
    );

  meta = with lib; {
    description = "Breeze Chameleon Icons";
    license = licenses.gpl3Only;
  };
}
