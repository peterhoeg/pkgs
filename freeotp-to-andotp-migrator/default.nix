{
  lib,
  fetchFromGitLab,
  python3Packages,
}:

python3Packages.buildPythonApplication rec {
  pname = "freeotp-to-andotp-migrator";
  version = "unstable-2020-12-08";

  format = "other";

  src = fetchFromGitLab {
    owner = "stavros";
    repo = "freeotp-to-andotp-migrator";
    rev = "2439af98cb64fb2956a5a75ed4cab63c5efd35d7";
    sha256 = "sha256-SL34V70iKT4vk4p925DyZ36QpJdPzlbi056y0dGzR2Q=";
  };

  installPhase = ''
    install -Dm555 freeotp_migrate.py $out/bin/freeotp_migrate
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md
  '';

  # No tests
  doCheck = false;

  meta = with lib; {
    description = "Convert FreeOTP tokens to andOTP";
    license = licenses.agpl3Only;
  };
}
