{
  lib,
  bundlerApp,
  ruby,
}:

bundlerApp rec {
  pname = "standard";
  gemdir = ./.;
  exes = [ "standardrb" ];

  meta = with lib; {
    description = "ruby formatter";
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.unix;
  };
}
