{
  stdenv,
  lib,
  makeWrapper,
  coreutils,
  curl,
  gawk,
  gnugrep,
  monitoring-plugins,
  traceroute,
}:

stdenv.mkDerivation rec {
  pname = "sensu-checks-shell";
  version = "unstable";

  src = ./src;

  dontUnpack = true;

  dontConfigure = true;

  dontBuild = true;

  buildInputs = [
    coreutils
    curl
    gawk
    gnugrep
    monitoring-plugins
    traceroute
  ];

  nativeBuildInputs = [ makeWrapper ];

  installPhase = ''
    runHook preInstall

    if [ ! -e $src ]; then
      echo "Source not found: $src"
      exit 1
    fi

    install -d $out/bin

    for f in $src/{check,metrics}*.sh ; do
      script=$out/libexec/$(basename $f .sh)

      install -Dm555 $f $script

      makeWrapper $script $out/bin/$(basename $f .sh) \
        --argv0 $(basename $f .sh)
    done

    runHook postInstall
  '';

  meta = with lib; {
    description = "Shell checks";
  };
}
