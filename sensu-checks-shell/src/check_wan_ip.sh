#!/usr/bin/env bash

###
# Report the WAN IP
###

set -eEuo pipefail

NAME=WAN_IP
URL=${1:-"https://icanhazip.com"}

trap "echo $NAME: UNKNOWN; exit 3" ERR

ip=$(curl -s $URL)

echo "$NAME: OK: $ip"
exit 0
