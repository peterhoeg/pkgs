#!/usr/bin/env bash

###
# Wrap our shell checks to show unknown if the program is not available instead
# of erroring out
###

set -eEuo pipefail

NAME=wrapper

trap "echo $NAME: UNKNOWN; exit 3" ERR

CMD=${1:-""}

unknown() {
  echo "UNKNOWN: $1"
  exit 3
}

if [ -z $CMD ]; then
  unknown "No command provided"
elif ! type -p $CMD >/dev/null; then
  unknown "Cannot find $CMD"
else
  exec $CMD
fi
