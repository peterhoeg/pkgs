#!/usr/bin/env bash

###
# Report the next few hops on WAN
###

set -eEuo pipefail

DEBUG_PATH=${DEBUG_PATH:-0}

NAME=WAN_ROUTE

trap "echo $NAME: UNKNOWN; exit 3" ERR

HOST=${CHECK_WAN_ROUTE_HOST:-"app4.speartail.net"}
HOPS=${CHECK_WAN_ROUTE_HOPS:-8}

if [ $DEBUG_PATH -eq 1 ]; then
  echo "$NAME: OK: $PATH"
  exit 0
fi

route=$(traceroute -n --max-hops=$HOPS $HOST)

echo "$NAME: OK: $route"
exit 0
