#!/usr/bin/env bash

###
# This script will check for duplicate replies when pinging a host.
#
# You probably will not need this regularly but if a problem is observed, this
# can be used to verify that it isn't coming back.
#
# By default this script will return CRITICAL if the host cannot be pinged or
# if duplicates are in fact seen.
#
# You can choose UNKNOWN and WARNING for the "cannot ping" and "dupes seen"
# cases.
#
###

set -u

PING=@ping@
GREP=@grep@

count=1
host=""
unknown=0
warn=0

print_help() {
  echo "usage: $0 -H host [-c count] [-w] [-h]"
  echo ""
  echo "  -H host  -- the host to ping"
  echo "  -c count -- the number of packets to send (default: $count)"
  echo "  -u       -- return UNKNOWN instead of CRITICAL if the host cannot be pinged"
  echo "  -w       -- return WARNING instead of CRITICAL if duplicates are found"
  echo "  -h       -- show this help screen"
  exit 3
}

msg() {
  echo "PingDuplicate ${1}: ${host}"
  exit $2
}

msg_ok() {
  msg "OK" 0
}

msg_warning() {
  msg "WARNING" 1
}

msg_critical() {
  msg "CRITICAL" 2
}

msg_unknown() {
  msg "UNKNOWN" 3
}

if ! [ $(command -v $PING) ]; then
  PING=ping
fi

if ! [ $(command -v $GREP) ]; then
  GREP=grep
fi

while getopts ":c:H:huw" opt; do
  case $opt in
  c)
    count=$OPTARG
    ;;
  H)
    host=$OPTARG
    ;;
  h)
    print_help
    ;;
  u)
    unknown=1
    ;;
  w)
    warn=1
    ;;
  \?)
    echo "Invalid option: -$OPTARG" >&2
    ;;
  esac
done

test -z "${host}" && print_help

pings=$(${PING} -c ${count} ${host})

# ping returns > 0 if it couldn't ping the host
if [ $? -gt 0 ]; then
  if [ $unknown -eq 1 ]; then
    msg_unknown
  else
    msg_critical
  fi
fi

echo "${pings}" | ${GREP} -q DUP

# grep returns 0 if it finds a match
if [ $? -eq 1 ]; then
  msg_ok
else
  if [ $warn -eq 1 ]; then
    msg_warning
  else
    msg_critical
  fi
fi
