#!/usr/bin/env bash

set -eEuo pipefail

# set -x

HOST="$1"
USER="$2"
PASS="$3"
TIME="$4"
SENSOR="$5"
WARN="$6"
CRIT="$7"

temp=$(check-sensor.rb \
  --host "$HOST" \
  --username "$USER" \
  --password "$PASS" \
  --driver lan20 \
  --sensor "$SENSOR" |
  cut -f2 -d ' ' | cut -f1 -d '.')

if [[ $temp -lt 10 ]]; then
  echo "UNKNOWN: temperature ${temp} is less than 10"
  exit 3
elif [[ $temp -ge $CRIT ]]; then
  echo "CRITICAL: temperature ${temp} is >= ${CRIT}"
  exit 2
elif [[ $temp -ge $WARN ]]; then
  echo "WARNING: temperature ${temp} is >= ${WARN}"
  exit 1
else
  echo "OK: temperature ${temp} is < ${WARN}"
  exit 0
fi
