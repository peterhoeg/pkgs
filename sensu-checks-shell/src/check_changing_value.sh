#!/usr/bin/env bash

set -eEuo pipefail

# set -x

BASENAME=$(basename $0 .sh)
APP=$(echo $BASENAME | cut -f2 -d '_')
BASEDIR=/tmp

case $BASENAME in
check_dropbox | .check_dropbox-wrapped)
  URL=${1}
  CUSTOMER=${2}
  STATUS=$BASEDIR/${APP}_${CUSTOMER}
  text="${APP}"

  # we need everything except for the last 3 bytes
  new=$(curl -L -s $URL | head -c -3)
  ;;
check_hemera | .check_hemera-wrapped)
  URL=${1}/metrics
  FIELD1=${2}
  FIELD2=${3}
  STATUS=$BASEDIR/${APP}_${FIELD1}_${FIELD2}
  text="${APP} ${FIELD1}.${FIELD2}"

  new=$(curl -s $URL | grep $FIELD1 | grep $FIELD2 | cut -f2 -d ' ' | cut -f1 -d '.')
  ;;
*)
  echo "UNKNOWN: I don't know what to do here as '$BASENAME'..."
  exit 3
  ;;
esac

if [[ ! -e $STATUS ]]; then
  echo 0 >$STATUS
  echo "UNKNOWN: ${text} is 0"
  exit 3
fi

old=$(cat $STATUS)
stats="old=${old};new=${new}"

if [[ $new -gt $old ]]; then
  echo $new >$STATUS
  echo "OK: ${text} is increasing|${stats}"
  exit 0
else
  echo "CRITICAL: ${text} is not increasing|${stats}"
  exit 2
fi
