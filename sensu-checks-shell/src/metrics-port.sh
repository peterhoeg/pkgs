#!/usr/bin/env bash

###
# This script will pass metrics for connecting to a TCP port
#
###

set -eEuo pipefail

AWK=awk
CHECK_TCP=check_tcp
CUT=cut
GREP=grep
PING=ping

host=""
port=80
scheme=""

print_help() {
  echo "usage: $0 -H host -p port -s scheme [-h]"
  echo ""
  echo "  -H host    -- the host to connect to"
  echo "  -p port    -- the port to connect to"
  echo "  -s scheme  -- the scheme prefix"
  echo "  -h         -- show this help screen"
  exit 3
}

while getopts ":H:p:s:h" opt; do
  case $opt in
  p)
    port=$OPTARG
    ;;
  H)
    host=$OPTARG
    ;;
  s)
    scheme=$OPTARG
    ;;
  h)
    print_help
    ;;
  \?)
    echo "Invalid option: -$OPTARG" >&2
    ;;
  esac
done

test -z "${host}" && print_help
test -z "${port}" && print_help
test -z "${scheme}" && print_help

result=$(${CHECK_TCP} -H $host -p $port | ${CUT} -f2- -d '|' | ${CUT} -f1 -d ';' | ${CUT} -f2 -d '=' | ${CUT} -f1 -d 's')
result_in_ms=$(${AWK} "BEGIN {print ($result * 1000)}")

echo "${scheme}.avg ${result_in_ms} $(date +%s)"
