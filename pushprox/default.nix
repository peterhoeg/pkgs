{
  buildGoModule,
  lib,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "pushprox";
  version = "0.1.0";

  src = fetchFromGitHub {
    owner = "prometheus-community";
    repo = "PushProx";
    rev = "v" + version;
    sha256 = "sha256-iTKZQuFAcPpWvH9c9+jFb1D1XWBfr9bHzVB2O6bTj24=";
  };

  vendorHash = null;

  meta = with lib; {
    description = "Prometheus Proxy and Proxy Client";
  };
}
