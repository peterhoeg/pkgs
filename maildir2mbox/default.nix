{
  stdenv,
  lib,
  python3Packages,
  fetchFromGitHub,
  makeWrapper,
}:

let
  pypkgs = python3Packages;

in
python3Packages.buildPythonApplication rec {
  pname = "maildir2mbox";
  version = "0.0.0.20200422";

  src = fetchFromGitHub {
    owner = "bluebird75";
    repo = pname;
    rev = "5a4ac6b4a8e72e5a9872faa85446507fcdefe599";
    sha256 = "sha256-pp4lEh0mFIzYVgG4/tPu3FQ+2DwJFw5IZQA8CGpvYK4=";
  };

  nativeBuildInputs = [ makeWrapper ];

  postInstall = ''
    mkdir -p $out/bin
    makeWrapper ${pypkgs.python.interpreter} $out/bin/maildir2mbox \
      --add-flags $out/lib/${python3Packages.python.libPrefix}/site-packages/maildir2mbox.py

    install -Dm444 -t $out/share/doc/${pname} *.{md,txt}
  '';

  meta = with lib; {
    description = "maildir to mbox converter";
  };
}
