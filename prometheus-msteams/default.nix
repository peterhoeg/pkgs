{
  buildGoModule,
  lib,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "prometheus-msteams";
  version = "1.5.0";

  src = fetchFromGitHub {
    owner = "prometheus-msteams";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-tG0b8IeoEptu4VS87b1Dot2k//i+4rCmxnG61JG8nPk=";
  };

  vendorHash = "sha256-AoMWxUqM3VTf7n8vLM8GOBVZMGvmovD4A4r3wFM7D5Y=";

  postPatch = ''
    substituteInPlace cmd/server/main.go \
      --replace-fail ./default-message-card.tmpl $out/share/${pname}/default-message-card.tmpl
  '';

  ldflags = [
    "-w"
    "-s"
    "-X github.com/prometheus/common/version.Branch=master"
    "-X github.com/prometheus/common/version.BuildDate=unknown"
    "-X github.com/prometheus/common/version.Revision=${src.rev}"
    "-X github.com/prometheus/common/version.Version=${version}-0"
  ];

  postInstall = ''
    mv $out/bin/server $out/bin/${pname}
    install -Dm444 -t $out/share/${pname} default-message-card.tmpl
    install -Dm444 -t $out/share/doc/${pname} *.md
  '';

  meta = with lib; {
    description = "Prometheus MS Teams proxy";
  };
}
