{
  stdenvNoCC,
  fetchFromGitHub,
  lib,
}:

stdenvNoCC.mkDerivation {
  pname = "kodi-plymouth-theme";
  version = "0-unstable-2017-05-18";

  src = fetchFromGitHub {
    owner = "solbero";
    repo = "plymouth-theme-kodi-animated-logo";
    rev = "f16d51632ef5d0182821749901af04bbe2efdfd6";
    hash = "sha256-e0ps9Fwdcc9iFK8JDRSayamTfAQIbzC+CoN0Yokv7kY=";
  };

  sourceRoot = "source/plymouth-theme-kodi-animated-logo/usr/share/plymouth/themes/kodi-animated-logo";

  installPhase = ''
    runHook preInstall

    install -D -t $out/share/plymouth/themes/kodi-animated-logo *
    substituteInPlace $out/share/plymouth/themes/kodi-animated-logo/*.plymouth \
      --replace-fail /usr $out

    runHook postInstall
  '';

  meta = with lib; {
    description = "Kodi Plymouth boot theme";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
