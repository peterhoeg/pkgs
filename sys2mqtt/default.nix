{
  lib,
  rustPlatform,
  fetchFromGitHub,
  pkg-config,
  formats,
  libX11,
  libXScrnSaver,
}:

let
  subDeps = (formats.toml { }).generate "subdeps.toml" {
    dependencies.user-idle = {
      features = [ "dbus" ];
    };
  };

in
rustPlatform.buildRustPackage rec {
  pname = "sys2mqtt";
  version = "0.1.1";

  src = fetchFromGitHub {
    owner = "chevdor";
    repo = "sys2mqtt";
    rev = "v" + version;
    hash = "sha256-BszYPIEi+3/q2/lzGpK/2bfs0yI+we2Dr0kBMZMUWwA=";
  };

  # looks like it doesn't actually need the specified version
  postPatch = ''
    substituteInPlace Cargo.toml \
      --replace-fail "1.83.0" ${rustPlatform.rust.rustc.version}
  '';

  cargoHash = "sha256-kYAmg3cMFRUvJHaR9sjHCeQDBapQCdmM5148PwwzrhE=";

  buildInputs = [
    libX11
    libXScrnSaver
  ];

  nativeBuildInputs = [ pkg-config ];

  meta = with lib; {
    description = "Send system info to MQTT";
    license = licenses.gpl3Only;
    maintainers = with maintainers; [ peterhoeg ];
    inherit (src.meta) homepage;
  };
}
