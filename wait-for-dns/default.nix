{
  lib,
  fetchgit,
  python3Packages,
  formats,
  crudini,
}:

let
  pypkgs = python3Packages;

  defaults = (formats.ini { }).generate "systemd.service" {
    Service = {
      ExecStart = "@out@/bin/wait_for_dns %I@";
      DynamicUser = true;
      ProtectHome = "tmpfs";
      ProtectSystem = "strict";
    };
  };

in
pypkgs.buildPythonApplication {
  pname = "wait-for-dns";
  version = "0-unstable-2024-12-08";
  pyproject = true;

  disabled = pypkgs.pythonOlder "3.11";

  src = fetchgit {
    url = "https://forge.km6g.us/km6g/wait-for-dns";
    rev = "ecf697af83e71d69081440cc1b8e4612dea3e92b";
    hash = "sha256-5QHOZbkQZ1L8eot+S1VWElCRQzLZMGFkBx79ll1lLdI=";
  };

  # our version tag format isn't allowed according to https://peps.python.org/pep-0440/, so set a dummy
  env.SETUPTOOLS_SCM_PRETEND_VERSION = "0.0.1";

  buildInputs = with pypkgs; [
    configargparse
    dnspython
  ];

  nativeBuildInputs = with pypkgs; [
    hatchling
    hatch-vcs
    crudini
  ];

  postInstall = ''
    install -Dm644 -t $out/lib/systemd/system *.service
    crudini --merge --inplace $out/lib/systemd/system/*.service < ${defaults}
    substituteInPlace $out/lib/systemd/system/*.service \
      --subst-var out
  '';

  meta = with lib; {
    description = "Wait for DNS";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
    mainProgram = "wait_for_dns";
  };
}
