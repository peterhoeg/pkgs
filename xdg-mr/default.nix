{
  stdenv,
  lib,
  mr,
}:

mr.overrideAttrs (old: rec {
  postPatch =
    old.postPatch or ""
    + ''
      substituteInPlace mr \
        --replace-fail '.mrcache'  '.cache/mr' \
        --replace-fail '.mrconfig' '.config/mr/config' \
        --replace-fail '.mrtrust'  '.config/mr/trust'
    '';

  meta.mainProgram = "mr";
})
