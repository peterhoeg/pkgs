{
  stdenv,
  lib,
  fetchFromGitHub,
  buildMaven,
  maven,
  graalvm11-ce,
  graalvm17-ce,
  graalvm19-ce,
  jdk11,
  jdk17,
  jdk19,
  makeWrapper,
  nativeImage ? false,
}:

let
  jdk' = jdk11;
  graalvm' =
    {
      "11" = graalvm11-ce;
      "17" = graalvm17-ce;
      "19" = graalvm19-ce;
    }
    ."${builtins.head (builtins.splitVersion jdk'.version)}";

  inherit (lib) optionals optionalString;

  inherit (buildMaven ./project-info.json) repo;

in
stdenv.mkDerivation rec {
  pname = "lemminx";
  version = "0.24.0";

  src = fetchFromGitHub {
    owner = "eclipse";
    repo = pname;
    rev = version;
    hash = "sha256-gO8zevGb5MUVznnwhDqhVMT869VtFXVavLrNWOoWmCs=";
  };

  nativeBuildInputs = [
    jdk'
    maven
  ] ++ optionals nativeImage [ graalvm' ];

  dontConfigure = true;

  buildPhase = ''
    runHook preBuild

    export MAVEN_PROJECTBASEDIR=/build/source
    export MVNW_VERBOSE=1

    mvn --offline \
      -Dmaven.repo.local=${repo} \
      package \
      ${optionalString nativeImage "-Dnative -Dgraalvm.static=--static"} \
      ${optionalString (!doCheck) "-DskipTests"}

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

    install -Dm444 -t $out/share/doc/${pname} *.md
    install -Dm555 \
      org.eclipse.lemminx/target/lemminx-linux-amd64-${version} \
      $out/bin/lemminx

    runHook postInstall
  '';

  doCheck = false;

  meta = with lib; {
    description = "XML Language Server";
    broken = true;
  };
}
