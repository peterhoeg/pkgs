#!/usr/bin/env bash

# Calibre Breeze Icon Theme Build Script
# Requires calibre, breeze-icons, optipng and librsvg

# From: https://github.com/fleger/calibre-breeze-icon-theme

# With further adjustments by Peter Hoeg.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -eEuo pipefail

# MAPPINGS="$1" # resholve complains if we don't use $1 directly
CALIBRE_RESOURCES_PATH="$2"
DARK_ICONS="$3"
LIGHT_ICONS="$4"
OUTPUT_DIR="$5"

ICON_EXT=.svg

CALIBRE_FMT=png
CALIBRE_EXT=.${CALIBRE_FMT}
CALIBRE_ICON_DPI=96
CALIBRE_ICON_SIZE=128

THEMED_ICON_REGEXP=".+-for-(dark|light)-theme"

# breeze icon mappins but might work with other FDo complient icon themes
source "$1"

_convert() {
    source="$1"
    target="$OUTPUT_DIR/$2${CALIBRE_EXT}"

    rsvg-convert \
        -d $CALIBRE_ICON_DPI \
        -p $CALIBRE_ICON_DPI \
        -w $CALIBRE_ICON_SIZE \
        -h $CALIBRE_ICON_SIZE \
        -f $CALIBRE_FMT \
        -o "$target" \
        "$source"
    optipng "$target" 2>/dev/null
}

shopt -s globstar
total=0
replaced=0

# check that files we have requested mappings for are still in use by calibre
for k in "${!mappings[@]}"; do
    file="${CALIBRE_RESOURCES_PATH}/images/${k}${CALIBRE_EXT}"
    f1="${CALIBRE_RESOURCES_PATH}/images/${k}-for-dark-theme${CALIBRE_EXT}"
    f2="${CALIBRE_RESOURCES_PATH}/images/${k}-for-light-theme${CALIBRE_EXT}"
    found=0

    if [[ $file =~ $THEMED_ICON_REGEXP ]]; then
        echo "Warning: You should not map the color scheme specific file: ${k}"
        continue
    elif [ -e "$file" ] || [ -e "$f1" ] || [ -e "$f2" ]; then
        found=1
    fi

    if [ $found -eq 0 ]; then
        echo "Warning: Unused mapping found: ${k} ($file)"
    fi
done

for i in "${CALIBRE_RESOURCES_PATH}/images/"**/*"${CALIBRE_EXT}"; do
    if [[ $i =~ $THEMED_ICON_REGEXP ]]; then
        i="${i/-for-dark-theme/}"
        i="${i/-for-light-theme/}"
    fi
    # we are miscounting if we have more than one of the following for a given icon: icon, icon-for-dark-theme, icon-for-light-theme
    total=$((total + 1))
    i="${i#"${CALIBRE_RESOURCES_PATH}/images/"}"
    i="${i%"${CALIBRE_EXT}"}"
    c="${mappings["$i"]-}"
    if [[ -n $c ]]; then
        f1s="${DARK_ICONS}/${c}-symbolic${ICON_EXT}"
        f1="${DARK_ICONS}/${c}${ICON_EXT}"
        f2s="${LIGHT_ICONS}/${c}-symbolic${ICON_EXT}"
        f2="${LIGHT_ICONS}/${c}${ICON_EXT}"
        if [ -e "$f1s" ]; then
            f1="$f1s"
        fi
        if [ -e "$f2s" ]; then
            f2="$f2s"
        fi
        if [ ! -e "$f1" ] || [ ! -e "$f2" ]; then
            echo "Error: Missing overriden icon: $c"
            exit 1
        fi
    else
        f1="${DARK_ICONS}/${i}${ICON_EXT}"
        f2="${LIGHT_ICONS}/${i}${ICON_EXT}"
    fi
    if [ -f "$f1" ] && [ -f "$f2" ]; then
        install -dm755 "$OUTPUT_DIR/$(dirname "$i")"
        _convert "$f1" "$i-for-dark-theme"
        _convert "$f2" "$i-for-light-theme"
        replaced=$((replaced + 1))
    else
        echo "Unmapped icon: $i"
    fi
done

echo "Replaced icons: $replaced/$total"
