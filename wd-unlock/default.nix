{
  lib,
  resholve,
  bash,
  python3,
  fetchFromGitHub,
  writeShellScript,
  coreutils,
  sg3_utils,
}:

let
  # works with both 2 and 3
  pypkgs = python3.pkgs;

  py-wd-unlock = pypkgs.buildPythonApplication rec {
    pname = "py-wd-unlock";
    version = "20180928";

    src = fetchFromGitHub {
      owner = "geekhaidar";
      repo = "WD-Passport-Unlock-Linux";
      rev = "e142a09d37aa9decb0102d6a85fc83eb6d1cd322";
      hash = "sha256-tq027SkiSDaTJHWEcgvMOaATgchDTR+1ojtRBGnW9EE=";
    };

    format = "other";

    installPhase = ''
      runHook preInstall

      install -Dm555 -t $out/bin *.py
      install -Dm444 -t $out/share/doc/${pname} *.md

      runHook postInstall
    '';

    meta = with lib; {
      description = "WD Unlocker";
      license = licenses.free;
    };
  };

in
resholve.writeScriptBin "wd-unlock"
  {
    interpreter = "${lib.getBin bash}/bin/bash";
    inputs = [
      coreutils
      py-wd-unlock
      sg3_utils
    ];
    execer = [
      "cannot:${py-wd-unlock}/bin/cookpw.py"
    ];
  }
  ''
    set -eEuo pipefail

    device=''${1:-""}
    password=''${2:-""}

    _help() {
      echo Usage: $(basename "$0") DEVICE PASSWORD
      exit 1
    }

    test -n "$device" || _help
    test -b "$device" || _help
    test -n "$password" || _help

    password_file=$(mktemp)

    cookpw.py $password > $password_file

    sg_raw \
      --send=$(du --bytes $password_file | cut -f1) \
      --infile="$password_file" \
      $device \
      c1 e1 00 00 00 00 00 00 28 00

    rm -f $password_file
  ''
