{
  stdenv,
  lib,
  fetchurl,
  cmake,
  pkg-config,
  cairo,
  freeimage,
  fribidi,
  gtk2, # v6 will move to gtk3
  inkscape,
  lerc,
  libXdmcp,
  libdatrie,
  libselinux,
  libsepol,
  libsysprof-capture,
  libthai,
  libzip,
  minixml,
  pcre2,
  util-linux,
  zlib,
}:

let
  freeimage' = freeimage.overrideAttrs (old: {
    meta.knownVulnerabilities = [ ];
  });

in
stdenv.mkDerivation (finalAttrs: {
  pname = "xtrkcad";
  version = "5.3.0";

  src = fetchurl {
    url = "mirror://sourceforge/xtrkcad-fork/XTrackCad/Version%20${finalAttrs.version}/xtrkcad-source-${finalAttrs.version}GA.tar.gz";
    hash = "sha256-5jxq5Lt6mMsUk6eWU3JjZPVSAyRw+SNm5sEPqjbMc4w=";
  };

  postPatch = ''
    substituteInPlace app/wlib/gtklib/wpref.c \
      --replace-fail /usr/share $out/share

    substituteInPlace app/lib/xtrkcad.desktop \
      --replace-fail Exec=xtrkcad Exec=$out/bin/xtrkcad
  '';

  preBuild = ''
    # exit 1
  '';

  cmakeFlags = [ "-Wno-dev" ];

  env.HOME = "/tmp";

  buildInputs = [
    cairo
    freeimage'
    fribidi
    gtk2
    inkscape
    lerc
    libXdmcp
    libdatrie
    libselinux
    libsepol
    libsysprof-capture
    libthai
    libzip
    minixml
    pcre2
    util-linux
    zlib
  ];

  nativeBuildInputs = [
    cmake
    pkg-config
  ];

  postInstall = ''
    mv $out/share/xtrkcad/applications $out/share/
  '';

  meta = with lib; {
    description = "Model Train track planner";
    license = licenses.gpl2Only;
    platforms = platforms.linux;
    mainProgram = "xtrkcad";
  };
})
