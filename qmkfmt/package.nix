{
  lib,
  rustPlatform,
  fetchFromGitHub,
  withClangFormat ? true,
  clang-tools,
}:

rustPlatform.buildRustPackage rec {
  pname = "qmkfmt";
  version = "0.1.8";

  src = fetchFromGitHub {
    owner = "rcorre";
    repo = "qmkfmt";
    rev = "v" + version;
    hash = "sha256-veL73cfPaEu+fd6m0p4BDu+cz7jNktlfJNhMXQoiCpY=";
  };

  postPatch = lib.optionalString withClangFormat ''
    substituteInPlace src/main.rs \
      --replace-fail '"clang-format"' '"${lib.getExe' clang-tools "clang-format"}"'
  '';

  cargoHash = "sha256-dJJL3GQ9/vwoGOCXLZPgFN5SiKpF0sxzWRGcPbpWylQ=";

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} *.md
  '';

  meta = with lib; {
    description = "Format QMK keymap.c files";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
    mainProgram = "qmkfmt";
    inherit (src.meta) homepage;
  };
}
