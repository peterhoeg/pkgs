{
  stdenv,
  lib,
  fetchurl,
  unzip,
}:

stdenv.mkDerivation rec {
  pname = "cnc-gold-assets";
  version = "1.0";

  src = fetchurl {
    url = "https://www.bestoldgames.net/download/games/command-and-conquer-gold/command-and-conquer-gold.zip";
    sha256 = "sha256-xK2QHaN+y8jvg72OZ0LF+WMGy4dCkDlVGgtgQwOr184=";
  };

  unpackPhase = "unzip $src -d .";

  installPhase = ''
    runHook preInstall

    dir=$out/share/games/cnc-gold

    mkdir -p $dir
    pushd 'Command & Conquer Gold/Data'
    cp -r * $dir/
    rm $dir/*.{dll,exe}

    runHook postInstall
  '';

  nativeBuildInputs = [ unzip ];

  meta = with lib; {
    description = "Command & Conquer game assets";
  };
}
