{
  stdenv,
  lib,
  fetchFromGitHub,
  makeWrapper,
  alsaLib,
  libGL,
  libGLU,
  libxcb,
  libX11,
  libXext,
  libXxf86dga,
  libXxf86vm,
  vulkan-headers,
  vulkan-loader,
}:
let
  arch =
    {
      "x86_64-linux" = "x64";
      "i686-linux" = "i386";
    }
    ."${stdenv.hostPlatform.system}";

in
stdenv.mkDerivation rec {
  pname = "vkquake2";
  version = "1.5.8";

  src = fetchFromGitHub {
    owner = "kondrak";
    repo = pname;
    rev = version;
    sha256 = "sha256-c+RTLkmaCdJIup8qZ7AtuvXQkheW+6FPpCA3JmGl29w=";
  };

  sourceRoot = "source/linux";

  nativeBuildInputs = [
    makeWrapper
    vulkan-headers
  ];

  # miniaudio is not a separate buildable library - it's included in vkquake2
  # and meant to be
  buildInputs = [
    alsaLib
    libGL
    libGLU
    libxcb
    libX11
    libXext
    libXxf86dga
    libXxf86vm
    vulkan-loader
  ];

  makeFlags = [
    "release"
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin release${arch}/quake2
    install -Dm444 -t $out/bin release${arch}/*.so
    for d in baseq2 ctf ; do
      install -Dm644 -t $out/bin/$d release${arch}/$d/game${arch}.so
    done
    install -Dm444 -t $out/share/doc/${pname} ../*.{md,txt}
    install -Dm444 -t $out/share/pixmaps      ../vkQuake2.png

    runHook postInstall
  '';

  postFixup = ''
    wrapProgram $out/bin/quake2 \
      --prefix LD_LIBRARY_PATH : $out/bin:$out/lib
  '';

  enableParallelBuilding = true;

  meta = with lib; {
    description = "id Software's Quake 2 v3.21 with mission packs and Vulkan support";
    inherit (src.meta) homepage;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
  };
}
