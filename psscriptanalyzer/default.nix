{
  lib,
  dotnetCorePackages,
  buildDotnetModule,
  fetchFromGitHub,
  autoPatchelfHook,
}:

let
  sdk = dotnetCorePackages.sdk_6_0;

  # Supposed to work with our patch for sdk6, but one of the dependencies is still targetting an EOL framework
  broken = true;

in
buildDotnetModule rec {
  pname = "PSScriptAnalyzer";
  version = "1.21.0";

  src = fetchFromGitHub {
    owner = "PowerShell";
    repo = pname;
    rev = version;
    hash = "sha256-5Cxez1te31zVk8v1pW9KeKKX9VOiWE5DbGfrqUL+lws=";
  };

  patches = [ ./sdk6.patch ];

  projectFile = [ "${pname}.sln" ];
  nugetDeps = ./deps.nix;
  dotnet-sdk = sdk;

  # "-f netstandard2.0" is needed by one of the dependecies
  dotnetInstallFlags = [
    "-f net${lib.versions.major sdk.version}"
  ];

  nativeBuildInputs = [ autoPatchelfHook ];

  # buildInputs = [ stdenv.cc.cc.lib fontconfig ];

  meta = with lib; {
    mainProgram = "psscriptanalyzer"; # TODO: check this
    description = "PowerShell linter";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
    inherit broken;
  };
}
