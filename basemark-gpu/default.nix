{
  stdenv,
  lib,
  fetchFromGitHub,
  fetchurl,
  autoPatchelfHook,
  wrapQtAppsHook,
  alsaLib,
  ffmpeg,
  glib,
  gtk3,
  cairo,
  mesa,
  nspr,
  nss,
  openssl,
  vulkan-loader,
  xorg,
}:

stdenv.mkDerivation rec {
  pname = "basemark-gpu";
  version = "1.2.3";

  src = fetchurl {
    url = "https://cdn.downloads.basemark.com/BasemarkGPU-linux-x64-${version}.tar.gz";
    sha256 = "16wwbgv1zhhqvkycilgmgzvizjf158njyy6jq99dp29flfkk2x1d";
  };

  installPhase = ''
    runHook preInstall

    dir=$out/opt/basemarkgpu/bin
    mkdir -p $out/bin $dir
    cp -r * $dir
    ln -s $dir/basemark-gpu $out/bin/

    runHook postInstall
  '';

  nativeBuildInputs = [ autoPatchelfHook ];

  buildInputs = [
    alsaLib
    glib
    gtk3
    cairo
    ffmpeg
    mesa
    nspr
    nss
    openssl
    vulkan-loader
    xorg.libXdamage
  ];

  enableParallelBuilding = true;

  meta = with lib; {
    description = "Basemark GPU";
    broken = true; # deps missing
  };
}
