BUILD = @nom build -L

.PHONY: 3cx
3cx: clean
	$(BUILD) .#3cx

.PHONY: acer-acpi-s3
acer-acpi-s3: clean
	$(BUILD) .#acer-acpi-s3

.PHONY: acme-dns
acme-dns: clean
	$(BUILD) .#acme-dns

.PHONY: additional-wallpapers
additional-wallpapers: clean
	$(BUILD) .#additional-wallpapers

.PHONY: adtool
adtool: clean
	$(BUILD) .#adtool

.PHONY: age-plugin-tpm
age-plugin-tpm: clean
	$(BUILD) .#age-plugin-tpm

.PHONY: alertmanager-webhook-logger
alertmanager-webhook-logger: clean
	$(BUILD) .#alertmanager-webhook-logger

.PHONY: amber
amber: clean
	$(BUILD) .#amber

.PHONY: amd_s2idle
amd_s2idle: clean
	$(BUILD) .#amd_s2idle

.PHONY: amdvbflash
amdvbflash: clean
	$(BUILD) .#amdvbflash

.PHONY: android-otp-extractor
android-otp-extractor: clean
	$(BUILD) .#android-otp-extractor

.PHONY: api7
api7: clean
	$(BUILD) .#api7

.PHONY: arigi
arigi: clean
	$(BUILD) .#arigi

.PHONY: arkmanager
arkmanager: clean
	$(BUILD) .#arkmanager

.PHONY: astaro-mibs
astaro-mibs: clean
	$(BUILD) .#astaro-mibs

.PHONY: a_to_b
a_to_b: clean
	$(BUILD) .#a_to_b

.PHONY: awatcher
awatcher: clean
	$(BUILD) .#awatcher

.PHONY: azure-storage-explorer
azure-storage-explorer: clean
	$(BUILD) .#azure-storage-explorer

.PHONY: bacap
bacap: clean
	$(BUILD) .#bacap

.PHONY: balongflash
balongflash: clean
	$(BUILD) .#balongflash

.PHONY: balong_usbdload
balong_usbdload: clean
	$(BUILD) .#balong_usbdload

.PHONY: basemark-gpu
basemark-gpu: clean
	$(BUILD) .#basemark-gpu

.PHONY: bd_prochot
bd_prochot: clean
	$(BUILD) .#bd_prochot

.PHONY: bit
bit: clean
	$(BUILD) .#bit

.PHONY: bitbucketconverter
bitbucketconverter: clean
	$(BUILD) .#bitbucketconverter

.PHONY: bitwarden-to-pass
bitwarden-to-pass: clean
	$(BUILD) .#bitwarden-to-pass

.PHONY: bkcrack
bkcrack: clean
	$(BUILD) .#bkcrack

.PHONY: black-bean-control
black-bean-control: clean
	$(BUILD) .#black-bean-control

.PHONY: breeze-chameleon-icons
breeze-chameleon-icons: clean
	$(BUILD) .#breeze-chameleon-icons

.PHONY: broadlinkmanager
broadlinkmanager: clean
	$(BUILD) .#broadlinkmanager

.PHONY: broadlink-mqtt
broadlink-mqtt: clean
	$(BUILD) .#broadlink-mqtt

.PHONY: brother_printer_fwupd
brother_printer_fwupd: clean
	$(BUILD) .#brother_printer_fwupd

.PHONY: bsnes
bsnes: clean
	$(BUILD) .#bsnes

.PHONY: calibre-breeze-icon-theme
calibre-breeze-icon-theme: clean
	$(BUILD) .#calibre-breeze-icon-theme

.PHONY: cc2538-bsl
cc2538-bsl: clean
	$(BUILD) .#cc2538-bsl

.PHONY: ccd2cue
ccd2cue: clean
	$(BUILD) .#ccd2cue

.PHONY: cc-tool
cc-tool: clean
	$(BUILD) .#cc-tool

.PHONY: cdctl
cdctl: clean
	$(BUILD) .#cdctl

.PHONY: ceedling
ceedling: clean
	$(BUILD) .#ceedling

.PHONY: check-dell-emc
check-dell-emc: clean
	$(BUILD) .#check-dell-emc

.PHONY: check-idrac
check-idrac: clean
	$(BUILD) .#check-idrac

.PHONY: citrix-adc-metrics-exporter
citrix-adc-metrics-exporter: clean
	$(BUILD) .#citrix-adc-metrics-exporter

.PHONY: cleanScriptsHook
cleanScriptsHook: clean
	$(BUILD) .#cleanScriptsHook

.PHONY: cloudmapper
cloudmapper: clean
	$(BUILD) .#cloudmapper

.PHONY: cloudprober
cloudprober: clean
	$(BUILD) .#cloudprober

.PHONY: cloveretl
cloveretl: clean
	$(BUILD) .#cloveretl

.PHONY: cncgold-assets
cncgold-assets: clean
	$(BUILD) .#cncgold-assets

.PHONY: concourse
concourse: clean
	$(BUILD) .#concourse

.PHONY: controlvault2-nfc-enable
controlvault2-nfc-enable: clean
	$(BUILD) .#controlvault2-nfc-enable

.PHONY: cpu_features
cpu_features: clean
	$(BUILD) .#cpu_features

.PHONY: csharpier
csharpier: clean
	$(BUILD) .#csharpier

.PHONY: davmail
davmail: clean
	$(BUILD) .#davmail

.PHONY: dbxtool
dbxtool: clean
	$(BUILD) .#dbxtool

.PHONY: ddns-updater
ddns-updater: clean
	$(BUILD) .#ddns-updater

.PHONY: descent3
descent3: clean
	$(BUILD) .#descent3

.PHONY: droplet-agent
droplet-agent: clean
	$(BUILD) .#droplet-agent

.PHONY: drsprinto
drsprinto: clean
	$(BUILD) .#drsprinto

.PHONY: dune2-assets
dune2-assets: clean
	$(BUILD) .#dune2-assets

.PHONY: dunedynasty
dunedynasty: clean
	$(BUILD) .#dunedynasty

.PHONY: dynamic-wallpapers
dynamic-wallpapers: clean
	$(BUILD) .#dynamic-wallpapers

.PHONY: edid-checksum
edid-checksum: clean
	$(BUILD) .#edid-checksum

.PHONY: egt
egt: clean
	$(BUILD) .#egt

.PHONY: emacs-fancy-logos
emacs-fancy-logos: clean
	$(BUILD) .#emacs-fancy-logos

.PHONY: emacsql-sqlite
emacsql-sqlite: clean
	$(BUILD) .#emacsql-sqlite

.PHONY: email-oauth2-proxy
email-oauth2-proxy: clean
	$(BUILD) .#email-oauth2-proxy

.PHONY: espack
espack: clean
	$(BUILD) .#espack

.PHONY: espota
espota: clean
	$(BUILD) .#espota

.PHONY: espota-server
espota-server: clean
	$(BUILD) .#espota-server

.PHONY: etlegacy
etlegacy: clean
	$(BUILD) .#etlegacy

.PHONY: evdoublebind
evdoublebind: clean
	$(BUILD) .#evdoublebind

.PHONY: evilginx
evilginx: clean
	$(BUILD) .#evilginx

.PHONY: exe-info
exe-info: clean
	$(BUILD) .#exe-info

.PHONY: extract-msg
extract-msg: clean
	$(BUILD) .#extract-msg

.PHONY: extract-xiso
extract-xiso: clean
	$(BUILD) .#extract-xiso

.PHONY: fallout
fallout: clean
	$(BUILD) .#fallout

.PHONY: feed2exec
feed2exec: clean
	$(BUILD) .#feed2exec

.PHONY: ferretdb
ferretdb: clean
	$(BUILD) .#ferretdb

.PHONY: ff-containers-sort
ff-containers-sort: clean
	$(BUILD) .#ff-containers-sort

.PHONY: filter-prometheus
filter-prometheus: clean
	$(BUILD) .#filter-prometheus

.PHONY: flexlm
flexlm: clean
	$(BUILD) .#flexlm

.PHONY: flexlm_exporter
flexlm_exporter: clean
	$(BUILD) .#flexlm_exporter

.PHONY: freeotp-migrator
freeotp-migrator: clean
	$(BUILD) .#freeotp-migrator

.PHONY: freeotp-to-andotp-migrator
freeotp-to-andotp-migrator: clean
	$(BUILD) .#freeotp-to-andotp-migrator

.PHONY: ftpgrab
ftpgrab: clean
	$(BUILD) .#ftpgrab

.PHONY: fusion
fusion: clean
	$(BUILD) .#fusion

.PHONY: fzf-marks
fzf-marks: clean
	$(BUILD) .#fzf-marks

.PHONY: gang-beasts
gang-beasts: clean
	$(BUILD) .#gang-beasts

.PHONY: gauth
gauth: clean
	$(BUILD) .#gauth

.PHONY: geminabox
geminabox: clean
	$(BUILD) .#geminabox

.PHONY: geo-nft
geo-nft: clean
	$(BUILD) .#geo-nft

.PHONY: geowifi
geowifi: clean
	$(BUILD) .#geowifi

.PHONY: gio-test
gio-test: clean
	$(BUILD) .#gio-test

.PHONY: gitbatch
gitbatch: clean
	$(BUILD) .#gitbatch

.PHONY: git-bonsai
git-bonsai: clean
	$(BUILD) .#git-bonsai

.PHONY: git-hooks
git-hooks: clean
	$(BUILD) .#git-hooks

.PHONY: gk6x
gk6x: clean
	$(BUILD) .#gk6x

.PHONY: gog
gog: clean
	$(BUILD) .#gog

.PHONY: gogextract
gogextract: clean
	$(BUILD) .#gogextract

.PHONY: gopupd
gopupd: clean
	$(BUILD) .#gopupd

.PHONY: habapp
habapp: clean
	$(BUILD) .#habapp

.PHONY: hamclock
hamclock: clean
	$(BUILD) .#hamclock

.PHONY: hatch
hatch: clean
	$(BUILD) .#hatch

.PHONY: hoegdotcom
hoegdotcom: clean
	$(BUILD) .#hoegdotcom

.PHONY: hoeg-icc
hoeg-icc: clean
	$(BUILD) .#hoeg-icc

.PHONY: homer
homer: clean
	$(BUILD) .#homer

.PHONY: homie-monitor
homie-monitor: clean
	$(BUILD) .#homie-monitor

.PHONY: hp-ams
hp-ams: clean
	$(BUILD) .#hp-ams

.PHONY: hp-health
hp-health: clean
	$(BUILD) .#hp-health

.PHONY: hpssacli
hpssacli: clean
	$(BUILD) .#hpssacli

.PHONY: huginn
huginn: clean
	$(BUILD) .#huginn

.PHONY: icantbelieveitsnotvaletudo
icantbelieveitsnotvaletudo: clean
	$(BUILD) .#icantbelieveitsnotvaletudo

.PHONY: idractools
idractools: clean
	$(BUILD) .#idractools

.PHONY: ilo_exploy
ilo_exploy: clean
	$(BUILD) .#ilo_exploy

.PHONY: ilorest
ilorest: clean
	$(BUILD) .#ilorest

.PHONY: immortal
immortal: clean
	$(BUILD) .#immortal

.PHONY: infinity-engine
infinity-engine: clean
	$(BUILD) .#infinity-engine

.PHONY: ini2json
ini2json: clean
	$(BUILD) .#ini2json

.PHONY: initool
initool: clean
	$(BUILD) .#initool

.PHONY: intel-mas-tool
intel-mas-tool: clean
	$(BUILD) .#intel-mas-tool

.PHONY: ip6words
ip6words: clean
	$(BUILD) .#ip6words

.PHONY: iperf3_exporter
iperf3_exporter: clean
	$(BUILD) .#iperf3_exporter

.PHONY: ipmi_exporter
ipmi_exporter: clean
	$(BUILD) .#ipmi_exporter

.PHONY: ippsample
ippsample: clean
	$(BUILD) .#ippsample

.PHONY: isochronous
isochronous: clean
	$(BUILD) .#isochronous

.PHONY: janet-lsp
janet-lsp: clean
	$(BUILD) .#janet-lsp

.PHONY: json2xml
json2xml: clean
	$(BUILD) .#json2xml

.PHONY: karma
karma: clean
	$(BUILD) .#karma

.PHONY: kde-control-station
kde-control-station: clean
	$(BUILD) .#kde-control-station

.PHONY: kded_rotation
kded_rotation: clean
	$(BUILD) .#kded_rotation

.PHONY: kde-shader-wallpaper
kde-shader-wallpaper: clean
	$(BUILD) .#kde-shader-wallpaper

.PHONY: keen4
keen4: clean
	$(BUILD) .#keen4

.PHONY: keydbcfg
keydbcfg: clean
	$(BUILD) .#keydbcfg

.PHONY: kls
kls: clean
	$(BUILD) .#kls

.PHONY: kodi-netflix-auth
kodi-netflix-auth: clean
	$(BUILD) .#kodi-netflix-auth

.PHONY: kodi-plymouth-theme
kodi-plymouth-theme: clean
	$(BUILD) .#kodi-plymouth-theme

.PHONY: kodi-texturecache
kodi-texturecache: clean
	$(BUILD) .#kodi-texturecache

.PHONY: kpinentry
kpinentry: clean
	$(BUILD) .#kpinentry

.PHONY: krohnkite
krohnkite: clean
	$(BUILD) .#krohnkite

.PHONY: kshift
kshift: clean
	$(BUILD) .#kshift

.PHONY: lac
lac: clean
	$(BUILD) .#lac

.PHONY: lemminx
lemminx: clean
	$(BUILD) .#lemminx

.PHONY: libchdr
libchdr: clean
	$(BUILD) .#libchdr

.PHONY: librebridge
librebridge: clean
	$(BUILD) .#librebridge

.PHONY: libsigrok-udev
libsigrok-udev: clean
	$(BUILD) .#libsigrok-udev

.PHONY: llama-bsl
llama-bsl: clean
	$(BUILD) .#llama-bsl

.PHONY: llama-cpp
llama-cpp: clean
	$(BUILD) .#llama-cpp

.PHONY: ltchiptool
ltchiptool: clean
	$(BUILD) .#ltchiptool

.PHONY: luanti-addons
luanti-addons: clean
	$(BUILD) .#luanti-addons

.PHONY: lz4json
lz4json: clean
	$(BUILD) .#lz4json

.PHONY: maildir2mbox
maildir2mbox: clean
	$(BUILD) .#maildir2mbox

.PHONY: maildirwatch
maildirwatch: clean
	$(BUILD) .#maildirwatch

.PHONY: mailtest
mailtest: clean
	$(BUILD) .#mailtest

.PHONY: marten
marten: clean
	$(BUILD) .#marten

.PHONY: mdopen
mdopen: clean
	$(BUILD) .#mdopen

.PHONY: memreserver
memreserver: clean
	$(BUILD) .#memreserver

.PHONY: mimembellish
mimembellish: clean
	$(BUILD) .#mimembellish

.PHONY: minimalistic-wallpaper-collection
minimalistic-wallpaper-collection: clean
	$(BUILD) .#minimalistic-wallpaper-collection

.PHONY: mkeosimg
mkeosimg: clean
	$(BUILD) .#mkeosimg

.PHONY: motioneye
motioneye: clean
	$(BUILD) .#motioneye

.PHONY: mp3wrap
mp3wrap: clean
	$(BUILD) .#mp3wrap

.PHONY: mqtt-explorer
mqtt-explorer: clean
	$(BUILD) .#mqtt-explorer

.PHONY: mqttx
mqttx: clean
	$(BUILD) .#mqttx

.PHONY: multicast-relay
multicast-relay: clean
	$(BUILD) .#multicast-relay

.PHONY: multi-git-status
multi-git-status: clean
	$(BUILD) .#multi-git-status

.PHONY: mutt_ics
mutt_ics: clean
	$(BUILD) .#mutt_ics

.PHONY: mutt-octet-filter
mutt-octet-filter: clean
	$(BUILD) .#mutt-octet-filter

.PHONY: muttprint
muttprint: clean
	$(BUILD) .#muttprint

.PHONY: mutt-scripts
mutt-scripts: clean
	$(BUILD) .#mutt-scripts

.PHONY: mutt-vcard-filter
mutt-vcard-filter: clean
	$(BUILD) .#mutt-vcard-filter

.PHONY: mycroft-core
mycroft-core: clean
	$(BUILD) .#mycroft-core

.PHONY: naemon
naemon: clean
	$(BUILD) .#naemon

.PHONY: nearinfinity
nearinfinity: clean
	$(BUILD) .#nearinfinity

.PHONY: niet
niet: clean
	$(BUILD) .#niet

.PHONY: nixos-shell
nixos-shell: clean
	$(BUILD) .#nixos-shell

.PHONY: nix-upgrade-scripts
nix-upgrade-scripts: clean
	$(BUILD) .#nix-upgrade-scripts

.PHONY: non-free
non-free: clean
	$(BUILD) .#non-free

.PHONY: notify-send-py
notify-send-py: clean
	$(BUILD) .#notify-send-py

.PHONY: notify-send-sh
notify-send-sh: clean
	$(BUILD) .#notify-send-sh

.PHONY: nuget2nix
nuget2nix: clean
	$(BUILD) .#nuget2nix

.PHONY: nyquist
nyquist: clean
	$(BUILD) .#nyquist

.PHONY: oauth2l
oauth2l: clean
	$(BUILD) .#oauth2l

.PHONY: oda-file-converter
oda-file-converter: clean
	$(BUILD) .#oda-file-converter

.PHONY: odf2txt
odf2txt: clean
	$(BUILD) .#odf2txt

.PHONY: offlinemsmtp
offlinemsmtp: clean
	$(BUILD) .#offlinemsmtp

.PHONY: ogmrip
ogmrip: clean
	$(BUILD) .#ogmrip

.PHONY: oh-brother
oh-brother: clean
	$(BUILD) .#oh-brother

.PHONY: onvifviewer
onvifviewer: clean
	$(BUILD) .#onvifviewer

.PHONY: opencv-face-recognition
opencv-face-recognition: clean
	$(BUILD) .#opencv-face-recognition

.PHONY: openfortivpn-webview
openfortivpn-webview: clean
	$(BUILD) .#openfortivpn-webview

.PHONY: openmw-git
openmw-git: clean
	$(BUILD) .#openmw-git

.PHONY: open-plc-utils
open-plc-utils: clean
	$(BUILD) .#open-plc-utils

.PHONY: openssl
openssl: clean
	$(BUILD) .#openssl

.PHONY: ops
ops: clean
	$(BUILD) .#ops

.PHONY: osticket
osticket: clean
	$(BUILD) .#osticket

.PHONY: pandoc-templates
pandoc-templates: clean
	$(BUILD) .#pandoc-templates

.PHONY: paperjam
paperjam: clean
	$(BUILD) .#paperjam

.PHONY: paq9a
paq9a: clean
	$(BUILD) .#paq9a

.PHONY: perccli
perccli: clean
	$(BUILD) .#perccli

.PHONY: pgbadger
pgbadger: clean
	$(BUILD) .#pgbadger

.PHONY: pg_catcheck
pg_catcheck: clean
	$(BUILD) .#pg_catcheck

.PHONY: pixelserv
pixelserv: clean
	$(BUILD) .#pixelserv

.PHONY: plasma-applet-ddcci
plasma-applet-ddcci: clean
	$(BUILD) .#plasma-applet-ddcci

.PHONY: plasma-applet-focus
plasma-applet-focus: clean
	$(BUILD) .#plasma-applet-focus

.PHONY: plasma-theme-switcher
plasma-theme-switcher: clean
	$(BUILD) .#plasma-theme-switcher

.PHONY: playground
playground: clean
	$(BUILD) .#playground

.PHONY: polonium-git
polonium-git: clean
	$(BUILD) .#polonium-git

.PHONY: postgres-checkup
postgres-checkup: clean
	$(BUILD) .#postgres-checkup

.PHONY: postgresqltuner
postgresqltuner: clean
	$(BUILD) .#postgresqltuner

.PHONY: praeda
praeda: clean
	$(BUILD) .#praeda

.PHONY: prename
prename: clean
	$(BUILD) .#prename

.PHONY: pret
pret: clean
	$(BUILD) .#pret

.PHONY: procmon
procmon: clean
	$(BUILD) .#procmon

.PHONY: projectlibre
projectlibre: clean
	$(BUILD) .#projectlibre

.PHONY: prometheus-msteams
prometheus-msteams: clean
	$(BUILD) .#prometheus-msteams

.PHONY: pscoverdl
pscoverdl: clean
	$(BUILD) .#pscoverdl

.PHONY: psscriptanalyzer
psscriptanalyzer: clean
	$(BUILD) .#psscriptanalyzer

.PHONY: psx-bios
psx-bios: clean
	$(BUILD) .#psx-bios

.PHONY: pushover-cli
pushover-cli: clean
	$(BUILD) .#pushover-cli

.PHONY: pushprox
pushprox: clean
	$(BUILD) .#pushprox

.PHONY: pykmip
pykmip: clean
	$(BUILD) .#pykmip

.PHONY: pykodi-cli
pykodi-cli: clean
	$(BUILD) .#pykodi-cli

.PHONY: pymedusa
pymedusa: clean
	$(BUILD) .#pymedusa

.PHONY: python-evtx
python-evtx: clean
	$(BUILD) .#python-evtx

.PHONY: python-onvif
python-onvif: clean
	$(BUILD) .#python-onvif

.PHONY: python-onvif-zeep
python-onvif-zeep: clean
	$(BUILD) .#python-onvif-zeep

.PHONY: qmkfmt
qmkfmt: clean
	$(BUILD) .#qmkfmt

.PHONY: qomui
qomui: clean
	$(BUILD) .#qomui

.PHONY: quickemu
quickemu: clean
	$(BUILD) .#quickemu

.PHONY: rancid
rancid: clean
	$(BUILD) .#rancid

.PHONY: re3
re3: clean
	$(BUILD) .#re3

.PHONY: redka
redka: clean
	$(BUILD) .#redka

.PHONY: repostat
repostat: clean
	$(BUILD) .#repostat

.PHONY: resholve-test
resholve-test: clean
	$(BUILD) .#resholve-test

.PHONY: rfetch
rfetch: clean
	$(BUILD) .#rfetch

.PHONY: roller-coaster-tycoon-collection
roller-coaster-tycoon-collection: clean
	$(BUILD) .#roller-coaster-tycoon-collection

.PHONY: ros
ros: clean
	$(BUILD) .#ros

.PHONY: rss2maildir
rss2maildir: clean
	$(BUILD) .#rss2maildir

.PHONY: s0ixselftesttool
s0ixselftesttool: clean
	$(BUILD) .#s0ixselftesttool

.PHONY: s25client
s25client: clean
	$(BUILD) .#s25client

.PHONY: salt
salt: clean
	$(BUILD) .#salt

.PHONY: samsung-magician
samsung-magician: clean
	$(BUILD) .#samsung-magician

.PHONY: samsung-toolkit
samsung-toolkit: clean
	$(BUILD) .#samsung-toolkit

.PHONY: san-francisco-fonts
san-francisco-fonts: clean
	$(BUILD) .#san-francisco-fonts

.PHONY: sasl2-oauth
sasl2-oauth: clean
	$(BUILD) .#sasl2-oauth

.PHONY: script_exporter
script_exporter: clean
	$(BUILD) .#script_exporter

.PHONY: scummvm-games
scummvm-games: clean
	$(BUILD) .#scummvm-games

.PHONY: sdcv
sdcv: clean
	$(BUILD) .#sdcv

.PHONY: sdl2-gamepad-mapper
sdl2-gamepad-mapper: clean
	$(BUILD) .#sdl2-gamepad-mapper

.PHONY: semantic-prompt
semantic-prompt: clean
	$(BUILD) .#semantic-prompt

.PHONY: sensu-checks-crystal
sensu-checks-crystal: clean
	$(BUILD) .#sensu-checks-crystal

.PHONY: sensu-checks-shell
sensu-checks-shell: clean
	$(BUILD) .#sensu-checks-shell

.PHONY: set_conf
set_conf: clean
	$(BUILD) .#set_conf

.PHONY: sfarkxtc
sfarkxtc: clean
	$(BUILD) .#sfarkxtc

.PHONY: sgx-software-enable
sgx-software-enable: clean
	$(BUILD) .#sgx-software-enable

.PHONY: shadow-warrior
shadow-warrior: clean
	$(BUILD) .#shadow-warrior

.PHONY: shyaml
shyaml: clean
	$(BUILD) .#shyaml

.PHONY: smartmon
smartmon: clean
	$(BUILD) .#smartmon

.PHONY: smhasher
smhasher: clean
	$(BUILD) .#smhasher

.PHONY: sparrow-wifi
sparrow-wifi: clean
	$(BUILD) .#sparrow-wifi

.PHONY: sping
sping: clean
	$(BUILD) .#sping

.PHONY: splashtop
splashtop: clean
	$(BUILD) .#splashtop

.PHONY: sql-relay
sql-relay: clean
	$(BUILD) .#sql-relay

.PHONY: ssacli
ssacli: clean
	$(BUILD) .#ssacli

.PHONY: stable-diffusion-cpp
stable-diffusion-cpp: clean
	$(BUILD) .#stable-diffusion-cpp

.PHONY: standardrb
standardrb: clean
	$(BUILD) .#standardrb

.PHONY: start
start: clean
	$(BUILD) .#start

.PHONY: star-trader
star-trader: clean
	$(BUILD) .#star-trader

.PHONY: stoml
stoml: clean
	$(BUILD) .#stoml

.PHONY: story_branch
story_branch: clean
	$(BUILD) .#story_branch

.PHONY: suitecrm
suitecrm: clean
	$(BUILD) .#suitecrm

.PHONY: synapse-purge
synapse-purge: clean
	$(BUILD) .#synapse-purge

.PHONY: syncthing_exporter
syncthing_exporter: clean
	$(BUILD) .#syncthing_exporter

.PHONY: sys2mqtt
sys2mqtt: clean
	$(BUILD) .#sys2mqtt

.PHONY: sysit
sysit: clean
	$(BUILD) .#sysit

.PHONY: systemd-swap
systemd-swap: clean
	$(BUILD) .#systemd-swap

.PHONY: teensy-loader-udev
teensy-loader-udev: clean
	$(BUILD) .#teensy-loader-udev

.PHONY: test
test: clean
	$(BUILD) .#test

.PHONY: toml-merge
toml-merge: clean
	$(BUILD) .#toml-merge

.PHONY: toolbox
toolbox: clean
	$(BUILD) .#toolbox

.PHONY: totp-cli
totp-cli: clean
	$(BUILD) .#totp-cli

.PHONY: traykeys
traykeys: clean
	$(BUILD) .#traykeys

.PHONY: uavs3
uavs3: clean
	$(BUILD) .#uavs3

.PHONY: ubports-installer
ubports-installer: clean
	$(BUILD) .#ubports-installer

.PHONY: uefi-shell
uefi-shell: clean
	$(BUILD) .#uefi-shell

.PHONY: umskt-rs
umskt-rs: clean
	$(BUILD) .#umskt-rs

.PHONY: unarc
unarc: clean
	$(BUILD) .#unarc

.PHONY: unicode
unicode: clean
	$(BUILD) .#unicode

.PHONY: unpoller
unpoller: clean
	$(BUILD) .#unpoller

.PHONY: uresourced
uresourced: clean
	$(BUILD) .#uresourced

.PHONY: usbasp-udev
usbasp-udev: clean
	$(BUILD) .#usbasp-udev

.PHONY: usbctl
usbctl: clean
	$(BUILD) .#usbctl

.PHONY: vga2usb
vga2usb: clean
	$(BUILD) .#vga2usb

.PHONY: virt-v2v
virt-v2v: clean
	$(BUILD) .#virt-v2v

.PHONY: vkquake2
vkquake2: clean
	$(BUILD) .#vkquake2

.PHONY: vmware_exporter
vmware_exporter: clean
	$(BUILD) .#vmware_exporter

.PHONY: vvvvvv
vvvvvv: clean
	$(BUILD) .#vvvvvv

.PHONY: wait-for-dns
wait-for-dns: clean
	$(BUILD) .#wait-for-dns

.PHONY: warble
warble: clean
	$(BUILD) .#warble

.PHONY: wd-unlock
wd-unlock: clean
	$(BUILD) .#wd-unlock

.PHONY: webster
webster: clean
	$(BUILD) .#webster

.PHONY: whatmask
whatmask: clean
	$(BUILD) .#whatmask

.PHONY: wordle
wordle: clean
	$(BUILD) .#wordle

.PHONY: wtwitch
wtwitch: clean
	$(BUILD) .#wtwitch

.PHONY: wxedid
wxedid: clean
	$(BUILD) .#wxedid

.PHONY: xash3d
xash3d: clean
	$(BUILD) .#xash3d

.PHONY: xbows-driver
xbows-driver: clean
	$(BUILD) .#xbows-driver

.PHONY: xdg-mr
xdg-mr: clean
	$(BUILD) .#xdg-mr

.PHONY: xdpi
xdpi: clean
	$(BUILD) .#xdpi

.PHONY: xdpiqt
xdpiqt: clean
	$(BUILD) .#xdpiqt

.PHONY: xroach
xroach: clean
	$(BUILD) .#xroach

.PHONY: xtrkcad
xtrkcad: clean
	$(BUILD) .#xtrkcad

.PHONY: yaml-merge
yaml-merge: clean
	$(BUILD) .#yaml-merge

.PHONY: zigpy-cli
zigpy-cli: clean
	$(BUILD) .#zigpy-cli

.PHONY: zram-generator
zram-generator: clean
	$(BUILD) .#zram-generator

.PHONY: zsh-history-filter
zsh-history-filter: clean
	$(BUILD) .#zsh-history-filter

.PHONY: all
all: 3cx acer-acpi-s3 acme-dns additional-wallpapers adtool age-plugin-tpm alertmanager-webhook-logger amber amd_s2idle amdvbflash android-otp-extractor api7 arigi arkmanager astaro-mibs a_to_b awatcher azure-storage-explorer bacap balongflash balong_usbdload basemark-gpu bd_prochot bit bitbucketconverter bitwarden-to-pass bkcrack black-bean-control breeze-chameleon-icons broadlinkmanager broadlink-mqtt brother_printer_fwupd bsnes calibre-breeze-icon-theme cc2538-bsl ccd2cue cc-tool cdctl ceedling check-dell-emc check-idrac citrix-adc-metrics-exporter cleanScriptsHook cloudmapper cloudprober cloveretl cncgold-assets concourse controlvault2-nfc-enable cpu_features csharpier davmail dbxtool ddns-updater descent3 droplet-agent drsprinto dune2-assets dunedynasty dynamic-wallpapers edid-checksum egt emacs-fancy-logos emacsql-sqlite email-oauth2-proxy espack espota espota-server etlegacy evdoublebind evilginx exe-info extract-msg extract-xiso fallout feed2exec ferretdb ff-containers-sort filter-prometheus flexlm flexlm_exporter freeotp-migrator freeotp-to-andotp-migrator ftpgrab fusion fzf-marks gang-beasts gauth geminabox geo-nft geowifi gio-test gitbatch git-bonsai git-hooks gk6x gog gogextract gopupd habapp hamclock hatch hoegdotcom hoeg-icc homer homie-monitor hp-ams hp-health hpssacli huginn icantbelieveitsnotvaletudo idractools ilo_exploy ilorest immortal infinity-engine ini2json initool intel-mas-tool ip6words iperf3_exporter ipmi_exporter ippsample isochronous janet-lsp json2xml karma kde-control-station kded_rotation kde-shader-wallpaper keen4 keydbcfg kls kodi-netflix-auth kodi-plymouth-theme kodi-texturecache kpinentry krohnkite kshift lac lemminx libchdr librebridge libsigrok-udev llama-bsl llama-cpp ltchiptool luanti-addons lz4json maildir2mbox maildirwatch mailtest marten mdopen memreserver mimembellish minimalistic-wallpaper-collection mkeosimg motioneye mp3wrap mqtt-explorer mqttx multicast-relay multi-git-status mutt_ics mutt-octet-filter muttprint mutt-scripts mutt-vcard-filter mycroft-core naemon nearinfinity niet nixos-shell nix-upgrade-scripts non-free notify-send-py notify-send-sh nuget2nix nyquist oauth2l oda-file-converter odf2txt offlinemsmtp ogmrip oh-brother onvifviewer opencv-face-recognition openfortivpn-webview openmw-git open-plc-utils openssl ops osticket pandoc-templates paperjam paq9a perccli pgbadger pg_catcheck pixelserv plasma-applet-ddcci plasma-applet-focus plasma-theme-switcher playground polonium-git postgres-checkup postgresqltuner praeda prename pret procmon projectlibre prometheus-msteams pscoverdl psscriptanalyzer psx-bios pushover-cli pushprox pykmip pykodi-cli pymedusa python-evtx python-onvif python-onvif-zeep qmkfmt qomui quickemu rancid re3 redka repostat resholve-test rfetch roller-coaster-tycoon-collection ros rss2maildir s0ixselftesttool s25client salt samsung-magician samsung-toolkit san-francisco-fonts sasl2-oauth script_exporter scummvm-games sdcv sdl2-gamepad-mapper semantic-prompt sensu-checks-crystal sensu-checks-shell set_conf sfarkxtc sgx-software-enable shadow-warrior shyaml smartmon smhasher sparrow-wifi sping splashtop sql-relay ssacli stable-diffusion-cpp standardrb start star-trader stoml story_branch suitecrm synapse-purge syncthing_exporter sys2mqtt sysit systemd-swap teensy-loader-udev test toml-merge toolbox totp-cli traykeys uavs3 ubports-installer uefi-shell umskt-rs unarc unicode unpoller uresourced usbasp-udev usbctl vga2usb virt-v2v vkquake2 vmware_exporter vvvvvv wait-for-dns warble wd-unlock webster whatmask wordle wtwitch wxedid xash3d xbows-driver xdg-mr xdpi xdpiqt xroach xtrkcad yaml-merge zigpy-cli zram-generator zsh-history-filter
