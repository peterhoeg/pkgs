{
  lib,
  fetchFromGitHub,
  perlPackages,
}:

perlPackages.buildPerlPackage rec {
  pname = "postgresqltuner";
  version = "1.0.1.2021-02-09";

  src = fetchFromGitHub {
    owner = "jfcoz";
    repo = pname;
    # rev = "${version}";
    rev = "288bce8ff4ffedfbc454163924132f36a9401332";
    sha256 = "sha256-xPt4FWtXHlEasC4KKInNLbuOIJKns0MLfzVEgvfGlqQ=";
  };

  # we need the patching that happens in the perl builder preConfigureHook but
  # nothing else from there so create a dummy file so it doesn't blow up
  postPatch = ''
    touch Makefile.PL

    substituteInPlace postgresqltuner.pl \
      --replace-fail /var/run /run
  '';

  buildInputs = with perlPackages; [
    DBDPg
    DBI
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 postgresqltuner.pl $out/bin/${pname}
    install -Dm444 -t $out/share/doc/${pname} READ*.md

    runHook postInstall
  '';

  # no checks
  doCheck = false;

  outputs = [ "out" ];

  meta = with lib; {
    description = "Analyse your PostgreSQL database configuration, and give tuning advice";
    homepage = "https://postgresqltuner.pl";
    license = licenses.gpl3;
  };
}
