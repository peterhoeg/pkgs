{
  stdenv,
  lib,
  fetchFromGitHub,
  writeShellScriptBin,
  janet,
  jpm,
}:

let
  version = "0.0.8";

  git' = writeShellScriptBin "git" ''
    echo "${version}"
  '';

  deps = {
    cmd = {
      src = fetchFromGitHub {
        owner = "CFiggers";
        repo = "cmd";
        rev = "5b9d97d404733dfc32b35c7683940f7031de4bf1";
        hash = "sha256-N4ct9H/0sx//2ml/b4HaY57FF7Dit1ogD0NuRbXmu04=";
      };
    };
    jayson = {
      src = fetchFromGitHub {
        owner = "CFiggers";
        repo = "jayson";
        rev = "v0.0.1";
        hash = "sha256-90N6Vce75KAf+wNh3bZzsfhnW8uGoyphHe7fd5Il5H4=";
      };
    };
    judge = {
      src = fetchFromGitHub {
        owner = "ianthehenry";
        repo = "judge";
        rev = "v2.9.0";
        hash = "sha256-1zvr1WsllZFeKPb4EgdvTpp67lo1T/M8dEXGNHzOHj0=";
      };
    };
    spork = {
      dir = "spork";
      src = fetchFromGitHub {
        owner = "janet-lang";
        repo = "spork";
        rev = "4c77afc17eb5447a1ae06241478afe11f7db607d";
        hash = "sha256-nYSCWX262nhWn9hfd+kqnkH8ydN+DYg/XbCmGkozYZM=";
      };
    };
  };

in
stdenv.mkDerivation (finalAttrs: {
  pname = "janet-lsp";
  inherit version;

  src = fetchFromGitHub {
    owner = "CFiggers";
    repo = "janet-lsp";
    rev = "v" + finalAttrs.version;
    hash = "sha256-o6CBHa5aHjHGcLOOqExgSdDWYOTkV9dxXcU2Nyv1kaI=";
  };

  patches = [ ];

  postPatch = ''
    substituteInPlace project.janet \
      --replace-fail 0.0.6 ${version}

    substituteInPlace src/main.janet \
      --replace-fail 0.0.7 ${version}
  '';

  nativeBuildInputs = [
    git'
    janet
    jpm
  ];

  dontConfigure = true;

  JANET_BINPATH = "./bin";
  JANET_MODPATH = "./lib";
  JANET_LIBPATH = "${lib.getLib janet}/lib";

  buildPhase = ''
    runHook preBuild

    mkdir -p lib
    ${lib.concatLines (
      lib.mapAttrsToList (k: v: ''
        ln -s ${v.src}/${v.dir or "src"} lib/${k}
      '') deps
    )}
    jpm build

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

    mkdir -p $out/bin
    cat >$out/bin/janet-lsp <<_EOF
    #!${lib.getExe janet}
    (put root-env :syspath "${placeholder "out"}/libexec")
    (import janet-lsp)
    # Reset the syspath after overriding
    (put root-env :syspath (os/getenv "JANET_PATH" (dyn :syspath)))
    (defn main [& args]
      (janet-lsp/main ;args))
    _EOF
    chmod 555 $out/bin/*

    install -Dm444 -t $out/libexec build/janet-lsp.jimage

    runHook postInstall
  '';

  meta = with lib; {
    description = "LSP server for Janet";
    mainProgram = "janet-lsp";
  };
})
