{
  lib,
  stdenv,
  dotnetCorePackages,
  buildDotnetModule,
  fetchFromGitHub,
  autoPatchelfHook,
}:

let
  sdk = dotnetCorePackages.sdk_6_0;
  # sdk = dotnetCorePackages.sdk_7_0;

in
buildDotnetModule rec {
  pname = "gk6x";
  version = "1.21";

  src = fetchFromGitHub {
    owner = "pixeltris";
    repo = "GK6X";
    rev = "GK6X-v" + version;
    hash = "sha256-sBtg54QojlGZYAOhQlcm4Qu8pN86jl7U7LGDk70rbeE=";
  };

  projectFile = [ "GK6X.sln" ];
  nugetDeps = ./deps.nix;
  dotnet-sdk = sdk;

  nativeBuildInputs = [ autoPatchelfHook ];

  buildInputs = [ stdenv.cc.cc.lib ];

  runtimeDeps = [ ];

  meta = with lib; {
    mainProgram = "gk6x";
    description = "Configure keyboards based on gk6x";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    broken = true;
  };
}
