{
  stdenv,
  lib,
  fetchurl,
  symlinkJoin,
  cmake,
  gettext,
  makeWrapper,
  makeBinaryWrapper,
  pkg-config,
  boost,
  bzip2,
  lua5_3,
  miniupnpc,
  openssl,
  libsamplerate,
  SDL2,
  SDL2_mixer,
  zlib,
  gameData ? null,
}:

# TODO: need to set RTTR_GAMEDIR in the wrapper

let
  version = "0.9.5";

  wrapper = makeWrapper; # change to makeBinaryWrapper when it's working

  SDL' = SDL2.override { withStatic = true; };
  SDL_mixer' = SDL2_mixer.override { SDL2 = SDL'; };

  unwrapped = stdenv.mkDerivation rec {
    pname = "s25rttr-unwrapped";
    inherit version;

    # this is an all-in source package
    src = fetchurl {
      url = "https://github.com/Return-To-The-Roots/s25client/releases/download/v${version}/s25client_src_v${version}.tar.gz";
      hash = "sha256-xqnvW5CUO18ugVQ/HjKQ/3c2Y6Reu7zFp4a7X1SV++w=";
    };

    cmakeFlags = [
      "-DRTTR_VERSION=${version}"
      "-DRTTR_USE_SYSTEM_LIBS=ON"
      "-DFETCHCONTENT_FULLY_DISCONNECTED=ON"
      "-DBUILD_TESTING=OFF"
    ];

    env.NIX_CFLAGS_COMPILE = "-Wno-error";

    buildInputs = [
      boost
      bzip2
      lua5_3
      miniupnpc
      openssl
      libsamplerate
      SDL'
      SDL2_mixer
      zlib
    ];

    nativeBuildInputs = [
      cmake
      gettext
      pkg-config
    ];

    meta = with lib; {
      description = "The Settlers II";
      license = licenses.free;
      maintainers = with maintainers; [ peterhoeg ];
      platforms = platforms.all;
    };
  };

in
symlinkJoin {
  name = "s25rttr-${version}";
  paths = [
    unwrapped
    gameData
  ];
  nativeBuildInputs = [ wrapper ];
  postBuild = ''
    mkdir -p $out/bin
    makeWrapper ${unwrapped}/bin/s25client $out/bin/s25rttr
  '';
}
