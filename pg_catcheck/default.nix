{
  stdenv,
  lib,
  fetchFromGitHub,
  krb5,
  openssl,
  postgresql,
  readline,
  zlib,
}:

stdenv.mkDerivation rec {
  pname = "pg_catcheck";
  version = "1.3.0";

  src = fetchFromGitHub {
    owner = "EnterpriseDB";
    repo = pname;
    rev = "${version}";
    hash = "sha256-VD9vPjOqyOBg9xLUxxZXjpn8foiS4VQn6kxaBRBxTOE=";
  };

  buildInputs = [
    krb5
    postgresql
    openssl
    readline
    zlib
  ];

  enableParallelBuilding = true;

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin ${pname}
    install -Dm444 -t $out/share/doc/${pname} README* LICENSE

    runHook postInstall
  '';

  meta = with lib; {
    description = "Analyse your PostgreSQL database configuration, and give tuning advice";
    homepage = "https://postgresqltuner.pl";
    license = licenses.gpl3;
  };
}
