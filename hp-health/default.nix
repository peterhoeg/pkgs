{
  stdenv,
  lib,
  fetchurl,
  autoPatchelfHook,
  dpkg,
  formats,
}:

let
  asr = (formats.ini { }).generate "hp-asr.service" {
    Unit = {
      Description = "HP Advanced Server Recovery Daemon";
    };

    Service = {
      Type = "exec";
      ExecStart = "@out@/bin/hp-asrd -p 1";
      Restart = "on-failure";
    };
  };

  health = (formats.ini { }).generate "hp-health.service" {
    Unit = {
      Description = "HP Proliant High Performance IPMI based System Health Monitor";
    };

    Service = {
      Type = "exec";
      ExecStart = "@out@/bin/hpasmxld -f /dev/hpilo";
      Restart = "on-failure";
    };
  };

in

stdenv.mkDerivation rec {
  pname = "hp-health";
  version = "10.80-1874.10";

  src = fetchurl {
    url = "https://downloads.linux.hpe.com/SDR/repo/mcp/pool/non-free/hp-health_${version}_amd64.deb";
    sha256 = "sha256-hrE5SZlYHTi2yG3rvZCDsFPRozfx2Q0O1LwuxNbLGck=";
  };

  unpackPhase = ''
    runHook preUnpack

    dpkg -x ${src} .

    runHook postUnpack
  '';

  dontBuild = true;

  installPhase = ''
    runHook preInstall

    mkdir -p $out/lib/systemd/system

    cp -r sbin usr/lib usr/share/* $out

    cp -r opt/hp/hp-health/addons/* $out/lib
    cp -r opt/hp/hp-health/bin $out/

    chmod +x $out/lib/*.so.*

    ${lib.concatMapStringsSep "\n"
      (e: ''
        substitute ${e} $out/lib/systemd/system/${e.name} --subst-var out
      '')
      [
        asr
        health
      ]
    }

    runHook postInstall
  '';

  buildInputs = [ stdenv.cc.cc.lib ];

  nativeBuildInputs = [
    autoPatchelfHook
    dpkg
  ];

  meta = with lib; {
    description = "HPE System Health Application and Command line Utilities (Gen9 and earlier)";
    license = licenses.unfree;
  };
}
