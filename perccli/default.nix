{
  stdenv,
  lib,
  requireFile,
  autoPatchelfHook,
  dpkg,
}:

let
  suffix = "A11";
  fileSuffix = "00.0000";

in
stdenv.mkDerivation rec {
  pname = "perccli";
  version = "7.1623.00";

  src = requireFile rec {
    name = "PERCCLI_${version}_${suffix}_Linux.tar.gz";
    sha256 = "0yryyhjii4hn53dfndwi81q5nnwfj4j4c2n1ra5jsxsyyppbnc84";

    message = ''
      In order to use PERCCLI you need to download the binaries, .tar.gz from:

      https://www.dell.com/support/home/en-sg/drivers/driversdetails?driverid=j91yg&oscode=us008&productcode=poweredge-r740

      Once you have downloaded the file, please use the following command and re-run the
      installation:

      nix-prefetch-url file://\$PWD/${name}
    '';
  };

  sourceRoot = ".";

  installPhase = ''
    runHook preInstall

    dpkg -x perccli_00${version}${fileSuffix}_all.deb .

    install -Dm555 opt/MegaRAID/perccli/perccli64 $out/bin/perccli

    mkdir -p $out/nix-support
    echo $src > $out/nix-support/_dependencies.txt

    runHook postInstall
  '';

  buildInputs = [ stdenv.cc.cc.lib ];

  nativeBuildInputs = [
    autoPatchelfHook
    dpkg
  ];

  meta = with lib; {
    description = "Dell PERCCLI / MegaRAID";
    license = licenses.unfree;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
    hydraPlatforms = platforms.none;
  };
}
