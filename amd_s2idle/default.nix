{
  lib,
  fetchFromGitLab,
  gobject-introspection,
  python3Packages,
  acpica-tools,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication {
  pname = "amd_s2idle";
  version = "2024-06-04";

  format = "other";

  disabled = !pypkgs.isPy3k;

  src = fetchFromGitLab {
    domain = "gitlab.freedesktop.org";
    owner = "drm";
    repo = "amd";
    rev = "eea1eb2d21b0433aed222f82a5159fb260adff90";
    hash = "sha256-MtHY48lsfM+KhUpJaO2UhyezIrWGDX1kqF9vng4tHds=";
  };

  dontBuild = true;

  installPhase = ''
    runHook preInstall

    makeWrapperArgs+=(--prefix PATH : ${lib.makeBinPath [ acpica-tools ]})

    install -Dm555 scripts/amd_s2idle.py $out/bin/amd_s2idle

    runHook postInstall
  '';

  nativeBuildInputs = [ gobject-introspection ];

  propagatedBuildInputs = with pypkgs; [
    distro
    gobject-introspection
    pygobject3
    packaging
    pyudev
    systemd
  ];

  doInstallCheck = true;

  installCheckPhase = ''
    runHook preInstallCheck

    $out/bin/amd_s2idle -h

    runHook postInstallCheck
  '';

  meta = with lib; {
    description = "S0i3/s2idle analysis script for AMD systems";
    license = licenses.mit;
    mainProgram = "amd_s2idle";
  };
}
