{
  mkDerivation,
  lib,
  fetchFromGitHub,
  extra-cmake-modules,
  pkg-config,
  kconfig,
}:

mkDerivation rec {
  pname = "plasma-theme-switcher";
  version = "0.1";

  src = fetchFromGitHub {
    owner = "maldoinc";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-sdcJ6K5QmglJEDIEl4sd8x7DuCPCqMHRxdYbcToM46Q=";
  };

  buildInputs = [ kconfig ];

  nativeBuildInputs = [
    extra-cmake-modules
    pkg-config
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin plasma-theme
    install -Dm444 -t $out/share/doc/${pname} ../LICENSE ../*.md

    runHook postInstall
  '';

  meta = with lib; {
    description = "Plasma Theme Switcher";
    license = licenses.gpl2Only;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
}
