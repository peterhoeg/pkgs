{
  lib,
  fetchFromGitHub,
  python3,
}:

let
  pypkgs = python3.pkgs;

in
pypkgs.buildPythonApplication {
  pname = "kodi-cli";
  version = "0.2.3";
  pyproject = true;

  src = fetchFromGitHub {
    owner = "JavaWiz1";
    repo = "kodi-cli";
    # releases past 0.1.10 are not tagged
    rev = "009769d6933e2351f36a57d022e6200731612d3d";
    hash = "sha256-AP10N6oqd3t+VzClixBwX7l6fvTrhWB8F/1SyemeTDI=";
  };

  propagatedBuildInputs = with pypkgs; [
    loguru
    requests
  ];

  nativeBuildInputs = with pypkgs; [
    poetry-core
  ];

  meta = with lib; {
    description = "Command Line Interface for Kodi";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
    mainProgram = "kodi-cli";
    inherit (python3.meta) platforms;
  };
}
