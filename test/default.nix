{
  stdenv,
  lib,
  fetchurl,
  fetchFromGitHub,
  fetchFromGitLab,
  makeBinaryWrapper,
  makeWrapper,
  pkg-config,
  tree,
  _7zz,
  openssl,
  zlib,
}:

let
  inherit (lib) attrNames;

  # wrapper' = makeBinaryWrapper;
  wrapper' = makeWrapper;

  fetchTexturePack =
    {
      url,
      hash,
      name,
      serialFrom ? null,
      serialTo ? null,
    }:
    fetchurl {
      inherit url hash name;
      downloadToTemp = true;
      recursiveHash = true;
      postFetch = ''
        mkdir -p $out
        ${lib.getExe _7zz} -o$out x $downloadedFile
        ${lib.optionalString ((serialFrom != null) && (serialTo != null)) ''
          ln -rs $out/${serialFrom} $out/${serialTo}
        ''}
        ${lib.getExe tree} $out/
      '';
    };

  fetcher = fetchTexturePack {
    url = "https://getsamplefiles.com/download/7z/sample-1.7z";
    hash = "sha256-VYHzzGyzsDE19nONxZff6q7Pq2PwcTPzc+aWDxfqjB0=";
    name = "woot-foo.bar";
    serialFrom = "sample-1";
    serialTo = "peter";
  };

  stddrv = stdenv.mkDerivation (finalAttrs: {
    pname = "test";
    version = "1.0";

    src = fetchurl {
      url = "https://www.hoeg.com";
      hash = "sha256-y10e8OrrVJT7aZ+SGxPCey0W6sdDVPDxagNQQ0gZ78s=";
      name = "woot.foobar";
      postFetch = ''
        env | sort | grep woot
        exit 77
      '';
    };

    buildInputs = [
      openssl
      zlib
    ];

    nativeBuildInputs = [
      wrapper'
      pkg-config
    ];

    meta = with lib; {
      description = "Test";
      mainProgram = "";
      license = licenses.free;
      maintainers = with maintainers; [ peterhoeg ];
      platforms = platforms.all;
    };
  });

in
fetcher
