{
  stdenv,
  lib,
  fetchFromGitHub,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "unarc";
  version = "0-unstable-2020-05-20";

  src = fetchFromGitHub {
    owner = "xredor";
    repo = "unarc";
    rev = "adc333d6cdd76d72da254cc80d766fbbcc683c95";
    hash = "sha256-ysOei44P3K+aA+h73DuHlgwTKqQx/Xq8z+DefB6Qhcs=";
  };

  env.NIX_CFLAGS_COMPILE = "-Wno-error=format-security";

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin unarc
    install -Dm444 -t $out/share/doc/unarc *.md

    runHook postInstall
  '';

  meta = with lib; {
    description = "Unpacker for ArC archives";
    mainProgram = "unarc";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
})
