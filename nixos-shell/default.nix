{
  stdenv,
  lib,
  fetchFromGitHub,
}:

stdenv.mkDerivation {
  pname = "nixos-shell";
  version = "20190716";

  src = fetchFromGitHub {
    owner = "mic92";
    repo = "nixos-shell";
    rev = "39936f8b60c6ff3644ae508af38505d39f97730c";
    sha256 = "11aj2ck9kh4iaiidrsf8kxp6rqpghi0zw88hncmv5r37wcby7ika";
  };

  dontConfigure = true;

  makeFlags = [
    "PREFIX=${placeholder "out"}"
  ];

  meta = { };
}
