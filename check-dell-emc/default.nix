{
  dos2unix,
  lib,
  fetchFromGitHub,
  fetchurl,
  python2Packages,
  makeWrapper,
  stdenv,
  unzip,
}:

let
  pypkgs = python2Packages;

  exes = lib.concatStringsSep " " [
    "dellemc_device_check.py"
    "dellemc_helper_utility.py"
  ];

  pysnmp-mibs = pypkgs.buildPythonPackage rec {
    pname = "pysnmp-mibs";
    version = "0.1.6";

    src = pypkgs.fetchPypi {
      inherit pname version;
      sha256 = "033jhdva1zhqr2fvjsyhnxbrsppv22zh51yr196a3s4qllf35yh0";
    };

    # no tests
    doCheck = false;

    propagatedBuildInputs = with pypkgs; [ pysnmp ];

    meta = { };
  };

  python_version = pypkgs.buildPythonPackage rec {
    pname = "python_version";
    version = "0.0.2";

    src = pypkgs.fetchPypi {
      inherit pname version;
      sha256 = "0rzy0jzh1w7rx7swgjb5qv7b7dhblbsyhs7l3ii19mpjqxbxw5jw";
    };

    # no tests
    doCheck = false;

    meta = { };
  };

  omsdk = pypkgs.buildPythonPackage rec {
    pname = "omsdk";
    version = "1.2.370";

    src = fetchFromGitHub {
      owner = "dell";
      repo = "omsdk";
      rev = "v${version}-master";
      sha256 = "0h9qf749rgimkhymn52qlham7xfkyb0di6nbxp4v7w5jajyszpm4";
    };

    postPatch = ''
      echo ${version} > /tmp/_version.txt
    '';

    buildPhase = ''
      runHook preBuild

      for f in omdrivers omsdk; do
        cp setup-$f.py setup.py
        ${pypkgs.python.interpreter} setup.py bdist_wheel --universal
      done

      runHook postBuild
    '';

    propagatedBuildInputs = with pypkgs; [
      enum34
      future
      ipaddress
      pyasn1
      pysnmp-mibs
      python_version
      pyyaml
      requests
    ];

    meta = {
      description = "Dell EMC OpenManage SDK";
    };
  };

  # TODO: fix .jar location
  # TODO: link check_warranty.py to bin as it can also be called as a command

in
pypkgs.buildPythonApplication rec {
  pname = "check-dell-emc";
  version = "3.0";

  format = "other";

  src = fetchurl {
    url = "https://downloads.dell.com/FOLDER04936409M/1/Dell_EMC_OpenManage_Plugin_v${version}_Nagios_XI_A00.tar.gz";
    sha256 = "0cgs7vnai67vcwvjw0dksmg5pm2fg1r318bv5588jrpkm5qq42ka";
  };

  propagatedBuildInputs = with pypkgs; [
    netaddr
    omsdk
  ];

  nativeBuildInputs = [
    dos2unix
    unzip
  ];

  sourceRoot = ".";

  unpackCmd = ''
    tar -xzf ${src}
    unzip -q Dell_EMC_OM_NagiosXI_monitoring_wizard.zip
  '';

  installPhase = ''
    runHook preInstall

    pushd Dell_EMC_OM_NagiosXI_monitoring_wizard/plugins >/dev/null

    find . -type f -print0 | xargs -0 dos2unix

    install -d $out/bin
    mv ${exes} $out/bin
    install -Dm444 -t $out/${pypkgs.python.sitePackages} *.py
    install -Dm444 -t $out/etc *.cfg *.conf
    install -Dm444 -t $out/share/doc/${pname}/nagios ../templates/*
    install -Dm444 -t $out/share/java *.jar

    chmod 555 $out/bin/*
    for f in $out/bin/*.py ; do
      sed -i $f -e '1i#!${pypkgs.python.interpreter}'
    done

    popd
    runHook postInstall
  '';

  # no tests
  doCheck = false;

  meta = with lib; {
    description = "Check Dell EMC Hardware";
    broken = true; # due to py2
  };
}
