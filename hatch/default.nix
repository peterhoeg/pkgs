{
  lib,
  fetchFromGitHub,
  python3Packages,
  makeBinaryWrapper,
  chromedriver,
  chromium,
  google-chrome,
}:

let
  pypkgs = python3Packages;

  chrome' = chromium;

in
pypkgs.buildPythonApplication rec {
  pname = "hatch";
  version = "20190307";

  format = "other";

  src = fetchFromGitHub {
    owner = "FlorianBord2";
    repo = "Hatch-python3-optimised";
    rev = "f5d5456da196ec9dc5d3572a9a541bbd7184cc76";
    sha256 = "sha256-DQYWLwA9YY8dZwzL2RETk2Vlfa0AMQ/2yw/Ksv3cSTc=";
  };

  postPatch = ''
    sed -i main.py \
      -e "s@^CHROME_DVR_DIR.*@CHROME_DVR_DIR = '${chromedriver}/bin/chromedriver'@"
  '';

  nativeBuildInputs = [ makeBinaryWrapper ];

  propagatedBuildInputs = with pypkgs; [
    requests
    selenium
  ];

  installPhase = ''
    runHook preInstall

    mkdir -p $out/bin

    install -Dm555 main.py $out/libexec/hatch.py
    # chromium also has a chrome binary for compatibility
    ln -s ${chrome'}/bin/chrome $out/bin/chrome
    install -Dm444 -t $out/share/doc/${pname} README.* LICENSE
    install -Dm444 -t $out/share/${pname} passlist.txt

    makeWrapper ${pypkgs.python.interpreter} $out/bin/${pname} \
      --prefix PATH       : $out/bin \
      --prefix PYTHONPATH : ${pypkgs.makePythonPath propagatedBuildInputs} \
      --add-flags $out/libexec/hatch.py

    runHook postInstall
  '';

  meta = with lib; {
    description = "Brute force most websites";
    license = licenses.gpl3Only;
  };
}
