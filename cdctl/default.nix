{
  stdenv,
  lib,
  fetchurl,
  autoreconfHook,
}:

stdenv.mkDerivation rec {
  pname = "cdctl";
  version = "0.16";

  src = fetchurl {
    url = "mirror://sourceforge/cdctl/cdctl-${version}.tar.gz";
    hash = "sha256-uYwXrM1y/cEt1M1N/p8Zi5YeRMa8FfYghecXZoWOqn8=";
  };

  postPatch = ''
    substituteInPlace Makefile.in \
      --replace-fail SEGFAULTS "" \
      --replace-fail /usr $out
  '';

  nativeBuildInputs = [ autoreconfHook ];

  enableParallelBuilding = true;

  meta = with lib; {
    description = "CD/DVD controller";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
    mainProgram = "cdctl";
  };
}
