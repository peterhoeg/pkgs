{
  stdenv,
  lib,
  msr-tools,
}:

stdenv.mkDerivation rec {
  pname = "bd_prochot";
  version = "unstable-2019-01-01";

  src = ./src;

  buildInputs = [ msr-tools ];

  dontUnpack = true;

  postPatch = ''
    substitute $src/* bd_prochot.c \
      --replace-fail @rdmsr@ ${msr-tools}/bin/rdmsr \
      --replace-fail @wrmsr@ ${msr-tools}/bin/wrmsr
  '';

  enableParallelBuilding = true;

  buildPhase = ''
    runHook preBuild

    gcc -O2 -o bd_prochot bd_prochot.c

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin bd_prochot

    runHook postInstall
  '';

  meta = with lib; {
    description = "Hammer MSR registers to force CPU to run fast";
    license = licenses.free;
  };
}
