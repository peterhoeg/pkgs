{
  stdenvNoCC,
  lib,
  fetchFromGitea,
  fetchFromGitHub,
  fetchFromGitLab,
  fetchpatch,
  fetchurl,
  coreutils-full,
  findutils,
  optipng,
  unzip,
}:

let
  inherit (lib) optionalString;

  contentDb =
    owner: pname: version: hash:
    fetchurl {
      url = "https://content.minetest.net/packages/${owner}/${pname}/releases/${toString version}/download/";
      name = "minetest-addon-${owner}-${pname}.zip";
      inherit hash;
    };

  mtAddon =
    {
      type,
      pname,
      version,
      src,
      createDirectory ? false,
    }:
    stdenvNoCC.mkDerivation {
      name = "minetest-${type}-${pname}-${toString version}";
      inherit version src;
      nativeBuildInputs = [
        coreutils-full
        findutils
        optipng
        unzip
      ];
      buildCommand = ''
        dir=$out/share/minetest/${type}
        ${optionalString createDirectory "dir=$dir/${pname}"}

        if [ -f "$src" ]; then
          mkdir -p $dir
          unzip -q -d $dir $src
        elif [ -d "$src" ]; then
          dir=$dir/${pname}
          mkdir -p $dir
          cp -r --no-preserve=all $src/* $dir
        else
          echo "I don't know how to handle this: $src"
          stat "$src"
          exit 77
        fi

        # PNG optimization is really only needed for texture packs, but we might as well run for all
        # Colors with 0 alpha need to be preserved, because opaque leaves ignore alpha.
        # For that purpose, the use of indexed colors is disabled (-nc).
        find "$dir" -name '*.png' -print0 | xargs -0 optipng -o7 -zm1-9 -nc -strip all -clobber

      '';
      passthru.dir = type;
    };

  mtGame =
    {
      pname,
      version,
      src,
      createDirectory ? false,
    }:
    mtAddon {
      inherit
        pname
        version
        src
        createDirectory
        ;
      type = "games";
    };

  mtMod =
    {
      pname,
      version,
      src,
      createDirectory ? false,
    }:
    mtAddon {
      inherit
        pname
        version
        src
        createDirectory
        ;
      type = "mods";
    };

  mtTexture =
    {
      pname,
      version,
      src,
      createDirectory ? false,
    }:
    mtAddon {
      inherit
        pname
        version
        src
        createDirectory
        ;
      type = "textures";
    };

in
{
  #########
  # games #
  #########
  games = {
    ctf = mtGame rec {
      pname = "capture-the-flag";
      version = "3.10";
      src = fetchFromGitHub {
        owner = "MT-CTF";
        repo = "capturetheflag";
        rev = "v" + version;
        hash = "sha256-uL6NLSlIk/lX4C9haKXEKI4keyDWIkr8oBcL+cJt4yg=";
        fetchSubmodules = true;
      };
    };

    epic-combat = mtGame rec {
      pname = "epic_combat";
      version = 27975;
      src = contentDb "hilol" pname version "sha256-qdKdtQuoqeYBA1ekeghDnT/VqqHOCzfaKPeEbkxS5l4=";
    };

    mineclonia = mtGame rec {
      pname = "mineclonia";
      version = "0.110.1";
      src = fetchFromGitea {
        domain = "codeberg.org";
        owner = "mineclonia";
        repo = "mineclonia";
        rev = version;
        hash = "sha256-msaa1Qu4cO62bZtD+sHiuj0HA9ejvhawMydonF1JF10=";
      };
    };

    minetest-game = mtGame rec {
      pname = "minetest_game";
      version = 25698;
      # there are untagged releases made via contentdb, so use those instead of github
      src = contentDb "Minetest" pname version "sha256-90Jw3LvG+41Jnr02XJtoX5KAVE5N1bN4JUuNmufizHY=";
    };

    voxelibre = mtAddon rec {
      type = "games";
      pname = "VoxeLibre";
      version = "0.87.2";
      src = fetchFromGitea {
        domain = "git.minetest.land";
        owner = "VoxeLibre";
        repo = "VoxeLibre";
        rev = version;
        hash = "sha256-/eOMJjZFMUeyrVe9ZqPkhG7bx0rZThMzKQWVqeCm8yA=";
      };
    };
  };

  ########
  # mods #
  ########
  mods = {
    advanced-trains = mtMod rec {
      pname = "advtrains";
      version = 19842;
      src = contentDb "orwell" pname version "sha256-WBDLFBeBT2empqcBX9yUVc9ZQApVsVdr4ZMuE7ZdqL4=";
    };

    aliases = mtMod rec {
      pname = "aliases";
      version = "28457";
      src = contentDb "Krunegan" pname version "sha256-KrvYHFbH3cDNu86fgFAt2Hv8em/nn/rAes29JVWN/aU=";
    };

    animalia = mtMod rec {
      pname = "animalia";
      version = 23715;
      src = contentDb "ElCeejo" pname version "sha256-kdHMmoJ7h18OznIN61+IJjMHQiRGX2rFWPyxqGmXmog=";
    };

    airutils = mtMod rec {
      pname = "airutils"; # needed by steampunk_blimp
      version = 26065;
      src = contentDb "apercy" pname version "sha256-K2jD/vL9Vnq9g3+waID06trVzhvF7/dBi9ITKrlo06o=";
    };

    back-to-spawn = mtMod rec {
      pname = "mcl_back_to_spawn";
      version = 18336;
      src = contentDb "Alex5002" pname version "sha256-gUEvVFkCBLNDjUpiBLTAYBXAAhkGCybVVyzBnS4dl1A=";
    };

    backpack = mtMod rec {
      pname = "mcl_backpack";
      version = "1.0";
      src = fetchFromGitea {
        domain = "git.minetest.land";
        owner = "MatthiasZM96";
        repo = "mcl2_backpack";
        rev = "mcl2_backpack" + version;
        hash = "sha256-mMRVaCOZRn5NOaGsVpbcQkiCGhAqJB/CCX5+sOQl780=";
      };
    };

    basic-trains = mtMod rec {
      pname = "basic_trains";
      version = 19843;
      src = contentDb "orwell" pname version "sha256-y9/GyTU9F7cuRGjfbIM13C1wo3VUxrvJMGcC5PYKC9U=";
    };

    better-screwdriver = mtMod rec {
      pname = "better-screwdriver";
      version = 24240;
      src =
        contentDb "12Me21" "screwdriver2" version
          "sha256-r3DTI5RcIso0wqe/vIshOYCKI0AsVHX4SoO9R3WM75M=";
    };

    birds = mtMod rec {
      pname = "birds";
      version = 24887;
      src = contentDb "shaft" pname version "sha256-2yrYvKj8n8WgrYgDg+sgCV1UH7UZ9ol/5S9XoCyp2N0=";
    };

    chest-recovery = mtMod rec {
      pname = "chest_recovery";
      version = 25885;
      src = contentDb "neocraft1293" pname version "sha256-XgmlKtjP6euGohWm/RopNFIcaPp+rupZIc0gEiei3s8=";
    };

    claycrafter = mtMod rec {
      pname = "claycrafter";
      version = 25375;
      src = contentDb "Dragonop" pname version "sha256-1SlgjQr/ue8QzCMS0P+9f8EbmrqXs9+lLxXhAyivIPg=";
    };

    creatura = mtMod rec {
      pname = "creatura"; # needed by animalia
      version = 22754;
      src = contentDb "ElCeejo" pname version "sha256-t1q0yDnHrL2VBDZaLa+hPG9Olb+0DM8SUWwJkZORZ24=";
    };

    draconis =
      let
        # draconis requires patches to work with mineclonia
        source =
          version:
          stdenvNoCC.mkDerivation {
            pname = "draconis-sources";
            inherit version;
            src = fetchFromGitHub {
              owner = "ElCeejo";
              repo = "draconis";
              rev = "cd53f3169e6d77b251e205f36cd8a22ff9bb6ce6";
              hash = "sha256-pUdRlfvnLhfwDhWnffnKFmbYsHr4Nh4lpibLmGsP/sw=";
            };
            patches = [
              (fetchpatch {
                url = "https://patch-diff.githubusercontent.com/raw/ElCeejo/draconis/pull/37.patch";
                hash = "sha256-/+wnMay+fFvOQ3CiBsqV8mn+jFQbBYNizauLrOrWDIY=";
              })
            ];
            installPhase = ''
              mkdir -p $out
              cp -r * $out
            '';
          };
      in
      mtMod rec {
        pname = "draconis";
        version = "0-unstable-2024-02-15";
        src = source version;
        # version = 23494;
        # src = contentDb "ElCeejo" pname version "sha256-6dl8XwGYR8rFKeswhWNJ96ofoIDFDKijZRWrUg5u2fI=";
      };

    hl-marker = "https://content.luanti.org/packages/Krunegan/hl_marker/releases/20462/download/";

    teleport-potion = "https://content.luanti.org/packages/TenPlus1/teleport_potion/releases/29202/download/";

    x-compat = "https://content.luanti.org/packages/mt-mods/xcompat/releases/27623/download/";

    i3 = mtMod rec {
      pname = "i3";
      version = 24431;
      src = contentDb "mt-mods" pname version "sha256-whQz725Qn2EXBFKQ/8BbGwDuYJpMIetdRQ+61CQdo3A=";
    };

    inventory-admin = mtMod rec {
      pname = "inventory_admin";
      version = 22965;
      src = contentDb "Impulse" pname version "sha256-eusd7R/JsZ/go2WlSIwem7tOVoJKomrxT+lqosrQO+s=";
    };

    inventory-pouches = mtMod rec {
      pname = "inventory_pouches";
      version = 22993;
      src = contentDb "Impulse" pname version "sha256-fC+JJMcOyCZ4dJyN2mY/0t5R112caXvL6kCNHwEN37M=";
    };

    leads = mtMod rec {
      pname = "leads";
      version = "0.3.2";
      src = fetchFromGitea {
        domain = "codeberg.org";
        owner = "SilverSandstone";
        repo = "leads";
        rev = "v" + version;
        hash = "sha256-b/pUdwSqtJ/8VtRkuNnR1oZTPDAuVtO6sgq/iNMmpFk=";
        # https://codeberg.org/SilverSandstone/leads/tags
      };
    };

    multitool = mtMod rec {
      pname = "mcl_multitool";
      version = 8254;
      src = contentDb "duckgo" pname version "sha256-s0nNAjASDMLpkelgQ5oZg47xbaUBQz1B3GCYyzo9h/o=";
    };

    objectuuids = mtMod rec {
      pname = "objectuuids";
      version = 19159;
      src =
        contentDb "SilverSandstone" pname version
          "sha256-cHdPTEiWpJbOsbuUa+v9ffXK2/NrARw9rTQT7grKCHg=";
      createDirectory = true;
    };

    panda = mtMod {
      pname = "panda";
      version = "0-unstable-2018-08-08";
      src = fetchFromGitHub {
        owner = "AspireMint";
        repo = "mob_panda";
        rev = "8581567a34c11eba51a4cf54947d0a4bc00bdafb";
        hash = "sha256-1ywmjWmQC0KUFLgGB2a0lXmPkhsVJxPnaGy/YzikMV0=";
      };
    };

    permatime = mtMod rec {
      pname = "permatime";
      version = 28358;
      src = contentDb "ROllerozxa" pname version "sha256-BB3W+SxZJqlbh6gf/QEJwafoNSzWLjyL/KwAMGwCnlo=";
      # createDirectory = true;
    };

    smart-inventory = mtMod rec {
      pname = "smart_inventory";
      version = 5622;
      src = contentDb "bell07" pname version "sha256-dfxSq10CRJ4aXG8+evCs8t1lXo53HyE0aXBvRgihh+k=";
      createDirectory = true;
    };

    steampunk-blimp = mtMod rec {
      pname = "steampunk_blimp";
      version = 25392;
      src = contentDb "apercy" pname version "sha256-yGi3DpvbgzUkyQROwhnf0djDzbKUJNK7nwJZt9bOxXY=";
    };

    worldedit = mtMod {
      pname = "worldedit";
      version = "0-unstable-2024-05-26";
      src = fetchFromGitHub {
        owner = "Uberi";
        repo = "Minetest-WorldEdit";
        rev = "28374f4f27f030fd77b218cdcfe975909bd4d40a";
        hash = "sha256-ECnZwrbSry5Pi+tBROjHGF5gXh2qHSq9w9oqBUoEljs=";
      };
    };

    wrench = mtMod rec {
      pname = "wrench";
      version = 25150;
      src = contentDb "mt-mods" pname version "sha256-VQBFbkOaNoRCR6xVzld2z1HLFZ4uOfwfDTi4BZqgC7Y=";
    };

    xcompat = mtMod rec {
      pname = "xcompat";
      version = 25620;
      src = contentDb "mt-mods" pname version "sha256-bUnxKLLjA3aB0Vk6Q6Qt8Cne7blGnJ+Hidv2Xj1lyPU=";
    };
  };

  ############
  # textures #
  ############
  textures = {
    artelhum = mtTexture rec {
      pname = "artelhum";
      version = 5200;
      src = contentDb "AndrOn" pname version "sha256-9cy1f036/U3drE29GibBr+D+CDqeXO48oFjzvh3o2U4=";
    };

    dungeon-soup = mtTexture {
      pname = "dungeon-soup";
      version = "0-unstable-2023-05-12";
      src = fetchFromGitHub {
        owner = "sirrobzeroone";
        repo = "DungeonSoup";
        rev = "56199528ac30a94cb9749b9c5298bfb4498026fe";
        hash = "sha256-HVaiSEDjp0a7ARZv/sdN3CBSrrVgK3zJWxTUurYxlic=";
      };
    };

    hand-painted = mtTexture {
      pname = "hand_painted_expanded";
      version = "0-unstable-2024-06-09";
      src = fetchFromGitLab {
        domain = "gitgud.io";
        owner = "blut";
        repo = "hand_painted_expanded";
        rev = "f77a0fbc8a1c0986fa7161903c496c747c67849e";
        hash = "sha256-Rxo4CYxFqDBWdGR7qs/bL5K/0mUYddoTgJVdtMa4qEc=";
      };

    };

    pixel-imperfection = mtTexture rec {
      pname = "pixel-imperfection";
      # The dates are the versions
      version = "2024-08-04";
      src = fetchFromGitea {
        domain = "codeberg.org";
        owner = "bramaudi";
        repo = "pixel_imperfection";
        rev = version;
        hash = "sha256-LZbh0SYoqvM5B7w2Cs0Iag1mTDXYYfZQBW0LiwCIFGo=";
      };
    };

    polygonia = mtTexture rec {
      pname = "polygonia";
      version = 17862;
      src = contentDb "Lokrates" pname version "sha256-9RxEV9CwbO23O9Ec2miJ1qq6Gh7VljMHB3PiaWV3+YE=";
    };

    sharpnet = mtTexture {
      pname = "sharpnet_textures";
      version = "0-unstable-2024-07-19";
      src = fetchFromGitHub {
        owner = "Sharpik";
        repo = "Minetest-SharpNet-Photo-Realism-Texturespack";
        rev = "eacc1bd57ae20879dda39a81d99cb708660d3b48";
        hash = "sha256-Zry8iB9bqst+vgh47NaAmHYRT3Fkh92qEScC9j/w/6o=";
      };
    };

    sharpnet-hd = mtTexture {
      pname = "sharpnet_textures_hd";
      # version = 26028;
      # src = contentDb "Sharpik" pname version "sha256-QHkLj2kszJATSh4yGfCRo/vP6yKzCf7P7Qm3aS5sZ6g=";
      version = "2024-07-19";
      src = fetchFromGitHub {
        owner = "Sharpik";
        repo = "sharpnet_hd_256";
        rev = "dc3dc311525352061c91fa4dda18a22a7b1a0103";
        hash = "sha256-KN/W7hWMCk/2Lx+K4Tx5YaWShRB2HtvD2wXQXMGhAfc=";
      };
    };

    viljapix2 = mtTexture rec {
      pname = "vilja_pix_2";
      version = 15080;
      src = contentDb "ROllerozxa" pname version "sha256-/fmDpSWPuPMVbaspntMl7n8D/lUy6AMPv+oUoHB2ViM=";
    };
  };
}
