{
  stdenv,
  lib,
  fetchurl,
  fetchFromGitHub,
  fetchFromGitLab,
  makeWrapper,
  pkg-config,
  openssl,
  zlib,
}:

let
  foo = stdenv.mkDerivation rec {
    pname = "foo";
    version = "1";

    src = ./.;

    buildCommand = ''
      mkdir -p $out/foo
      echo "hello from foo" > $out/foo/foo.txt
    '';

    passthru.foo = "${foo.out}/foo/foo.txt";
  };

in
stdenv.mkDerivation rec {
  pname = "bar";
  version = "2";

  # src = fetchurl {
  # src = fetchFromGitLab {
  # src = fetchFromGitHub {
  #   owner = "";
  #   repo = "";
  #   rev = "vversion";
  #   sha256 = "lib.fakeSha256";
  # };

  buildInputs = [
    openssl
    zlib

    foo
  ];

  nativeBuildInputs = [
    makeWrapper
    pkg-config
  ];

  buildCommand = ''
    mkdir -p $out
    cat ${foo.foo} | tee $out/bar.txt
  '';
}
