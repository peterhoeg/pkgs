{
  stdenv,
  lib,
  fetchurl,
  _7zz,
}:

stdenv.mkDerivation rec {
  pname = "dune2-assets";
  version = "1.07";

  src = fetchurl {
    url = "https://archive.org/download/Dune2_201709/dune%202.iso";
    hash = "sha256-k+1wt6OjDjw8bq5sEWNh5zIplYC86e+FNpYsBBvf38c=";
    name = "dune2.iso";
  };

  unpackPhase = "7zz x $src";

  dontConfigure = true;
  dontBuild = true;
  dontFixup = true;

  installPhase = ''
    runHook preInstall

    dir=$out/share/games/dune2

    install -Dm444 -t $dir DUNE2.CD/*
    install -Dm444 -t $out/share/doc/${pname} Manual/pdf/*

    runHook postInstall
  '';

  nativeBuildInputs = [ _7zz ];

  meta = with lib; {
    description = "Dune 2 game assets";
    hydraPlatforms = platforms.none;
  };
}
