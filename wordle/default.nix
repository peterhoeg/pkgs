{
  stdenv,
  lib,
  fetchurl,
  writeText,
  coreutils,
  runtimeShell,
}:

let
  header = writeText "header.sh" ''
    #!${runtimeShell}
    set -eEuo pipefail
    PATH=$PATH:${lib.makeBinPath [ coreutils ]}
  '';

in
stdenv.mkDerivation rec {
  pname = "wordle";
  version = "2022-02-03";

  src = fetchurl {
    url = "https://gist.github.com/huytd/6a1a6a7b34a0d0abcac00b47e3d01513/raw/ca41929c10a6c2ed8faa77c298bb188abfe5145a/wordle.sh";
    sha256 = "sha256-GNBdyJ+OJQYlJRX/1JxkPC4xRh8rAB728K7Q9Ngz6uQ=";
  };

  dontUnpack = true;

  buildPhase = ''
    file=$out/bin/wordle.sh

    install -Dm777 ${header} $file
    cat $src >> $file

    substituteInPlace $file \
      --replace-fail /usr/share/dict \''${XDG_DATA_HOME:-\$HOME/.local/share}/dict \
      --replace-fail '$1' \''${1:-""}
  '';

  dontInstall = true;

  meta = with lib; {
    description = "Wordle in bash";
    license = licenses.free;
  };
}
