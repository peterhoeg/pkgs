{
  lib,
  fetchFromGitHub,
  buildGoModule,
}:

buildGoModule {
  pname = "age-plugin-tpm";
  version = "0.0.1.20230503";

  src = fetchFromGitHub {
    owner = "Foxboron";
    repo = "age-plugin-tpm";
    rev = "c570739b05c067087c44f651efce6890eedc0647";
    # rev = "v${version}";
    hash = "sha256-xlJtyNAYi/6vBWLsjymFLGfr30w80OplwG2xGTEB118=";
  };

  vendorHash = "sha256-S9wSxw0ZMibCOspgGt5vjzFhPL+bZncjTdIX2mkX5vE=";

  doCheck = false;

  preCheck = ''
    export HOME=$(mktemp -d)
  '';

  meta = with lib; {
    description = "TPM 2.0 plugin for age ";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
}
