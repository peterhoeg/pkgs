{
  lib,
  fetchFromGitHub,
  python3Packages,
  net-snmp,
}:

let
  pypkgs = python3Packages;
  mib = "$out/share/snmp/mibs/idrac-smiv2.mib";

in
pypkgs.buildPythonApplication rec {
  pname = "check-idrac";
  version = "2.2rc4";

  format = "other";

  src = fetchFromGitHub {
    owner = "dangmocrang";
    repo = "check_idrac";
    rev = "f1580015a146312aa0923dce7a47f82d19438cc9";
    sha256 = "1xrw2h19mxzaf6nymay597p7gjicx7b1nm8307wl55ypxs665726";
  };

  installPhase = ''
    runHook preInstall

    install -Dm555 idrac_${version} $out/libexec/check_idrac
    install -Dm444 -t $out/share/${pname}/etc *.conf
    install -Dm444 -t $out/share/doc/${pname} *.md
    install -Dm444 ${builtins.baseNameOf mib} ${mib}

    makeWrapper $out/libexec/check_idrac $out/bin/check_idrac \
      --add-flags "-m ${mib}" \
      --prefix PATH : ${lib.makeBinPath [ net-snmp ]}

    runHook postInstall
  '';

  # no automated tests
  doCheck = false;

  meta = with lib; {
    description = "Check Dell iDRAC";
    license = licenses.gpl3;
  };
}
