{
  lib,
  stdenv,
  fetchFromGitHub,
  fetchurl,
  autoreconfHook,
  db,
  openldap,
}:

# https://www.openldap.org/faq/data/cache/152.html

let
  inherit (lib) getDev getLib;

  regex = stdenv.mkDerivation rec {
    pname = "regex";
    version = "0.12";

    src = fetchurl {
      url = "ftp://ftp.gnu.org/old-gnu/regex/regex-${version}.tar.gz";
      hash = "sha256-824tjVa/FQVKhQEo/LL1GAdwapLTu0s3zq3XMVNc4jA=";
    };

    postPatch = ''
      substituteInPlace doc/Makefile.in \
        --replace-fail /usr/local $out
    '';

    nativeBuildInputs = [ autoreconfHook ];

    # gcc14 is a lot stricter
    env.NIX_CFLAGS_COMPILE = toString [
      "-Wno-error=implicit-function-declaration"
    ];

    postInstall = ''
      ar r libgnuregex.a regex.o
      ranlib libgnuregex.a
      install -Dm555 -t $out/lib libgnuregex.a
    '';
  };

  rx = stdenv.mkDerivation rec {
    pname = "rx";
    version = "1.5";

    src = fetchurl {
      url = "ftp://ftp.gnu.org/old-gnu/rx/rx-${version}.tar.gz";
      hash = "sha256-N6XsyY3VJpCVj0WU0PXC0FhuDjCIjodX8UGwC75RFgk=";
    };
  };

  rx' = regex;

  # adtools needs openldap 2.4.x
  openldap' = openldap.overrideAttrs (old: rec {
    version = "2.4.59";
    src = fetchurl {
      url = "https://www.openldap.org/software/download/OpenLDAP/openldap-release/openldap-${version}.tgz";
      hash = "sha256-mfN9Z0fYggbEcAZ+2mJNXkjBAR6UPsCrIXuuhxLiLzQ=";
    };
    buildInputs = old.buildInputs ++ [
      db
      rx'
    ];
    extraContribModules = [
      "passwd/sha2"
      "passwd/pbkdf2"
    ];
    env = {
      CPPFLAGS = "-I${rx'}/lib";
      LDFLAGS = "-L${rx'}/lib";
      LIBS = "-lgnuregex";
    };
    # tests are slow and there is not much point in running them when we have pinned this version
    doCheck = false;
    # patchelf gets confused and adds libraries found under /build to RPATH
    preFixup =
      ''
        rm -r libraries/*/.libs
      ''
      + old.preFixup;
  });

in
stdenv.mkDerivation (finalAttrs: {
  pname = "adtool";
  version = "1.3.3.20180426";

  src = fetchFromGitHub {
    owner = "blroot";
    repo = "adtool";
    rev = "4159ddec386db9d6545fe5a386686638553bc6af";
    hash = "sha256-LSTn1f/A9rFv7MgmkF+tMFchGNmilDJ6ahj9b4DkHS0=";
  };

  postPatch = ''
    substituteInPlace src/lib/active_directory.c \
      --replace-fail 'AD_CONFIG_FILE SYSCONFDIR "/adtool.cfg"' 'AD_CONFIG_FILE "/etc/adtool.cfg"'
  '';

  configureFlags = [
    "--sysconfdir=/etc"
  ];

  installFlags = [
    "sysconfdir=$(out)/share/doc/${finalAttrs.pname}"
  ];

  buildInputs = [ openldap' ];

  # it expects headers and libraries to exist under the same prefix
  env = {
    NIX_CFLAGS_COMPILE = "-I${getDev openldap'}/include";
    NIX_LDFLAGS = "-L${getLib openldap'}/lib";
  };

  enableParallelBuilding = true;

  # It requires an LDAP server for tests
  doCheck = false;

  meta = with lib; {
    description = "Active Directory administration utility for Unix";
    homepage = "https://gp2x.org/adtool";
    license = licenses.gpl2Only;
    maintainers = with maintainers; [ peterhoeg ];
    mainProgram = "adtool";
  };
})
