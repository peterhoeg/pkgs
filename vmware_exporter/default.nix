{
  lib,
  fetchFromGitHub,
  python3Packages,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "vmware_exporter";
  version = "0.18.4";

  src = fetchFromGitHub {
    owner = "pryorda";
    repo = pname;
    rev = "v" + version;
    hash = "sha256-hYW6yzDysqkQEOiaYTub6LsAmMLLN/Vhy+Eta0h3yKM=";
  };

  postPatch = ''
    substituteInPlace requirements.txt \
      --replace-fail prometheus-client==0.0.19 prometheus-client
  '';

  propagatedBuildInputs = with pypkgs; [
    prometheus_client
    pyopenssl
    pytz
    pyvmomi
    pyyaml
    service-identity
    twisted
  ];

  meta = with lib; {
    description = "vmware exporter for Prometheus";
    license = licenses.mit;
  };
}
