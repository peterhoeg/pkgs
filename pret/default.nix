{
  lib,
  fetchFromGitHub,
  python3Packages,
  ghostscript,
  imagemagick,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "pret";
  version = "0.0.20230713";

  format = "other";

  src = fetchFromGitHub {
    owner = "RUB-NDS";
    repo = "PRET";
    rev = "a04bd04b5490ea6a88d11951a55997113f526b77";
    hash = "sha256-9hLWxGzgiFdGHPC/s7AG3R2dokSUExWh4htOO3IFEBU=";
  };

  # BROKEN, this patch needs to be fixed for the new version
  patches = [
    ./point_to_nix_store.patch
  ];

  postPatch = ''
    mv pret.py pret

    dir=$out/share/${pname}

    substituteInPlace pret \
      --replace-fail @version@ ${version}

    substituteInPlace capabilities.py \
      --replace-fail @dbdir@ $dir/db

    substituteInPlace postscript.py \
      --replace-fail @fontsdir@ $dir/fonts

    substituteInPlace printer.py \
      --replace-fail @convert@ ${imagemagick}/bin/convert

    unset dir
  '';

  # colorama is only needed on windows
  propagatedBuildInputs = with pypkgs; [ pysnmp ];

  makeWrapperArgs = [
    "--prefix PATH : ${
      lib.makeBinPath [
        ghostscript
        imagemagick
      ]
    }"
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t         $out/bin pret
    install -Dm444 -t         $out/${pypkgs.python.sitePackages} *.py
    install -d                $out/share/${pname}
    cp -r {db,fonts,lpd,mibs} $out/share/${pname}

    install -Dm444 -t $out/share/doc/${pname} *.md

    runHook postInstall
  '';

  meta = {
    description = "Printer exploit framework";
    broken = true;
  };
}
