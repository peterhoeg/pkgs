{
  crystal,
  fetchFromGitHub,
  ameba,
  pkg-config,
  # , openssl
  sqlite,
}:

crystal.buildCrystalPackage rec {
  pname = "amber";
  version = "1.4.1";

  format = "shards";

  src = fetchFromGitHub {
    owner = "amberframework";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-AAjFSdEOPVYmqtGiyJ1I3xofDflRVxNFO5xdvkYO0bo=";
  };

  shardsFile = ./shards.nix;
  lockFile = ./shard.lock;

  nativeBuildInputs = [
    ameba
    pkg-config
  ];

  buildInputs = [ sqlite ];

  options = [ "--verbose" ];

  # some tests are failing
  doCheck = false; # needs redis
  doInstallCheck = false;
}
