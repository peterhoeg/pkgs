{
  amber_router = {
    url = "https://github.com/amberframework/amber-router.git";
    rev = "v0.4.4";
    sha256 = "1bydnq2q91aqkr40zsyb5dxizln7dprp9bjq9xiszmfxfgrfwip9";
  };
  ameba = {
    url = "https://github.com/crystal-ameba/ameba.git";
    rev = "v1.5.0";
    sha256 = "1idivsbpmi40aqvs82fsv37nrgikirprxrj3ls9chsb876fq9p2d";
  };
  backtracer = {
    url = "https://github.com/sija/backtracer.cr.git";
    rev = "v1.2.2";
    sha256 = "1rknyylsi14m7i77x7c3138wdw27i4f6sd78m3srw851p47bwr20";
  };
  callback = {
    url = "https://github.com/amberframework/callback.git";
    rev = "v0.9.2";
    sha256 = "05a4jj3cy8hzy9f4zs9ng1005r6q5c0j2s3iv0b7a9dgkici7snk";
  };
  cli = {
    url = "https://github.com/amberframework/cli.git";
    rev = "v0.11.4";
    sha256 = "0mv7a49zk5p5lqdzbmadxzb2g4vpz0g0hcaswhv7l0zscgz4j359";
  };
  compiled_license = {
    url = "https://github.com/elorest/compiled_license.git";
    rev = "v1.2.2";
    sha256 = "1f412r6m31cc093lcw31m2rp5s3y7vh6q3wc3xh9b8vccvmj21p7";
  };
  db = {
    url = "https://github.com/crystal-lang/crystal-db.git";
    rev = "v0.11.0";
    sha256 = "1ylfhpn64p72ywi39niqb179f61z08q4qd4hhjza05z18mdaghl3";
  };
  exception_page = {
    url = "https://github.com/crystal-loot/exception_page.git";
    rev = "v0.3.1";
    sha256 = "00fpkhwaf94mz9d9qiinsa7hdbs3x2yqjwwzvbjwv86dv8s5008n";
  };
  future = {
    url = "https://github.com/crystal-community/future.cr.git";
    rev = "v1.0.0";
    sha256 = "1mji2djkrf4vxgs432kgkzxx54ybzk636789k2vsws3sf14l74i8";
  };
  inflector = {
    url = "https://github.com/phoffer/inflector.cr.git";
    rev = "v1.0.0";
    sha256 = "0g3932b04xmxsyrl1cnwalcj072hiw6jhi9aqh2ngn0gxhn6idbx";
  };
  json_mapping = {
    url = "https://github.com/crystal-lang/json_mapping.cr.git";
    rev = "v0.1.1";
    sha256 = "1n8bwl5k9hjj9rr0kwzgk3ckqiv1v0yz5vxc3y5yxs446lh70s68";
  };
  kilt = {
    url = "https://github.com/jeromegn/kilt.git";
    rev = "v0.6.1";
    sha256 = "0dpc15y9m8c5l9zdfif6jlf7zmkrlm9w4m2igi5xa22fdjwamwfp";
  };
  liquid = {
    url = "https://github.com/dare892/liquid.cr.git";
    rev = "e4ef5ca06bf5bafe74e0b270c7aae2941a91ddd3";
    sha256 = "1nnz2wyplsmqgs23xsgc0vi8pq7fn0jhpf99hkl0zr8gpqlyipa0";
  };
  micrate = {
    url = "https://github.com/amberframework/micrate.git";
    rev = "v0.15.0";
    sha256 = "1dfnhl4azm1lphkyg7i0kynh7gichbi73gp2w0ip4i8bnm0gmjvp";
  };
  mysql = {
    url = "https://github.com/crystal-lang/crystal-mysql.git";
    rev = "v0.14.0";
    sha256 = "16877vp8srq8yai8664y7dnkpz8xiaszkhnp6f65b1a3prwdyqxk";
  };
  optarg = {
    url = "https://github.com/amberframework/optarg.git";
    rev = "v0.9.3";
    sha256 = "0jnv0ndqmk4629igga9dgk45rdfdghdmg4bw98hca160fsjahgva";
  };
  pg = {
    url = "https://github.com/will/crystal-pg.git";
    rev = "v0.26.0";
    sha256 = "04fwbgrlf2nzma0p2c8ki7p8sk113jhziq2al3ivif2lpmhr39fy";
  };
  pool = {
    url = "https://github.com/ysbaddaden/pool.git";
    rev = "v0.3.0";
    sha256 = "0rn16fhp61zrwqjdyi957jinhffx4c7ga6yxj86x89zjgpln2lpa";
  };
  redis = {
    url = "https://github.com/stefanwille/crystal-redis.git";
    rev = "v2.8.3";
    sha256 = "04bjlqv70gg3nj35qq3by8hz69xwrq6cimfgj92qhdyahm0b0ab0";
  };
  shell-table = {
    url = "https://github.com/luckyframework/shell-table.cr.git";
    rev = "v0.9.3";
    sha256 = "046vymm2r37c6j5bqyjzxdgg5h62slsannzvfhbckkv2r9chwd3w";
  };
  slang = {
    url = "https://github.com/jeromegn/slang.git";
    rev = "v1.7.3";
    sha256 = "131wb7ajr9hsg4mb1rgpwnf7p2ljjnqphp19b59cahh02pkkj59b";
  };
  sqlite3 = {
    url = "https://github.com/crystal-lang/crystal-sqlite3.git";
    rev = "v0.19.0";
    sha256 = "1gaxd56cvh7gqyl5pmcxzi8nrnwcqpvplhxfzxj3fw4hk3l5kx4v";
  };
  teeplate = {
    url = "https://github.com/amberframework/teeplate.git";
    rev = "v0.11.2";
    sha256 = "0r48pp3yql1d3rza63dayq9wr9h40j2q4i664w9px0b20sy148s4";
  };
  yaml_mapping = {
    url = "https://github.com/crystal-lang/yaml_mapping.cr.git";
    rev = "v0.1.1";
    sha256 = "02py6lskdnkil0iqn578vdcda4il3xsaqpd7vpdvi848nklv0ph9";
  };
}
