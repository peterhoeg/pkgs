{
  lib,
  buildGoModule,
  fetchFromGitHub,
  runtimeShell,
}:

buildGoModule rec {
  pname = "bit";
  version = "0.5.12";

  src = fetchFromGitHub {
    owner = "chriswalz";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-d+I9gry3uCU9SK6IiX+zlE8txHrTOGAlKeT2daQxvFE=";
  };

  postPatch = ''
    substituteInPlace cmd/util.go \
      --replace-fail /bin/bash ${runtimeShell}
  '';

  vendorHash = "sha256-/jtuq6RnSuVts6NTK1Lxx4q+41eQgHR7vCf88TergZk=";

  ldflags = [ "-X main.version=${version}" ];

  # tests expect to be run from a checkout
  doCheck = false;

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md
  '';

  meta = with lib; {
    description = "Modern Git client in Go.";
    # license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
