{
  lib,
  buildGoModule,
  fetchFromGitHub,
  go-md2man,
  help2man,
  installShellFiles,
}:

buildGoModule rec {
  pname = "sping";
  version = "0.0.0.20220311";

  src = fetchFromGitHub {
    owner = "benjojo";
    repo = pname;
    rev = "a2f453d1c91d3cfdeba85d2ecb05b9b1f193f800";
    hash = "sha256-40o4zKxMO6XTgoNE7o2sfPE+f/9DUtCXwSaW5awE43Q=";
  };

  # dependencies are vendored
  vendorHash = null;

  nativeBuildInputs = [
    go-md2man
    help2man
    installShellFiles
  ];

  # tests require a live internet connection
  doCheck = false;

  # I don't know why help2man doesn't work...
  # help2man -o sping.1 $out/bin/sping
  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} *.md
    go-md2man -in README.md -out sping.1
    installManPage sping.1
  '';

  meta = with lib; {
    description = "Split ping, see what direction the loss or latency is on";
    homepage = "https://blog.benjojo.co.uk/";
    maintainers = with maintainers; [ peterhoeg ];
  };
}
