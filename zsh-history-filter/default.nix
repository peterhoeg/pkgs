{
  stdenvNoCC,
  lib,
  fetchFromGitHub,
}:

stdenvNoCC.mkDerivation {
  pname = "zsh-history-filter";
  version = "0.4.1.20210405";

  src = fetchFromGitHub {
    owner = "MichaelAquilina";
    repo = "zsh-history-filter";
    rev = "49d7987f093374fb72f0f7eef2d63e641085a226";
    hash = "sha256-DSq/4j4KEtirrjB3nW8fR7BOFtLdaLhlnIMv2A6Ec6s=";
  };

  strictDeps = true;
  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    install -Dm444 -t $out/share/zsh/zsh-history-filter *.zsh
    install -Dm444 -t $out/share/doc/zsh-history-filter *.md *.rst
  '';

  meta = with lib; {
    description = "Filter commands from zsh command history";
    license = with licenses; [
      gpl3
      mit
    ];
    platforms = platforms.unix;
  };
}
