# shellcheck shell=bash
postFixupPhases="${postFixupPhases:-} cleanScriptsPhase"

cleanScriptsPhase() {
  declare -a dirs
  for d in bin libexec; do
    # shellcheck disable=SC2154
    test -e "$out/$d" && dirs+=("$out/$d")
  done
  shfmt \
    --case-indent \
    --indent 2 \
    --language-dialect auto \
    --simplify \
    --space-redirects \
    --write \
    "${dirs[@]}"
}
