{
  makeSetupHook,
  shfmt,
}:

makeSetupHook {
  name = "clean-scripts-hook";
  propagatedBuildInputs = [ shfmt ];
} ./clean-scripts.sh
