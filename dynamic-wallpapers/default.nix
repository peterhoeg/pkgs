{
  stdenv,
  lib,
  fetchFromGitHub,
  fetchpatch,
  vips,
  withGnome ? false,
}:

let
  baseDir = "share/wallpapers";

in
stdenv.mkDerivation rec {
  pname = "dynamic-wallpapers";
  version = "20230523";

  src = fetchFromGitHub {
    owner = "saint-13";
    repo = "Linux_Dynamic_Wallpapers";
    rev = "45128514ae51c6647ab3e427dda2de40c74a40e5";
    hash = "sha256-gmGtu28QfUP4zTfQm1WBAokQaZEoTJ2jL/Qk4BUNrhU=";
  };

  # there is no point rebuilding this just because some transitive dependency was changed
  outputHashAlgo = "sha256";
  outputHashMode = "recursive";
  outputHash = "sha256-AbunTC1AC7IiLpn5MZ+OAX/xnuuFraxo4qWg8DGTOXk=";

  dontConfigure = true;

  dontBuild = true;

  nativeBuildInputs = [ vips ];

  installPhase =
    ''
      mkdir -p $out/${baseDir}
      test -d $out/share || mkdir -p $out/share
      ln -s $out/${baseDir} $out/share/backgrounds
      cp -r Dynamic_Wallpapers/* $out/${baseDir}
      rm $out/${baseDir}/*.xml

      pushd "$out/${baseDir}" >/dev/null

      shopt -s globstar

      _link() {
        local file="$1"
        local scheme="$2"

        if [ ! -e "$file" ]; then
          echo "File not found: $file"
          exit 77
        fi

        ln -s "$file" "$scheme"
      }

      for dir in **; do
        test -d "$dir" || continue
        echo "Directory: $dir"

        pushd "$dir" > /dev/null

        dark=0
        light=0
        dark_file=""
        light_file=""

        for f in *; do
          test -e "$f" || continue
          t=$(vips avg "$f" | cut -f1 -d '.')
          if [ "$dark" -eq 0 ] || [ "$t" -lt "$dark" ]; then
            dark=$t
            dark_file="$f"
          fi
          if [ "$light" -eq 0 ] || [ "$t" -gt "$light" ]; then
            light=$t
            light_file="$f"
          fi
        done

        _link "$dark_file" dark
        _link "$light_file" light

        popd >/dev/null
      done

      popd > /dev/null

    ''
    + lib.optionalString withGnome ''
      install -Dm444 -t $out/share/gnome-background-properties xml/*.xml

      for f in $out/share/gnome-background-properties/*.xml $out/${baseDir}/*.xml ; do
        substituteInPlace "$f" \
          --replace-fail /usr/share $out/share
      done
    '';

  passthru = {
    inherit baseDir;
    supportsDarkLight = true;
  };

  meta = with lib; {
    description = "Dynamic Wallpapers";
    license = licenses.free;
  };
}
