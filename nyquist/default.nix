{
  stdenv,
  lib,
  fetchurl,
  cmake,
  makeWrapper,
  unzip,
  jdk11_headless,
  alsaLib,
  useSystemLibs ? false, # doesn't detect portaudio, so build vendored version by default
  libsndfile,
  portaudio,
}:

let
  jre' = jdk11_headless;

in
stdenv.mkDerivation rec {
  pname = "nyquist";
  version = "3.17";

  src = fetchurl {
    url = "mirror://sourceforge/project/${pname}/${pname}/${version}/nyqsrc${
      lib.replaceStrings [ "." ] [ "" ] version
    }.zip";
    hash = "sha256-stkjeKKiniZZm/k13g7hRL4smv5UD9Gf80Poax1rDkg=";
  };

  nativeBuildInputs = [
    cmake
    makeWrapper
    jre' # we don't care about the IDE built with java, but we need it to build
    unzip
  ];

  buildInputs =
    [
      alsaLib
    ]
    ++ lib.optionals useSystemLibs [
      libsndfile
      portaudio
    ];

  cmakeFlags = [
    (lib.cmakeBool "USE_SOURCE_LIBS" useSystemLibs)
  ];

  installPhase = ''
    runHook preInstall

    dir=$out/share/${pname}

    mkdir -p $out/bin $dir

    install -Dm555 -t $out/libexec ny
    cp -r ../demos ../lib ../runtime $dir

    makeWrapper $out/libexec/ny $out/bin/ny \
      --suffix XLISPPATH : "$dir/runtime:$dir/lib"

    runHook postInstall
  '';

  meta = with lib; {
    description = "Language for sound synthesis and music composition";
    license = with licenses; [
      free
      lgpl2
    ];
    maintainers = with maintainers; [ peterhoeg ];
  };
}
