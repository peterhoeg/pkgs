{
  stdenv,
  resholve,
  lib,
  fetchFromGitHub,
  bash,
  runtimeShell,
  coreutils,
  curl,
  gawk,
  gnugrep,
}:

resholve.mkDerivation rec {
  pname = "pushover-cli";
  version = "1.2.3";

  src = fetchFromGitHub {
    owner = "aaronfagan";
    repo = pname;
    rev = "v" + version;
    sha256 = "sha256-9s1MIAMv6Ra4B24xMc+PX7Gokb8nyvOnxvWHaFK0eu0=";
  };

  # the part with install.sh invokes bash on a downloaded script which resholve
  # doesn't like and we don't have support for this anyway, so fuck it.
  postPatch = ''
    substituteInPlace pushover \
      --replace-fail '"$HOME/.pushover' '"''${XDG_CONFIG_HOME:-$HOME/.config}/pushover' \
      --replace-fail /usr/bin/ ""

    sed -i pushover \
      -e '/install\.sh/d'
  '';

  dontConfigure = true;

  dontBuild = true;

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin pushover
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md

    runHook postInstall
  '';

  solutions.default = {
    scripts = [ "bin/pushover" ];
    interpreter = runtimeShell;
    inputs = [
      coreutils
      curl
      gawk
      gnugrep
    ];
  };

  meta = with lib; {
    description = "PushOver CLI client";
    mainProgram = "pushover";
  };
}
