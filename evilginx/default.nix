{
  lib,
  buildGoModule,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "evilginx";
  version = "3.1.0";

  src = fetchFromGitHub {
    owner = "kgretzky";
    repo = "evilginx2";
    rev = "v${version}";
    hash = "sha256-ReGjGxy1AwoirgZ1orCyLyeon23JFFW2tF0xHPuPvH8=";
  };

  vendorHash = null;

  postInstall = ''
    doc=$out/share/doc/${pname}
    install -Dm444 -t $doc CHANGELOG* LICENSE* README*
    install -Dm444 -t $doc/examples/phishlets phishlets/*
  '';

  meta = with lib; {
    description = "MitM attack framework used for phishing login credentials along with session cookies";
    license = licenses.bsd3;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
