{
  stdenv,
  lib,
  fetchFromGitHub,
  python3Packages,
  wrapQtAppsHook,
  makeWrapper,
  qtbase,
  qtcharts,
}:

let
  pypkgs = python3Packages;

in
stdenv.mkDerivation rec {
  pname = "sparrow-wifi";
  version = "0.0.0.20230516";

  format = "other";

  src = fetchFromGitHub {
    owner = "ghostop14";
    repo = pname;
    rev = "3154f36913379efff502aeab7aa4a3d3a94b496e";
    hash = "sha256-jzCfcnqdX2Znvwv6UPKMNBIfm+4La91RZRPN5UGWkM4=";
  };

  dontWrapQtApps = true;

  nativeBuildInputs = [
    pypkgs.wrapPython
    wrapQtAppsHook
  ];

  buildInputs = [
    qtbase
    qtcharts
  ];

  propagatedBuildInputs = with pypkgs; [
    # beautifulsoup4
    # lxml
    # pexpect
    # psutil
    # pydbus
    # agps3
    dateutil
    gps3
    pyqt5
    pyqtchart
    requests
  ];

  dontBuild = true;

  installPhase = ''
    runHook preInstall

    dir=$out/lib/${pypkgs.python.libPrefix}/site-packages/${pname}
    doc=$out/share/doc/${pname}

    mkdir -p $out/bin $dir $doc
    cp -r * $dir/
    mv $dir/docs/* $dir/images $dir/*.md $doc/

    makeWrapper ${pypkgs.python.interpreter} $out/bin/sparrow-wifi \
      --add-flags $dir/sparrow-wifi.py \
      --prefix PYTHONPATH : $dir:$PYTHONPATH

    runHook postInstall
  '';

  postFixup = ''
    # wrapPythonPrograms
    # wrapPythonProgramsIn $dir
  '';

  doCheck = false;

  meta = with lib; {
    description = "Next-Gen GUI-based WiFi and Bluetooth Analyzer for Linux ";
    license = licenses.gpl3;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
  };
}
