{ lib, python3Packages }:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "PyKMIP";
  version = "0.9.1";

  src = pypkgs.fetchPypi {
    inherit pname version;
    sha256 = "1b9fz7r2cmgwxqrv1lh47pfhv2b813y6lkvksl0d7b9l987jjzar";
  };

  # pypkgs.enum34 is null on newer pythons
  postPatch = lib.optionalString (pypkgs.pythonAtLeast "3.5") ''
    sed -i setup.py -e '/enum34/d'
  '';

  propagatedBuildInputs = with pypkgs; [
    cryptography
    enum-compat
    enum34
    requests
    six
    sqlalchemy
  ];

  checkInputs = with pypkgs; [
    mock
    pytest
    testtools
  ];

  # we need to skip just the tests that require network
  doCheck = false;

  meta = with lib; {
    description = "A Python implementation of the KMIP specification";
    license = licenses.gpl3;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
