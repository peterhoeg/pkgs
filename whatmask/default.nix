{
  stdenv,
  lib,
  fetchurl,
  autoreconfHook,
}:

stdenv.mkDerivation rec {
  pname = "whatmask";
  version = "1.2";

  src = fetchurl {
    url = "http://downloads.laffeycomputer.com/current_builds/whatmask/whatmask-${version}.tar.gz";
    hash = "sha256-fcoDieIukOwbHBmaKYOIA6GumrNMCGqSY3m3ntsGnYk=";
  };

  postPatch = ''
    mv configure.in configure.ac
  '';

  nativeBuildInputs = [ autoreconfHook ];

  enableParallelBuilding = true;

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} \
      AUTHORS COPYING NEWS README*
  '';

  meta = with lib; {
    description = "Network subnet calculator";
    license = licenses.gpl2Only;
  };
}
