{
  stdenv,
  lib,
  fetchurl,
  autoPatchelfHook,
  unzip,
}:

stdenv.mkDerivation rec {
  pname = "samsung-magician";
  version = "2.0";

  src = fetchurl {
    url = "https://www.samsung.com/semiconductor/global.semi.static/Samsung_Magician_DC_Linux_64bit.zip";
    sha256 = "sha256-nc/A7P0PqSlieef9BOzCsDKWuHDo8U1QM2XByyKoEK4=";
  };

  sourceRoot = ".";

  nativeBuildInputs = [
    autoPatchelfHook
    unzip
  ];

  buildInputs = [ stdenv.cc.cc.lib ];

  installPhase = ''
    install -Dm555 -t $out/bin ./magician
  '';

  meta = with lib; {
    description = "Samsung Magician DC";
  };
}
