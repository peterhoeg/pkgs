{
  stdenv,
  lib,
  fetchFromGitHub,
  cmake,
  SDL2,
  SDL2_mixer,
}:

stdenv.mkDerivation rec {
  pname = "vvvvvv";
  version = "2.3.6";

  src = fetchFromGitHub {
    owner = "TerryCavanagh";
    repo = pname;
    rev = version;
    hash = "sha256-sLNO4vkmlirsqJmCV9YWpyNnIiigU1KMls7rOgWgSmQ=";
    fetchSubmodules = true;
  };

  # this will no longer be needed (or even work!) after 2.3.6
  postPatch = ''
    substituteInPlace src/SoundSystem.h \
      --replace-fail SDL_mixer.h SDL2/SDL_mixer.h
  '';

  sourceRoot = "./source/desktop_version";

  nativeBuildInputs = [ cmake ];

  buildInputs = [
    SDL2
    SDL2_mixer
  ];

  installPhase = ''
    install -Dm555 VVVVVV $out/bin/vvvvvv
    install -Dm444 -t $out/share/doc/${pname} ../{LICENSE*,README*}
  '';

  meta = with lib; {
    description = "Award-winning 2D indie platform game";
    homepage = "http://distractionware.com/";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
