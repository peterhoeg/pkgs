{
  lib,
  python3Packages,
  fetchFromGitHub,
  fetchurl,
}:

let
  pypkgs = python3Packages;

  ebcdic = pypkgs.buildPythonPackage rec {
    pname = "ebcdic";
    version = "1.1.1";

    format = "wheel";

    src = fetchurl {
      url = "https://files.pythonhosted.org/packages/0d/2f/633031205333bee5f9f93761af8268746aa75f38754823aabb8570eb245b/ebcdic-${version}-py2.py3-none-any.whl";
      hash = "sha256-M7TLcpvC0L9GzBhHsOWUaJfLjT9TUgxbmqX6mNfnNfE=";
    };
  };

  imapclient = pypkgs.imapclient.overrideAttrs (super: rec {
    version = "2.1.0";
    src = fetchFromGitHub {
      owner = "mjs";
      repo = "imapclient";
      rev = version;
      hash = "sha256-ndAEJwLS5lUlZwwB4LjOF8rzmDOrFPHX8194F5HEiP0=";
    };
  });

  compressed-rtf = pypkgs.buildPythonPackage rec {
    pname = "compressed-rtf";
    version = "1.0.6";

    src = pypkgs.fetchPypi {
      pname = "compressed_rtf";
      inherit version;
      hash = "sha256-wcgn8dEk0kYImBpW6LhpHrHyppp4zK1kQOfZL94Xgd0=";
    };

    doCheck = false;
  };

  lark-parser = pypkgs.buildPythonPackage rec {
    pname = "lark-parser";
    version = "0.12.0";

    src = pypkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-FZZ9sfEhQBPcplsRgHRQR7m+RX1z2iJPzaPZ3U6WoTg=";
    };
  };

  rtfde = pypkgs.buildPythonPackage rec {
    pname = "rtfde";
    version = "0.0.2";

    src = pypkgs.fetchPypi {
      pname = "RTFDE";
      inherit version;
      hash = "sha256-uGtdc0lQ/odFpbiRM/UFVCUtvWfG0bkmXiPuFA5+qKI=";
    };

    propagatedBuildInputs = with pypkgs; [
      lark-parser
      lxml
      oletools
    ];

    doCheck = false;
  };

in
pypkgs.buildPythonApplication rec {
  pname = "extract-msg";
  version = "0.36.1";

  src = pypkgs.fetchPypi {
    inherit version;
    pname = "extract_msg";
    hash = "sha256-VXKaWR/s2Xw8skqzdfD3YqNNuFCjlykWcMQo+7wlBPY=";
  };

  propagatedBuildInputs = with pypkgs; [
    beautifulsoup4
    chardet
    compressed-rtf
    ebcdic
    imapclient
    olefile
    rtfde
    six
    tzlocal
  ];

  pythonImportsCheck = [ "extract_msg" ];

  meta = with lib; {
    description = "Extracts emails and attachments saved in Microsoft Outlook's .msg files";
    license = licenses.gpl3;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
