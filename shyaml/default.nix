{
  lib,
  fetchFromGitHub,
  python3,
  git,
  writeText,
}:

# TODO: doesn't work

let
  pypkgs = python3.pkgs;

in
pypkgs.buildPythonApplication rec {
  pname = "shyaml";
  version = "0.6.2";

  format = "other";

  src = fetchFromGitHub {
    owner = "0k";
    repo = pname;
    rev = version;
    hash = "sha256-vYLAK1Od2wgzv+OVHb9tyNaLG+u8UR+dUymoE3nMz0I=";
  };

  pythonImportsCheck = [ "shyaml" ];

  # no tests from pypi
  doCheck = false;

  meta = with lib; {
    description = "Query YAML from shell scripts";
    broken = true;
  };
}
