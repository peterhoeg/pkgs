{
  stdenv,
  lib,
  fetchFromGitHub,
  zlib,
}:

stdenv.mkDerivation rec {
  pname = "balongflash";
  version = "20180611";

  src = fetchFromGitHub {
    owner = "forth32";
    repo = "balongflash";
    rev = "de76d92154a7035904b523e730366ac4890ff64a";
    sha256 = "16yj7sabmzqxzixyhclxpwpm8d3zqzs504kvdnspm8x3lhx8516h";
  };

  buildInputs = [ zlib ];

  enableParallelBuilding = true;

  installPhase = ''
    runHook preInstall

    install -Dm755 -t $out/bin balong_flash

    runHook postInstall
  '';
}
