{
  stdenv,
  lib,
  fetchFromGitHub,
}:

stdenv.mkDerivation rec {
  pname = "emacs-fancy-logos";
  version = "2023-04-23";

  # src = fetchurl {
  # src = fetchFromGitLab {
  src = fetchFromGitHub {
    owner = "egstatsml";
    repo = lib.replaceStrings [ "-" ] [ "_" ] pname;
    rev = "d37f57854fb1fab379cf5c39a7c554f4e23f1801";
    hash = "sha256-BirkM+QDsmCFZWQAiA7vN4dJ/NNyYDDMwO8VZ5hBD54=";
  };

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    runHook preInstall

    install -Dm444 -t $out/share/logos/emacs *.{png,svg}
    install -Dm444 -t $out/share/doc/${pname} *.md

    runHook postInstall
  '';

  meta = with lib; {
    description = "Emacs Fancy Logos";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
}
