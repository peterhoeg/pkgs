{
  lib,
  fetchFromGitHub,
  python3,
  writeText,
}:

let
  pypkgs = python3.pkgs;

  # json2xml wraps everything in a root element and as we cannot avoid it, make
  # it a required argument
  #
  # 1. arg: root element
  # 2. arg: file containing JSON
  #
  # Outputs XML to stdout

  bins = [
    (writeText "json2xml" ''
      #!${python3.interpreter}

      from json2xml import json2xml
      from json2xml.utils import readfromjson

      root = sys.argv[1]
      data = readfromjson(sys.argv[2])
      item = sys.argv[3]

      print(json2xml.Json2xml(data, wrapper=root, item_name=item, pretty=True, attr_type=False).to_xml())
    '')

    (writeText "json2xml-nowrapper" ''
      #!${python3.interpreter}

      from json2xml import json2xml
      from json2xml.utils import readfromjson

      data = readfromjson(sys.argv[1])
      item = sys.argv[2]

      print(json2xml.Json2xml(data, item_name=item, pretty=True, attr_type=False).to_xml())
    '')
  ];

in
pypkgs.buildPythonApplication rec {
  pname = "json2xml";
  version = "3.20.0.1";

  src = fetchFromGitHub {
    owner = "peterhoeg";
    # owner = "vinitkumar";
    repo = "json2xml";
    rev = "4341683c6d27e345ee441f6ba04edfcc9accc323";
    hash = "sha256-TbnA21hBTfcKhA+UvD8wvZp84KoVRlWbUSphFlO2vxg=";
  };

  # version constraints are pretty strict, so just relax them
  postPatch = ''
    substituteInPlace requirements.in \
      --replace-fail '==' '>='
  '';

  propagatedBuildInputs = with pypkgs; [
    dicttoxml
    defusedxml
    requests
    xmltodict
  ];

  checkInputs = with pypkgs; [ pytest ];

  postInstall = lib.concatMapStringsSep "\n" (e: ''
    install -Dm555 ${e} $out/bin/${e.name}
  '') bins;

  pythonImportsCheck = [ "json2xml" ];

  # no tests from pypi
  doCheck = false;

  meta = with lib; {
    description = "Convert JSON to XML";
  };
}
