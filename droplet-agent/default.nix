{
  lib,
  buildGoModule,
  fetchFromGitHub,
  runtimeShell,
}:

buildGoModule rec {
  pname = "droplet-agent";
  version = "1.2.3";

  src = fetchFromGitHub {
    owner = "digitalocean";
    repo = pname;
    rev = "refs/tags/${version}";
    hash = "sha256-DqZdRpUQkHsow/CXw3ymi3SdnCuPOlE3ttp0l1eWUP0=";
  };

  postPatch = ''
    substituteInPlace packaging/syscfg/systemd/droplet-agent.service \
      --replace-fail /opt/digitalocean $out
  '';

  vendorHash = null;

  ldflags = [ "-X main.version=${version}" ];

  postInstall = ''
    mv $out/bin/agent $out/bin/droplet-agent
    install -Dm444 -t $out/share/doc/${pname} *.md
    install -Dm444 -t $out/lib/systemd/system packaging/syscfg/systemd/droplet-agent.service
  '';

  # checks require network
  doCheck = false;

  meta = with lib; {
    description = "Digital Ocean Droplet Agent for web console";
    license = licenses.asl20;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
