{
  buildGoModule,
  lib,
  fetchFromGitHub,
  iperf3,
}:

buildGoModule rec {
  pname = "iperf3_exporter";
  version = "0.1.13.20211018";

  src = fetchFromGitHub {
    owner = "edgard";
    repo = pname;
    rev = "9630a7b44c9ff67549fdd4873b016e3b7f76c57e";
    hash = "sha256-r6es3qXJ3Yqrn+CsXO72W2rFUMOBFd/IzaXt+p2u2LI=";
  };

  vendorHash = "sha256-SJh1PYgifnPmygPLj9ufG6QPeAF7tL+Lnd9SOvhMhSI=";

  postPatch = ''
    substituteInPlace iperf3_unix.go \
      --replace-fail '"iperf3"' '"${lib.getBin iperf3}/bin/iperf3"'
  '';

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} *.md
  '';

  meta = with lib; {
    description = "Prometheus iperf3 exporter";
  };
}
