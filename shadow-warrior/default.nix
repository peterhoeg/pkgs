# 32bit

{
  stdenv,
  lib,
  requireFile,
  runtimeShell,
  autoPatchelfHook,
  makeWrapper,
  unzip,
  alsaLib,
  dbus,
  libGL,
  libX11,
}:

stdenv.mkDerivation rec {
  pname = "shadow-warrior";
  version = "1.0.16382";

  src = requireFile rec {
    name = "shadow_warrior_classic_redux_en_${lib.replaceStrings [ "." ] [ "_" ] version}.sh";
    sha256 = "0d7bhif7ivp3nhhzrjzy8pwr9jvcz3ismk4rnpx4czcj4ska8fdz";
    message = ''
      nix-prefetch-url file://\$PWD/${name}
    '';
  };

  nativeBuildInputs = [
    autoPatchelfHook
    makeWrapper
    unzip
  ];

  buildInputs = [
    libGL
    libX11
  ];

  unpackPhase = ''
    runHook preUnpack

    unzip -q -d . ${src} || true

    runHook postUnpack
  '';

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    runHook preInstall

    dir=$out/opt/${pname}

    mkdir -p $out/bin $dir

    mv data/noarch/game/* $dir

    makeWrapper $dir/sw $out/bin/sw \
      --run "cd $dir"

    rm $dir/libs/libasound.*

    ln -s ${lib.getLib dbus}/lib/libdbus-1.so.3        $dir/libs/
    ln -s ${lib.getLib alsaLib}/lib/libasound.so.2     $dir/libs/
    ln -s ${lib.getLib alsaLib}/lib/libasound.so.2.0.0 $dir/libs/

    runHook postInstall
  '';

  meta = with lib; {
    description = "Shadow Warrior Classic Redux";
    license = licenses.unfree;
    maintainers = with maintainers; [ peterhoeg ];
    broken = true; # just hangs with a blank window https://www.gog.com/forum/shadow_warrior_series/shadow_warrior_classic_redux_on_linux
  };
}
