{
  stdenv,
  lib,
  fetchurl,
  fetchFromGitHub,
  pkg-config,
  python3,
  # , waf
  unzip,
  autoPatchelfHook,
  makeWrapper,
  symlinkJoin,
  curl,
  fontconfig,
  freetype,
  glew,
  libGL,
  libjpeg_turbo,
  libpng,
  libtheora,
  libvorbis,
  lua5_4,
  minizip,
  openal,
  openssl,
  sqlite,
  SDL2,
  zlib,
  xorg,
  baseDir ? "/.local/share/etlegacy",
  withAssets ? true,
  withClient ? true,
}:

# 32bit

let
  inherit (lib) optional optionalString replaceStrings;

  binName = "xash3d";

  xash3d = stdenv.mkDerivation rec {
    pname = "xash3d";
    version = "2021-12-28";

    src = fetchFromGitHub {
      owner = "FWGS";
      repo = "xash3d-fwgs";
      rev = "7cb06956c23a439c262eed89d5611333240266f8";
      sha256 = "sha256-NnPNtn84OwvaTeAeleRUFwon+w32IuXO3+tEhtm/Eog=";
      fetchSubmodules = true;
    };

    postPatch = ''
      patchShebangs waf
    '';

    configurePhase = ''
      runHook preConfigure

      ./waf configure -T release

      runHook postConfigure
    '';

    buildPhase = ''
      runHook preBuild

      ./waf build

      runHook postBuild
    '';

    installPhase = ''
      runHook preInstall

      ./waf install --destdir=.tmp

      install -Dm555 -t $out/opt/${pname} .tmp/${binName}
      install -Dm444 -t $out/opt/${pname} .tmp/*.so
      install -Dm444 -t $out/share/doc/${pname} *.md

      mkdir -p $out/bin
      makeWrapper $out/opt/${pname}/${binName} $out/bin/${binName} \
        --run "cd $out/opt/${pname}"

      runHook postInstall
    '';

    buildInputs = [
      fontconfig
      freetype
      glew
      libGL
      SDL2
    ];

    nativeBuildInputs = [
      makeWrapper
      pkg-config
      python3
    ];

    meta = with lib; {
      description = "XASH3d";
      license = licenses.gpl3Plus;
      broken = true;
    };
  };

  # etlegacy = symlinkJoin {
  #   name = "etlegacy${optionalString (!withClient) "-server"}-${version}";
  #   paths = [ etlegacy-unwrapped ] ++ optional withAssets etlegacy-assets;
  #   nativeBuildInputs = [ makeWrapper ];
  #   postBuild = ''
  #     mkdir $out/bin
  #     for f in ${optionalString withClient "etl"} etlded; do
  #       makeWrapper ${etlegacy-unwrapped}/libexec/$f $out/bin/$f \
  #         --argv0 $f \
  #         --add-flags "+set fs_basepath $out/share/etlegacy"
  #     done
  #   '';
  # };

in
xash3d
