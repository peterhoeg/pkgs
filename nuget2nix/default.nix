{
  lib,
  rustPlatform,
  fetchFromGitHub,
}:

rustPlatform.buildRustPackage rec {
  pname = "nuget2nix";
  version = "20220209";

  src = fetchFromGitHub {
    owner = "winterqt";
    repo = pname;
    rev = "d9a28389d93e153047e517738671c9efe35dbd24";
    sha256 = "sha256-SNZl+c2AB9d5dxg9JymjUH8cgKae2r9/CocvyS0Q/DM=";
  };

  cargoSha256 = "sha256-m17cBGbzPrNiCQx98q/IgZreuqIHKVLjp1y4+7l/p7g=";

  meta = with lib; {
    description = "nuget to nix";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
