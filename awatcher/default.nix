{
  rustPlatform,
  lib,
  fetchFromGitHub,
  pkg-config,
  dbus,
  openssl,
}:

# 1. we have git dependencies (ie, non-released) so we need to fiddle around with the cargo lock and
# manually specifying the checksums for the depedencies.
#
# 2. at some point there was no Cargo.lock in the download, so we had to fetch that manually, but
# that is no longer (0.3.0) the case.

rustPlatform.buildRustPackage rec {
  pname = "awatcher";
  version = "0.3.0";

  src = fetchFromGitHub {
    owner = "2e3s";
    repo = pname;
    rev = "v" + version;
    hash = "sha256-G7UH2JcKseGZUA+Ac431cTXUP7rxWxYABfq05/ENjUM=";
  };

  # See comment above
  # cargoHash = "sha256-11111111a93ea8c9ee3db4b659467578e7eedfcb0e6=";
  cargoLock = {
    lockFile = "${src}/Cargo.lock";
    outputHashes = {
      "aw-client-rust-0.1.0" = "sha256-yliRLPM33GWTPcNBDNuKMOkNOMNfD+TI5nRkh+5YSnw=";
    };
  };

  nativeBuildInputs = [ pkg-config ];

  buildInputs = [
    dbus
    openssl
  ];

  meta = {
    description = "Activity and idle watchers (with wayland support)";
    license = lib.licenses.gpl3Only;
    maintainers = with lib.maintainers; [ peterhoeg ];
    mainProgram = "awatcher";
    inherit (src.meta) homepage;
  };
}
