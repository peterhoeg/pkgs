require "option_parser"

module Check
  class Base
    def run
      options = {} of Symbol => (String | Bool | Int32)

      OptionParser.parse! do |parser|
        parser.on("-c CUSTOMER", "--customer CUSTOMER", "Name of customer") do |opt|
          options[:customer] = opt
        end

        parser.on("-h", "--help", "Show this help") do
          puts parser
          exit 0
        end
      end
    end
  end
end
