require "http/client"
require "json"
require "json_mapping"
require "uri"

NAME    = "check_solar_data.txt"
HEADERS = HTTP::Headers{
  "X-Name"  => ARGV.first,
  "X-Token" => ARGV.last,
}
PARAMS = [
  "format=i",
  "start_time=#{(Time.local - 5.minutes).to_s("%Y-%m-%d%20%H:%M:00")}",
].join("&")
URL = URI.parse "https://api.cleantech.solar/v1/station_reads?#{PARAMS}"

class Meter
  JSON.mapping(
    id: Int32,
    pac: Int32?
  )
end

class DataPoint
  JSON.mapping(
    reportedAt: Int32,
    station: String,
    ghi: Int32?,
    meters: Array(Meter)
  )
end

class DataPointList
  JSON.mapping(
    data: Array(DataPoint)
  )
end

begin
  file_name = [ENV["TMP"]? ? ENV["TMP"] : "/tmp", NAME].join(File::SEPARATOR)

  unless File.exists?(file_name)
    File.write(file_name, 0)
    puts "UNKNOWN: This looks like the first run"
    exit 3
  end
  old_value = File.read_lines(file_name).first.to_i

  response = HTTP::Client.get url: URL, headers: HEADERS
  raise "Unable to connect" unless response.status_code == 200

  data_points = DataPointList.from_json(response.body).data
  raise "Empty dataset" if data_points.empty?
  new_value = data_points.last.reportedAt.to_i
  raise "Time-stamp not incrementing|old=#{old_value};new=#{new_value}" unless new_value > old_value
  File.write(file_name, new_value)
rescue e
  puts "CRITICAL: #{e.message}"
  exit 2
end

puts "OK: All good|old=#{old_value};new=#{new_value}"
exit 0
