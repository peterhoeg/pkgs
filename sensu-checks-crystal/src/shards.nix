{
  ameba = {
    owner = "veelenga";
    repo = "ameba";
    rev = "v0.9.0";
    sha256 = "01713qc7xhahw66avbz34zbvmrc66jyy797p6z3cnc3d0vc0wkb3";
  };
  callback = {
    owner = "mosop";
    repo = "callback";
    rev = "v0.6.3";
    sha256 = "145plarfrrl2gdnph94dgw3lc03fxfzjbvsq79w0dbg1m58x7jki";
  };
  cli = {
    owner = "mosop";
    repo = "cli";
    rev = "v0.7.0";
    sha256 = "00q1n0gq9brxmrnhc079yancnfmxd7bbkv0y7ai9km1fv1bp73mj";
  };
  optarg = {
    owner = "mosop";
    repo = "optarg";
    rev = "v0.5.8";
    sha256 = "1z9fza6nxb3g5fih3d10chwgkldw35ima6cv413900s8bfz1s8xq";
  };
  string_inflection = {
    owner = "mosop";
    repo = "string_inflection";
    rev = "v0.2.1";
    sha256 = "10vkr28h7n53ijjv57ldxhh473086qg313lzs55a7wsh0zgc104m";
  };
}
