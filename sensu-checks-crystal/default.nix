{
  stdenv,
  lib,
  crystal,
  openssl,
  zlib,
}:

crystal.buildCrystalPackage rec {
  pname = "sensu-checks-crystal";
  version = "unstable";

  buildInputs = [
    openssl
    zlib
  ];

  src = ./src;

  doCheck = false;

  doInstallCheck = false;

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin bin/*

    runHook postInstall
  '';
}
