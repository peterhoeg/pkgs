{
  stdenv,
  lib,
  fetchurl,
  makeWrapper,
  zulu,
}:

let
  jre = zulu;
  lnf = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";

in
stdenv.mkDerivation rec {
  pname = "projectlibre";
  version = "1.9.3";

  src = fetchurl {
    url = "mirror://sourceforge/${pname}/ProjectLibre/${version}/${pname}.jar";
    sha256 = "sha256-m/ap7XoXWX1DbGeKEmn5PVJ+0yyN7hNIvMjETajodxc=";
  };

  nativeBuildInputs = [ makeWrapper ];

  dontUnpack = true;

  dontBuild = true;

  installPhase = ''
    jar=$out/share/java/${pname}-${version}.jar

    mkdir -p $out/bin
    install -Dm444 ${src} $jar

    makeWrapper ${jre}/bin/java $out/bin/${pname} \
      --run "mkdir -p \''${XDG_CONFIG_HOME:-\$HOME/.config}/projectlibre" \
      --set-default JAVA_HOME ${jre} \
      --add-flags "-Djava.util.prefs.systemRoot=\''${XDG_CONFIG_HOME:-\$HOME/.config}/projectlibre" \
      --add-flags "-Dswing.defaultlaf=${lnf}" \
      --add-flags "-Dswing.crossplatformlaf=${lnf}" \
      --add-flags "-jar $jar"
  '';

  meta = with lib; {
    description = "Project Management";
  };
}
