{
  stdenv,
  lib,
  fetchurl,
  platformio,
}:

stdenv.mkDerivation rec {
  name = "teensy-loader-udev-rules";

  src = fetchurl {
    url = "https://www.pjrc.com/teensy/49-teensy.rules";
    hash = "sha256-e01tgQKlJvBb5bet8ZMlZVuwxFziKRr+eA0wCYgLdOE=";
  };

  dontUnpack = true;

  installPhase = ''
    runHook preInstall

    install -Dm444 ${src} $out/lib/udev/rules.d/${src.name}

    runHook postInstall
  '';

  meta = {
    description = "Teensy Loader udev rules";
    inherit (platformio.meta) homepage license platforms;
  };
}
