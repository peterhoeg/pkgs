#!/usr/bin/env bash

set -eEuo pipefail

MAKEFILE=pkgs.mk
NIXFILE=default.nix

TAB="$(printf '\t')"

packages=()

cat >$NIXFILE <<_EOF
{ callPackage, pkgs, ... }:

{
_EOF

rm -f $MAKEFILE
for d in *; do
  # bash tests will automatically derefence symlinks so ignore anything that is
  test -L $d && continue

  if [ -d $d ]; then
    packages+=($d)

    cat >>$MAKEFILE <<_EOF
.PHONY: $d
$d: clean
_EOF

    if $(grep -q qtbase $d/default.nix); then
      cat >>$MAKEFILE <<_EOF
$TAB\$(call QTBUILD,$d)
_EOF
    elif $(grep -q '# 32bit' $d/default.nix); then
      cat >>$MAKEFILE <<_EOF
$TAB\$(call BUILD32,$d)
_EOF
    else
      cat >>$MAKEFILE <<_EOF
$TAB\$(call BUILD,$d)
_EOF
    fi
    echo "" >>$MAKEFILE

    cat >>$NIXFILE <<_EOF
  $d = callPackage ./$d { inherit pkgs; };
_EOF
  fi
done

echo "all: ${packages[@]}" >>$MAKEFILE

echo "}" >>$NIXFILE
