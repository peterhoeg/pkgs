{
  stdenv,
  lib,
  fetchurl,
}:

stdenv.mkDerivation rec {
  pname = "mime-vcard-filter";
  version = "0.0.1";

  # src = fetchurl {
  # url = "http://www.davep.org/mutt/mutt.vcard.filter";
  # sha256 = "0v3zjx01cc7300i7isyfclqrksww8z6d311s3k970sxky48s0fsb";
  # };

  src = ./mutt.vcard.filter;

  dontUnpack = true;
  dontBuild = true;

  installPhase = ''
    runHook preInstall

    install -Dm755 ${src} $out/libexec/mutt.vcard.filter

    runHook postInstall
  '';

  meta = with lib; {
    description = "MIME vcard filter for Mutt";
  };
}
