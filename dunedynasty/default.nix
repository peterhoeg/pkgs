{
  stdenv,
  lib,
  fetchFromGitHub,
  cmake,
  pkg-config,
  allegro5,
  alsa-lib,
  enet,
  libGL,
  libmad,
  openssl,
  zlib,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "dune-dynasty";
  version = "1.6.1";

  src = fetchFromGitHub {
    owner = "gameflorist";
    repo = "dunedynasty";
    rev = "v${finalAttrs.version}";
    hash = "sha256-6Yrm1xIrhn86aZxf6UdM0SXpeCe0dc+gAbvRhEafnL4=";
  };

  buildInputs = [
    allegro5
    alsa-lib
    enet
    libGL
    libmad
    openssl
    zlib
  ];

  nativeBuildInputs = [
    cmake
    pkg-config
  ];

  env.NIX_CFLAGS_COMPILE = "-Wno-format-security";

  cmakeFlags = [
    "-DOpenGL_GL_PREFERENCE=GLVND"
  ];

  meta = with lib; {
    description = "Dune 2 re-created";
    license = licenses.gpl2Only;
    maintainers = with maintainers; [ peterhoeg ];
  };
})
