{
  stdenv,
  lib,
  fetchurl,
}:

# https://stackoverflow.com/a/12486703
stdenv.mkDerivation rec {
  pname = "exe-info";
  version = "0.0.1";

  src = fetchurl {
    url = "https://gist.githubusercontent.com/djhaskin987/d1860a7d98193913bcfa/raw/c12ddd31a4c4593d33fa352358ba6b575aa228d6/get_exe_version.c";
    hash = "sha256-fRIGL/PqzymsWref3QrekTPOfX4zEExGM6WMSRyjw6k=";
  };

  buildCommand = ''
    gcc $src -o ${src.name} -Wno-unused-result
    install -Dm555 ${src.name} $out/bin/${pname}
  '';

  meta = with lib; {
    description = "Show Windows PE executable version info";
  };
}
