{
  stdenv,
  lib,
  fetchFromGitHub,
  nix,
}:

stdenv.mkDerivation rec {
  pname = "nix-upgrade-scripts";
  version = "0.0.4";

  src = fetchFromGitHub {
    owner = "peterhoeg";
    repo = "nix-upgrade-scripts";
    rev = "v${version}";
    sha256 = "13v91nniwrs1fr92msrq6sq7vy1whrlxqd1ll4q3p1nrnbzpnxkn";
  };

  installPhase = ''
    runHook preInstall

    install -Dm544 -t $out/bin bin/*

    runHook postInstall
  '';

  meta = with lib; {
    description = "Scripts for making package upgrades easier";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.unix;
  };
}
