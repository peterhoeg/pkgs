{
  stdenv,
  lib,
  fetchFromGitHub,
  cmake,
  patchelf,
}:

{
  uavs3d = stdenv.mkDerivation (finalAttrs: {
    pname = "uavs3d";
    version = "1.1.20230223";

    src = fetchFromGitHub {
      owner = "uavs3";
      repo = "uavs3d";
      rev = "1fd04917cff50fac72ae23e45f82ca6fd9130bd8";
      hash = "sha256-ZSuFgTngOd4NbZnOnw4XVocv4nAR9HPkb6rP2SASLrM=";
    };

    nativeBuildInputs = [
      cmake
      patchelf
    ];

    cmakeFlags = [
      (lib.cmakeBool "COMPILE_10BIT" true)
      (lib.cmakeBool "BUILD_SHARED_LIBS" true)
    ];

    # 1. The actual binary isn't installed by default
    # 2. It contains an invalid RPATH
    # 3. The binary name doesn't match the documentation
    postInstall = ''
      install -Dm777 uavs3dec $out/bin/${finalAttrs.meta.mainProgram}
      patchelf --set-rpath ${
        lib.makeLibraryPath [ (placeholder "out") ]
      } $out/bin/${finalAttrs.meta.mainProgram}
    '';

    meta = {
      description = "AVS3 decoder";
      mainProgram = "uavs3d";
      license = lib.licenses.free;
      maintainers = with lib.maintainers; [ peterhoeg ];
      platforms = lib.platforms.all;
    };
  });
}
