{
  lib,
  bundlerApp,
  ruby,
}:

bundlerApp rec {
  pname = "ros";
  gemdir = ./.;
  exes = [ "ros" ];

  installManpages = false;

  meta = with lib; {
    description = "Rails on Services - CLI";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.unix;
    broken = true;
  };
}
