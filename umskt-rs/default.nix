{
  stdenv,
  lib,
  fetchurl,
  fetchFromGitHub,
  rustPlatform,
}:

let
  keysJson = fetchurl {
    url = "https://raw.githubusercontent.com/UMSKT/UMSKT/master/keys.json";
    hash = "sha256-+Zpm0T3nRZ9mtoADn9jA7pZICPo5IhaiDurbPBUue3s=";
  };

in
rustPlatform.buildRustPackage rec {
  pname = "umskt-rs";
  version = "20230930";

  src = fetchFromGitHub {
    owner = "anpage";
    repo = "umskt-rs";
    rev = "80e5da31210bb6cd7ceb8135a127915b95f1aa8b";
    hash = "sha256-/PeMlM0WxSo5RtTCueKLKuvt1k2iPy7pED5RKLvN4KA=";
  };

  cargoHash = "sha256-Nkp1HNlpk6J0NpJ07lZsDAG8uvnEvw84Me9BNQhiGbc=";

  postPatch = ''
    cp ${keysJson} keys.json
  '';

  doCheck = false;

  meta = with lib; {
    description = "XP key generator";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
    mainProgram = "xpkey";
  };
}
