{
  stdenv,
  lib,
  fetchFromGitHub,
  musl,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "espack";
  version = "1.2";

  src = fetchFromGitHub {
    owner = "ventoy";
    repo = "espack";
    rev = "v${finalAttrs.version}";
    hash = "sha256-fDWy324jG79Qf6jQXsWTVW8buiesNqzT6Gt3eokbXzM=";
  };

  # will segfault if not statically built
  buildPhase = ''
    runHook preBuild

    SRCS="./src/fat_io_lib/release/fat*.c ./src/espack.c ./src/crc32.c ./src/espack_linux.c"

    gcc -specs "${lib.getDev musl}/lib/musl-gcc.specs" \
      -static -O2 -Wall -Wno-unused-function -D_FILE_OFFSET_BITS=64 $SRCS -I./src/ -I./src/fat_io_lib/release -o espack64

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

    install -Dm555 espack64 $out/bin/${finalAttrs.meta.mainProgram}

    runHook postInstall
  '';

  buildInputs = [ musl ];

  meta = {
    description = "Combine .efi files into a .img for use by ventoy";
    mainProgram = "espack";
    license = lib.licenses.free;
    maintainers = with lib.maintainers; [ peterhoeg ];
    platforms = lib.platforms.all;
  };
})
