{
  stdenv,
  lib,
  fetchFromGitHub,
  cmake,
  makeWrapper,
  pkg-config,
  ncurses,
  SDL_compat,
  SDL_image,
  xorg,
  zlib,

  tree,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "descent3";
  version = "1.4.999.20240429";

  src = fetchFromGitHub {
    owner = "DescentDevelopers";
    repo = "descent3";
    # rev = "v${finalAttrs.version}";
    rev = "aa7407a8d53367767e16bd1adcfd2ec005a47826";
    hash = "sha256-Dfn8hVnWDOLN+Wsp8h06oA4UrXhxPtIbmWNaH+8mJl8=";
  };

  buildInputs = [
    ncurses
    SDL_compat
    SDL_image
    xorg.libXext
    zlib
  ];

  nativeBuildInputs = [
    cmake
    makeWrapper
    pkg-config
    tree
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin Descent3/Descent3
    install -Dm444 -t $out/share/doc/${finalAttrs.pname} ../*.md

    runHook postInstall
  '';

  meta = with lib; {
    description = "Descent 3";
    license = licenses.gpl3Only;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
})
