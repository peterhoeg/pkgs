{
  stdenv,
  gcc9Stdenv,
  gcc10Stdenv,
  useGoldLinker,
  lib,
  fetchurl,
  fetchFromGitHub,
  autoPatchelfHook,
  writeShellScript,
  makeBinaryWrapper,
  dpkg,
  rpm,
  rpmextract,
  file,
  formats,
  coreutils,
  openssl,
}:

let
  pname = "hp-ams";

  # stdenv' = gcc10Stdenv;
  stdenv' = gcc9Stdenv;
  # stdenv' = useGoldLinker stdenv;

  wrapper' = makeBinaryWrapper;

  meta = with lib; {
    description = "HPE Agentless Management Service daemon";
    license = licenses.unfree;
    mainProgram = "hp-ams";
  };

  # we could override net-snmp and then apply the patches from hp-ams, but this is just easier
  netSnmpSrc = fetchurl {
    url = "mirror://sourceforge/net-snmp/net-snmp-5.7.3.tar.gz";
    hash = "sha256-Eu+JYTx3B9yW0TM18VPBkh78nWHTcI7wnz/EpwFPtPA=";
  };

  # we need *something* to provide some output
  dpkgQuery = writeShellScript "dpkg-query" ''
    set -eEuo pipefail

    echo "${pname} @version@"
  '';

  customFiles = ''
    install -Dm555 ${dpkgQuery} $out/libexec/${dpkgQuery.name}
    substituteInPlace $out/libexec/${dpkgQuery.name} \
      --subst-var version

    mv $out/sbin/amsHelper $out/libexec
    makeWrapper $out/libexec/amsHelper $out/bin/hp-ams \
      --prefix PATH : $out/libexec

    if [ -d $out/share/usr ]; then
      mv $out/share/usr/* $out/share
      rmdir $out/share/usr
    fi
  '';

  # the ilo5 version is called amsd
  binaryDebian = stdenv'.mkDerivation rec {
    inherit pname meta;
    version = "2.8.3-3056";

    src = fetchurl {
      url = "https://downloads.linux.hpe.com/SDR/repo/mcp/pool/non-free/hp-ams_${version}.1ubuntu16_amd64.deb";
      hash = "sha256-6EwGC1IsMX1/aN/Qp2+NK+dvyXYsZXsm8E3uHfQSXik=";
    };

    unpackPhase = ''
      runHook preUnpack

      dpkg -x ${src} .

      runHook postUnpack
    '';

    dontBuild = true;

    buildInputs = [
      stdenv'.cc.cc.lib
      openssl
    ];

    nativeBuildInputs = [
      autoPatchelfHook
      dpkg
      wrapper'
    ];

    installPhase = ''
      runHook preInstall

      mkdir -p $out
      cp -r sbin usr/share/* $out

      ${customFiles}

      runHook postInstall
    '';

    passthru.from = "debian";
  };

  binaryRhel = stdenv'.mkDerivation rec {
    inherit pname meta;
    version = "2.10.4-885.5";

    src = fetchurl {
      url = "https://downloads.hpe.com/pub/softlib2/software1/pubsw-linux/p922329456/v195164/hp-ams-${version}.rhel8.x86_64.rpm";
      hash = "sha256-oxq/7jfTaFiPIIXOcP6UoTYHNtAnJcXbfyQuN6jeLoA=";
    };

    unpackPhase = ''
      runHook preUnpack

      rpmextract ${src}

      runHook postUnpack
    '';

    dontBuild = true;

    buildInputs = [
      stdenv'.cc.cc.lib
      openssl
    ];

    nativeBuildInputs = [
      autoPatchelfHook
      wrapper'
      rpmextract
    ];

    runtimeDependencies = [ rpm ];

    installPhase = ''
      runHook preInstall

      mkdir -p $out/lib
      cp -r sbin usr/share/* $out
      for f in librpm librpmio; do
        ln -s ${lib.getLib rpm}/lib/$f.so.9 $out/lib/$f.so.8
      done

      rm -rf $out/smartupdate

      ${customFiles}

      runHook postInstall
    '';

    passthru.from = "rhel";
  };

  fromSource = stdenv'.mkDerivation rec {
    inherit pname meta;
    version = "2.5.1";

    src = fetchFromGitHub {
      owner = "marker55";
      repo = "hp-ams";
      rev = "hp-ams-${version}";
      hash = "sha256-wCd2AwV42Iz/6FezM/FQRIqhtbA6+KyvtRUyDrzyl5M=";
    };

    # NOTE: everything put into UNITDIR is blown away in postInstall anyway as
    # we create our own which we need for the binary version anyway
    postPatch = ''
      substituteInPlace Makefile \
        --replace-fail '$(NETSNMP).tar.gz' ${netSnmpSrc} \
        --replace-fail opt/hp share/doc \
        --replace-fail /usr/local $out \
        --replace-fail /var/net-snmp /var/lib/hp-ams

      sed -i Makefile \
        -e 's@^SYSTEMD.*@SYSTEMD=1@' \
        -e "s@^UNITDIR.*@UNITDIR=$out/etc/systemd/system@"

      substituteInPlace cpqHost/libcpqhost/distro.c \
        --replace-fail 'unknown' NixOS \
        --replace-fail /etc/lsb-release /etc/os-release

      substituteInPlace recorder/crash.c \
        --replace-fail /tmp/hp-ams.log /var/log/hp-ams/hp-ams.log
    '';

    postInstall = ''
      mv $out/usr/* $out/share
      rmdir $out/usr
      rm -rf $out/{etc,opt}

      ${customFiles}
    '';

    buildInputs = [ openssl ];

    nativeBuildInputs = [
      file
      wrapper'
    ];

    # we need to wait for net-snmp to build, so no parallel building for us
    enableParallelBuilding = false;

    passthru.from = "source";
  };

in
{
  inherit binaryDebian binaryRhel fromSource;
}
