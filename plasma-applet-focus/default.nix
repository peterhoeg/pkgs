{
  lib,
  stdenv,
  fetchFromGitLab,
  plasma-framework,
}:

stdenv.mkDerivation rec {
  pname = "plasma-applet-focus";
  version = "1.5.5";

  src = fetchFromGitLab {
    owner = "divinae";
    repo = "focus-plasmoid";
    rev = "v${version}";
    hash = "sha256-t/WcbIPjR+IdUkqUaz53ZePXwjnyAnqLK1zIIKG1+VI=";
  };

  postPatch = ''
    for f in package/contents/{config/main.xml,ui/configGeneral.qml}; do
      substituteInPlace $f \
        --replace-fail /usr/share /run/current-system/sw/share
    done
  '';

  dontBuild = true;

  installPhase = ''
    runHook preInstall

    plasmapkg2 --install ./package --packageroot $out/share/plasma/plasmoids

    runHook postInstall
  '';

  nativeBuildInputs = [ plasma-framework ];

  dontWrapQtApps = true;

  meta = with lib; {
    description = "A simple pomodoro KDE plasmoid";
    license = licenses.gpl3Only;
    maintainers = with maintainers; [ peterhoeg ];
    inherit (plasma-framework.meta) platforms;
  };
}
