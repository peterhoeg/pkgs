{
  lib,
  python3,
  fetchFromGitHub,
  writeShellScript,
  sg3_utils,
}:

let
  pypkgs = python3.pkgs;

  version = "0.7.4";

  src = fetchFromGitHub {
    owner = "williballenthin";
    repo = "python-evtx";
    rev = "v" + version;
    hash = "sha256-sStef0Yj6yZkzc+PIsj7jGZ9PRYWT1pEdTN5yLFrzYs=";
  };

  module = pypkgs.buildPythonPackage rec {
    pname = "python-evtx";
    inherit version src;

    postPatch = ''
      substituteInPlace setup.py \
        --replace-fail '==' '>='
    '';

    propagatedBuildInputs = with pypkgs; [
      configparser
      hexdump
      more-itertools
      pyparsing
      six
      zipp
    ];
  };

in
pypkgs.buildPythonApplication {
  pname = "evtx-tools";
  inherit version src;

  propagatedBuildInputs = [ module ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin scripts/*

    runHook postInstall
  '';

  # requires network
  doCheck = false;

  meta = with lib; {
    description = "Windows Event log tools";
    license = licenses.free;
  };
}
