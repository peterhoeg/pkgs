{
  stdenv,
  lib,
  fetchFromGitHub,
  autoreconfHook, # libegt has moved to cmake, but launcher not yet as of 1.4
  cmake,
  pkg-config,
  alsa-lib,
  cairo,
  curl,
  libdrm,
  expat,
  flac,
  fmt,
  gst_all_1,
  libinput,
  libjpeg,
  lua5_3,
  libmpg123,
  libogg,
  openssl,
  libopus,
  pcre2,
  readline,
  librsvg,
  SDL2,
  libvorbis,
  libsndfile,
  xxd,
  zlib,
  withExamples ? false,

  # for egt-dashboard
  lm_sensors,
  sqlite,
  sqlitecpp,

# for egt-launcher
}:

# egt-launcher compiles but segfaults on run

let
  inherit (lib) cmakeBool;

  # I don't know why autotools can find libegt with this change and not without?!?!?!
  fixWonkyAutotools = ''
    substituteInPlace configure.ac \
      --replace-fail AC_MSG_ERROR AC_MSG_NOTICE
  '';

in
rec {
  libegt = stdenv.mkDerivation (finalAttrs: {
    pname = "libegt";
    version = "1.9.1";

    outputs = [
      "out"
      "dev"
    ];

    src = fetchFromGitHub {
      owner = "linux4sam";
      repo = "egt";
      rev = finalAttrs.version;
      hash = "sha256-wv/fyjA3KLiRzKwG+kc+fBCjEEb42C67bRRyte8gw9w=";
      fetchSubmodules = true;
    };

    postPatch = ''
      patchShebangs scripts
    '';

    buildInputs =
      [
        alsa-lib
        cairo
        curl
        libdrm
        expat
        flac
        fmt
        libinput
        libjpeg
        libsndfile
        lua5_3
        libmpg123
        libogg
        openssl
        libopus
        pcre2
        readline
        librsvg
        SDL2
        libvorbis
        xxd
        zlib
      ]
      ++ (with gst_all_1; [
        gstreamer
        gst-libav
        gst-plugins-base
        gst-plugins-good
      ]);

    nativeBuildInputs = [
      cmake
      pkg-config
    ];

    cmakeFlags = [
      (cmakeBool "ENABLE_EXAMPLES" withExamples)
      (cmakeBool "ENABLE_UNITTESTS" (finalAttrs.doCheck or false))
    ];

    meta = with lib; {
      description = "Ensemble Graphics Toolkit - Modern C++ GUI Toolkit for AT91/SAMA5 Microprocessors";
      license = licenses.free;
      maintainers = with maintainers; [ peterhoeg ];
      platforms = platforms.all;
    };
  });

  egt-launcher = stdenv.mkDerivation (finalAttrs: {
    pname = "egt-launcher";
    version = "1.4";

    src = fetchFromGitHub {
      owner = "linux4sam";
      repo = "egt-launcher";
      rev = finalAttrs.version;
      hash = "sha256-Mp0c8B0YBZwO4O9aeSh6/uJ+pYPQNI81tWkcoqXOyNc=";
    };

    postPatch = fixWonkyAutotools;

    buildInputs = [
      libegt
      cairo
    ];

    nativeBuildInputs = [
      autoreconfHook
      pkg-config
    ];

    meta = with lib; {
      description = "EGT Application Launcher and Home Screen ";
      license = licenses.free;
      maintainers = with maintainers; [ peterhoeg ];
      platforms = platforms.all;
      mainProgram = "egt-launcher";
    };
  });

  egt-thermostat = stdenv.mkDerivation (finalAttrs: {
    pname = "egt-thermostat";
    version = "1.7";

    src = fetchFromGitHub {
      owner = "linux4sam";
      repo = "egt-thermostat";
      rev = finalAttrs.version;
      hash = "sha256-HQiPwy8ca7uetmFrzPvevS/xuK6ZGdNImLtMtfBtJNU=";
      fetchSubmodules = true;
    };

    postPatch = fixWonkyAutotools;

    buildInputs = [
      libegt
      cairo
      lm_sensors
      sqlite
      sqlitecpp # TODO: make it use this rather than the vendored version
    ];

    nativeBuildInputs = [
      autoreconfHook
      pkg-config
    ];

    meta = with lib; {
      description = "EGT Thermostat Demo Application";
      license = licenses.free;
      maintainers = with maintainers; [ peterhoeg ];
      platforms = platforms.all;
      mainProgram = "egt-thermostat";
    };
  });
}
