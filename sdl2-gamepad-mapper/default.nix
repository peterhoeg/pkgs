{
  stdenv,
  lib,
  fetchFromGitLab,
  cmake,
  pkg-config,
  qt6Packages,
  SDL2,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "sdl2-gamepad-mapper";
  version = "0.0.9";

  src = fetchFromGitLab {
    owner = "ryochan7";
    repo = "sdl2-gamepad-mapper";
    rev = "v${finalAttrs.version}";
    hash = "sha256-hkSl9cT+qZd0Ock3wU+uqE3dh4TgRP5t/+dzhD1aZRU=";
  };

  buildInputs = [
    qt6Packages.qtbase
    qt6Packages.qtquick3d
    SDL2
  ];

  nativeBuildInputs = [
    qt6Packages.wrapQtAppsHook
    cmake
    pkg-config
  ];

  env.LANG = "C.UTF-8";

  meta = with lib; {
    description = "Map a generic controller to the SDL2 GameController spec";
    mainProgram = "sdl2-gamepad-mapper";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
})
