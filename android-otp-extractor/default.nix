{
  lib,
  python3Packages,
  fetchFromGitHub,
  makeWrapper,
  makeBinaryWrapper,
  androidenv,
}:

let
  inherit (lib) head makeBinPath replaceStrings;

  wrapper' = makeBinaryWrapper;
  # wrapper' = makeWrapper;

  pypkgs = python3Packages;

  moduleName = "android_otp_extractor";

in
pypkgs.buildPythonApplication rec {
  pname = replaceStrings [ "_" ] [ "-" ] moduleName;
  version = "1.0.7"; # from setup.py

  src = fetchFromGitHub {
    owner = "puddly";
    repo = pname;
    rev = "cf282c71b178faa893aacf70b46ba9a78e694c86";
    hash = "sha256-QVyPbD6P/SsqD/e2e99Utr3X9MccQgzmu//to2tOihY=";
  };

  propagatedBuildInputs = with pypkgs; [
    coloredlogs
    cryptography
  ];

  nativeBuildInputs = [ wrapper' ];

  postInstall = ''
    makeWrapper ${pypkgs.python.interpreter} $out/bin/${pname} \
      --prefix PATH ":" ${makeBinPath [ androidenv.androidPkgs.platform-tools ]} \
      --prefix PYTHONPATH ":" $out/${pypkgs.python.sitePackages}:$PYTHONPATH \
      --add-flags '-m ${moduleName}'
  '';

  pythonImportsCheck = [ moduleName ];

  meta = with lib; {
    description = "Extract OTP secrets from various OTP applications on Android";
    license = licenses.gpl3Only;
    maintainer = with maintainers; [ peterhoeg ];
    mainProgram = pname;
  };
}
