{
  stdenv,
  lib,
  fetchurl,
  intltool,
  pkg-config,
  dbus-glib,
  enca,
  glib,
  gocr,
  libdvdread,
  libnotify,
  libxml2,
  mplayer,
}:

stdenv.mkDerivation rec {
  pname = "ogmrip";
  version = "1.0.1";

  src = fetchurl {
    url = "mirror://sourceforge/project/${pname}/${pname}/${lib.versions.majorMinor version}/${version}/${pname}-${version}.tar.gz";
    sha256 = "sha256-Ti6XeKxNqf4asVnj3G1DZ7ep29jzUB35lzPs7bJbAv8=";
  };

  buildInputs = [
    dbus-glib
    enca
    glib
    gocr
    libdvdread
    libnotify
    libxml2
    mplayer
  ];

  nativeBuildInputs = [
    intltool
    pkg-config
  ];

  enableParallelBuilding = true;

  meta = with lib; {
    description = "Subtitle tools";
  };
}
