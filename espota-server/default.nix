{
  lib,
  rustPlatform,
  fetchFromGitLab,
}:

rustPlatform.buildRustPackage rec {
  pname = "espota-server";
  version = "0.3.0.20210608"; # from Cargo.lock

  src = fetchFromGitLab {
    owner = "stavros";
    repo = pname;
    rev = "30ef85cd81d87b8b493b72e49c29282fda172524";
    sha256 = "sha256-nfya9XEdv6kNkJrbjPdoY11OPubLIORmEk87n21pYbs=";
  };

  cargoSha256 = "sha256-ANqHt/rQJQ2c/GcUsLY4jFjaUAWe+ixRJhtjLIpWw20=";

  # no tests
  doCheck = false;

  meta = with lib; {
    description = "HTTP server that holds firmware for the ESP8266 Arduino framework.";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
    inherit (src.meta) homepage;
  };
}
