{
  stdenv,
  lib,
  fetchurl,
}:

stdenv.mkDerivation rec {
  pname = "mime-octet-filter";
  version = "1.15";

  src = fetchurl {
    # url = "http://www.davep.org/mutt/mutt.octet.filter";
    url = "https://github.com/conghui/mutt-config/raw/master/bin/mutt.octet.filter";
    sha256 = "1c0kl72fcs6g61jhwx47glb1m06xdr0v6bcylyvvmbgw68vxvb63";
  };

  dontUnpack = true;
  dontBuild = true;

  installPhase = ''
    runHook preInstall

    install -Dm755 ${src} $out/libexec/mutt.octet.filter

    runHook postInstall
  '';

  meta = with lib; {
    description = "MIME octet filter for Mutt";
  };
}
