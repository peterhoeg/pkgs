{
  lib,
  python3Packages,
  gobject-introspection,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "notify-send.py";
  version = "1.2.7";

  src = pypkgs.fetchPypi {
    inherit pname version;
    sha256 = "sha256-9olZRJ9q1mx1hGqUal6XdlZX6v5u/H1P/UqVYiw9lmM=";
  };

  # the installer cannot find dbus-python but the actual application can at runtime
  postPatch = ''
    substituteInPlace setup.py \
      --replace-fail "'dbus-python'," ""
  '';

  nativeBuildInputs = [ gobject-introspection ];

  propagatedBuildInputs = with pypkgs; [
    dbus-python
    pygobject3
  ];

  meta = with lib; {
    description = "Send proper desktop notifications on Linux";
    license = licenses.free;
    mainProgram = "notify-send.py";
  };
}
