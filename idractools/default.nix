{
  stdenv,
  lib,
  requireFile,
  fetchurl,
  autoPatchelfHook,
  dpkg,
  makeBinaryWrapper,
  argtable,
  ipmitool,
  openssl,
}:

let
  inherit (lib) replaceStrings;

  argtable2 = argtable.overrideAttrs (old: rec {
    version = "2.13";

    src = fetchurl {
      url = "mirror://sourceforge/argtable/argtable${replaceStrings [ "." ] [ "-" ] version}.tar.gz";
      sha256 = "sha256-j3fop87VMBr24i9HMC/bw7H/QfK4PEPHeuXKBBdx3b8=";
    };

    # otherwise we get cmake which doesn't give us shared libs
    nativeBuildInputs = [ ];
  });

  # argtable' = argtable; # this is argtable3

  # This release is from 2014. Everybody else has moved to argtable3 which we have in nixpkgs
  argtable' = argtable2;

in
stdenv.mkDerivation rec {
  pname = "idractools";
  version = "10.1.0.0";

  src = requireFile rec {
    name = "DellEMC-iDRACTools-Web-LX-${version}-4566_A00.tar.gz";
    sha256 = "0ahxbh6ldp5cf10l5r194cq0v18qzvi7asdzrszhaj388379ncp8";

    message = ''
      In order to use idractools you need to download the binaries, .tar.gz from:

      https://www.dell.com/support/home/en-sg/drivers/driversdetails?driverid=df1nd

      Once you have downloaded the file, please use the following command and re-run the
      installation:

      nix-prefetch-url file://\$PWD/${name}
    '';
  };

  # idractools doesn't look for libssl.so anywhere sane, so LD_LIBRARY_PATH is
  # needed in order to find it
  installPhase = ''
    runHook preInstall

    for f in racadm/UBUNTU20/x86_64/*.deb; do
      dpkg --extract "$f" $out
    done

    rm -rf $out/etc $out/usr/lib

    for d in $out/opt/dell/srvadmin $out/usr; do
      cp -r $d/* $out
    done

    substituteInPlace $out/sbin/racadm-wrapper-idrac7 \
      --replace-fail /opt/dell/srvadmin/bin $out/bin

    rm -rf $out/{opt,src,usr,var}

    for f in $out/lib64/lib*.so.${version}; do
      for e in .so.5 .so.${lib.versions.major version} 64.so; do
        ln -sf $f $out/lib64/$(basename $f .so.${version})$e
      done
    done

    wrapProgram $out/bin/idracadm7 \
      --prefix PATH : ${lib.makeBinPath buildInputs} \
      --prefix LD_LIBRARY_PATH : ${lib.makeLibraryPath buildInputs}

    runHook postInstall
  '';

  buildInputs = [
    stdenv.cc.cc.lib
    argtable'
    ipmitool
    openssl
  ];

  nativeBuildInputs = [
    autoPatchelfHook
    dpkg
    makeBinaryWrapper
  ];

  meta = with lib; {
    description = "Dell iDRAC";
    license = licenses.unfree;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
    hydraPlatforms = platforms.none;
  };
}
