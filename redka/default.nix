{
  buildGoModule,
  lib,
  fetchFromGitHub,
}:

let
  version = "0.2.0";

in
buildGoModule {
  pname = "redka";
  inherit version;

  src = fetchFromGitHub {
    owner = "nalgeon";
    repo = "redka";
    rev = "v" + version;
    hash = "sha256-GIbUcQlrvqwp/atq4xxLfAZxiMph0uixGDo0g2GhpQ0=";
  };

  vendorHash = "sha256-aX0X6TWVEouo884LunCt+UzLyvDHgmvuxdV0wh0r7Ro=";

  ldflags = [ "-X main.version=${version}" ];

  subPackages = [
    "."
    "cmd/cli"
    "cmd/redka"
  ];

  postInstall = ''
    install -Dm444 -t $out/share/doc/redka *.md
  '';

  meta = with lib; {
    description = "Redis re-implemented with SQLite ";
  };
}
