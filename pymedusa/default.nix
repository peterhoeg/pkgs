{
  lib,
  fetchFromGitHub,
  python39Packages,
}:

let
  # need to test with a later python
  pypkgs = python39Packages;

  # this is for mapping directories of vendored dependencies to the ones we carry in nixpkgs
  # basically we blow away what we can and then add it as real dependency instead
  # there's a TON more that we can purge although we should probably have a proper test for that
  deps = with pypkgs; {
    # DIRECTORY = pypkgs.package
    boto = boto;
    bs4 = beautifulsoup4;
    chardet = chardet;
    # # dirtyjson = dirtyjson;
    enzyme = enzyme;
    feedparser = feedparser;
    guessit = guessit;
    html5lib = html5lib;
    jwt = pyjwt;
    pint = pint;
    pytz = pytz;
    rebulk = rebulk;
    requests = requests;
    stevedore = stevedore;
    subliminal = subliminal;
    tornado = tornado_5;
    twitter = twitter;
    urllib3 = urllib3;
    yaml = pyyaml;
  };

  inherit (lib) concatStringsSep concatMapStringsSep optionalString;

  adba = pypkgs.buildPythonPackage rec {
    pname = "adba";
    version = "1.1.0.20210803";

    src = fetchFromGitHub {
      owner = "pymedusa";
      repo = pname;
      # rev = version;
      rev = "37b0c74e76b40b3dbde29e71da75a1808eb121de";
      hash = "sha256-AAwgFfmlmLiopGi0OjKRIds9m1jLJwBhvroZdXhNCK0=";
    };

    propagatedBuildInputs = with pypkgs; [
      requests
      six
      ttl-cache
    ];

    doCheck = false;
  };

  bencodepy = pypkgs.buildPythonPackage rec {
    pname = "bencode.py";
    version = "4.0.0";

    src = pypkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-KiTM2hclpRplCJPQtjJgE4NZ6qKZu256CZYTUKKm4Fw=";
    };

    propagatedBuildInputs = with pypkgs; [ pbr ];

    checkInputs = with pypkgs; [ pytest ];
  };

  knowit = pypkgs.buildPythonPackage rec {
    pname = "knowit";
    version = "0.4.0";

    src = pypkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-25uSw2zE0St3bj930q6rfUdqBpeu2ApPqD04f7OB0Ms=";
    };

    propagatedBuildInputs = with pypkgs; [
      babelfish
      enzyme
      pint
      pymediainfo
      pyyaml
      six
    ];

    # checkInputs = with pypkgs; [ pytest ];
  };

  ttl-cache = pypkgs.buildPythonPackage rec {
    pname = "ttl-cache";
    version = "1.6";

    src = pypkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-xM5K821Dq6VJo1erMXphkUOdO0e+4WbvZ7HWsHo+/7U=";
    };
  };

in
pypkgs.buildPythonApplication rec {
  pname = "pymedusa";
  version = "1.0.6";

  src = fetchFromGitHub {
    owner = "pymedusa";
    repo = "Medusa";
    rev = "v" + version;
    hash = "sha256-oE3+yb721WQPvmRUZN6G6ulJAun/1IqDFfKJ0zck3vg=";
  };

  # 1. medusa will by default try to write to the program directory. This can be
  # overwritten using the --datadir parameter, which is probably what you want
  # but the binary directory is *definitely* wrong
  #
  # 2. we remove the vendored dependencies for which we supply our own
  postPatch =
    ''
      substituteInPlace medusa/__main__.py \
        --replace-fail 'app.DATA_DIR = app.PROG_DIR' 'app.DATA_DIR = os.getenv("MEDUSA_HOME", ".")'

      # it tries to run tests from vendored deps
      substituteInPlace setup.py \
        --replace-fail "'ext'," "" \
        --replace-fail "'ext2'," "" \
        --replace-fail "'ext3'," ""

    ''
    + concatMapStringsSep "\n" (e: ''
      # deps are split between `ext` (py2 and py3) and `ext3` (py3 only)
      if [ -d ext/${e} ] || [ -d ext3/${e} ]; then
        rm -rf ext/${e} ext3/${e}
      else
        echo "Unable to find vendored dependency: ${e}"
        exit 1
      fi
    '') (builtins.attrNames deps);

  propagatedBuildInputs =
    with pypkgs;
    [
      adba
      bencodepy
      knowit
    ]
    ++ builtins.attrValues deps;

  postInstall = ''
    install -Dm444 runscripts/init.systemd $out/lib/systemd/system/medusa.service
  '';

  checkInputs = with pypkgs; [
    pytestCheckHook
    mock
    requests-mock
    vcrpy
  ];

  pythonImportsCheck = [ "medusa" ];

  # not working yet
  doCheck = false;

  meta = with lib; {
    description = "Automated TV series downloader";
    license = licenses.gpl3Only;
    broken = true;
  };
}
