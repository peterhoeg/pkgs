{
  stdenv,
  lib,
  boost,
  cmake,
  fetchFromGitHub,
  xorg-rgb,
  hidapi,
  libyamlcpp,
  ninja,
  pkg-config,
}:

let
  meta = with lib; {
    license = licenses.free; # TODO: check the actual license
    maintainers = with maintainers; [ peterhoeg ];
  };

  # we can get the rgb.txt from multiple places:
  # - xorg-rgb: this should be the most canonical source
  # - emacs: not tried but same entries as neovim
  # - netpbm: crashes xbows-driver
  # - neovim: works but has fewer entries than vim
  # - vim: has dropped it at some point in 8.2, so just fetch from an older version
  # Eventually upstream should vendor it: https://github.com/jlquinn/xbows-driver/issues/9

  libcrc = stdenv.mkDerivation rec {
    pname = "libcrc";
    version = "2.0.20210422";

    src = fetchFromGitHub {
      owner = "lammertb";
      repo = pname;
      rev = "7719e2112a9a960b1bba130d02bebdf58e8701f1";
      hash = "sha256-bCjnd/pTh5kG+X6uMHtXUMM7KS36SngUKwvH6tPpBwg=";
    };

    enableParallelBuilding = true;

    postPatch = ''
      substituteInPlace Makefile \
        --replace-fail -Werror -Wno-format-truncation \
        --replace-fail /bin/rm rm
    '';

    installPhase = ''
      runHook preInstall

      mkdir -p $out
      cp -r lib include $out

      runHook postInstall
    '';

    inherit meta;
  };

in
stdenv.mkDerivation rec {
  pname = "xbows-driver";
  version = "0.0.0.20220129";

  src = fetchFromGitHub {
    owner = "jlquinn";
    repo = pname;
    rev = "97c4998290781c7a54c3814292169dc0ac24ee05";
    hash = "sha256-FbKeYrdGIEQ5zJJjn0ILZw+kNdzRQ44PM6G1LSub9Ws=";
  };

  patches = [ ./build.patch ];

  postPatch = ''
    rgb=${xorg-rgb}/share/X11/rgb.txt

    if [ ! -e $rgb ]; then
      echo "rgb.txt not found: $rgb"
      exit 1
    fi

    for f in kbdtest.cc kbdtest2.cc xbows.hh ; do
      substituteInPlace src/$f --replace-fail '<hidapi' '<hidapi/hidapi'
    done

    substituteInPlace src/driver_parse.cc \
      --replace-fail /usr/share/X11/rgb.txt $rgb

    # also in src with a bunch of others so prefer src
    rm share/test*.yaml
  '';

  buildInputs = [
    boost
    hidapi
    libcrc
    libyamlcpp
  ];

  nativeBuildInputs = [
    cmake
    ninja
    pkg-config
  ];

  installPhase =
    let
      # 1. we need to force set RPATH as all the binaries will otherwise contain
      # a reference to /build which is not allowed
      #
      # 2. for some reason $out/lib is stripped out as part of our fixup phase
      # so although it's in there to begin with, after having fixed it up, it no
      # longer is if we don't force it
      rpath = lib.makeLibraryPath (
        [
          (placeholder "out")
          stdenv.cc.cc.lib
        ]
        ++ buildInputs
      );
    in
    ''
      runHook preInstall


      # do this *before* installing as the 0555 permissions means that we cannot do it *after*
      for f in src/{kbdtest,kbdtest2,testparse}; do
        patchelf $f --set-rpath ${rpath}
      done

      install -Dm0555 -t $out/bin                     src/{kbdtest,kbdtest2,testparse}
      install -Dm0444 -t $out/lib                     src/*.so
      install -Dm0444 -t $out/share/${pname}/examples ../share/*.yaml ../src/test*.yaml
      install -Dm0444 -t $out/share/doc/${pname}      ../doc/*

      runHook postInstall
    '';

  inherit meta;
}
