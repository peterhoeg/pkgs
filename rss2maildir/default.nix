{
  lib,
  fetchFromGitHub,
  python3Packages,
}:

let
  pypkgs = python3Packages;

  mailbox' = pypkgs.buildPythonPackage rec {
    pname = "mailbox";
    version = "0.4";
    src = pypkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-+b+WeZMveiRZAtM5JN/xFIW89JysBSbxpeuVULxRDXM=";
    };
  };

in
pypkgs.buildPythonApplication rec {
  pname = "rss2maildir";
  version = "20160904";

  format = "other";

  src = fetchFromGitHub {
    owner = "edt";
    repo = pname;
    rev = "3309439557232e3d900693291ec949d7a5a165ac";
    hash = "sha256-LeyHz2z+k0KgEx+ix/HsOe3zQIH0j4InAnkFlfi1qYM=";
  };

  installPhase = ''
    runHook preInstall

    install -Dm0555 rss2maildir.py $out/bin/rss2maildir
    install -Dm0444 LICENSE *.md *.json -t $out/share/doc/${pname}

    runHook postInstall
  '';

  propagatedBuildInputs = with pypkgs; [
    feedparser
    mailbox'
  ];

  doCheck = true;

  # checkInputs = with pypkgs; [ pytest pytest-cov pytest-runner ];

  # pythonImportChecks = [ pname ];

  meta = with lib; {
    description = "RSS to Maildir";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
