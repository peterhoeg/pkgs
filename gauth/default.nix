{
  lib,
  buildGoModule,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "gauth";
  version = "1.1";

  src = fetchFromGitHub {
    owner = "pcarrier";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-Ia4x53CSgHYqoHpYWQ50r7QzSGjQ47xKB7ouY8XtMp0=";
  };

  vendorHash = "sha256-3/uXrVpi0kkRuulnu05+6AB54G/4l2iJVOKXvWvz7lc=";

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md
  '';

  meta = with lib; {
    description = "Google Auth on your computer";
    # license = licenses.asl20;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.unix;
  };
}
