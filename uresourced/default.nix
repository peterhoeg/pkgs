{
  stdenv,
  lib,
  fetchFromGitLab,
  meson,
  ninja,
  pkg-config,
  glib,
  pipewire,
  systemd,
}:

let
  inherit (lib) optionals;

  appmng = true;

in
stdenv.mkDerivation rec {
  pname = "uresourced";
  version = "0.5.3";

  src = fetchFromGitLab {
    domain = "gitlab.freedesktop.org";
    owner = "benzea";
    repo = "uresourced";
    rev = "v${version}";
    hash = "sha256-4RtJ7fChVo2wVuHpVCT8Btvm09uFKjzaMEpYhIVkwko=";
  };

  buildInputs = [
    glib
    systemd
  ] ++ optionals appmng [ pipewire ];

  nativeBuildInputs = [
    meson
    ninja
    pkg-config
  ];

  mesonFlags = [
    "-Dsystemdsystemunitdir=${placeholder "out"}/lib/systemd/system"
    "-Dsystemduserunitdir=${placeholder "out"}/lib/systemd/user"
    "-Dcgroupify=true"
    "-Dappmanagement=${lib.boolToString appmng}"
  ];

  meta = with lib; {
    description = "Give resource allocations to active graphical users.";
    license = licenses.lgpl21Only;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
  };
}
