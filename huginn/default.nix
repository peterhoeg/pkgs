{
  stdenv,
  lib,
  fetchurl,
  fetchFromGitHub,
  bundlerEnv,
  ruby,
  tzdata,
  git,
  nettools,
  nixosTests,
  nodejs,
  callPackage,
  replace,
}:

let
  pname = "huginn";
  version = "20200503";

  src = fetchFromGitHub {
    owner = "huginn";
    repo = "huginn";
    rev = "13d0ca344ae72515795645db058e7c329fd3de5d";
    sha256 = "15ikbwb4sv1nlc1is6zx959wamha0v7k21gdkmfjarjq3g0ydng8";
  };

  rubyEnv = bundlerEnv rec {
    name = "${pname}-env-${version}";
    inherit ruby;
    gemdir = ./.;
    # ignoreCollisions = true;
  };

  assets = stdenv.mkDerivation {
    pname = "${pname}-assets";
    inherit version src;

    nativeBuildInputs = [
      rubyEnv.wrappedRuby
      rubyEnv.bundler
      nodejs
      git
    ];

    RAILS_ENV = "production";

    configurePhase = ''
      runHook preConfigure

      # Some rake tasks try to run yarn automatically, which won't work
      rm lib/tasks/yarn.rake

      # The rake tasks won't run without a basic configuration in place
      mv config/database.yml.env config/database.yml
      mv config/gitlab.yml.example config/gitlab.yml

      # Yarn and bundler wants a real home directory to write cache, config, etc to
      export HOME=$NIX_BUILD_TOP/fake_home

      # Make yarn install packages from our offline cache, not the registry
      yarn config --offline set yarn-offline-mirror ${yarnOfflineCache}

      # Fixup "resolved"-entries in yarn.lock to match our offline cache
      ${fixup_yarn_lock}/bin/fixup_yarn_lock yarn.lock

      # fixup_yarn_lock currently doesn't correctly fix the dagre-d3
      # url, so we have to do it manually
      ${replace}/bin/replace-literal -f -e '"https://codeload.github.com/dagrejs/dagre-d3/tar.gz/e1a00e5cb518f5d2304a35647e024f31d178e55b"' \
                                           '"https___codeload.github.com_dagrejs_dagre_d3_tar.gz_e1a00e5cb518f5d2304a35647e024f31d178e55b"' yarn.lock

      yarn install --offline --frozen-lockfile --ignore-scripts --no-progress --non-interactive

      patchShebangs node_modules/

      runHook postConfigure
    '';

    buildPhase = ''
      runHook preBuild

      bundle exec rake gettext:po_to_json RAILS_ENV=production NODE_ENV=production
      bundle exec rake rake:assets:precompile RAILS_ENV=production NODE_ENV=production
      bundle exec rake webpack:compile RAILS_ENV=production NODE_ENV=production NODE_OPTIONS="--max_old_space_size=2048"
      bundle exec rake gitlab:assets:fix_urls RAILS_ENV=production NODE_ENV=production

      runHook postBuild
    '';

    installPhase = ''
      runHook preInstall

      mv public/assets $out

      runHook postInstall
    '';
  };

in
stdenv.mkDerivation {
  inherit pname version src;

  buildInputs = [
    rubyEnv
    rubyEnv.wrappedRuby
    rubyEnv.bundler
    tzdata
    git
    nettools
  ];

  postPatch = ''
    sed -ri config/environments/production.rb \
      -e '/log_level/a config.logger = Logger.new(STDERR)' 

    # Always require lib-files and application.rb through their store
    # path, not their relative state directory path. This gets rid of
    # warnings and means we don't have to link back to lib from the
    # state directory.
    # ${replace}/bin/replace-literal -f -r -e '../lib' "$out/share/gitlab/lib" config
    # ${replace}/bin/replace-literal -f -r -e "require_relative 'application'" "require_relative '$out/share/gitlab/config/application'" config
  '';

  buildPhase = ''
    rm -f config/secrets.yml
    mv config config.dist
    rm -r tmp
  '';

  installPhase = ''
    mkdir -p $out/share
    cp -r . $out/share/gitlab
    ln -sf ${assets} $out/share/gitlab/public/assets
    rm -rf $out/share/gitlab/log
    ln -sf /run/gitlab/log $out/share/gitlab/log
    ln -sf /run/gitlab/uploads $out/share/gitlab/public/uploads
    ln -sf /run/gitlab/config $out/share/gitlab/config
    ln -sf /run/gitlab/tmp $out/share/gitlab/tmp
  '';

  passthru = {
    inherit rubyEnv assets;
    ruby = rubyEnv.wrappedRuby;
  };

  meta = with lib; {
    description = "GitLab Community Edition";
    homepage = "http://www.gitlab.com/";
    platforms = platforms.linux;
    maintainers = with maintainers; [ peterhoeg ];
    license = licenses.mit;
  };
}
