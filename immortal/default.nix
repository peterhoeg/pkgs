{
  buildGoModule,
  fetchFromGitHub,
  lib,
}:

buildGoModule rec {
  pname = "immortal";
  version = "0.24.4";

  src = fetchFromGitHub {
    owner = "immortal";
    repo = "immortal";
    rev = "${version}";
    hash = "sha256-C1ISIt1Rj/Q1f2CzFrcwRxmDZjXS/EPW87GwKYJqn/s=";
  };

  vendorHash = "sha256-jaQLPuz2hFCJVFnOeRE9dU3WCvTVRyRhwWuF7gFUNWo=";

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} *.md
  '';

  preCheck = ''
    export HOME=$(mktemp -d)
  '';

  # some tests fail due to our sandbox
  doCheck = false;

  meta = with lib; {
    description = "Service supervisor and retrier";
    homepage = "https://immortal.run";
    license = licenses.bsd3;
    maintainers = with maintainers; [ ];
  };
}
