{
  stdenv,
  lib,
  fetchurl,
  unzip,
}:

stdenv.mkDerivation rec {
  pname = "webster";
  version = "2.4.2";

  src = fetchurl {
    url = "https://s3.amazonaws.com/jsomers/dictionary.zip";
    hash = "sha256-+qbj/Jgp6eRTF27wX/Vyni3RJfgyVHvF0deQXfA4xic=";
  };

  sourceRoot = ".";

  postUnpack = ''
    tar xf dictionary/stardict-dictd-web1913-${version}.tar.bz2
  '';

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    runHook preInstall

    install -Dm444 -t $out/share/stardict/dic stardict-dictd-web1913-${version}/*

    runHook postInstall
  '';

  meta = with lib; {
    description = "Webster's Dictionary 1913 edition";
    homepage = "https://jsomers.net/blog/dictionary";
    maintainers = with maintainers; [ peterhoeg ];
  };
}
