{
  buildGoModule,
  stdenv,
  lib,
  fetchFromGitLab,
  makeWrapper,
}:

let
  shareDir = "${placeholder "out"}/share/${pname}";

  pname = "postgres-checkup";
  version = "1.5.0";

  src = fetchFromGitLab {
    owner = "postgres-ai";
    repo = pname;
    rev = version;
    hash = "sha256-aCEpOb4ptfLJfpKU3m36maVQzB4pe4XMziXh0D2beao=";
  };

  pghrep = buildGoModule rec {
    pname = "pghrep";
    inherit version src;

    vendorHash = "sha256-wqb9/P8UePtuWoOJX1bOGjGIO7BtspptehbrmQv6cuQ=";

    sourceRoot = "source/pghrep";

    # the tests need a real postgres server
    doCheck = false;

    meta = {
      description = "Create markdown reports from JSON reports";
      mainProgram = "pghrep";
    };
  };

in
stdenv.mkDerivation {
  inherit pname version src;

  postPatch = ''
    sed -i checkup \
      -e 's@^PGHREP_BIN=.*@PGHREP_BIN=${lib.getExe pghrep}@g' \
      -e 's@^SCRIPT_DIR=.*@SCRIPT_DIR=${shareDir}@g' \
      -e 's@^SCRIPT_NAME=.*@SCRIPT_NAME=checkup@'
  '';

  buildInputs = [ pghrep ];

  nativeBuildInputs = [ makeWrapper ];

  installPhase = ''
    install -Dm555 -t $out/bin checkup *.sh
    install -d ${shareDir}
    cp -r resources ${shareDir}
  '';

  meta = with lib; {
    description = "PostgreSQL Health Check and SQL Performance Analysis";
    platform = platforms.unix;
  };
}
