{
  buildGo121Module,
  lib,
  fetchFromGitHub,
}:

let
  buildGoModule = buildGo121Module;

in
buildGoModule rec {
  pname = "ferretdb";
  version = "1.12.1";

  src = fetchFromGitHub {
    owner = "FerretDB";
    repo = "FerretDB";
    rev = "v" + version;
    hash = "sha256-3fLTiI13Mm+G6EEEOzCuJ9KVebCq5O54hyH6JiCRRL8=";
  };

  vendorHash = "sha256-l45KFDpqprBWnsVRhOJkCWolZapArRvjUb52R5hc5zs=";

  ldflags = [ "-X main.version=${version}" ];

  meta = with lib; {
    description = "A truly Open Source MongoDB alternative";
    license = licenses.asl20;
    broken = true; # still wip
  };
}
