{
  ameba = {
    url = "https://github.com/crystal-ameba/ameba.git";
    rev = "c27c605f7b818f61d149441d58f65661fafd2221";
    sha256 = "1cfjvnfj6wjjbsn4g0aqz53j5xmbkv499hf3qb98m08w3idqwx8a";
  };
  db = {
    url = "https://github.com/crystal-lang/crystal-db.git";
    rev = "v0.13.1";
    sha256 = "02b79bdv5h460ay0vkpwi5q69b1qrm9z29z02p09xb03hijxskpd";
  };
  i18n = {
    url = "https://github.com/crystal-i18n/i18n.git";
    rev = "v0.2.1";
    sha256 = "16valnsr3qyxa3cc0pzrix3pficgicji9s6hwnmy9qhnkmyq6ilj";
  };
  msgpack = {
    url = "https://github.com/crystal-community/msgpack-crystal.git";
    rev = "v1.3.4";
    sha256 = "19l8lh9z0cx40hwx2855xnaka2makkdri8y3d2v1rf2mnzgh09p6";
  };
  mysql = {
    url = "https://github.com/crystal-lang/crystal-mysql.git";
    rev = "v0.16.0";
    sha256 = "1g8cv8pacymxqbds3jpg6iqrfbfzij18r99s3fpjjx5xc3dnml0x";
  };
  pg = {
    url = "https://github.com/will/crystal-pg.git";
    rev = "v0.29.0";
    sha256 = "0sac3cwx10jc8as9r8xy80gfkcmpk2g8jbw5b6y0fr02ka414mqp";
  };
  sqlite3 = {
    url = "https://github.com/crystal-lang/crystal-sqlite3.git";
    rev = "v0.21.0";
    sha256 = "1mhg59l6qgmf14zjrais0jfn1h5jj9j32fy7fb0gkp9zhpa7x6vs";
  };
  timecop = {
    url = "https://github.com/crystal-community/timecop.cr.git";
    rev = "v0.5.0";
    sha256 = "1qdza9jcihpwhgas967b1gwa1sklxhvijx8sy475qxr79praq5k1";
  };
}
