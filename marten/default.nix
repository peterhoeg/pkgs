{
  crystal,
  fetchFromGitHub,
  pkg-config,
  # , openssl
  sqlite,
}:

crystal.buildCrystalPackage rec {
  pname = "marten";
  version = "0.5.4";

  format = "shards";

  src = fetchFromGitHub {
    owner = "martenframework";
    repo = "marten";
    rev = "v${version}";
    hash = "sha256-BtwnJ3dzLA2V6rnsVQ4HdAnjWPjjarqI/DfjnwvwloY=";
  };

  shardsFile = ./shards.nix;
  lockFile = ./shard.lock;

  nativeBuildInputs = [ pkg-config ];

  buildInputs = [ sqlite ];

  options = [ "--verbose" ];

  # some tests are failing
  doCheck = false; # needs redis
  doInstallCheck = false;
}
