{
  lib,
  fetchFromGitHub,
  python311Packages, # https://github.com/MilhouseVH/texturecache.py/issues/75
}:

let
  pypkgs = python311Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "kodi-texturecache";
  version = "2.5.4";

  format = "other";

  src = fetchFromGitHub {
    owner = "MilhouseVH";
    repo = "texturecache.py";
    rev = version;
    hash = "sha256-BJREcySbH6ooIFn1IQZ/HS3dsFLTh35W+L6RDhMk+y0=";
  };

  postPatch = ''
    substituteInPlace texturecache.py \
      --replace-fail 'self.CONFIG_NAME = "texturecache.cfg"' 'self.CONFIG_NAME = os.environ.get("XDG_CONFIG_HOME", "~/.config") + "/texturecache.cfg"' \
      --replace-fail '.setDaemon(True)' '.daemon = True'
  '';

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin *.py tools/*.py
    install -Dm444 -t $out/etc *.defaults
    install -Dm444 -t $out/share/doc/${pname} *.md

    runHook postInstall
  '';

  meta = with lib; {
    description = "Kodi Texture Cache Maintenance Utility";
    license = licenses.free;
    mainProgram = "texturecache.py";
  };
}
