{
  stdenv,
  lib,
  fetchFromGitHub,
  go,
  go-md2man,
  meson,
  ninja,
  pkg-config,
  python3,
  glib,
  lua5_4,
  pipewire,
  systemd,
}:

stdenv.mkDerivation rec {
  pname = "toolbox";
  version = "0.0.99.3";

  src = fetchFromGitHub {
    owner = "containers";
    repo = "toolbox";
    rev = version;
    sha256 = "sha256-9HiWgEtaMypLOwXJ6Xg3grLSZOQ4NInZtcvLPV51YO8=";
  };

  buildInputs = [ systemd ];

  nativeBuildInputs = [
    go
    go-md2man
    meson
    ninja
    pkg-config
  ];

  mesonFlags = [
    "-Dprofile_dir=/etc/profile.d"
  ];

  meta = with lib; {
    description = "Mutable container tool";
    broken = true;
  };
}
