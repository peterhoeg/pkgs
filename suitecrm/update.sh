#!/usr/bin/env bash

set -eEuo pipefail
IFS=$'\n\t'

composer2nix --symlink-dependencies
