{
  composerEnv,
  fetchurl,
  fetchgit ? null,
  fetchhg ? null,
  fetchsvn ? null,
  noDev ? false,
}:

let
  packages = {
    "consolidation/annotated-command" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "consolidation-annotated-command-512a2e54c98f3af377589de76c43b24652bcb789";
        src = fetchurl {
          url = "https://api.github.com/repos/consolidation/annotated-command/zipball/512a2e54c98f3af377589de76c43b24652bcb789";
          sha256 = "168q4ykkdphib6sl1203sk8x8naymyvxs3cv1mcb71jg84ckdfk5";
        };
      };
    };
    "consolidation/config" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "consolidation-config-cac1279bae7efb5c7fb2ca4c3ba4b8eb741a96c1";
        src = fetchurl {
          url = "https://api.github.com/repos/consolidation/config/zipball/cac1279bae7efb5c7fb2ca4c3ba4b8eb741a96c1";
          sha256 = "1ybjn2750abh4qlhnizrqx595sg3linjpk41sac9afhifal0wzbv";
        };
      };
    };
    "consolidation/log" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "consolidation-log-b2e887325ee90abc96b0a8b7b474cd9e7c896e3a";
        src = fetchurl {
          url = "https://api.github.com/repos/consolidation/log/zipball/b2e887325ee90abc96b0a8b7b474cd9e7c896e3a";
          sha256 = "0priynm0pp3bryggf89nf2sdyb0gc6k6glgg0xid4fzix1xmxgxx";
        };
      };
    };
    "consolidation/output-formatters" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "consolidation-output-formatters-0881112642ad9059071f13f397f571035b527cb9";
        src = fetchurl {
          url = "https://api.github.com/repos/consolidation/output-formatters/zipball/0881112642ad9059071f13f397f571035b527cb9";
          sha256 = "06kvkdvwxx7xqky6x3a6fkk7v0ndjivxa5xgb1d2m0fdlff9an22";
        };
      };
    };
    "consolidation/robo" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "consolidation-robo-5c6b3840a45afda1cbffbb3bb1f94dd5f9f83345";
        src = fetchurl {
          url = "https://api.github.com/repos/consolidation/Robo/zipball/5c6b3840a45afda1cbffbb3bb1f94dd5f9f83345";
          sha256 = "0lzwwh52nzhwlq45hmjdwyx0jqikia4k1vdf3nhwf7vivx38jgrn";
        };
      };
    };
    "consolidation/self-update" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "consolidation-self-update-a1c273b14ce334789825a09d06d4c87c0a02ad54";
        src = fetchurl {
          url = "https://api.github.com/repos/consolidation/self-update/zipball/a1c273b14ce334789825a09d06d4c87c0a02ad54";
          sha256 = "1sd32hardpsc1xw8x3h4dcpcg8p0i911bgfl1dlw24fwcra2y0ml";
        };
      };
    };
    "container-interop/container-interop" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "container-interop-container-interop-79cbf1341c22ec75643d841642dd5d6acd83bdb8";
        src = fetchurl {
          url = "https://api.github.com/repos/container-interop/container-interop/zipball/79cbf1341c22ec75643d841642dd5d6acd83bdb8";
          sha256 = "1pxm461g5flcq50yabr01nw8w17n3g7klpman9ps3im4z0604m52";
        };
      };
    };
    "defuse/php-encryption" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "defuse-php-encryption-0f407c43b953d571421e0020ba92082ed5fb7620";
        src = fetchurl {
          url = "https://api.github.com/repos/defuse/php-encryption/zipball/0f407c43b953d571421e0020ba92082ed5fb7620";
          sha256 = "1fwmhxzw27x37hmbc056liymbq0j39yi6qf8as59n2kkz6xgnpm5";
        };
      };
    };
    "dflydev/dot-access-data" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "dflydev-dot-access-data-3fbd874921ab2c041e899d044585a2ab9795df8a";
        src = fetchurl {
          url = "https://api.github.com/repos/dflydev/dflydev-dot-access-data/zipball/3fbd874921ab2c041e899d044585a2ab9795df8a";
          sha256 = "0n9jb8chx4k0aigapi9rvxwfqrg18x5dqgwnrnigq8243bsjg6nc";
        };
      };
    };
    "elasticsearch/elasticsearch" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "elasticsearch-elasticsearch-d3c5b55ad94f5053ca76c48585b4cde2cdc6bc59";
        src = fetchurl {
          url = "https://api.github.com/repos/elastic/elasticsearch-php/zipball/d3c5b55ad94f5053ca76c48585b4cde2cdc6bc59";
          sha256 = "1vkp17nsybvcaw03xn13vm92f97lri0m98hn3kh66hymbsn95w02";
        };
      };
    };
    "ezyang/htmlpurifier" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "ezyang-htmlpurifier-d85d39da4576a6934b72480be6978fb10c860021";
        src = fetchurl {
          url = "https://api.github.com/repos/ezyang/htmlpurifier/zipball/d85d39da4576a6934b72480be6978fb10c860021";
          sha256 = "0ifc2ck9x8xbp2wc1i901qapyxrdq45l1wps5jd3dz9q0ryh8bhb";
        };
      };
    };
    "firebase/php-jwt" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "firebase-php-jwt-9984a4d3a32ae7673d6971ea00bae9d0a1abba0e";
        src = fetchurl {
          url = "https://api.github.com/repos/firebase/php-jwt/zipball/9984a4d3a32ae7673d6971ea00bae9d0a1abba0e";
          sha256 = "00s8f75qsb7vzjmf9ca6nvp5pj59cri0fljvzvpr13s0cm4qbhm4";
        };
      };
    };
    "google/apiclient" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "google-apiclient-4e0fd83510e579043e10e565528b323b7c2b3c81";
        src = fetchurl {
          url = "https://api.github.com/repos/googleapis/google-api-php-client/zipball/4e0fd83510e579043e10e565528b323b7c2b3c81";
          sha256 = "1na5b32vv0fin4ks8fcj45md510pvfws64y8hnfb30kc2rdcyh2g";
        };
      };
    };
    "google/apiclient-services" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "google-apiclient-services-682f5ea10b63a4a008933afae5185c5239b653cb";
        src = fetchurl {
          url = "https://api.github.com/repos/googleapis/google-api-php-client-services/zipball/682f5ea10b63a4a008933afae5185c5239b653cb";
          sha256 = "15cfn89hmg3j86q4r72rq68b3vp2b3ycfqydbjkyfqawcl87dqhg";
        };
      };
    };
    "google/auth" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "google-auth-0f75e20e7392e863f5550ed2c2d3d50af21710fb";
        src = fetchurl {
          url = "https://api.github.com/repos/googleapis/google-auth-library-php/zipball/0f75e20e7392e863f5550ed2c2d3d50af21710fb";
          sha256 = "0jc70yd0wsk17ss9bk24ml6qgfx3cl4hdy0i9grhq3yq571rr1r7";
        };
      };
    };
    "google/recaptcha" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "google-recaptcha-e7add3be59211482ecdb942288f52da64a35f61a";
        src = fetchurl {
          url = "https://api.github.com/repos/google/recaptcha/zipball/e7add3be59211482ecdb942288f52da64a35f61a";
          sha256 = "052s1nm2nh0khy721z9mcqa5n944l8wc5c4fnv4bnfw81qw3srad";
        };
      };
    };
    "grasmash/expander" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "grasmash-expander-95d6037344a4be1dd5f8e0b0b2571a28c397578f";
        src = fetchurl {
          url = "https://api.github.com/repos/grasmash/expander/zipball/95d6037344a4be1dd5f8e0b0b2571a28c397578f";
          sha256 = "15ilnmhcyl9zh2glpc9a88iffzi1j3mry0kwr13bkslf9iq0vzxk";
        };
      };
    };
    "grasmash/yaml-expander" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "grasmash-yaml-expander-3f0f6001ae707a24f4d9733958d77d92bf9693b1";
        src = fetchurl {
          url = "https://api.github.com/repos/grasmash/yaml-expander/zipball/3f0f6001ae707a24f4d9733958d77d92bf9693b1";
          sha256 = "0pbj0421g3psh5lqm9gzmjlf82yzd2cr0skpiv9r1lx23nvf0zqg";
        };
      };
    };
    "guzzlehttp/guzzle" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "guzzlehttp-guzzle-407b0cb880ace85c9b63c5f9551db498cb2d50ba";
        src = fetchurl {
          url = "https://api.github.com/repos/guzzle/guzzle/zipball/407b0cb880ace85c9b63c5f9551db498cb2d50ba";
          sha256 = "19m6lgb0blhap3qiqm00slgfc1sc6lzqpbdk47fqg4xgcbn0ymmb";
        };
      };
    };
    "guzzlehttp/promises" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "guzzlehttp-promises-a59da6cf61d80060647ff4d3eb2c03a2bc694646";
        src = fetchurl {
          url = "https://api.github.com/repos/guzzle/promises/zipball/a59da6cf61d80060647ff4d3eb2c03a2bc694646";
          sha256 = "1kpl91fzalcgkcs16lpakvzcnbkry3id4ynx6xhq477p4fipdciz";
        };
      };
    };
    "guzzlehttp/psr7" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "guzzlehttp-psr7-9f83dded91781a01c63574e387eaa769be769115";
        src = fetchurl {
          url = "https://api.github.com/repos/guzzle/psr7/zipball/9f83dded91781a01c63574e387eaa769be769115";
          sha256 = "1xv2zlwfazhb6jykm27cscl5m37hq0ifgdnblk0hhnyr1dm8yrvk";
        };
      };
    };
    "guzzlehttp/ringphp" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "guzzlehttp-ringphp-5e2a174052995663dd68e6b5ad838afd47dd615b";
        src = fetchurl {
          url = "https://api.github.com/repos/guzzle/RingPHP/zipball/5e2a174052995663dd68e6b5ad838afd47dd615b";
          sha256 = "09n1znwxawmsidyq6zk94mg85hibsg8kxm1j0bi795pa55fiqzj9";
        };
      };
    };
    "guzzlehttp/streams" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "guzzlehttp-streams-47aaa48e27dae43d39fc1cea0ccf0d84ac1a2ba5";
        src = fetchurl {
          url = "https://api.github.com/repos/guzzle/streams/zipball/47aaa48e27dae43d39fc1cea0ccf0d84ac1a2ba5";
          sha256 = "1ax2b61l31vsx5814iak7l35rmh9yk0rbps5gndrkwlf27ciq9jy";
        };
      };
    };
    "jeremykendall/php-domain-parser" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "jeremykendall-php-domain-parser-026a459bb2d32b0352731b5cb525f2c1d2b9d673";
        src = fetchurl {
          url = "https://api.github.com/repos/jeremykendall/php-domain-parser/zipball/026a459bb2d32b0352731b5cb525f2c1d2b9d673";
          sha256 = "0xz09zcw0z51x7p36s65lkpv0d0lv44j9xs49zj8dmfh2rz4jl31";
        };
      };
    };
    "justinrainbow/json-schema" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "justinrainbow-json-schema-dcb6e1006bb5fd1e392b4daa68932880f37550d4";
        src = fetchurl {
          url = "https://api.github.com/repos/justinrainbow/json-schema/zipball/dcb6e1006bb5fd1e392b4daa68932880f37550d4";
          sha256 = "175m4r5d3y7kk82907v7k3rvxc5g4k4sg566jnhrcyz18zs681jz";
        };
      };
    };
    "lcobucci/jwt" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "lcobucci-jwt-82be04b4753f8b7693b62852b7eab30f97524f9b";
        src = fetchurl {
          url = "https://api.github.com/repos/lcobucci/jwt/zipball/82be04b4753f8b7693b62852b7eab30f97524f9b";
          sha256 = "08lgs386g8a91w9hz54adysl8h7g9dbjm6xc98pqgzgh0h0277zh";
        };
      };
    };
    "league/container" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "league-container-43f35abd03a12977a60ffd7095efd6a7808488c0";
        src = fetchurl {
          url = "https://api.github.com/repos/thephpleague/container/zipball/43f35abd03a12977a60ffd7095efd6a7808488c0";
          sha256 = "1kj3xzbplydqldj5bbz70m3c4aq51mmlv2nrfifc99vg30xk8cbk";
        };
      };
    };
    "league/event" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "league-event-d2cc124cf9a3fab2bb4ff963307f60361ce4d119";
        src = fetchurl {
          url = "https://api.github.com/repos/thephpleague/event/zipball/d2cc124cf9a3fab2bb4ff963307f60361ce4d119";
          sha256 = "1fc8aj0mpbrnh3b93gn8pypix28nf2gfvi403kfl7ibh5iz6ds5l";
        };
      };
    };
    "league/oauth2-server" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "league-oauth2-server-a1a6cb7b4c7e61b5d2b40384c520b72f192d07c4";
        src = fetchurl {
          url = "https://api.github.com/repos/thephpleague/oauth2-server/zipball/a1a6cb7b4c7e61b5d2b40384c520b72f192d07c4";
          sha256 = "0s2ibwh1xmj0khdm4389s8wicibyswhij5p02zhg1v85z3pwwb85";
        };
      };
    };
    "league/uri" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "league-uri-e7a31846c3f00c190bd2817a36e943c22a1e2512";
        src = fetchurl {
          url = "https://api.github.com/repos/thephpleague/uri/zipball/e7a31846c3f00c190bd2817a36e943c22a1e2512";
          sha256 = "0ic8lq0f3f3pcfbz2d9sj6mv6vmpq02svc9cvznn0z1khqc9r7vv";
        };
      };
    };
    "monolog/monolog" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "monolog-monolog-bfc9ebb28f97e7a24c45bdc3f0ff482e47bb0266";
        src = fetchurl {
          url = "https://api.github.com/repos/Seldaek/monolog/zipball/bfc9ebb28f97e7a24c45bdc3f0ff482e47bb0266";
          sha256 = "0h3nnxjf2bdh7nmpqnpij99lqv6bw13r2bx83d8vn5zvblwg5png";
        };
      };
    };
    "nesbot/carbon" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "nesbot-carbon-5be4fdf97076a685b23efdedfc2b73ad0c5eab70";
        src = fetchurl {
          url = "https://api.github.com/repos/briannesbitt/Carbon/zipball/5be4fdf97076a685b23efdedfc2b73ad0c5eab70";
          sha256 = "0cb2p5xd71gn9qy52vdsxim98k8f8hbpcmc4df6nzzmhw7gzhzrz";
        };
      };
    };
    "nikic/fast-route" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "nikic-fast-route-181d480e08d9476e61381e04a71b34dc0432e812";
        src = fetchurl {
          url = "https://api.github.com/repos/nikic/FastRoute/zipball/181d480e08d9476e61381e04a71b34dc0432e812";
          sha256 = "0sjqivm0gp6d6nal58n4r5wzyi21r4hdzn4v31ydgjgni7877p4i";
        };
      };
    };
    "onelogin/php-saml" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "onelogin-php-saml-c9026b26395a65184550055d9a01bdf9dbd30861";
        src = fetchurl {
          url = "https://api.github.com/repos/onelogin/php-saml/zipball/c9026b26395a65184550055d9a01bdf9dbd30861";
          sha256 = "0pbxp69jydwzr486gz0m6srvgfqi5ix55ns1200q10ad98pzq93g";
        };
      };
    };
    "paragonie/random_compat" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "paragonie-random_compat-0a58ef6e3146256cc3dc7cc393927bcc7d1b72db";
        src = fetchurl {
          url = "https://api.github.com/repos/paragonie/random_compat/zipball/0a58ef6e3146256cc3dc7cc393927bcc7d1b72db";
          sha256 = "10sr7y5mg7y6drxf3bibcnj8afm5ax4qwwfv8v9jaxkl5lxbqgm2";
        };
      };
    };
    "phpmailer/phpmailer" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "phpmailer-phpmailer-0c41a36d4508d470e376498c1c0c527aa36a2d59";
        src = fetchurl {
          url = "https://api.github.com/repos/PHPMailer/PHPMailer/zipball/0c41a36d4508d470e376498c1c0c527aa36a2d59";
          sha256 = "0hf8fsxn47vy90h7fhhr6543ldvzslgldmf14yxgk6xp8dxvigj7";
        };
      };
    };
    "phpseclib/phpseclib" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "phpseclib-phpseclib-11cf67cf78dc4acb18dc9149a57be4aee5036ce0";
        src = fetchurl {
          url = "https://api.github.com/repos/phpseclib/phpseclib/zipball/11cf67cf78dc4acb18dc9149a57be4aee5036ce0";
          sha256 = "0f41l88qrgk21zbvbqmjc02nxpjxxnn68dv2aj6yrqbrbyqc12wj";
        };
      };
    };
    "pimple/pimple" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "pimple-pimple-9e403941ef9d65d20cba7d54e29fe906db42cf32";
        src = fetchurl {
          url = "https://api.github.com/repos/silexphp/Pimple/zipball/9e403941ef9d65d20cba7d54e29fe906db42cf32";
          sha256 = "0lmsv8k6cyqis9hmwgqx0crxxgdd6s9jzh8c1qxlr762dxs94lbj";
        };
      };
    };
    "psr/cache" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "psr-cache-d11b50ad223250cf17b86e38383413f5a6764bf8";
        src = fetchurl {
          url = "https://api.github.com/repos/php-fig/cache/zipball/d11b50ad223250cf17b86e38383413f5a6764bf8";
          sha256 = "06i2k3dx3b4lgn9a4v1dlgv8l9wcl4kl7vzhh63lbji0q96hv8qz";
        };
      };
    };
    "psr/container" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "psr-container-b7ce3b176482dbbc1245ebf52b181af44c2cf55f";
        src = fetchurl {
          url = "https://api.github.com/repos/php-fig/container/zipball/b7ce3b176482dbbc1245ebf52b181af44c2cf55f";
          sha256 = "0rkz64vgwb0gfi09klvgay4qnw993l1dc03vyip7d7m2zxi6cy4j";
        };
      };
    };
    "psr/http-message" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "psr-http-message-f6561bf28d520154e4b0ec72be95418abe6d9363";
        src = fetchurl {
          url = "https://api.github.com/repos/php-fig/http-message/zipball/f6561bf28d520154e4b0ec72be95418abe6d9363";
          sha256 = "195dd67hva9bmr52iadr4kyp2gw2f5l51lplfiay2pv6l9y4cf45";
        };
      };
    };
    "psr/log" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "psr-log-6c001f1daafa3a3ac1d8ff69ee4db8e799a654dd";
        src = fetchurl {
          url = "https://api.github.com/repos/php-fig/log/zipball/6c001f1daafa3a3ac1d8ff69ee4db8e799a654dd";
          sha256 = "1i351p3gd1pgjcjxv7mwwkiw79f1xiqr38irq22156h05zlcx80d";
        };
      };
    };
    "ralouphie/getallheaders" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "ralouphie-getallheaders-5601c8a83fbba7ef674a7369456d12f1e0d0eafa";
        src = fetchurl {
          url = "https://api.github.com/repos/ralouphie/getallheaders/zipball/5601c8a83fbba7ef674a7369456d12f1e0d0eafa";
          sha256 = "1axanjwrxcmnh6am7a813j1xqa1cx2jp0gal93dr33wpqq0ys09l";
        };
      };
    };
    "react/promise" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "react-promise-31ffa96f8d2ed0341a57848cbb84d88b89dd664d";
        src = fetchurl {
          url = "https://api.github.com/repos/reactphp/promise/zipball/31ffa96f8d2ed0341a57848cbb84d88b89dd664d";
          sha256 = "12pz35wc1zy4djkigqikg74fxi88s46fk22hzp5qkyjy829bbj42";
        };
      };
    };
    "robrichards/xmlseclibs" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "robrichards-xmlseclibs-406c68ac9124db033d079284b719958b829cb830";
        src = fetchurl {
          url = "https://api.github.com/repos/robrichards/xmlseclibs/zipball/406c68ac9124db033d079284b719958b829cb830";
          sha256 = "0fqd4q056b950z1n1bcyv0wf1ym3wb6mkdfrysy0k8nghd8xi1y2";
        };
      };
    };
    "slim/slim" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "slim-slim-eaee12ef8d0750db62b8c548016d82fb33addb6b";
        src = fetchurl {
          url = "https://api.github.com/repos/slimphp/Slim/zipball/eaee12ef8d0750db62b8c548016d82fb33addb6b";
          sha256 = "16iqfl12amaj4l14wqzb5qy48n7fvhgl6pcyn8zzbvqhknxjg7ky";
        };
      };
    };
    "soundasleep/html2text" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "soundasleep-html2text-cdb89f6ffa2c4cc78f8ed9ea6ee0594a9133ccad";
        src = fetchurl {
          url = "https://api.github.com/repos/soundasleep/html2text/zipball/cdb89f6ffa2c4cc78f8ed9ea6ee0594a9133ccad";
          sha256 = "13hzxh8fph8imzkhrp81614aig35jjs4ki290jkq30s4hjyn8z7w";
        };
      };
    };
    "symfony/console" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-console-15a9104356436cb26e08adab97706654799d31d8";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/console/zipball/15a9104356436cb26e08adab97706654799d31d8";
          sha256 = "08bhjc738vk9wcbm3a5kph0lfdyavis6hvy6sp4zrgvh33qxnhff";
        };
      };
    };
    "symfony/debug" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-debug-681afbb26488903c5ac15e63734f1d8ac430c9b9";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/debug/zipball/681afbb26488903c5ac15e63734f1d8ac430c9b9";
          sha256 = "1j6waq4a471qqniv16hmaiaa4cs7f895xx2945d7z8cy8ldrc90f";
        };
      };
    };
    "symfony/event-dispatcher" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-event-dispatcher-a088aafcefb4eef2520a290ed82e4374092a6dff";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/event-dispatcher/zipball/a088aafcefb4eef2520a290ed82e4374092a6dff";
          sha256 = "1b46m2ajd6l5fc5lgijf9rxil0y9pzjhqh1zcih6rzv209qgr8hn";
        };
      };
    };
    "symfony/filesystem" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-filesystem-acf99758b1df8e9295e6b85aa69f294565c9fedb";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/filesystem/zipball/acf99758b1df8e9295e6b85aa69f294565c9fedb";
          sha256 = "01p8d80zskw04sn6rz4sq6lsjgxlsp1x2w8zdwbj2xarn2rfgqpl";
        };
      };
    };
    "symfony/finder" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-finder-61af5ce0b34b942d414fe8f1b11950d0e9a90e98";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/finder/zipball/61af5ce0b34b942d414fe8f1b11950d0e9a90e98";
          sha256 = "0x3c5401a521pc813as944nsybwj2369dryrm0g27abplj9p7yb9";
        };
      };
    };
    "symfony/options-resolver" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-options-resolver-ed3b397f9c07c8ca388b2a1ef744403b4d4ecc44";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/options-resolver/zipball/ed3b397f9c07c8ca388b2a1ef744403b4d4ecc44";
          sha256 = "0hlw9f40v3h1w7060gds1bfh30jsjxw2f7ka4dqjvabm7jmzfb7m";
        };
      };
    };
    "symfony/polyfill-ctype" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-polyfill-ctype-82ebae02209c21113908c229e9883c419720738a";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/polyfill-ctype/zipball/82ebae02209c21113908c229e9883c419720738a";
          sha256 = "1p3grd56c4agrv3v5lfnsi0ryghha7f0jx5hqs2lj7hvcx1fzam5";
        };
      };
    };
    "symfony/polyfill-mbstring" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-polyfill-mbstring-fe5e94c604826c35a32fa832f35bd036b6799609";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/polyfill-mbstring/zipball/fe5e94c604826c35a32fa832f35bd036b6799609";
          sha256 = "18n89mqn3nw62gmd10h63ci8s45jya3kcvx5g0pxdnm7grxn0ykr";
        };
      };
    };
    "symfony/process" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-process-a9c4dfbf653023b668c282e4e02609d131f4057a";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/process/zipball/a9c4dfbf653023b668c282e4e02609d131f4057a";
          sha256 = "1ch065l0dnsfmpcfzqvqga1bams5qkbq20aj6kj41v3irm03wq4y";
        };
      };
    };
    "symfony/translation" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-translation-301a5d627220a1c4ee522813b0028653af6c4f54";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/translation/zipball/301a5d627220a1c4ee522813b0028653af6c4f54";
          sha256 = "0iqnr26hmdkf9wb6qqhc58l3fprv7dn6gccm30k0sf08k6vahsvx";
        };
      };
    };
    "symfony/validator" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-validator-cc3f577d8887737df4d77a4c0cc6e3c22164cea4";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/validator/zipball/cc3f577d8887737df4d77a4c0cc6e3c22164cea4";
          sha256 = "0nwgdrs4z40wy45dwh7pzd55s8g7nmppczf8n8i9js67s6n9bpk3";
        };
      };
    };
    "symfony/yaml" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-yaml-212a27b731e5bfb735679d1ffaac82bd6a1dc996";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/yaml/zipball/212a27b731e5bfb735679d1ffaac82bd6a1dc996";
          sha256 = "1wyfab49aias5p0l5cl50r27wn2j0zc7q85rxsl0sxz8q7dxmk59";
        };
      };
    };
    "tuupola/slim-jwt-auth" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "tuupola-slim-jwt-auth-bca54de41a8207d4d67faf3601a06a96cb7ed48f";
        src = fetchurl {
          url = "https://api.github.com/repos/tuupola/slim-jwt-auth/zipball/bca54de41a8207d4d67faf3601a06a96cb7ed48f";
          sha256 = "0j3vsb5vrk1yhvb43wyis8mqx3i1p81260pqvixmw7h90a37jafw";
        };
      };
    };
    "wikimedia/composer-merge-plugin" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "wikimedia-composer-merge-plugin-81c6ac72a24a67383419c7eb9aa2b3437f2ab100";
        src = fetchurl {
          url = "https://api.github.com/repos/wikimedia/composer-merge-plugin/zipball/81c6ac72a24a67383419c7eb9aa2b3437f2ab100";
          sha256 = "0nfc7vwffpd1yskp3dj1vl2774ik3amxbsdv5pfvq6ibk9lkwcq4";
        };
      };
    };
  };
  devPackages = {
    "behat/gherkin" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "behat-gherkin-ab0a02ea14893860bca00f225f5621d351a3ad07";
        src = fetchurl {
          url = "https://api.github.com/repos/Behat/Gherkin/zipball/ab0a02ea14893860bca00f225f5621d351a3ad07";
          sha256 = "1znxil7lkyr87ksampxqnzvfsnb5my9k4pjn74yk544398pg4hkf";
        };
      };
    };
    "behat/transliterator" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "behat-transliterator-826ce7e9c2a6664c0d1f381cbb38b1fb80a7ee2c";
        src = fetchurl {
          url = "https://api.github.com/repos/Behat/Transliterator/zipball/826ce7e9c2a6664c0d1f381cbb38b1fb80a7ee2c";
          sha256 = "1mgc9azx79fkrxahji3xwbgqhlcnvh3xk6llqdvhjb7vgzj4bqq0";
        };
      };
    };
    "browserstack/browserstack-local" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "browserstack-browserstack-local-491c6e31960ce8111d2cb70cb84d03e73f270dbb";
        src = fetchurl {
          url = "https://api.github.com/repos/browserstack/browserstack-local-php/zipball/491c6e31960ce8111d2cb70cb84d03e73f270dbb";
          sha256 = "0459896kkmrrczfxmp4ym2rjns34r6llya1m30afhmzsww51y023";
        };
      };
    };
    "codeception/codeception" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "codeception-codeception-88a0a3eb27c5c16868b8c745e8c2812d610caf68";
        src = fetchurl {
          url = "https://api.github.com/repos/Codeception/Codeception/zipball/88a0a3eb27c5c16868b8c745e8c2812d610caf68";
          sha256 = "0akmyn3i1579215dsd6554832fkpsj2x8ws9zzfg4kd0jj1qzyns";
        };
      };
    };
    "codeception/phpunit-wrapper" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "codeception-phpunit-wrapper-299e3aece31489ed962e6c39fe2fb6f3bbd2eb16";
        src = fetchurl {
          url = "https://api.github.com/repos/Codeception/phpunit-wrapper/zipball/299e3aece31489ed962e6c39fe2fb6f3bbd2eb16";
          sha256 = "01wjxvs0j875gqjinkkgfznh0s8avqih0bs4yyh0qbrpdlk27qmx";
        };
      };
    };
    "codeception/stub" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "codeception-stub-853657f988942f7afb69becf3fd0059f192c705a";
        src = fetchurl {
          url = "https://api.github.com/repos/Codeception/Stub/zipball/853657f988942f7afb69becf3fd0059f192c705a";
          sha256 = "08lbylnjx965m8bn389xcp69hidiqhvqmdxv8fbb8hp4zack83yb";
        };
      };
    };
    "composer/semver" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "composer-semver-46d9139568ccb8d9e7cdd4539cab7347568a5e2e";
        src = fetchurl {
          url = "https://api.github.com/repos/composer/semver/zipball/46d9139568ccb8d9e7cdd4539cab7347568a5e2e";
          sha256 = "11nq81abq684v12xfv6xg2y6h8fhyn76s50hvacs51sqqs926i0d";
        };
      };
    };
    "composer/xdebug-handler" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "composer-xdebug-handler-46867cbf8ca9fb8d60c506895449eb799db1184f";
        src = fetchurl {
          url = "https://api.github.com/repos/composer/xdebug-handler/zipball/46867cbf8ca9fb8d60c506895449eb799db1184f";
          sha256 = "0y4axhr65ygd2a619xrbfd3yr02jxnazf3clfxrzd63m1jwc5mmx";
        };
      };
    };
    "doctrine/annotations" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "doctrine-annotations-54cacc9b81758b14e3ce750f205a393d52339e97";
        src = fetchurl {
          url = "https://api.github.com/repos/doctrine/annotations/zipball/54cacc9b81758b14e3ce750f205a393d52339e97";
          sha256 = "1wi5skihqbcinlkrkr15nmmvqkn2gydqib8xl232abdvfq1q0w24";
        };
      };
    };
    "doctrine/instantiator" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "doctrine-instantiator-8e884e78f9f0eb1329e445619e04456e64d8051d";
        src = fetchurl {
          url = "https://api.github.com/repos/doctrine/instantiator/zipball/8e884e78f9f0eb1329e445619e04456e64d8051d";
          sha256 = "15dcja45rnwya431pcm826l68k1g8f1fabl7rih69alcdyvdlln4";
        };
      };
    };
    "doctrine/lexer" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "doctrine-lexer-1febd6c3ef84253d7c815bed85fc622ad207a9f8";
        src = fetchurl {
          url = "https://api.github.com/repos/doctrine/lexer/zipball/1febd6c3ef84253d7c815bed85fc622ad207a9f8";
          sha256 = "0ndvnx841cqr3myvvv4j7isyiaz6zmp2g8lpc42q5gqi1rv4n8vj";
        };
      };
    };
    "facebook/webdriver" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "facebook-webdriver-bd8c740097eb9f2fc3735250fc1912bc811a954e";
        src = fetchurl {
          url = "https://api.github.com/repos/facebook/php-webdriver/zipball/bd8c740097eb9f2fc3735250fc1912bc811a954e";
          sha256 = "0vwfc1fpxx6grmi43x2aril9744ymcbl5iif8c2ck2qwc9iyd448";
        };
      };
    };
    "filp/whoops" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "filp-whoops-bc0fd11bc455cc20ee4b5edabc63ebbf859324c7";
        src = fetchurl {
          url = "https://api.github.com/repos/filp/whoops/zipball/bc0fd11bc455cc20ee4b5edabc63ebbf859324c7";
          sha256 = "0bc9czyqxfpp9bdyxk2av18aii1x4n6g92jik87zm47y3yxmlmfv";
        };
      };
    };
    "flow/jsonpath" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "flow-jsonpath-f0222818d5c938e4ab668ab2e2c079bd51a27112";
        src = fetchurl {
          url = "https://api.github.com/repos/FlowCommunications/JSONPath/zipball/f0222818d5c938e4ab668ab2e2c079bd51a27112";
          sha256 = "0h8iz50hik593p3yv9mkl551wc18flpibl2hhp6vlvv2b80iznka";
        };
      };
    };
    "friendsofphp/php-cs-fixer" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "friendsofphp-php-cs-fixer-20064511ab796593a3990669eff5f5b535001f7c";
        src = fetchurl {
          url = "https://api.github.com/repos/FriendsOfPHP/PHP-CS-Fixer/zipball/20064511ab796593a3990669eff5f5b535001f7c";
          sha256 = "016jyjjhn0g2963cf92vsl0f2py6g286wgyx1r2787g9sw379ck6";
        };
      };
    };
    "fzaninotto/faker" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "fzaninotto-faker-f72816b43e74063c8b10357394b6bba8cb1c10de";
        src = fetchurl {
          url = "https://api.github.com/repos/fzaninotto/Faker/zipball/f72816b43e74063c8b10357394b6bba8cb1c10de";
          sha256 = "18dlb13c7ablzad7ixjsydig1z2zmgd8jvjk3az8y2k7496yqxb6";
        };
      };
    };
    "hamcrest/hamcrest-php" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "hamcrest-hamcrest-php-776503d3a8e85d4f9a1148614f95b7a608b046ad";
        src = fetchurl {
          url = "https://api.github.com/repos/hamcrest/hamcrest-php/zipball/776503d3a8e85d4f9a1148614f95b7a608b046ad";
          sha256 = "12f2xsamhcksxcma4yzmm4clmhms1lz2aw4391zmb7y6agpwvjma";
        };
      };
    };
    "hoa/consistency" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "hoa-consistency-fd7d0adc82410507f332516faf655b6ed22e4c2f";
        src = fetchurl {
          url = "https://api.github.com/repos/hoaproject/Consistency/zipball/fd7d0adc82410507f332516faf655b6ed22e4c2f";
          sha256 = "1vwf5rpn4v85fsbwwjasxb2f9d4r9r6wa9n6lcnjxxk6l30j32fb";
        };
      };
    };
    "hoa/console" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "hoa-console-e231fd3ea70e6d773576ae78de0bdc1daf331a66";
        src = fetchurl {
          url = "https://api.github.com/repos/hoaproject/Console/zipball/e231fd3ea70e6d773576ae78de0bdc1daf331a66";
          sha256 = "1ykv2gx236mq3pgaph85l4nww1fzix85nycdkwy8pydx1c5hshkw";
        };
      };
    };
    "hoa/event" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "hoa-event-6c0060dced212ffa3af0e34bb46624f990b29c54";
        src = fetchurl {
          url = "https://api.github.com/repos/hoaproject/Event/zipball/6c0060dced212ffa3af0e34bb46624f990b29c54";
          sha256 = "1yqs54b7jihsfn8hzxhhac4k205f0skri06ik3jd3vvl4dgkk3ih";
        };
      };
    };
    "hoa/exception" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "hoa-exception-091727d46420a3d7468ef0595651488bfc3a458f";
        src = fetchurl {
          url = "https://api.github.com/repos/hoaproject/Exception/zipball/091727d46420a3d7468ef0595651488bfc3a458f";
          sha256 = "105npikirx21b7g06j2dv6k0n686d3wxf8n8afn2fxifp0r9y9yx";
        };
      };
    };
    "hoa/file" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "hoa-file-35cb979b779bc54918d2f9a4e02ed6c7a1fa67ca";
        src = fetchurl {
          url = "https://api.github.com/repos/hoaproject/File/zipball/35cb979b779bc54918d2f9a4e02ed6c7a1fa67ca";
          sha256 = "0ldgcy1i6r9kmg8gc4s44m3d3dgnmr3nvxav01zw3bhppcpwqndc";
        };
      };
    };
    "hoa/iterator" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "hoa-iterator-d1120ba09cb4ccd049c86d10058ab94af245f0cc";
        src = fetchurl {
          url = "https://api.github.com/repos/hoaproject/Iterator/zipball/d1120ba09cb4ccd049c86d10058ab94af245f0cc";
          sha256 = "0g3whchg1bv7mlvpp056saji7zsz5hmp4npl95f6bbdq7c41c4a0";
        };
      };
    };
    "hoa/protocol" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "hoa-protocol-5c2cf972151c45f373230da170ea015deecf19e2";
        src = fetchurl {
          url = "https://api.github.com/repos/hoaproject/Protocol/zipball/5c2cf972151c45f373230da170ea015deecf19e2";
          sha256 = "1w29dclrdyg3bkqcxpp1sccmbx0aakh2y6l5m1lw1s3245k029d4";
        };
      };
    };
    "hoa/stream" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "hoa-stream-3293cfffca2de10525df51436adf88a559151d82";
        src = fetchurl {
          url = "https://api.github.com/repos/hoaproject/Stream/zipball/3293cfffca2de10525df51436adf88a559151d82";
          sha256 = "1bjdzf3q3hqahlc4ckxal3izbrf32hf0mdfrisr72lyz3wk462my";
        };
      };
    };
    "hoa/ustring" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "hoa-ustring-e6326e2739178799b1fe3fdd92029f9517fa17a0";
        src = fetchurl {
          url = "https://api.github.com/repos/hoaproject/Ustring/zipball/e6326e2739178799b1fe3fdd92029f9517fa17a0";
          sha256 = "166dd103j8m7xna0m6ddsa03frayvkmlmg8wkq9ksh2yn642zhb3";
        };
      };
    };
    "jeroendesloovere/vcard" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "jeroendesloovere-vcard-2a0b7dc48e6ee75ca5ff7372e0a7854100d4ed0f";
        src = fetchurl {
          url = "https://api.github.com/repos/jeroendesloovere/vcard/zipball/2a0b7dc48e6ee75ca5ff7372e0a7854100d4ed0f";
          sha256 = "06x9k96rl2p87wdc7hjvdm232ij7sqnvcacl9yj9gwvss9zqwcfh";
        };
      };
    };
    "leafo/scssphp" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "leafo-scssphp-1d656f8c02a3a69404bba6b28ec4e06edddf0f49";
        src = fetchurl {
          url = "https://api.github.com/repos/leafo/scssphp/zipball/1d656f8c02a3a69404bba6b28ec4e06edddf0f49";
          sha256 = "0rlc7y9v6fmf2hk3lnpspa2n9ij21s62nqrbw17lfzjlh8185bmg";
        };
      };
    };
    "mikey179/vfsStream" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "mikey179-vfsStream-095238a0711c974ae5b4ebf4c4534a23f3f6c99d";
        src = fetchurl {
          url = "https://api.github.com/repos/bovigo/vfsStream/zipball/095238a0711c974ae5b4ebf4c4534a23f3f6c99d";
          sha256 = "1clnajnd2scsk8bm5n90f30i4h9932gfba68smkav5nzdrphy85s";
        };
      };
    };
    "mockery/mockery" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "mockery-mockery-0eb0b48c3f07b3b89f5169ce005b7d05b18cf1d2";
        src = fetchurl {
          url = "https://api.github.com/repos/mockery/mockery/zipball/0eb0b48c3f07b3b89f5169ce005b7d05b18cf1d2";
          sha256 = "14iq33y1hxnz5q0v4kwg4gz17sd3xx7bs87zhym5jhb7f93s0xbc";
        };
      };
    };
    "myclabs/deep-copy" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "myclabs-deep-copy-3b8a3a99ba1f6a3952ac2747d989303cbd6b7a3e";
        src = fetchurl {
          url = "https://api.github.com/repos/myclabs/DeepCopy/zipball/3b8a3a99ba1f6a3952ac2747d989303cbd6b7a3e";
          sha256 = "1mxn444j48gnk11pjm2b4ixxa8y6lcmrqslshm7zwqbl96k4mx48";
        };
      };
    };
    "php-cs-fixer/diff" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "php-cs-fixer-diff-78bb099e9c16361126c86ce82ec4405ebab8e756";
        src = fetchurl {
          url = "https://api.github.com/repos/PHP-CS-Fixer/diff/zipball/78bb099e9c16361126c86ce82ec4405ebab8e756";
          sha256 = "082w79q2bipw5iibpw6whihnz2zafljh5bgpfs4qdxmz25n8g00l";
        };
      };
    };
    "phpdocumentor/reflection-common" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "phpdocumentor-reflection-common-21bdeb5f65d7ebf9f43b1b25d404f87deab5bfb6";
        src = fetchurl {
          url = "https://api.github.com/repos/phpDocumentor/ReflectionCommon/zipball/21bdeb5f65d7ebf9f43b1b25d404f87deab5bfb6";
          sha256 = "1yaf1zg9lnkfnq2ndpviv0hg5bza9vjvv5l4wgcn25lx1p8a94w2";
        };
      };
    };
    "phpdocumentor/reflection-docblock" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "phpdocumentor-reflection-docblock-bf329f6c1aadea3299f08ee804682b7c45b326a2";
        src = fetchurl {
          url = "https://api.github.com/repos/phpDocumentor/ReflectionDocBlock/zipball/bf329f6c1aadea3299f08ee804682b7c45b326a2";
          sha256 = "1sk0la8k0d4adi149ghbs37cbli158j9xb2qd17b1wjlp5lriw0a";
        };
      };
    };
    "phpdocumentor/type-resolver" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "phpdocumentor-type-resolver-9c977708995954784726e25d0cd1dddf4e65b0f7";
        src = fetchurl {
          url = "https://api.github.com/repos/phpDocumentor/TypeResolver/zipball/9c977708995954784726e25d0cd1dddf4e65b0f7";
          sha256 = "0h888r2iy2290yp9i3fij8wd5b7960yi7yn1rwh26x1xxd83n2mb";
        };
      };
    };
    "phpspec/prophecy" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "phpspec-prophecy-4ba436b55987b4bf311cb7c6ba82aa528aac0a06";
        src = fetchurl {
          url = "https://api.github.com/repos/phpspec/prophecy/zipball/4ba436b55987b4bf311cb7c6ba82aa528aac0a06";
          sha256 = "0sz9fg8r4yvpgrhsh6qaic3p89pafdj8bdf4izbcccq6mdhclxn6";
        };
      };
    };
    "phpunit/php-code-coverage" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "phpunit-php-code-coverage-ef7b2f56815df854e66ceaee8ebe9393ae36a40d";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/php-code-coverage/zipball/ef7b2f56815df854e66ceaee8ebe9393ae36a40d";
          sha256 = "0i6lbr08g63vzd0dh1ax6b0x8m86r79ia7iggx6k42898332qgw3";
        };
      };
    };
    "phpunit/php-file-iterator" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "phpunit-php-file-iterator-730b01bc3e867237eaac355e06a36b85dd93a8b4";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/php-file-iterator/zipball/730b01bc3e867237eaac355e06a36b85dd93a8b4";
          sha256 = "0kbg907g9hrx7pv8v0wnf4ifqywdgvigq6y6z00lyhgd0b8is060";
        };
      };
    };
    "phpunit/php-text-template" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "phpunit-php-text-template-31f8b717e51d9a2afca6c9f046f5d69fc27c8686";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/php-text-template/zipball/31f8b717e51d9a2afca6c9f046f5d69fc27c8686";
          sha256 = "1y03m38qqvsbvyakd72v4dram81dw3swyn5jpss153i5nmqr4p76";
        };
      };
    };
    "phpunit/php-timer" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "phpunit-php-timer-3dcf38ca72b158baf0bc245e9184d3fdffa9c46f";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/php-timer/zipball/3dcf38ca72b158baf0bc245e9184d3fdffa9c46f";
          sha256 = "1j04r0hqzrv6m1jk5nb92k2nnana72nscqpfk3rgv3fzrrv69ljr";
        };
      };
    };
    "phpunit/php-token-stream" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "phpunit-php-token-stream-1ce90ba27c42e4e44e6d8458241466380b51fa16";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/php-token-stream/zipball/1ce90ba27c42e4e44e6d8458241466380b51fa16";
          sha256 = "0j1v83m268cddhyzi8qvqfzhpz12hrm3dyw6skyqvljdp7l9x6lk";
        };
      };
    };
    "phpunit/phpunit" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "phpunit-phpunit-b7803aeca3ccb99ad0a506fa80b64cd6a56bbc0c";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/phpunit/zipball/b7803aeca3ccb99ad0a506fa80b64cd6a56bbc0c";
          sha256 = "0m3bimpkv0cw4l35mnqzda50yhg8zgikfliq9lmdf36wda00rri7";
        };
      };
    };
    "phpunit/phpunit-mock-objects" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "phpunit-phpunit-mock-objects-a23b761686d50a560cc56233b9ecf49597cc9118";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/phpunit-mock-objects/zipball/a23b761686d50a560cc56233b9ecf49597cc9118";
          sha256 = "19sa45fzw9fhjdl470i444y64iymhdad7hmlx9q54qjh9y6fy8gk";
        };
      };
    };
    "roave/security-advisories" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "roave-security-advisories-baeb6f512b5ec8f0fd58bf890082b179f985c5a4";
        src = fetchurl {
          url = "https://api.github.com/repos/Roave/SecurityAdvisories/zipball/baeb6f512b5ec8f0fd58bf890082b179f985c5a4";
          sha256 = "0mnzq0fzfwibxk1jp3akplnh8jlmiah631h8k62bnlh0qly4x2hd";
        };
      };
    };
    "sebastian/code-unit-reverse-lookup" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "sebastian-code-unit-reverse-lookup-4419fcdb5eabb9caa61a27c7a1db532a6b55dd18";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/code-unit-reverse-lookup/zipball/4419fcdb5eabb9caa61a27c7a1db532a6b55dd18";
          sha256 = "0n0bygv2vx1l7af8szbcbn5bpr4axrgvkzd0m348m8ckmk8akvs8";
        };
      };
    };
    "sebastian/comparator" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "sebastian-comparator-2b7424b55f5047b47ac6e5ccb20b2aea4011d9be";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/comparator/zipball/2b7424b55f5047b47ac6e5ccb20b2aea4011d9be";
          sha256 = "0ymarxgnr8b3iy0w18h5z13iiv0ja17vjryryzfcwlqqhlc6w7iq";
        };
      };
    };
    "sebastian/diff" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "sebastian-diff-7f066a26a962dbe58ddea9f72a4e82874a3975a4";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/diff/zipball/7f066a26a962dbe58ddea9f72a4e82874a3975a4";
          sha256 = "1ppx21vjj79z6d584ryq451k7kvdc511awmqjkj9g4vxj1s1h3j6";
        };
      };
    };
    "sebastian/environment" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "sebastian-environment-5795ffe5dc5b02460c3e34222fee8cbe245d8fac";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/environment/zipball/5795ffe5dc5b02460c3e34222fee8cbe245d8fac";
          sha256 = "0z1zv8v7k2cycw3vzilpbs7y3mjpwdzcspzgl6pbzi8rj7f4a93l";
        };
      };
    };
    "sebastian/exporter" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "sebastian-exporter-ce474bdd1a34744d7ac5d6aad3a46d48d9bac4c4";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/exporter/zipball/ce474bdd1a34744d7ac5d6aad3a46d48d9bac4c4";
          sha256 = "1g8b7nm7f5dk7rkxhv3l6pclb95az28gi0j5g3inymysa95myh5d";
        };
      };
    };
    "sebastian/global-state" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "sebastian-global-state-bc37d50fea7d017d3d340f230811c9f1d7280af4";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/global-state/zipball/bc37d50fea7d017d3d340f230811c9f1d7280af4";
          sha256 = "0y1x16mf9q38s7rlc7k2s6sxn2ccxmyk1q5zgh24hr4yp035f0pb";
        };
      };
    };
    "sebastian/object-enumerator" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "sebastian-object-enumerator-1311872ac850040a79c3c058bea3e22d0f09cbb7";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/object-enumerator/zipball/1311872ac850040a79c3c058bea3e22d0f09cbb7";
          sha256 = "0f4vdgpq2alsj43bap0sarr79fxnzwpddq96kd18kgfl6n6m730y";
        };
      };
    };
    "sebastian/recursion-context" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "sebastian-recursion-context-2c3ba150cbec723aa057506e73a8d33bdb286c9a";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/recursion-context/zipball/2c3ba150cbec723aa057506e73a8d33bdb286c9a";
          sha256 = "0rfa6qwayrlzaf4ycwm10m870bmzq152w1rn7wp4vrm283zkf4cs";
        };
      };
    };
    "sebastian/resource-operations" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "sebastian-resource-operations-ce990bb21759f94aeafd30209e8cfcdfa8bc3f52";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/resource-operations/zipball/ce990bb21759f94aeafd30209e8cfcdfa8bc3f52";
          sha256 = "19jfc8xzkyycglrcz85sv3ajmxvxwkw4sid5l4i8g6wmz9npbsxl";
        };
      };
    };
    "sebastian/version" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "sebastian-version-99732be0ddb3361e16ad77b68ba41efc8e979019";
        src = fetchurl {
          url = "https://api.github.com/repos/sebastianbergmann/version/zipball/99732be0ddb3361e16ad77b68ba41efc8e979019";
          sha256 = "0wrw5hskz2hg5aph9r1fhnngfrcvhws1pgs0lfrwindy066z6fj7";
        };
      };
    };
    "symfony/browser-kit" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-browser-kit-7f2b0843d5045468225f9a9b27a0cb171ae81828";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/browser-kit/zipball/7f2b0843d5045468225f9a9b27a0cb171ae81828";
          sha256 = "1mb9znb0vknphvj3rqwk4l2i8zqnc1zzil09api5w6mmk6j3jqid";
        };
      };
    };
    "symfony/css-selector" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-css-selector-8ca29297c29b64fb3a1a135e71cb25f67f9fdccf";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/css-selector/zipball/8ca29297c29b64fb3a1a135e71cb25f67f9fdccf";
          sha256 = "15kcpdnki3bljd5z6xlqq5vwn42dn3i0yi91j6l8546wh7gfsbwm";
        };
      };
    };
    "symfony/dom-crawler" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-dom-crawler-d40023c057393fb25f7ca80af2a56ed948c45a09";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/dom-crawler/zipball/d40023c057393fb25f7ca80af2a56ed948c45a09";
          sha256 = "03l9sp0q14jkk7jl6ghddp192qvpg0wp7mvjrkvxwx1vkjiq3dcg";
        };
      };
    };
    "symfony/polyfill-php70" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-polyfill-php70-bc4858fb611bda58719124ca079baff854149c89";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/polyfill-php70/zipball/bc4858fb611bda58719124ca079baff854149c89";
          sha256 = "0b3cmb7bmixwqqjclzkah6aq03ic00g7rnla3scq100y7fc5za0n";
        };
      };
    };
    "symfony/polyfill-php72" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-polyfill-php72-ab50dcf166d5f577978419edd37aa2bb8eabce0c";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/polyfill-php72/zipball/ab50dcf166d5f577978419edd37aa2bb8eabce0c";
          sha256 = "0a2qn3n12kzd79g08aazcjv6zd834zrrlxcskhcp5vag8z46psgg";
        };
      };
    };
    "symfony/stopwatch" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-stopwatch-2a651c2645c10bbedd21170771f122d935e0dd58";
        src = fetchurl {
          url = "https://api.github.com/repos/symfony/stopwatch/zipball/2a651c2645c10bbedd21170771f122d935e0dd58";
          sha256 = "07rn1p7ygqdmicg3igri88b33ffy82milfvqjl6k9wvy5fdnyn6h";
        };
      };
    };
    "webmozart/assert" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "webmozart-assert-83e253c8e0be5b0257b881e1827274667c5c17a9";
        src = fetchurl {
          url = "https://api.github.com/repos/webmozart/assert/zipball/83e253c8e0be5b0257b881e1827274667c5c17a9";
          sha256 = "0d84b0ms9mjpqx368gs7c3qs06mpbx5565j3vs43b1ygnyhhhaqk";
        };
      };
    };
  };
in
composerEnv.buildPackage {
  inherit packages devPackages noDev;
  name = "salesagility-suitecrm";
  src = ./.;
  executable = false;
  symlinkDependencies = false;
  meta = {
    homepage = "https://suitecrm.com";
    license = "GPL-3.0";
  };
}
