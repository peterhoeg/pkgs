{
  lib,
  fetchpatch,
  fetchFromGitHub,
  python3Packages,
  makeWrapper,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "black-bean-control";
  version = "20190812";

  format = "other";

  src = fetchFromGitHub {
    owner = "davorf";
    repo = "BlackBeanControl";
    rev = "d8987b6470169b10399c2309dd0d4e22f4632459";
    sha256 = "sha256-W6TsPuMcF3xS2IxqnOr0kWEFmZMJIm/9E5BcwKoiabM=";
  };

  patches = [
    # needed for python3 and working with the latest broadlink library
    (fetchpatch {
      url = "https://patch-diff.githubusercontent.com/raw/davorf/BlackBeanControl/pull/45.patch";
      sha256 = "sha256-928mr36aed6IxVJza3yGZ9TnkU7BTdYpPMFHTxTTH/A=";
    })
  ];

  postPatch = ''
    substituteInPlace Settings.py \
      --replace-fail 'from os import path' 'import os' \
      --replace-fail ' path' ' os.path' \
      --replace-fail '(path' '(os.path'

    sed -i Settings.py \
      -e "s/^ApplicationDir.*/ApplicationDir = os.getenv('BBC_INI_DIR', os.getcwd())/"
  '';

  nativeBuildInputs = [ makeWrapper ];

  propagatedBuildInputs = with pypkgs; [
    broadlink
    configparser
    netaddr
    pycrypto
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 BlackBeanControl.py $out/bin/black-bean-control
    install -Dm444 -t $out/share/${pname} *.ini
    install -Dm444 -t $out/${pypkgs.python.sitePackages} Settings.py nec.py samsung.py

    wrapProgram $out/bin/black-bean-control \
      --set-default BBC_INI_DIR . \
      --run "test -e \$BBC_INI_DIR/BlackBeanControl.ini || cp --no-preserve=all $out/share/${pname}/BlackBeanControl.ini \$BBC_INI_DIR/BlackBeanControl.ini"

    runHook postInstall
  '';

  # no tests
  doCheck = false;

  pythonImportsCheck = [ "broadlink" ];

  meta = with lib; {
    description = "Control Broadlink RM devices";
    license = licenses.gpl3;
  };
}
