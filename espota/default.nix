{
  lib,
  python3,
  fetchurl,
  writeShellScript,
  sg3_utils,
}:

let
  pypkgs = python3.pkgs;

in
pypkgs.buildPythonApplication rec {
  pname = "espota";
  version = "20160103";

  src = fetchurl {
    url = "https://raw.githubusercontent.com/esp8266/Arduino/master/tools/espota.py";
    sha256 = "sha256-6fWuQAZM53KZhjJPObxBKmHV6VqltQ6Mo16awhW4VDA=";
  };

  format = "other";

  dontUnpack = true;

  installPhase = ''
    install -Dm555 ${src} $out/bin/espota
  '';

  meta = with lib; {
    description = "ESPOTA tool";
    license = licenses.free;
  };
}
