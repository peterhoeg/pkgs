{
  buildGoModule,
  lib,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "toml-merge";
  version = "0.2.0";

  src = fetchFromGitHub {
    owner = "mesosphere";
    repo = "toml-merge";
    rev = "v" + version;
    hash = "sha256-UpsdWEm2Vn07Lb9EGayPmrw8GjtagqCHjwjouHS5wio=";
  };

  vendorHash = "sha256-YqI+8uk+YTPrINpLNXQw3xmaOHcSw9Xxducd0RIB/UI=";

  postInstall = ''
    install -Dm444 -t $out/share/doc/toml-merge *.md
  '';

  meta = {
    description = "A utility to merge TOML configuration";
    license = lib.licenses.afl20;
  };
}
