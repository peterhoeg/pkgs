{
  stdenv,
  lib,
  fetchurl,
  autoPatchelfHook,
  rpmextract,
  installShellFiles,
  zlib,
}:

stdenv.mkDerivation rec {
  pname = "ilorest";
  version = "3.5.1.0-8";

  src = fetchurl {
    url = "https://downloads.linux.hpe.com/SDR/repo/ilorest/current/ilorest-${version}.x86_64.rpm";
    hash = "sha256-096mBK36VG4nHe79DT1PtJz+HWyBjndYA9fEWRzhHBU=";
  };

  unpackPhase = ''
    runHook preUnpack

    rpmextract $src

    runHook postUnpack
  '';

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin usr/sbin/*
    cp -r etc $out/etc
    install -Dm444 -t $out/lib usr/lib64/*
    installManPage usr/share/man/man?/*

    substituteInPlace $out/etc/ilorest/redfish.conf \
      --replace-fail /usr/share /var/lib

    runHook postInstall
  '';

  buildInputs = [
    stdenv.cc.cc.lib
    zlib
  ];

  nativeBuildInputs = [
    autoPatchelfHook
    installShellFiles
    rpmextract
  ];

  meta = with lib; {
    description = "HP iLO REST utility";
    license = licenses.unfree;
  };
}
