#!/usr/bin/env bash

set -eEuo pipefail

MAKEFILE=pkgs.mk

TAB="$(printf '\t')"

packages=()

echo "BUILD = @nom build -L" >$MAKEFILE
echo "" >>$MAKEFILE

for d in *; do
  # bash tests will automatically derefence symlinks so ignore anything that is
  test -L "$d" && continue

  if [ -d "$d" ]; then
    packages+=("$d")

    cat >>$MAKEFILE <<_EOF
.PHONY: $d
$d: clean
${TAB}\$(BUILD) .#$d

_EOF

  fi
done

echo ".PHONY: all" >>$MAKEFILE
echo "all: ${packages[*]}" >>$MAKEFILE
