{
  stdenv,
  lib,
  fetchFromGitHub,
  cmake,
  pkg-config,
  glib,
  pcre,
  readline,
  zlib,
}:

stdenv.mkDerivation rec {
  pname = "sdcv";
  version = "0.5.4";

  src = fetchFromGitHub {
    owner = "Dushistov";
    repo = pname;
    rev = "v" + version;
    hash = "sha256-i6odmnkoSqDIQAor7Dn26Gu+td9aeMIkwsngF7beBtE=";
  };

  buildInputs = [
    glib
    pcre
    readline
    zlib
  ];

  nativeBuildInputs = [
    cmake
    pkg-config
  ];

  cmakeFlags = [ "-DENABLE_NLS=False" ];

  meta = with lib; {
    description = "Command line interface to dictionaries in StarDict's format.";
    license = licenses.gpl2Only;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
