{
  stdenvNoCC,
  lib,
  requireFile,
  unzip,
}:

let
  generic =
    model: hash:
    stdenvNoCC.mkDerivation (finalAttrs: {
      pname = "ps${toString model}-bios";
      version = "1";

      src = requireFile rec {
        name = "ps${toString model}_bios.zip";
        inherit hash;

        message = ''
          Need a zip file with the PS${toString model} bios files.

          nix-prefetch-url file://\$PWD/${name}
        '';
      };

      nativeBuildInputs = [ unzip ];

      buildCommand = ''
        dir=$out/share/ps${toString model}/bios

        mkdir -p $dir
        unzip -d "$dir" $src
      '';

      meta = {
        description = "PS${toString model} BIOS files";
        license = lib.licenses.free; # this is obviously not true
        maintainers = with lib.maintainers; [ peterhoeg ];
        platforms = lib.platforms.all;
      };
    });

in
{
  ps1-bios = generic 1 "sha256-ldW2EkkA4ZPIrL0kv9uEIyprID5WXGqyaXqwYyb/tSo=";
  ps2-bios = generic 2 "sha256-PZAZUmsfCMRcB5soFOs9wCpa3T0GyDOjuhEqbHhEvn0=";
}
