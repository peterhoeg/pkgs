#!/usr/bin/env bash

set -eEuo pipefail

source_file=$1
ext=${2:-"${1##*.}"}

target_file=$(mktemp --suffix=.${ext})
cp ${source_file} ${target_file}
@kde-open5@ ${target_file}
