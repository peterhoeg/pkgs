#!/usr/bin/env bash

set -eEuo pipefail

KEY=${1}

if [[ "$2" == "--sign" ]]; then
  SIGN="--sign %?a?-u %a?"
fi

pgpewrap @gpg2@ \
  --batch \
  --quiet \
  --no-verbose \
  --textmode \
  --output - \
  --encrypt ${SIGN} \
  --armor \
  --always-trust \
  --encrypt-to ${KEY} \
  -- -r %r \
  -- %f
