#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

MU_HOME=${MU_HOME:-$HOME/.cache/mu}

mu cfind --muhome=$MU_HOME --format=mutt-ab "$1"
