{
  callPackage,
  stdenv,
  lib,
  libnotify,
  kde-cli-tools,
}:
let
  notify-send = callPackage ../notify-send.sh/default.nix { };
  open = if stdenv.isDarwin then "open" else "${lib.getBin kde-cli-tools}/bin/kde-open5";
in
stdenv.mkDerivation rec {
  pname = "mutt-scripts";
  version = "0.0.1";

  src = ./src;

  postPatch = ''
    substituteInPlace Makefile \
      --replace-fail /usr/local $out

    substituteInPlace mutt-open.sh \
      --replace-fail @kde-open5@ ${open}
  '';

  unpackPhase = ''
    cp ${src}/* .
  '';

  dontConfigure = true;

  dontBuild = true;

  meta = with lib; {
    description = "mutt scripts";
  };
}
