{
  stdenv,
  lib,
  fetchurl,
  libX11,
  resolution ? "1600x960", # "3200x1920" "2400x1440" "800x480"
}:

stdenv.mkDerivation rec {
  pname = "hamclock";
  version = "3.01";

  src = fetchurl {
    url = "https://www.clearskyinstitute.com/ham/HamClock/ESPHamClock.tgz";
    hash = "sha256-gpdM0QS5474SlVV8acY2gCygrESjbOOaZg8yUsqx8mE=";
  };

  postPatch = ''
    substituteInPlace ArduinoLib/Arduino.cpp \
      --replace-fail '"HOME"'        '"XDG_CONFIG_HOME"' \
      --replace-fail '"/.hamclock/"' '"/hamclock/"'
  '';

  buildFlags = [ "hamclock-${resolution}" ];

  buildInputs = [ libX11 ];

  enableParallelBuilding = true;

  # the installer bails if we are not root
  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin hamclock-[0-9]*
    install -Dm444 -t $out/share/doc/${pname} LICENSE README
    ln -s $out/bin/hamclock-${resolution} $out/bin/hamclock

    runHook postInstall
  '';

  meta = with lib; {
    description = "HAM clock";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
