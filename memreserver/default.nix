{
  lib,
  stdenv,
  fetchFromGitLab,
  meson,
  ninja,
  pkg-config,
  libdrm,
}:

stdenv.mkDerivation rec {
  pname = "memreserver";
  version = "0.0.0.20230401";

  src = fetchFromGitLab {
    domain = "git.dolansoft.org";
    owner = "lorenz";
    repo = pname;
    rev = "480253e565dab935df1d1c4e615ebc8a8dc81ba4";
    hash = "sha256-HjcrH98hH2zKdsHolYCFugL39sT1VjroVhRf8a8dpIA=";
  };

  nativeBuildInputs = [
    meson
    ninja
    pkg-config
  ];

  buildInputs = [ libdrm ];

  postPatch = ''
    substituteInPlace memreserver.service \
      --replace-fail /usr/local $out
  '';

  postInstall = ''
    install -Dm444 -t $out/lib/systemd/system ../*.service
  '';

  meta = with lib; {
    description = "Reserve memory for AMDGPU VRAM";
    license = licenses.gpl3Only;
  };
}
