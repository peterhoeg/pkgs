{
  stdenv,
  lib,
  fetchFromGitHub,
  pkg-config,
  python3Packages,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "pscoverdl";
  version = "1.1";

  format = "other";

  src = fetchFromGitHub {
    owner = "xlenore";
    repo = "pscoverdl";
    rev = "v" + version;
    hash = "sha256-nPY98YJtnJ6NHvz2V6Y4GUK4UkkG18ZSA4alstbBAhU=";
  };

  buildInputs = [
  ];

  propagatedBuildInput = with pypkgs; [
    customtkinter
    pillow
    pyyaml
    requests
    termcolor
    tqdm
  ];

  nativeBuildInputs = [
    pkg-config
  ];

  installPhase = ''
    dir=$out/${pypkgs.python.sitePackages}/pscoverdl
    mkdir -p $out/bin $dir
    cp -r src/* $dir
    mv $dir/pscoverdl.py $out/bin/${meta.mainProgram}
    chmod 555 $out/bin/*
  '';

  doCheck = false; # no tests

  pythonImportChecks = [ ];

  meta = with lib; {
    description = "PSx Cover Downloader";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
    mainProgram = "pscoverdl";
  };
}
