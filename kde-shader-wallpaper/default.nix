{
  lib,
  stdenv,
  fetchFromGitHub,
  kdePackages,
}:

let
  baseDir = "share/plasma/wallpapers";
  shadersDir = "${placeholder "out"}/${baseDir}/online.knowmad.shaderwallpaper/contents/ui/Shaders6";

in
stdenv.mkDerivation (finalAttrs: {
  pname = "kde-shader-wallpaper";
  version = "3.0.1";

  src = fetchFromGitHub {
    owner = "y4my4my4m";
    repo = "kde-shader-wallpaper";
    rev = "v" + finalAttrs.version + "-plasma6";
    hash = "sha256-n2yAiXzUxLrPPFe3Z6kL+aL8k66HVUBrJmBKCVyA8NU=";
  };

  postPatch =
    let
      sep = ''"'';
      # sep = "`";
      dir = sep + shadersDir + sep;

    in
    ''
      sed -E -i package/contents/ui/config.qml \
        -e 's@ folder:.*$@ folder: ${dir}@' \
        -e 's@ currentFolder:.*$@ currentFolder: ${dir}@'
    '';

  env.LANG = "C.UTF-8";

  nativeBuildInputs = [ kdePackages.kpackage ];

  installPhase = ''
    runHook preInstall

    # kpackagetool is missing some dependency to allow it to take the --type argument so just
    # specify the full path
    kpackagetool6 \
      --install package \
      --packageroot ${placeholder "out"}/${baseDir}

    runHook postInstall
  '';

  passthru = { inherit shadersDir; };

  meta = with lib; {
    description = "Shader wallpaper plugin";
    license = licenses.gpl3Plus;
    maintainers = with maintainers; [ peterhoeg ];
    inherit (kdePackages.kpackage.meta) platforms;
  };
})
