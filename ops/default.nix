{
  buildGoModule,
  fetchFromGitHub,
  lib,
}:

buildGoModule rec {
  pname = "ops";
  version = "0.1.31";

  src = fetchFromGitHub {
    owner = "nanovms";
    repo = pname;
    rev = "${version}";
    hash = "sha256-wSwm/bvovrziKxpfSitE5nQ6PUPl2EzSarNRJ+LLXKU=";
  };

  vendorHash = "sha256-tOtgtT33Id/w6G3dNLdZz/3uDH1Qr2fhJwnBMaO577o=";

  doCheck = false;

  # subPackages = [ "go-concourse" ];

  # ldflags = [ "-X github.com/concourse/concourse.Version=${version}" ];

  meta = with lib; {
    description = "Concourse CI";
    homepage = "https://concourse-ci.org";
    license = licenses.asl20;
    maintainers = with maintainers; [ ];
  };
}
