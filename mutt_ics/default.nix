{
  lib,
  writeScript,
  python3,
  fetchFromGitHub,
}:
let
  pypkgs = python3.pkgs;

in
pypkgs.buildPythonApplication rec {
  pname = "mutt_ics";
  version = "0.9.2";

  src = pypkgs.fetchPypi {
    inherit pname version;
    sha256 = "sha256-1E1L7E5xx/FN8BuQ/blWPNx4Ts5CUKv+pbC2dc/oWlA=";
  };

  propagatedBuildInputs = with pypkgs; [ icalendar ];

  meta = with lib; {
    description = "Show .ics files in Mutt";
    license = licenses.gpl3;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
