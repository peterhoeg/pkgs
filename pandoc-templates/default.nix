{
  stdenv,
  lib,
  buildGoModule,
  fetchFromGitHub,
}:

let
  template =
    {
      name,
      version,
      src,
    }:
    stdenv.mkDerivation rec {
      pname = "pandoc-template-${name}";
      inherit version src;

      installPhase = ''
        install -Dm555 ${name}.tex $out/share/pandoc/templates/${name}.latex
        install -Dm444 -t $out/share/doc/${pname} *.md
      '';

      meta = with lib; {
        description = "Pandoc Template";
        license = licenses.free;
        maintainers = with maintainers; [ peterhoeg ];
      };
    };

in
{
  eisvogel = template rec {
    name = "eisvogel";
    version = "2.0.0";
    src = fetchFromGitHub {
      owner = "Wandmalfarbe";
      repo = "pandoc-latex-template";
      rev = "v${version}";
      sha256 = "sha256-6o7WzfdJmq8/zL2ND1QGK5rrzZoHGyUaZ0HiTX9XEm8=";
    };
  };
}
