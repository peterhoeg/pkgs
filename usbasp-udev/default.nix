{
  stdenv,
  lib,
  fetchurl,
  platformio,
}:

stdenv.mkDerivation rec {
  pname = "usbasp-udev";
  version = "2011-05-28";

  src = fetchurl {
    url = "https://fischl.de/usbasp/usbasp.${version}.tar.gz";
    hash = "sha256-111tgQKlJvBb5bet8ZMlZVuwxFziKRr+eA0wCYgLdOE=";
  };

  dontUnpack = true;

  installPhase = ''
    runHook preInstall

    install -Dm444 ${src} $out/lib/udev/rules.d/${src.name}

    runHook postInstall
  '';

  meta = {
    description = "Teensy Loader udev rules";
    inherit (platformio.meta) homepage license platforms;
  };
}
