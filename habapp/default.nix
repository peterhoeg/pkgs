{
  stdenv,
  lib,
  runtimeShell,
  fetchFromGitHub,
  python3,
}:

let
  python = python3.override {
    packageOverrides = self: super: rec {
      # aiohttp = super.aiohttp.overridePythonAttrs (oldAttrs: rec {
      #   pname = "aiohttp";
      #   version = "3.7.4";
      #   src = super.fetchPypi {
      #     inherit pname version;
      #     sha256 = "sha256-XYTsxzFB0KDWHs4HQrt/9XUbBlfauEBfiZ086xBMx94=";
      #   };
      #   patches = [ ];
      #   doCheck = false;
      # });

      aiohttp-sse-client = super.buildPythonPackage rec {
        pname = "aiohttp-sse-client";
        version = "0.2.1";
        src = super.fetchPypi {
          inherit pname version;
          sha256 = "sha256-UATiknFiSvWGFY3HFmywaHp6WZeqtbgI9LU0AOG3Ljs=";
        };
        postPatch = ''
          substituteInPlace setup.py \
            --replace-fail "'pytest-runner'," ""
        '';
        propagatedBuildInputs = with super; [ aiohttp ];
        checkInputs = [
          super.pytestCheckHook
        ] ++ lib.optional (builtins.hasAttr "pytest-runner" super) super.pytest-runner;
        doCheck = false;
      };

      eascheduler = super.buildPythonPackage rec {
        pname = "eascheduler";
        version = "0.1.4";
        src = super.fetchPypi {
          pname = "EAScheduler";
          sha256 = "sha256-Oq9UzefqpKSZbIStlTHd8FQUhvTSmaSvBuVA9Q64shs=";
          inherit version;
        };
      };

      easyco = super.buildPythonPackage rec {
        pname = "easyco";
        version = "0.2.3";
        src = super.fetchPypi {
          pname = "EasyCo";
          sha256 = "sha256-TndMzO05inYkyOSpJrXqnaUa0SEq/w2x1zcmMHIG6vY=";
          inherit version;
        };
        propagatedBuildInputs = with super; [
          ruamel_yaml
          voluptuous
        ];
      };

      pytest-runner = super.buildPythonPackage rec {
        pname = "pytest-runner";
        version = "5.2";

        src = super.fetchPypi {
          inherit pname version;
          sha256 = "96c7e73ead7b93e388c5d614770d2bae6526efd997757d3543fe17b557a0942b";
        };

        nativeBuildInputs = with super; [
          setuptools_scm
          pytest
        ];

        doCheck = false;

        meta = with lib; {
          description = "Invoke py.test as distutils command with dependency resolution";
          homepage = "https://github.com/pytest-dev/pytest-runner";
          license = licenses.mit;
        };
      };

      stackprinter = super.buildPythonPackage rec {
        pname = "stackprinter";
        version = "0.2.5";
        src = super.fetchPypi {
          inherit pname version;
          sha256 = "sha256-4Bl8VNr+eGcCBzHD3KuWoU0QPB45Ew+gtRWaLwr0J7Y=";
        };
      };
    };
  };

  pypkgs = python.pkgs;

in
pypkgs.buildPythonApplication rec {
  pname = "habapp";
  version = "0.31.0";

  src = pypkgs.fetchPypi {
    pname = "HABApp";
    hash = "sha256-j7EhDpZC+K6+89g5UBZFANjMAMH4ziSzDK+vm9TID4Q=";
    inherit version;
  };

  propagatedBuildInputs = with pypkgs; [
    aiohttp
    aiohttp-sse-client
    astral
    bidict
    eascheduler
    easyco
    paho-mqtt
    pendulum
    pydantic
    stackprinter
    tzlocal
    ujson
    voluptuous
    watchdog
  ];

  checkInputs = with pypkgs; [ pytestCheckHook ];

  pythonImportsCheck = [ "HABApp" ];

  # they fail - haven't checked why...
  doCheck = false;

  meta = with lib; {
    description = "openHAB daemon";
    license = licenses.gpl3Only;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
