{
  stdenv,
  lib,
  fetchFromGitHub,
  makeWrapper,
  # no makeBinaryWrapper on 21.11
  makeBinaryWrapper ? null,
  bundlerEnv,
  postgresql,
}:

let
  env = bundlerEnv {
    name = "ruby-env";
    gemdir = ./.;
  };

  makeWrapper' = if makeBinaryWrapper == null then makeWrapper else makeBinaryWrapper;

  chdir = if makeBinaryWrapper == null then ''--run "cd $out/libexec"'' else ''--chdir $out/libexec'';

in
stdenv.mkDerivation rec {
  pname = "synapse-purge";
  version = "unstable-20210609";

  src = fetchFromGitHub {
    owner = "djmaze";
    repo = pname;
    rev = "83c63ef4c2f96290750a0ff13bc22340aa905a71";
    hash = "sha256-WCjjHLHgvwO3U34Glii6dqVoYwq85mvpZ8VCYlcDTN8=";
  };

  buildInputs = [
    env.wrappedRuby
    postgresql
  ];

  nativeBuildInputs = [ makeWrapper' ];

  installPhase = ''
    runHook preInstall

    install -d $out/{bin,libexec}
    cp -r *.rb visualizers $out/libexec
    install -Dm444 -t $out/share/doc/${pname} *.md

    makeWrapper $out/libexec/synapse-purge.rb $out/bin/synapse-purge \
      --set-default DATABASE_URL db \
      ${chdir}

    runHook postInstall
  '';

  meta = with lib; {
    description = "Purge Matrix events";
    mainProgram = "synapse-purge";
  };
}
