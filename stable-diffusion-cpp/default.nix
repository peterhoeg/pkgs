{
  lib,
  fetchFromGitHub,
  llama-cpp,
  makeBinaryWrapper,
  ...
}:

let
  bin = "sd";

  vulkan = true;

in
(llama-cpp.override { vulkanSupport = vulkan; }).overrideAttrs (old: {
  pname = "stable-diffusion";
  version = "0-unstable-2024-10-24";

  src = fetchFromGitHub {
    owner = "leejet";
    repo = "stable-diffusion.cpp";
    rev = "ac54e0076052a196b7df961eb1f792c9ff4d7f22";
    hash = "sha256-VtCCyiKdHUq+Tu52ubIC0xB9Ftd8SAORhBNv9/4wTjw=";
    fetchSubmodules = true;
  };

  nativeBuildInputs = old.nativeBuildInputs ++ [ makeBinaryWrapper ];

  cmakeFlags = old.cmakeFlags ++ [
    (lib.cmakeBool "SD_FLASH_ATTN" true)
    (lib.cmakeBool "SD_VULKAN" vulkan)
  ];

  postPatch = ":";
  postInstall = ":";

  preFixup = ''
    mkdir -p $out/libexec
    mv $out/bin/{${bin},vulkan-shaders-gen} $out/libexec
    makeWrapper $out/libexec/${bin} $out/bin/${bin} \
      --prefix PATH : $out/libexec
  '';

  meta = {
    description = "Stable Diffusion";
    mainProgram = bin;
  };
})
