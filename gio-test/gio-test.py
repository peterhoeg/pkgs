#!/usr/bin/env python2

import sys, gio, os
if len(sys.argv) < 2:
    print "Check for handler applications for specified filename."
    print ""
    print "Usage: ", sys.argv[0], "FILENAME"
    exit(1)
f = gio.File(sys.argv[1])
filename, file_extension = os.path.splitext(sys.argv[1])
info = f.query_info('standard::content-type')
abspath = os.path.abspath(sys.argv[1])
print "Absolute path:              ", abspath
mimeType = gio.content_type_guess(file_extension)
print "MIME type from extension:   ", mimeType
contentType = gio.content_type_from_mime_type(mimeType)
print "Content type from MIME type:", contentType
appInfo = gio.app_info_get_default_for_type(contentType, False)
print "Default Handler app:        ", appInfo.get_name()
print "Default Handler app command:", appInfo.get_commandline()
print "All possible handlers:"
allHandlers = gio.app_info_get_all_for_type(contentType)
for h in allHandlers:
    print "  ", h.get_name(),":",
    print h.get_commandline()
print ""
print "xdg-open check"
print "output of: xdg-mime query filetype "+ abspath
xdgContentType = os.popen("xdg-mime query filetype " + abspath).read()
print "  ", xdgContentType
print "output of: xdg-mime query default " + xdgContentType
handlerDesktopFile = os.popen("xdg-mime query default " + xdgContentType).read()
print "  ", handlerDesktopFile

