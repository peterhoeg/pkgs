{
  lib,
  python2,
  xdg-utils,
}:

let
  pypkgs = python2.pkgs;
  file = "gio-test.py";

in
pypkgs.buildPythonApplication rec {
  pname = "gio-test";
  version = "0.1";

  format = "other";

  src = ./gio-test.py;

  unpackPhase = "cp --no-preserve=all ${src} ./${file}";

  postPatch = ''
    substituteInPlace ${file} \
      --replace-fail 'popen("xdg-mime' 'popen("${xdg-utils}/bin/xdg-mime'
  '';

  propagatedBuildInputs = with pypkgs; [
    pygtk
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 ${file} $out/bin/gio-test

    runHook postInstall
  '';

  # no tests
  doCheck = false;

  meta = with lib; {
    description = "Check GIO mime types vs XDG mime types";
    license = licenses.free;
    broken = true;
  };
}
