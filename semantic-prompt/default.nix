{
  stdenv,
  lib,
  fetchFromGitLab,
}:

stdenv.mkDerivation rec {
  pname = "semantic-prompt";
  version = "0.0.0.20200129";

  src = fetchFromGitLab {
    domain = "gitlab.freedesktop.org";
    owner = "Per_Bothner";
    repo = "specifications";
    rev = "4d2e1d75d4861a1d924895e106f8f016880e12a7";
    hash = "sha256-7jpL1wXZM6qxF1JCVHvpLpLhVrdzctEqwgf2wCEO/Q4=";
  };

  dontBuild = true;

  dontConfigure = true;

  # we only care about zsh for now
  installPhase = ''
    install -Dm444  \
      proposals/prompts-data/shell-integration.zsh \
      $out/semantic-prompt.plugin.zsh

    install -Dm444 -t $out/share/doc/${pname} proposals/*.md
  '';

  meta = with lib; {
    description = "Semantic Prompt - shell integration";
    license = licenses.free;
  };
}
