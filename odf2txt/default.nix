{
  lib,
  python3Packages,
  fetchFromGitHub,
}:

# BROKEN: requires py2

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "odf2txt";
  version = "20130330";

  format = "other";

  src = fetchFromGitHub {
    owner = "mwoehlke";
    repo = pname;
    rev = "da5ccac1e6e82dd24560d0cbd0e0e0a252b78b52";
    hash = "sha256-3Nc7sxRKQL2B3HcEI1z4xpGdFu1RgpcJZBvyYuKIwIc=";
  };

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin odf2txt
    install -Dm444 -t $out/share/doc/${pname} *.rst

    runHook postInstall
  '';

  doInstallCheck = true;

  installCheckPhase = ''
    $out/bin/odf2txt --help
  '';

  meta = with lib; {
    description = "Convert LibreOffice documents to text";
    license = licenses.free;
    broken = true; # see above
  };
}
