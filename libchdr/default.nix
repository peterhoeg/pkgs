{
  stdenv,
  lib,
  fetchFromGitHub,
  cmake,
  pkg-config,
  zlib,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "libchdr";
  version = "0-unstable-2024-09-30";

  src = fetchFromGitHub {
    owner = "rtissera";
    repo = "libchdr";
    rev = "aaca599e18e43933fc193bd1b715c368c306208b";
    hash = "sha256-jMT1OzwsCUygIdtXaEIfO0zGrcEWbQPDzoEtqi7bJJg=";
  };

  # we end up with broken pkg-config files if we don't strip the prefix
  postPatch = ''
    sed -i pkg-config.pc.in \
      -e 's@''${prefix}/@@g'
  '';

  buildInputs = [ zlib ];

  nativeBuildInputs = [
    cmake
    pkg-config
  ];

  cmakeFlags = [
    (lib.cmakeBool "WITH_SYSTEM_ZLIB" true)
  ];

  meta = with lib; {
    description = "Standalone library for reading MAME's CHDv1-v5 formats. ";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
})
