{
  stdenv,
  lib,
  fetchFromGitHub,
  pkg-config,
  efivar,
  popt,
}:

stdenv.mkDerivation rec {
  pname = "dbxtool";
  version = "8.0.20210116";

  src = fetchFromGitHub {
    owner = "rhboot";
    repo = "dbxtool";
    # rev = "dbxtool-${version}";
    rev = "9e05173636af6caf080597227951d19e1e2a8153";
    hash = "sha256-NImp3+kJThIRG2ylkNqWIm3XJZR1cLg4F5p7W7M9nrE=";
  };

  postPatch = ''
    substituteInPlace Make.defaults \
      --replace-fail ' /usr' ' ${placeholder "out"}'

    substituteInPlace src/dbxtool.service \
      --replace-fail '/usr/' '${placeholder "out"}/'
  '';

  PREFIX = placeholder "out";

  env.NIX_CFLAGS_COMPILE = "-Wno-error";

  buildInputs = [
    efivar
    popt
  ];

  nativeBuildInputs = [ pkg-config ];

  meta = with lib; {
    description = "DBX signature updater for UEFI machines";
  };
}
