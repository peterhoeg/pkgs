{
  runCommandNoCC,
  fetchurl,
  dos2unix,
  unzip,
  ...
}:

runCommandNoCC "keydb.cfg"
  {
    src = fetchurl {
      url = "https://web.archive.org/web/20250118144740/http://fvonline-db.bplaced.net/fv_download.php?lang=eng";
      hash = "sha256-dNU0YLF3SagnBSoZovujV0J375mGrdHkv1rzCgdgM0w=";
      name = "keydb_eng.zip";
    };
    nativeBuildInputs = [
      dos2unix
      unzip
    ];
  }
  ''
    unzip $src
    install -Dm444 keydb.cfg $out
    dos2unix $out
  ''
