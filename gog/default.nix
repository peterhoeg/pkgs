{
  stdenv,
  lib,
  fetchurl,
  requireFile,
  gogUnpackHook,
  autoPatchelfHook,
  runCommandLocal,
  dosbox,
  dosbox-staging,
  unzip,
  writeShellScript,
  steam-run,
  local,
  pkgs,
}:

let
  inherit (lib)
    optionals
    optionalString
    concatStringsSep
    replaceStrings
    toLower
    ;

  dosboxBin = "${lib.getBin dosbox-staging}/bin/dosbox";

  gog =
    {
      name,
      version,
      file,
      sha256,
      libs ? [ ],
      bin ? null,
      dosbox ? false,
      inno ? false,
      viaSteam ? false,
    }:
    let
      pname = replaceStrings [ " " "'" "(" ")" ] [ "_" "" "" "" ] (toLower name);

    in
    stdenv.mkDerivation rec {
      inherit pname version;

      src = requireFile rec {
        name = file;
        message = ''
          nix-prefetch-url file://\$PWD/${name}
        '';
        inherit sha256;
      };

      unpackPhase = optionalString (!inno) ''
        mkdir -p $out/tmp
        unzip -q $src -d $out/tmp || true
      '';

      buildInputs = [ ] ++ libs;

      nativeBuildInputs = [
        autoPatchelfHook
        unzip
      ] ++ optionals inno [ gogUnpackHook ];

      installPhase =
        let
          runDir = "\\$XDG_CACHE_HOME/games/${pname}";

          dosboxLauncher = writeShellScript "launch-${pname}" ''
            set -eEuo pipefail

            src=@out@/share/${pname}
            dir=''${XDG_CACHE_HOME:-$HOME/.cache}/${pname}

            test -e $dir || mkdir -p $dir
            cp --update --recursive $src/* $dir/

            pushd $dir >/dev/null
            ${dosboxBin} ${bin}
            popd >/dev/null
          '';

          launcher = writeShellScript "launch-${pname}" ''
            set -eEuo pipefail

            dir=@out@/share/${pname}

            pushd $dir >/dev/null
            ${optionalString viaSteam "${steam-run}/bin/steam-run "}${bin}
            popd >/dev/null
          '';

          # makeDesktopItem doesn't like @out@ in paths
          desktopItem = (pkgs.formats.ini { }).generate "app.desktop" {
            "Desktop Entry" = {
              Name = name;
              Exec = "@out@/bin/${pname}";
              Icon = "@out@/share/icons/${pname}.png";
              Categories = concatStringsSep ";" [ "Game" ];
              Version = "1.1";
            };
          };

        in
        ''
          runHook preInstall

          dir=$out/share/${pname}
          mkdir -p $dir $out/bin $out/share/{applications,icons}

          _maybe_move() {
            local d="$1"

            if [ -d "$d" ]; then
              mv "$d"/* "$dir"
            fi
          }

          _maybe_move $out/tmp/data/noarch/data
          _maybe_move $out/tmp/data/noarch/game

          substitute ${desktopItem} $out/share/applications/${pname}.desktop \
            --subst-var out

          mv $out/tmp/data/noarch/support/icon.png $out/share/icons/${pname}.png

        ''
        + (
          if dosbox then
            ''
              install -Dm555 ${dosboxLauncher} $out/bin/${pname}
            ''
          else if viaSteam then
            ''
              install -Dm555 ${launcher} $out/bin/${pname}
              autoPatchelf $out/share/${pname}/${bin}
            ''
          else
            ''
              install -Dm555 ${launcher} $out/bin/${pname}
            ''
        )
        + ''

          rm -r $out/tmp

          runHook postInstall
        '';

      fixupPhase = ''
        runHook preFixup

        substituteInPlace $out/bin/${pname} \
          --subst-var out

        chmod 555 $out/bin/*

        runHook postFixup
      '';

      meta = with lib; {
        description = "GOG game: ${pname}";
        license = licenses.free; # it's not though...
        platforms = platforms.linux;
        hydraPlatforms = platforms.none;
      };
    };

in
{
  baldurs_gate_enhanced_edition = gog rec {
    name = "Baldur's Gate (Enhanced Edition)";
    version = "2.6.6.0.47291";
    file = "baldur_s_gate_enhanced_edition_${replaceStrings [ "." ] [ "_" ] version}.sh";
    sha256 = "1v32cv25sdlsc5l227wwn0bzivcsiwxir4hpf9fxgy2qvpydyzks";
    bin = "./BaldursGate";
    libs = with pkgs; [
      stdenv.cc.cc.lib
      expat
      libGL
      openal
      local.openssl_1_0_1
    ];
    viaSteam = false; # steam-run only supports 32 bit
  };

  eotb = gog rec {
    name = "Eye of the Beholder";
    version = "2.0.0.3";
    file = "gog_eye_of_the_beholder_${version}.sh";
    sha256 = "03j0q8l54zznhgyh8q853aql76dhdsdzm9fxzzq8pk1n0sngknbf";
    bin = "START1.EXE";
    dosbox = true;
  };
}
