{
  stdenv,
  lib,
  libsigrok,
}:

stdenv.mkDerivation rec {
  pname = "libsigrok-udev-rules";
  inherit (libsigrok) version src;

  dontConfigure = true;

  dontBuild = true;

  installPhase = ''
    runHook preInstall

    install -Dm444 -t $out/lib/udev/rules.d contrib/*.rules

    runHook postInstall
  '';

  meta = with lib; {
    inherit (libsigrok.meta) description homepage license;
    platforms = platforms.linux;
  };
}
