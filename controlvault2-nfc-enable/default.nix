{
  lib,
  fetchFromGitHub,
  python3Packages,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "controlvault2-nfc-enable";
  version = "unstable-2021-03-30";

  format = "other";

  src = fetchFromGitHub {
    owner = "peterhoeg"; # upstream: "jacekkow"; # I forked to allow configuring logging via env vars
    repo = pname;
    rev = "8c220eb4f4668991803862d5e90fe87497613753"; # logging branch
    hash = "sha256-l+V/5fpaYcFaB4gUIeIcnSE9GPnKzZEUKty+VYy67Ec=";
  };

  propagatedBuildInputs = with pypkgs; [ pyusb ];

  installPhase = ''
    runHook preInstall

    install -Dm555    nfc.py $out/bin/nfc
    install -Dm444 -t $out/${pypkgs.python.sitePackages} c*.py

    install -Dm444 -t $out/share/doc/${pname} README* *.txt

    runHook postInstall
  '';

  meta = with lib; {
    description = "Enable NFC on Broadcom 5880 USH devices (ControlVault 2/3) on Dell laptops";
    license = licenses.bsd3;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
