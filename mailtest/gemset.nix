{
  arrayfields = {
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "1az4bl9br5plrh22ia19dydscpgvn2a7k6q9wnjlmqj8r6xg14qm";
      type = "gem";
    };
    version = "4.9.2";
  };
  awesome_print = {
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "14arh1ixfsd6j5md0agyzvksm5svfkvchb90fp32nn7y3avcmc2h";
      type = "gem";
    };
    version = "1.8.0";
  };
  chronic = {
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "1hrdkn4g8x7dlzxwb1rfgr8kw3bp4ywg5l4y4i9c2g5cwv62yvvn";
      type = "gem";
    };
    version = "0.10.2";
  };
  fattr = {
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "027w53inb8i7s3ap8w9xxpwfscp9p3rkci10bsag4vvyjxjlcm57";
      type = "gem";
    };
    version = "2.4.0";
  };
  highline = {
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "01ib7jp85xjc4gh4jg0wyzllm46hwv8p0w1m4c75pbgi41fps50y";
      type = "gem";
    };
    version = "1.7.10";
  };
  mail = {
    dependencies = [ "mini_mime" ];
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "10dyifazss9mgdzdv08p47p344wmphp5pkh5i73s7c04ra8y6ahz";
      type = "gem";
    };
    version = "2.7.0";
  };
  mailtest = {
    dependencies = [
      "awesome_print"
      "mail"
      "main"
      "progress_bar"
      "rainbow"
      "random-word"
    ];
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "0ywgcqy80p0ijim3s9z6grif3bc9ry7avpwa2j94aln1jn6fd8q4";
      type = "gem";
    };
    version = "0.0.4";
  };
  main = {
    dependencies = [
      "arrayfields"
      "chronic"
      "fattr"
      "map"
    ];
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "0xcnyk3a8i1n642ycfmd4228pb8ddfw8ms8pmfsmwidpnhzfw15g";
      type = "gem";
    };
    version = "6.2.2";
  };
  map = {
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "1g1w6x802c541ws43wvnnvdcklzr339kjn2zh22l1c8m8lw6yfhm";
      type = "gem";
    };
    version = "6.6.0";
  };
  mini_mime = {
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "1lwhlvjqaqfm6k3ms4v29sby9y7m518ylsqz2j74i740715yl5c8";
      type = "gem";
    };
    version = "1.0.0";
  };
  options = {
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "1s650nwnabx66w584m1cyw82icyym6hv5kzfsbp38cinkr5klh9j";
      type = "gem";
    };
    version = "2.3.2";
  };
  progress_bar = {
    dependencies = [
      "highline"
      "options"
    ];
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "035f75fk37dj4qrr3jkrh256zkhdhdcz50ha3d9d3bg2bncxz6qd";
      type = "gem";
    };
    version = "1.2.0";
  };
  rainbow = {
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "0bb2fpjspydr6x0s8pn1pqkzmxszvkfapv0p4627mywl7ky4zkhk";
      type = "gem";
    };
    version = "3.0.0";
  };
  random-word = {
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "1prim6lc585ivwc7kv30gayd3xvskcyzawhi26y0p6gvmldry06m";
      type = "gem";
    };
    version = "2.1.0";
  };
}
