{ lib, bundlerApp }:

bundlerApp rec {
  pname = "mailtest";
  gemdir = ./.;
  exes = [ "mailtest" ];

  meta = with lib; {
    description = "A mail test program";
  };
}
