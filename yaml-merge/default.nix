{
  lib,
  buildGoModule,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "yaml-merge";
  version = "0.0.0";

  src = fetchFromGitHub {
    owner = "ericwenn";
    repo = "yaml-merge-cli";
    # rev = version;
    rev = "b7f2cc80f6497ee6083b6973c69c28e8318c6dc3";
    hash = "sha256-LDNoPLe972hBfKbi7BXzZX22OGXUZpE4ZURnCt+4vF4=";
  };

  vendorHash = "sha256-aIs3tIZQdo+rH6+i5Xan96JFg6yWwvp6M3fj5NvYKTw=";

  meta = with lib; {
    description = "Merge YAML";
    mainProgram = "yaml-merge-cli";
  };
}
