{
  lib,
  fetchFromGitHub,
  fetchurl,
  python3Packages,
  makeWrapper,
}:
let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "python-onvif-zeep";
  version = "0.2.12.20190418";

  src = fetchFromGitHub {
    owner = "FalkTannhaeuser";
    repo = pname;
    rev = "d549eed77d9dd5fa23100db4090b89817d280d4c";
    sha256 = "sha256-31VYxyY6VXBUjMmGNTWs4iFxPSFAkjuJqSB9sJYW0m4=";
  };

  nativeBuildInputs = [ makeWrapper ];

  propagatedBuildInputs = with pypkgs; [ zeep ];

  postInstall = ''
    wrapProgram $out/bin/onvif-cli \
      --add-flags "--wsdl $out/${pypkgs.python.sitePackages}/wsdl"
  '';

  meta = with lib; {
    description = "Python ONVIF library and client";
  };
}
