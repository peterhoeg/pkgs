{
  lib,
  rustPlatform,
  fetchFromGitHub,
  installShellFiles,
  ronn,
}:

rustPlatform.buildRustPackage rec {
  pname = "rfetch";
  version = "0.0.0.20240205";

  src = fetchFromGitHub {
    owner = "kamui-fin";
    repo = "rfetch";
    rev = "4c79d42fa68f8bbf0eb46d0f585442c6daecc5ea";
    hash = "sha256-EamCt1Xhov1d7F/+dL/N/9GOQ/XBxOQfGScsoEiPTV0=";
  };

  cargoHash = "sha256-01TdeX9s8J+X17c1w5u5FdyzDOH1c+euz3hPusRlh9E=";

  meta = with lib; {
    description = "fetch program written in Rust";
    license = licenses.gpl3Only;
    maintainers = with maintainers; [ peterhoeg ];
    inherit (src.meta) homepage;
  };
}
