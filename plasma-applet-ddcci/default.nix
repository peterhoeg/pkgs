{
  stdenv,
  lib,
  fetchFromGitHub,
  python3Packages,
  makeWrapper,
  ddcutil,
  qtbase,
  plasma5Packages,
  kdePackages,
}:

let
  pypkgs = python3Packages;

  isQt5 = (lib.versions.major qtbase.version) == "5";

  version = "0.1.10";

  src = fetchFromGitHub {
    owner = "davidhi7";
    repo = "ddcci-plasmoid";
    rev = "v" + version + (if isQt5 then "-fix" else "-kf6");
    hash =
      if isQt5 then
        "sha256-/2xN3arP4KxWREVYFwY3Njj8YdST0rkDf0CVqOTfFcg="
      else
        "sha256-/UTIflcUyPHMQ2CQG0d2R0MaKuXYmlvnYnLNQ+nMWvw=";
  };

  genMeta =
    kind: attrs:
    lib.recursiveUpdate (with lib; {
      description = "Manage brightness of external monitors over DDC - ${kind}";
      license = licenses.mit;
      maintainers = with maintainers; [ peterhoeg ];
      inherit (qtbase.meta) platforms;
    }) attrs;

  inherit (lib) getExe makeBinPath;

  fasteners' = pypkgs.fasteners.overridePythonAttrs (old: rec {
    version = "0.18";
    src = pypkgs.fetchPypi {
      inherit (old) pname;
      inherit version;
      hash = "sha256-y3wT75Hgx+T+SvOOyva5BOw/XODdoG00kktrdLhp2VM=";
    };
    doCheck = false;
  });

  backend = pypkgs.buildPythonApplication rec {
    pname = "ddcci_plasmoid_backend";
    inherit version src;

    format = "pyproject";

    disabled = pypkgs.pythonOlder "3.8";

    sourceRoot = "source/backend";

    nativeBuildInputs = [
      pypkgs.poetry-core
      makeWrapper
    ];

    propagatedBuildInputs = # with pypkgs;
      [ fasteners' ];

    dontConfigure = true;

    env.makeWrapperArgs = "--prefix PATH : ${makeBinPath [ ddcutil ]}";

    meta = genMeta "backend" { mainProgram = pname; };
  };

  frontend = stdenv.mkDerivation {
    pname = "plasma-applet-ddcci";
    inherit version src;

    # backend and frontend do not have the same version tag, so we simply force the backend to that of the frontend
    # yes, there are much nicer ways to deal with JSON than this...
    postPatch = ''
      sed -E -i plasmoid/metadata.json \
        -e 's/("Version":).*"(,)?/\1 "${version}"\2/g'

      substituteInPlace plasmoid/contents/config/main.xml \
        --replace-fail 'python3 -m ${backend.pname}' '${getExe backend}'
    '';

    buildInputs = [
      backend
    ] ++ (if isQt5 then [ plasma5Packages.kpackage ] else [ kdePackages.kpackage ]);

    dontConfigure = true;

    dontBuild = true;

    env.LANG = "C.UTF-8";

    # --global still installs to $HOME/.local/share so we use --packageroot
    installPhase = ''
      runHook preInstall

      kpackagetool${if isQt5 then "5" else "6"} --install plasmoid --packageroot $out/share/plasma/plasmoids
      rm -r $out/share/plasma/plasmoids/de.davidhi.ddcci-brightness/translate

      runHook postInstall
    '';

    dontWrapQtApps = true;

    meta = genMeta "frontend" { };
  };

in
frontend
