{
  config,
  lib,
  pkgs,
  ...
}:

let

in
{
  services.nginx = {
    enable = true;
    virtualHosts.threecx = {
      root = "${pkg}/share/webroot";
    };
  };

  systemd.services =
    let
      commonService = {
        Type = "notify";
        NotifyAccess = "main";
        User = "phonesystem";
        StandardOutput = "null";
        LimitCORE = "2147483648";
        WorkingDirectory = libDir;
        StateDirectory = builtins.baseNameOf libDir;
        RuntimeDirectory = builtins.baseNameOf runDir;
        RuntimeDirectoryPreserve = true;
        TimeoutStopSec = "15";
      };

    in
    {
      threecx-config-server = rec {
        description = "3CX PhoneSystem Configuration Service";
        requires = [ "postgresql.service" ]; # should be Wants for remote DB
        after = requires;
        wantedBy = [ "threecx.target" ];
        serviceConfig = commonService // {
          ExecStart = "${pkg}/bin/3CXSLDBServ -n -p ${runDir}/3CXCfgServ01.pid -o ${libDir}/Instance1/Bin/Objects.cls -c ${iniFile}";
          LimitNOFILE = "1024";
        };
      };

      threecx-audio = rec {
        description = "3CX PhoneSystem Audio Provider Server";
        requires = [ "threecx-config-server.service" ];
        after = requires;
        wantedBy = [ "threecx.target" ];
        serviceConfig = commonService // {
          ExecStart = "${pkg}/bin/3CXAudioProvider -n -p ${runDir}/3CXAudioProvider01.pid -c ${iniFile}";
          TimeoutStopSec = "60";
          LimitNOFILE = "4096";
        };
      };

      threecx-tunnel = rec {
        description = "3CX SIP/RTP Tunnel Proxy";
        wantedBy = [ "threecx.target" ];
        serviceConfig = commonService // {
          ExecStart = "${pkg}/bin/3CXTunnel -n -p ${runDir}/3CXTunnel01.pid -c ${iniFile}";
          LimitNOFILE = "1024";
          LimitNICE = "40";
        };
      };
    };

  systemd.targets.threecx = {
    description = "3CX VoIP PBX";
    wantedBy = [ "multi-user.target" ];
  };

  users = {
    groups.phonesystem = { };
    users.phonesystem = {
      isNormalUser = false;
    };
  };
}
