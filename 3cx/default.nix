{
  stdenv,
  lib,
  fetchurl,
  formats,
  autoPatchelfHook,
  dpkg,
  coreutils,
  freeimage,
  glibc,
  gsm,
  lttng-ust,
  openssl,
  libopus,
  postgresql,
  spandsp,
  systemd,
}:

let
  inherit (lib) getBin getLib;

  target = (formats.ini { }).generate "3cxpbx.target" {
    Unit.Description = "3CX PBX";
    Install.WantedBy = "multi-user.target";
  };

  # we may have to override this .so.0 vs .so.1
  lttng-ust' = lttng-ust.overrideAttrs (old: { });

in
stdenv.mkDerivation rec {
  pname = "3cxpbx";
  version = "18.0.2.314";

  src = fetchurl {
    url = "http://downloads-global.3cx.com/downloads/debian/pool/buster/3cxpbx_${version}_amd64.deb";
    sha256 = "sha256-F6xjK+p31X4RrZtbQvYuuLgpe+J0fNtqSFwhhYqdc58=";
  };

  buildInputs = [
    stdenv.cc.cc.lib
    freeimage
    glibc
    gsm
    openssl
    libopus
    postgresql.lib
    spandsp
    systemd
    lttng-ust'
  ];

  nativeBuildInputs = [
    dpkg
    autoPatchelfHook
  ];

  unpackPhase = ''
    runHook preUnpack

    dpkg -x $src .

    runHook postUnpack
  '';

  dontConfigure = true;

  dontBuild = true;

  installPhase = ''
    runHook preInstall

    mkdir -p $out
    mv usr/* $out/

    mkdir -p $out/{lib/systemd,share/doc/${pname}}
    mv lib/systemd/system    $out/lib/systemd/
    install -Dm444 ${target} $out/lib/systemd/system/${target.name}
    mv etc                   $out/share/doc/${pname}/

    lib=$out/lib/3cxpbx
    ln -sf ${getLib freeimage}/lib/libfreeimage.so.3  $lib/FreeImage.so
    ln -sf ${getLib gsm}/lib/libgsm.so                $lib/libgsm.so.1
    ln -sf ${getLib lttng-ust'}/lib/liblttng-ust.so.1 $lib/liblttng-ust.so.0

    find $out/lib -name '*.so' -type f | xargs chmod +x

    for f in $out/sbin/* $out/share/3cxpbx/3cxpbx.conf ; do
      if [ $(stat -c %s $f) -lt 20000 ]; then
        substituteInPlace $f \
          --replace-fail /usr/share/3cxpbx $out/share/3cxpbx \
          --replace-fail /usr/lib/3cxpbx   $out/lib/3cxpbx \
          --replace-fail /usr/sbin/3CX     $out/bin/3CX
      fi
    done

    for f in $out/lib/systemd/system/*.service ; do
      substituteInPlace $f \
        --replace-fail /usr $out \
        --replace-fail multi-user.target 3cxpbx.target \
        --replace-fail /bin/rm ${getBin coreutils}/bin/rm \
        --replace-fail /var/run /run

      sed -i $f -e '/^Alias/d'
    done

    substituteInPlace $out/sbin/3CXServiceControl \
      --replace-fail /usr/share/3cxpbx $out/share/3cxpbx \
      --replace-fail /bin/systemctl ${systemd}/bin/systemctl

    runHook postInstall
  '';

  meta = with lib; {
    description = "3CX PBX VoIP";
    hydraPlatforms = platforms.none;
  };
}
