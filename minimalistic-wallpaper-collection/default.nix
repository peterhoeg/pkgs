{
  stdenv,
  lib,
  fetchFromGitHub,
}:

let
  baseDir = "share/wallpapers";

in
stdenv.mkDerivation rec {
  pname = "minimalistic-wallpaper-collection";
  version = "20230426";

  src = fetchFromGitHub {
    owner = "DenverCoder1";
    repo = pname;
    rev = "c331e05252895fd1a998d61bf0d01c3ce669b839";
    hash = "sha256-RS9t1hH0ceEG34jv1ZG0+akVICjw6Wqzm8GpZKGajRU=";
  };

  # # there is no point rebuilding this just because some transitive dependency was changed
  outputHashAlgo = "sha256";
  outputHashMode = "recursive";
  outputHash = "sha256-Yu16SM9N7WoUVMHTQcRuOnsrO0BKsSI71eQRNtp0cmA=";

  dontConfigure = true;

  dontBuild = true;

  installPhase = ''
    runHook preInstall

    install -Dm444 -t $out/${baseDir} images/*

    runHook postInstall
  '';

  passthru = {
    inherit baseDir;
    supportsDarkLight = false;
  };

  meta = with lib; {
    description = "Minimalistic Wallpapers";
    license = licenses.free;
  };
}
