{
  lib,
  stdenv,
  fetchFromGitHub,
  mlton,
}:

stdenv.mkDerivation rec {
  pname = "initool";
  version = "0.10.0";

  src = fetchFromGitHub {
    owner = "dbohdan";
    repo = "initool";
    rev = "v${version}";
    sha256 = "sha256-pszlP9gy1zjQjNNr0L1NY0XViejUUuvUZH6JHtUxdJI=";
  };

  installPhase = ''
    install -Dm555 -t $out/bin initool
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md
  '';

  nativeBuildInputs = [ mlton ];

  meta = with lib; {
    description = "Get and set values from/to .ini files";
    license = licenses.mit;
  };
}
