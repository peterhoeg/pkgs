{
  stdenv,
  fetchFromGitHub,
  lib,
  pkg-config,
  libX11,
  libXinerama,
  libXrandr,
  libxcb,
  xcbutilxrm,
}:

stdenv.mkDerivation rec {
  pname = "xdpi";
  version = "20181025";

  src = fetchFromGitHub {
    owner = "Oblomov";
    repo = "xdpi";
    rev = "2deff3ae58f9a09b73b4bb2db851110062f2e9b6";
    hash = "sha256-GNND33IFKS/CyPII9xObIXRVg0fn6jJl8G2aWTH9J4M=";
  };

  buildInputs = [
    libX11
    libXinerama
    libXrandr
    libxcb
    xcbutilxrm
  ];

  nativeBuildInputs = [ ];

  enableParallelBuilding = true;

  NIX_CFLAGS_COMPILE = [
    "-Wno-error=array-bounds"
    "-Wno-error=format-truncation"
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin xdpi
    install -Dm444 -t $out/share/doc/${pname} *.md

    runHook postInstall
  '';

  meta = with lib; {
    description = "X11 DPI information retrieval";
    license = licenses.mpl20;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
