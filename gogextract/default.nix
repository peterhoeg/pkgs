{
  lib,
  fetchFromGitHub,
  python3Packages,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "gogextract";
  version = "20161010";

  format = "other";

  src = fetchFromGitHub {
    owner = "Yepoleb";
    repo = pname;
    rev = "6601b32feacecd18bc12f0a4c23a063c3545a095";
    sha256 = "0hh4hc9mskawsph859cx5n194qkwa3122gkcsywm917n77fncfq5";
  };

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin gogextract.py
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md

    runHook postInstall
  '';

  # no automated tests
  doCheck = false;

  meta = with lib; {
    description = "GOG Extractor";
    license = licenses.mit;
  };
}
