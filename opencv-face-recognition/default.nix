{
  stdenv,
  lib,
  fetchurl,
  cmake,
  gtest,
  unzip,
  opencv,
  openssl,
  tree,
}:

stdenv.mkDerivation rec {
  pname = "opencv-face-recognition";
  version = "0.0.1";

  src = fetchurl {
    url = "https://docs.opencv.fr/cpp/sdk.zip";
    hash = "sha256-BjP2zWpGU/+tzKHQbZsMOAc2SL3K3LbQ5aH8EQP4Il8=";
  };

  sourceRoot = "sdk/linux";

  nativeBuildInputs = [
    cmake
    unzip
    tree
  ];

  buildInputs = [
    gtest
    opencv
    openssl
  ];

  installPhase = ''
    install -Dm555 -t $out/bin /build/sdk/linux/bin/*
  '';

  meta = with lib; {
    description = "OpenCV Face Recognition";
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
  };
}
