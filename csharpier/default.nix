{
  lib,
  stdenv,
  dotnetCorePackages,
  buildDotnetModule,
  fetchFromGitHub,
  autoPatchelfHook,
  runtimeShell,
  bash,
  fontconfig,
  xorg,
  libglvnd,
  makeDesktopItem,
  copyDesktopItems,
  graphicsmagick,
}:

buildDotnetModule rec {
  pname = "csharpier";
  version = "0.22.1";

  src = fetchFromGitHub {
    owner = "belav";
    repo = "csharpier";
    rev = version;
    hash = "sha256-K3PzjElwsLKZQfkzQkgkNeV/yFQJp+dA6cjKymeoRXY=";
  };

  projectFile = [ "CSharpier.sln" ];
  nugetDeps = ./deps.nix;
  dotnetFlags = [ "-p:Runtimeidentifier=linux-x64" ];
  dotnet-sdk = dotnetCorePackages.sdk_7_0;

  nativeBuildInputs = [
    autoPatchelfHook
    # copyDesktopItems
    graphicsmagick
    bash
  ];

  buildInputs = [
    stdenv.cc.cc.lib
    fontconfig
  ];

  # runtimeDeps = [
  #   libglvnd
  #   xorg.libSM
  #   xorg.libICE
  #   xorg.libX11
  # ];

  # postFixup = ''
  #   mkdir -p $out/share/icons/hicolor/256x256/apps/
  #   gm convert $src/GalaxyBudsClient/Resources/icon_white.ico $out/share/icons/hicolor/256x256/apps/${meta.mainProgram}.png
  # '';

  # desktopItems = makeDesktopItem {
  #   name = meta.mainProgram;
  #   exec = meta.mainProgram;
  #   icon = meta.mainProgram;
  #   desktopName = meta.mainProgram;
  #   genericName = "Galaxy Buds Client";
  #   comment = meta.description;
  #   type = "Application";
  #   categories = [ "Settings" ];
  #   startupNotify = true;
  # };

  meta = with lib; {
    mainProgram = "GalaxyBudsClient";
    description = "Unofficial Galaxy Buds Manager for Windows and Linux";
    homepage = "https://github.com/ThePBone/GalaxyBudsClient";
    license = licenses.gpl3;
    maintainers = [ maintainers.icy-thought ];
    platforms = platforms.linux;
    broken = true;
  };
}
