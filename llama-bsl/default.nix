{
  lib,
  fetchFromGitHub,
  python3Packages,
  git,
}:

let
  pypkgs = python3Packages;

  overrideVersions = [
    "requests"
  ];

in
pypkgs.buildPythonApplication rec {
  pname = "llama-bsl";
  version = "20210913";

  disabled = !pypkgs.isPy3k;

  src = fetchFromGitHub {
    owner = "electrolama";
    repo = pname;
    rev = "f1dad381b32fc14bd4bb581798c68a894ae9135d";
    hash = "sha256-d315vISAMQBW3cad2GIG1QmZPn2FpogMis3IhSit+F8=";
    leaveDotGit = true;
  };

  postPatch = lib.concatMapStringsSep "\n" (e: ''
    sed -i -e 's/"${e}=.*"/"${e}"/' setup.py
  '') overrideVersions;

  nativeBuildInputs = with pypkgs; [
    git
    setuptools_scm
  ];

  propagatedBuildInputs = with pypkgs; [
    intelhex
    pyserial
    python_magic
    requests
  ];

  # not working yet
  doCheck = false;

  checkInputs = with pypkgs; [
    pytestCheckHook
    scripttest
  ];

  meta = with lib; {
    description = "Upload firmware via the serial boot loader onto the CC13xx, CC2538 and CC26xx SoC.";
    license = licenses.bsd3;
  };
}
