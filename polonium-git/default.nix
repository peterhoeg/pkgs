{
  lib,
  fetchFromGitHub,
  buildNpmPackage,
  qtbase,
  plasma5Packages ? null,
  kdePackages ? null,
}:

# how to update:
# 1. check out the tag for the version in question
# 2. run `prefetch-npm-deps package-lock.json`
# 3. update npmDepsHash with the output of the previous step

let
  isQt5 = (lib.versions.major qtbase.version) == "5";

  version = "1.0rc";

in
buildNpmPackage {
  pname = "polonium-git";
  inherit version;

  src = fetchFromGitHub {
    owner = "zeroxoneafour";
    repo = "polonium";
    rev = if isQt5 then "dbb86e5e829d8ae57caf78cd3ef0606fdc1fbca5" else "v" + version;
    hash =
      if isQt5 then
        "sha256-MKG255AtybzXLHaxaBjk6HhcMbgGUMPiNn5tjQDaLMQ="
      else
        "sha256-AdMeIUI7ZdctpG/kblGdk1DBy31nDyolPVcTvLEHnNs=";
  };

  npmDepsHash =
    if isQt5 then
      "sha256-NBEkn4wNV34YWyADtjWdhnUXEfe/xomeCoiWAmin4+M="
    else
      "sha256-kaT3Uyq+/JkmebakG9xQuR4Kjo7vk6BzI1/LffOj/eo=";

  # the installer does a bunch of stuff that fails in our sandbox, so just build here and then we
  # manually do the install
  postPatch = ''
    substituteInPlace Makefile \
      --replace-fail "build install cleanall" "res src"
  '';

  nativeBuildInputs = [ (if isQt5 then plasma5Packages else kdePackages).kpackage ];

  dontNpmBuild = true;

  dontWrapQtApps = true;

  installPhase = ''
    runHook preInstall

    kpackagetool${if isQt5 then "5" else "6"} --install pkg --packageroot $out/share/kwin/scripts

    runHook postInstall
  '';

  env.LANG = "C.UTF-8";

  meta = with lib; {
    description = "Auto-tiler that uses KWin ${if isQt5 then "5.27" else "6"}+ tiling functionality";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
    inherit (kdePackages.kpackage.meta) platforms;
  };
}
