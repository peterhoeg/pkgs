{
  stdenv,
  lib,
  clang,
}:

stdenv.mkDerivation rec {
  pname = "unicode";
  version = "0.0.1";

  nativeBuildInputs = [ clang ];

  # export LANG=en_US.UTF-8
  buildCommand = ''
    echo 'int main(void){}' > test.c
    clang -o clang_×‽😂 test.c
    gcc -o gcc_×‽😂 test.c
    install -Dm444 -t $out/foo *
  '';

  meta = with lib; {
    description = "Building unicode files";
    license = licenses.mit;
  };
}
