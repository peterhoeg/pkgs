{
  lib,
  fetchFromGitHub,
  buildNpmPackage,
  cargo,
  rustc,
}:

# how to update:
# 1. check out the tag for the version in question
# 2. run `prefetch-npm-deps package-lock.json`
# 3. update npmDepsHash with the output of the previous step

buildNpmPackage rec {
  pname = "icantbelieveitsnotvaletudo";
  version = "2023.08.0";

  src = fetchFromGitHub {
    owner = "Hypfer";
    repo = pname;
    rev = version;
    hash = "sha256-4MZ0BsPgt6h+kBv4O7VDCSxiCHa6GmKOUYVE1L9PJfY=";
  };

  patches = [ ./devdeps.patch ];

  postPatch = ''
    mkdir -p $out/_logs
  '';

  # NODE_OPTIONS = "--loglevel=verbose";

  npmDepsHash = "sha256-v6+eMbDks8s5lz1bl/yOP2MaV9zKif/4mn7NJH/0IlE=";

  # npmPackFlags = [ "--ignore-scripts" ];

  nativeBuildInputs = [
    cargo
    rustc
  ];

  # dontNpmBuild = true;

  # dontWrapQtApps = true;

  # installPhase = ''
  #   runHook preInstall

  #   plasmapkg2 --install pkg --packageroot $out/share/kwin/scripts

  #   runHook postInstall
  # '';

  meta = with lib; {
    description = "Something for Valetudo";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
    broken = true;
  };
}
