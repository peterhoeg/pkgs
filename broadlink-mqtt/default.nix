{
  lib,
  fetchFromGitHub,
  python3Packages,
  libffi,
  openssl,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "broadlink-mqtt";
  version = "0.0.0.20220104";

  format = "other";

  src = fetchFromGitHub {
    owner = "eschava";
    repo = pname;
    rev = "4f21878e09db27b2162ef0f135f4b7c721388869";
    hash = "sha256-5CmB61XxkeHDrunhSPtV10P61CcqMdRX40P2e3ZfEWk=";
  };

  postPatch = ''
    substituteInPlace mqtt.py \
      --replace-fail "dirname + 'logging.conf'" "os.getenv('BROADLINKMQTTCONFIGLOGGING', dirname + 'logging.conf')" \
      --replace-fail 'dirname + "commands/"'    'os.getenv("BROADLINKMQTTHOME", dirname) + "commands/"' \
      --replace-fail 'dirname + "macros/"'      'os.getenv("BROADLINKMQTTHOME", dirname) + "macros/"' \
      --replace-fail 'from test import TestDevice' ""
  '';

  buildInputs = [
    libffi
    openssl
  ];

  propagatedBuildInputs = with pypkgs; [
    broadlink
    paho-mqtt
  ];

  installPhase = ''
    runHook preInstall

    install -Dm555 mqtt.py $out/bin/mqtt
    install -Dm444 -t $out/etc *.conf
    install -Dm444 -t $out/share/doc/${pname} *.md LICENSE
    install -d $out/share/${pname}
    cp -r --no-preserve=all commands macros $out/share/${pname}

    runHook postInstall
  '';

  # No tests
  doCheck = false;

  meta = with lib; {
    description = "Control BroadLink devices via MQTT";
    license = licenses.mit;
  };
}
