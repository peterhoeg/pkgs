{ stdenv, lib }:

let
  mibs = [
    ./ASTARO-MIB.txt
  ];

in
stdenv.mkDerivation rec {
  pname = "astaro-mibs";
  version = "201605060000Z";

  buildCommand = lib.concatMapStringsSep "\n" (e: ''
    install -Dm444 ${e} $out/share/snmp/mibs/${(builtins.baseNameOf e)}
  '') mibs;

  meta = with lib; {
    description = "Astaro/Sophos MIBs";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
}
