{
  lib,
  rustPlatform,
  fetchFromGitHub,
}:

rustPlatform.buildRustPackage rec {
  pname = "sysit";
  version = "0.4.0";

  src = fetchFromGitHub {
    owner = "crodjer";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-EJG3m3sJ3XgnbmPqSXyk8srse6yTRmLEBjP5ZRtuYzI=";
  };

  cargoSha256 = "sha256-RUrNg+yDFpRPpZfBjiv0S9GmnY+JhPuM+r281aOLg9A=";

  doCheck = true;

  meta = with lib; {
    description = "System information";
    license = licenses.gpl3Only;
    maintainers = with maintainers; [ peterhoeg ];
    inherit (src.meta) homepage;
  };
}
