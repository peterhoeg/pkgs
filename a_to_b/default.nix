{ lib, python3 }:

let
  pypkgs = python3.pkgs;

in
pypkgs.buildPythonApplication rec {
  pname = "a_to_b";
  version = "0.0.1";
  format = "other";

  src = ./a_to_b.py;
  dontUnpack = true;
  installPhase = ''
    install -Dm555 $src $out/bin/${pname}.py
  '';

  propagatedBuildInputs = with pypkgs; [ WazeRouteCalculator ];
}
