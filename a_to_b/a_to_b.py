#!/usr/bin/env python3

import getopt, logging, sys
import WazeRouteCalculator

logger = logging.getLogger('WazeRouteCalculator.WazeRouteCalculator')
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
logger.addHandler(handler)

home = '213 Bedok South Avenue 1, Singapore'
office = '8 Shenton Way, Singapore'

def main(argv):

  if len(argv) > 0 and argv[0] == "home_to_office":
    from_address = home
    to_address = office
  else:
    from_address = office
    to_address = home
  region = 'EU'

  # try:
  #   opts, args = getopt.getopt(argv,"hf:t:",["from=","to="])
  # except getopt.GetoptError:
  #   print('a_to_b -f <from_address> -t <to_address>')
  #   sys.exit(2)
  # for opt, arg in opts:
  #   if opt == '-h':
  #     print('a_to_b -f <from_address> -t <to_address>')
  #     sys.exit()
  #   elif opt in ("-f", "--from"):
  #     from_address = arg
  #   elif opt in ("-t", "--to"):
  #     to_address = arg

  route = WazeRouteCalculator.WazeRouteCalculator(from_address, to_address, region)
  route.calc_route_info()

if __name__ == "__main__":
   main(sys.argv[1:])
