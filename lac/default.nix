{
  stdenv,
  lib,
  fetchurl,
  autoPatchelfHook,
}:

let
  changelog = fetchurl {
    url = "http://losslessaudiochecker.com/dl/CHANGELOG-CLI.md";
    hash = "sha256-QQQfCUwvvuSez5i7dHCNR9ecSDyg+cqjv1oPE14hc9k=";
  };

in
stdenv.mkDerivation rec {
  pname = "lac";
  version = "2.0.5";

  src = fetchurl {
    url = "http://losslessaudiochecker.com/dl/LAC-Linux-64bit.tar.gz";
    hash = "sha256-eHIgFgXtD4mgAeub44eNBc1ChUj/0YQ4rLxzrA9dP8Q=";
  };

  sourceRoot = ".";

  nativeBuildInputs = [ autoPatchelfHook ];

  installPhase = ''
    runHook preInstall

    install -Dm555 LAC $out/bin/lac
    install -Dm444 ${changelog} $out/share/doc/${pname}/${changelog.name}

    runHook postInstall
  '';

  meta = {
    description = "A utility to check whether a WAVE or FLAC file is truly lossless or not";
    homepage = "http://losslessaudiochecker.com/";
  };
}
