{
  lib,
  fetchFromGitHub,
  python3Packages,
}:

python3Packages.buildPythonApplication rec {
  pname = "set_conf";
  version = "0.0.1.20200126";

  format = "other";

  src = fetchFromGitHub {
    owner = "ichtm";
    repo = "set_conf";
    rev = "74f99481d94721fdcc1cb6ebfa935df0cb7bbb36";
    sha256 = "sha256-Yt49PvQ/YYiHcDEE+wDr2Ho7Nks092g+PweN6seWtGU=";
  };

  postPatch = ''
    substituteInPlace set_conf.py \
      --replace-fail 'python3 set_conf.py' set_conf
  '';

  installPhase = ''
    install -Dm555 set_conf.py $out/bin/set_conf
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md
  '';

  meta = with lib; {
    description = "INI set";
    license = licenses.gpl3;
  };
}
