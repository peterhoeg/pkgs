{
  stdenv,
  lib,
  requireFile,
  unzip,
  autoPatchelfHook,
  zlib,
}:

stdenv.mkDerivation rec {
  pname = "api7";
  version = "2.0.0";

  src = requireFile {
    name = "API7-FR-Server-Ubuntu-20-04-LTS-latest.zip";
    sha256 = "1y5mna50s70jl9qin0pz2xqbg35ihj6vv11yfhzr1y9z5aqyyivk";
    message = ''
      Register and download from https://seventhsense.ai
    '';
  };

  buildInputs = [
    stdenv.cc.cc.lib
    zlib
  ];

  nativeBuildInputs = [
    autoPatchelfHook
    unzip
  ];

  dontConfigure = true;

  dontBuild = true;

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin bin/*
    install -Dm444 -t $out/share/doc/${pname} NOTICE
    install -Dm444 -t $out/lib/systemd/system *.service
    substituteInPlace $out/lib/systemd/system/*.service \
      --replace-fail /opt/seventhsense/api7/.env /etc/api7/.env \
      --replace-fail /opt/seventhsense/api7/bin $out/bin \
      --replace-fail /opt/seventhsense/api7 %h

    runHook postInstall
  '';

  dontStrip = true;

  meta = with lib; {
    description = "SeventhSenseu API v7";
    license = licenses.unfreeRedistributable;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = [ "x86_64-linux" ];
    hydraPlatforms = platforms.none;
  };
}
