{
  stdenv,
  lib,
  fetchFromGitHub,
  cmake,
  installShellFiles,
  libX11,
}:

stdenv.mkDerivation rec {
  pname = "xroach";
  version = "0.0.1";

  src = fetchFromGitHub {
    owner = "interkosmos";
    repo = "xroach";
    rev = "3ef9309005936050a45e43f93e821eece2ac6eb7";
    hash = "sha256-XUkIzt1e3zc5I9bRTJng6gDfUbJ0FtRmJaUyvwMyxr8=";
  };

  buildInputs = [ libX11 ];

  nativeBuildInputs = [
    cmake
    installShellFiles
  ];

  installPhase = ''
    install -Dm555 -t $out/bin xroach
    installManPage ../xroach.man
  '';

  meta = with lib; {
    description = "xroach";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
}
