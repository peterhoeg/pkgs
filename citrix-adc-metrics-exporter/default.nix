{
  lib,
  fetchFromGitHub,
  python3Packages,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "citrix-adc-metrics-exporter";
  version = "1.4.9";

  format = "other";

  src = fetchFromGitHub {
    owner = "citrix";
    repo = pname;
    rev = version;
    hash = "sha256-6Mct8CZTV6Mud14ucDOqRmb/aNxvQ7+24BSvlZ5jPIg=";
  };

  installPhase = ''
    runHook preInstall

    install -Dm555 exporter.py $out/bin/${pname}
    install -Dm444 -t $out/share/doc/${pname} *.md
    install -Dm444 -t $out/share/doc/${pname}/examples *.{example,json}

    runHook postInstall
  '';

  propagatedBuildInputs = with pypkgs; [
    prometheus_client
    requests
    pyyaml
    tenacity
    urllib3
  ];

  meta = with lib; {
    description = "Citrix ADC (NetScaler) exporter for Prometheus";
    license = licenses.mit;
  };
}
