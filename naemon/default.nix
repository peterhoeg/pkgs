{
  stdenv,
  lib,
  fetchFromGitHub,
  autoreconfHook,
  pkg-config,
  perlPackages,
  glib,
  gperf,
  help2man,
}:

let
  version = "1.4.1";

in
{
  # naemon-core = perlPackages.buildPerlPackage rec {
  naemon-core = stdenv.mkDerivation rec {
    pname = "naemon-core";
    inherit version;

    src = fetchFromGitHub {
      owner = "naemon";
      repo = pname;
      rev = "v" + version;
      hash = "sha256-bLUGToEXiIcKKm5UL05rRaSl9+mmuEYdkYeHzeK5ZKE=";
    };

    postPatch = ''
      sed -i Makefile.am -E \
        -e '/mkinstalldirs.*CHECKRESULTDIR/d' \
        -e '/mkinstalldirs.*logdir/d' \
        -e '/naemon.logrotate.el7/d'

      autoupdate
    '';

    # autoreconfFlags = [
    configureFlags = [
      # "--logdir=/var/log/naemon"
      "--localstatedir=/var"
      # "--lockfile=/run/naemon/naemon.pid"
      "--sysconfdir=${placeholder "out"}/etc/naemon"
      "--with-initdir=${placeholder "out"}/etc/init.d"
    ];

    env.NIX_CFLAGS_COMPILE = "-Wno-error";

    buildInputs = [
      glib
      gperf
    ];

    nativeBuildInputs = [
      autoreconfHook
      help2man
      pkg-config
    ];

    enableParallelBuilding = true;
  };

  naemon = stdenv.mkDerivation rec {
    pname = "naemon";
    inherit version;

    src = fetchFromGitHub {
      owner = "naemon";
      repo = pname;
      rev = "v" + version;
      hash = "sha256-d34I4eM7ek+Ef6Q9kigw2jzx5/hv1RiDyOE1eikt9XY=";
    };

    PREFIX = "";
    DESTDIR = placeholder "out";

    meta = with lib; {
      description = "Naemon meta package";
      license = licenses.free;
    };
  };
}
