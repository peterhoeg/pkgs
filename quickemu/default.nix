{
  stdenv,
  lib,
  fetchFromGitHub,
  cdrkit,
  coreutils,
  curl,
  gnugrep,
  gnused,
  jq,
  procps,
  qemu,
  python3,
  spice-gtk,
  swtpm,
  usbutils,
  util-linux,
  wget,
  xrandr,
  zsync,
}:

let
  pypkgs = python3.pkgs;

  common = [
    coreutils
    gnugrep
    gnused
  ];

  paths = {
    quickemu = common ++ [
      cdrkit
      jq
      procps
      qemu
      spice-gtk
      swtpm
      usbutils
      util-linux
      xrandr
    ];
    quickget = common ++ [
      wget
      zsync
    ];
  };

in
pypkgs.buildPythonApplication rec {
  pname = "quickemu";
  version = "2.2.3";

  format = "other";

  src = fetchFromGitHub {
    owner = "wimpysworld";
    repo = "quickemu";
    rev = version;
    sha256 = "sha256-uTgfPeqlXtD9XrOq1WQ5Yo9Oanezed5QnfR5Nti9bnA=";
  };

  nativeBuildInputs = [ gnused ];

  postPatch = lib.concatStringsSep "\n" (
    lib.mapAttrsToList (n: v: ''
      sed -i -e '3iexport PATH=${lib.makeBinPath v}:\$PATH' ${n}
    '') paths
  );

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin macrecovery quick{emu,get}
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md

    runHook postInstall
  '';

  doCheck = false;

  meta = with lib; {
    description = "Launch a VM quickly";
    platforms = platforms.linux;
  };
}
