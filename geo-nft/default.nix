{
  lib,
  stdenv,
  fetchFromGitHub,
  resholve,
  bash,
  coreutils,
  curl,
  gawk,
  gnugrep,
  gnused,
  gzip,
  nftables,
  systemd,
}:

resholve.mkDerivation rec {
  pname = "geo-nft";
  version = "2.2.8";

  src = fetchFromGitHub {
    owner = "wirefalls";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-LnC8byKUtESrWkbgPppJeG2my4RftAbnHuDwiK5szyI=";
  };

  postPatch = ''
    substituteInPlace systemd/geo-update.service \
      --replace-fail '/usr/sbin' "$out/bin"

    substituteInPlace geo-nft.sh \
      --replace-fail geo-nft.sh            geo-nft \
      --replace-fail /etc/nftables/geo-nft /var/lib/geo-nft \
      --replace-fail /etc                  /var/lib/geo-nft

    cat >> systemd/geo-update.service <<_EOF
      StateDirectory=geo-nft
    _EOF
  '';

  solutions.default = {
    scripts = [ "bin/geo-nft" ];
    interpreter = lib.getExe bash;
    inputs = [
      coreutils
      curl
      gawk
      gnugrep
      gnused
      gzip
      nftables
      systemd
    ];
    execer = [
      "cannot:${gzip}/bin/gunzip"
      "cannot:${systemd}/bin/systemctl"
    ];
  };

  installPhase = ''
    runHook preInstall

    install -Dm555 geo-nft.sh $out/bin/geo-nft
    install -Dm444 -t $out/lib/systemd/system systemd/*
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md

    runHook postInstall
  '';

  meta = with lib; {
    description = "Geo IP support for nftables";
    license = licenses.gpl2Only;
  };
}
