{
  lib,
  fetchFromGitLab,
  python3Packages,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "feed2exec";
  version = "0.19.0";

  format = "pyproject";

  src = fetchFromGitLab {
    owner = "anarcat";
    repo = pname;
    rev = version;
    hash = "sha256-ZX5Xx31BwMXy///mycct9LxjciD90bbDjP7DPgBuEIM=";
  };

  # setuptools_scm wants a .git repository, but we don't care - we can just generate the required
  # file from $version
  postPatch = ''
    substituteInPlace pyproject.toml \
      --replace-fail ', "setuptools_scm>=6.2"' ""

    echo 'version="${version}"' > feed2exec/_version.py

    makeWrapperArgs+=(--prefix PYTHONPATH : $out/${pypkgs.python.sitePackages})
  '';

  # postInstall = ''
  #   mkdir -p $out/libexec
  #   mv $out/bin/feed2exec $out/libexec/
  # '';

  # postFixup = ''
  #   makeWrapper ${pypkgs.python.interpreter} $out/bin/feed2exec \
  #     --prefix PYTHONPATH : ${pypkgs.makePythonPath ([ (placeholder "out") (lib.getLib pypkgs.python) ] ++ propagatedBuildInputs)} \
  #     --add-flags "$out/${pypkgs.python.sitePackages}/feed2exec/__main__.py"
  # '';

  propagatedBuildInputs = with pypkgs; [
    cachecontrol
    chardet
    click
    dateparser
    docutils
    feedparser
    # logging
    lxml
    requests-file
    sphinx
    unidecode
    pyxdg
  ];

  doCheck = true;

  checkInputs = with pypkgs; [
    pytest
    pytest-cov
    pytest-runner
  ];

  pythonImportChecks = [ pname ];

  meta = with lib; {
    description = "RSS to Maildir";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
