{
  lib,
  fetchFromGitHub,
  python3Packages,
  gobject-introspection,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "kshift";
  version = "1.0.0";

  pyproject = true;

  src = fetchFromGitHub {
    owner = "justjokiing";
    repo = "kshift";
    rev = "v" + version;
    sha256 = "sha256-6F/e/5cK0c/uhHL/fU9Hyz8S9q3DcIqJbGM8BlRmVy4=";
  };

  nativeBuildInputs = with pypkgs; [ hatchling ];

  propagatedBuildInputs = with pypkgs; [
    click
    colorama
    pydantic
    pyyaml
    requests
  ];

  postInstall = ''
    # should create the systemd files
  '';

  meta = with lib; {
    description = "KDE Plasma Theme Switcher";
    license = licenses.free;
    mainProgram = "kshift";
  };
}
