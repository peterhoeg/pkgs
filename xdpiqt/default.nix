{
  mkDerivation,
  qmake,
  xdpi,
}:

mkDerivation {
  pname = "xdpiqt";
  inherit (xdpi) version src meta;

  sourceRoot = "source/qt";

  nativeBuildInputs = [ qmake ];

  enableParallelBuilding = true;

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin qtdpi

    runHook postInstall
  '';
}
