{ stdenvNoCC, lib }:

let
  files = [
    {
      file = ./lg_hdr_4k.icm;
      name = "lg_hdr_4k.icm";
    }
  ];

in
stdenvNoCC.mkDerivation {
  pname = "monitor-icm";
  version = "0.0.1";

  buildCommand = lib.concatMapStringsSep "\n" (e: ''
    install -Dm444 ${e.file} $out/${e.name}
  '') files;

  passthru = {
    inherit files;
  };

  meta = with lib; {
    description = "ICC color profiles for monitors";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
}
