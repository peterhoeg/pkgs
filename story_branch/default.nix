{ lib, bundlerApp }:

bundlerApp rec {
  pname = "story_branch";
  gemdir = ./.;

  exes = [ "story_branch" ];

  meta = with lib; {
    description = "story_branch";
    homepage = "https://example.com";
    license = licenses.mit;
    maintainers = with maintainers; [ ];
    platforms = platforms.unix;
  };
}
