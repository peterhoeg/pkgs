{
  lib,
  fetchFromGitHub,
  python3Packages,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "ip6words";
  version = "1.1.5";

  pyproject = true;

  src = fetchFromGitHub {
    owner = "lstn";
    repo = "ip6words";
    rev = "v${version}";
    hash = "sha256-znSKZF5LQSBtafOI+Jv1mXoBdpw0T2CcczDXpnaKtIo=";
  };

  # we should *probably* use the pinned versions to ensure that the word distributions do not
  # change, but it's ancient and I cannot be bothered to try and get the the old versions going
  postPatch = ''
    substituteInPlace requirements.txt \
      --replace-fail '==' '>='
  '';

  propagatedBuildInputs = [
    pypkgs.dill
    pypkgs.nltk
  ];

  nativeBuildInputs = with pypkgs; [ setuptools ];

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} *.md
  '';

  meta = with lib; {
    description = "IPv6 addresses <-> words";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
  };
}
