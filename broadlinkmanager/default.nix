{
  lib,
  fetchFromGitHub,
  python3Packages,
  makeWrapper,
}:

let
  pypkgs = python3Packages;

  starlette' = pypkgs.buildPythonPackage rec {
    pname = "starlette_exporter";
    version = "0.12.0";

    src = pypkgs.fetchPypi {
      inherit pname version;
      sha256 = "sha256-GNldCc+0VCfm9UrlkZgrXvkAqhUM6bQecXZ1sYxb23Q=";
    };

    propagatedBuildInputs = with pypkgs; [
      prometheus-client
      starlette
    ];

    meta = with lib; { };
  };

in
pypkgs.buildPythonApplication rec {
  pname = "broadlinkmanager";
  version = "4.0.0";

  format = "other";

  src = fetchFromGitHub {
    owner = "t0mer";
    repo = "broadlinkmanager-docker";
    rev = version;
    sha256 = "sha256-Lnaowd2PK9KbFIRJtDQxgbM7H40G9LGi7dtEzk/iLC4=";
  };

  postPatch =
    let
      baseDir = "${placeholder "out"}/share/${pname}";
    in
    ''
      substituteInPlace broadlinkmanager.py \
        --replace-fail 'os.getenv("ENABLE_GOOGLE_ANALYTICS")' 'os.getenv("ENABLE_GOOGLE_ANALYTICS", False)' \
        --replace-fail 'hostname -I' 'hostname -i' \
        --replace-fail '"VERSION"' '"${baseDir}/VERSION"' \
        --replace-fail 'directory="dist'      'directory="${baseDir}/dist' \
        --replace-fail 'directory="templates' 'directory="${baseDir}/templates' \
        --replace-fail "app.root_path, 'data'" "'/var/lib/${pname}'"

      rm dist/*.egg
    '';

  sourceRoot = "source/broadlinkmanager";

  nativeBuildInputs = [ makeWrapper ];

  propagatedBuildInputs = with pypkgs; [
    aiofiles
    broadlink
    cryptography
    fastapi
    loguru
    starlette'
    uvicorn
  ];

  installPhase = ''
    runHook preInstall

    dir=$out/${pypkgs.python.sitePackages}

    mkdir -p $out/bin
    makeWrapper ${pypkgs.python.interpreter} $out/bin/broadlinkmanager \
      --add-flags $dir/broadlinkmanager.py \
      --set PYTHONPATH ${pypkgs.makePythonPath propagatedBuildInputs}

    mkdir -p $dir $out/share/${pname}
    mv {dist,templates,VERSION} $out/share/${pname}
    cp -r * $dir

    runHook postInstall
  '';

  # no tests
  doCheck = false;

  # pythonImportsCheck = [ "broadlink" ];

  meta = with lib; {
    description = "Control Broadlink RM devices";
    license = licenses.gpl3;
  };
}
