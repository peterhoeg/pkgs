{
  lib,
  buildGoModule,
  fetchFromGitHub,
}:

buildGoModule rec {
  pname = "git-hooks";
  version = "1.3.1";

  src = fetchFromGitHub {
    owner = "git-hooks";
    repo = "git-hooks";
    rev = "v${version}";
    hash = "sha256-bFOrX0VDaTFus9V9dELAAraEvoTNgodgmbIKRQuZPZs=";
  };

  vendorHash = "sha256-ok1RZO1mr3PdSL/vbzCjcxbM+ZBgo2jJpYvg0Y2Nh/Q=";

  doCheck = false;

  meta = with lib; {
    description = "git commit hooks manager";
    maintainers = with maintainers; [ peterhoeg ];
  };
}
