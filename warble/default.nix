{
  lib,
  stdenv,
  fetchFromGitHub,
  vala,
  desktop-file-utils,
  meson,
  ninja,
  pkg-config,
  wrapGAppsHook,
  gtk3,
  libgee,
  libhandy,

  pantheon, # for granite
}:

stdenv.mkDerivation rec {
  pname = "warble";
  version = "1.2.0";

  src = fetchFromGitHub {
    owner = "avojak";
    repo = pname;
    rev = version;
    hash = "sha256-H41rDuP0wJxyIaIJNqGPrnu4NVHF8A/qBzRKOt7xICk=";
  };

  nativeBuildInputs = [
    desktop-file-utils
    meson
    ninja
    pkg-config
    vala
    wrapGAppsHook
  ];

  buildInputs = [
    gtk3
    pantheon.granite
    libgee
    libhandy
  ];

  meta = with lib; {
    description = "Wordle like game";
    license = licenses.gpl3Only;
    platforms = platforms.linux;
  };
}
