{
  lib,
  mkDerivation,
  fetchFromGitLab,
  extra-cmake-modules,
  ninja,
  pkg-config,
  qmake,
  boost,
  libqtav,
  qtdeclarative,
  qtmultimedia,
  qtquickcontrols2,
  qtsvg,
  kcoreaddons,
  kdsoap,
  ki18n,
  kirigami2,
  kxmlgui,
}:

let
  nativeBuildInputs = [
    extra-cmake-modules
    pkg-config
  ];

  kdsoap-discovery = mkDerivation rec {
    pname = "kdsoap-ws-discovery-client";
    version = "unstable-20200927";

    src = fetchFromGitLab {
      owner = "caspermeijn";
      repo = "kdsoap-ws-discovery-client";
      rev = "dcefb65c88e76f1f9eda8b0318006e93d15a0e1e";
      sha256 = "sha256-UqYI5Zj0OVwYAg856pftG8ZqRZEddskwr66RWK/rKzU=";
    };

    buildInputs = [ kdsoap ];

    inherit nativeBuildInputs;
  };

in
mkDerivation rec {
  pname = "onvif-viewer";
  version = "0.13";

  src = fetchFromGitLab {
    owner = "caspermeijn";
    repo = "onvifviewer";
    rev = "v" + version;
    sha256 = "sha256-p2y2uPG/rcEd1RYwusHeJ8dHAR3N3N5x7kE8ZPFfPrc=";
  };

  buildInputs = [
    libqtav
    qtmultimedia
    qtquickcontrols2
    kcoreaddons
    kdsoap
    kdsoap-discovery
    ki18n
    kirigami2
    kxmlgui
  ];

  inherit nativeBuildInputs;

  cmakeFlags = [ "-Wno-dev" ];

  # TODO: get this added automatically by the relevant Qt hook
  # If the application fails to run, check that the qml directory hasn't moved
  qtWrapperArgs = [
    "--prefix QML2_IMPORT_PATH : ${libqtav}/lib/qml"
  ];

  meta = with lib; {
    description = "ONVIF camera viewer for Android, Plasma Mobile and Linux desktop";
    license = licenses.gpl3;
    maintainers = with maintainers; [ peterhoeg ];
    inherit (kcoreaddons.meta) platforms;
  };
}
