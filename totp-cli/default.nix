{
  lib,
  buildGoModule,
  fetchFromGitHub,
  installShellFiles,
}:

buildGoModule rec {
  pname = "totp-cli";
  version = "1.8.7";

  src = fetchFromGitHub {
    owner = "yitsushi";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-WCPDrKGIRrYJFeozXtf8YPgHJ8m6DAsMBD8Xgjvclvc=";
  };

  vendorHash = "sha256-VTlSnw3TyVOQPU+nMDhRtyUrBID2zesGeG2CgDyjwWY=";

  nativeBuildInputs = [ installShellFiles ];

  ldflags = [
    "-X github.com/yitsushi/totp-cli/internal/info.Version=${version}"
  ];

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} LICENSE *.md docs/*.md
    installShellCompletion \
      --cmd ${pname} \
      --bash autocomplete/bash_autocomplete \
      --zsh autocomplete/zsh_autocomplete
  '';

  doInstallCheck = true;

  installCheckPhase = ''
    $out/bin/totp-cli --version | grep ${version}
  '';

  meta = with lib; {
    description = "TOTP client for CLI use";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
