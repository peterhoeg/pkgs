{
  stdenv,
  lib,
  fetchFromGitHub,
}:

stdenv.mkDerivation rec {
  pname = "emacsql-sqlite";
  version = "4.0.3"; # the embedded sqlite version is 3.39.3

  src = fetchFromGitHub {
    owner = "magit";
    repo = "emacsql";
    rev = "v" + version;
    hash = "sha256-MaL3t+2MhWOE6eLmt8m4ImpsKeNeUZ4S8zEoQVu51ZY=";
  };

  sourceRoot = "source/sqlite";

  enableParallelBuilding = true;

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin ${meta.mainProgram}

    runHook postInstall
  '';

  meta = with lib; {
    description = "Custom sqlite for emacsql";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    mainProgram = "emacsql-sqlite";
  };
}
