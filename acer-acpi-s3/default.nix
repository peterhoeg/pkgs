{
  lib,
  stdenvNoCC,
  fetchFromGitHub,
  acpica-tools,
  cpio,
}:

let
  dir = ./sf314-43_v106;
  file = "acpi-override.img";

in
stdenvNoCC.mkDerivation rec {
  pname = "acer-acpi-s3";
  # we follow the firmware version
  version = "1.06";

  src = fetchFromGitHub {
    owner = "lbschenkel";
    repo = "acer-sf314_43-acpi-patch";
    rev = "0fc172e644d15ed5147bb6dc4520e1c117d91088";
    hash = "sha256-yOIY1XTeDjCPyX6clU5ZZg3OzpU/42mAE+IdzPG6kXg=";
  };

  postPatch = ''
    install -Dm444 -t acpi ${dir}/*.dat

    substituteInPlace Makefile \
      --replace-fail 'sudo ' ""
  '';

  nativeBuildInputs = [
    acpica-tools
    cpio
  ];

  makeFlags = [ file ];

  installPhase = ''
    runHook preInstall

    install -Dm444 -t $out ${file}

    runHook postInstall
  '';

  meta = with lib; {
    description = "Acer SF314-43 ACPI S3 patch for v${version}";
    license = licenses.free;
  };
}
