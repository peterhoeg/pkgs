{
  lib,
  buildGoModule,
  fetchFromGitHub,
  runtimeShell,
}:

let
  # need npm dependencies in here. split it into multiple

in
buildGoModule rec {
  pname = "karma";
  version = "0.106";

  src = fetchFromGitHub {
    owner = "prymitive";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-IKURioYYVVXGcQ8cyc79bHERAd0VlYZM6S4AwiFtPc8=";
  };

  # postPatch = ''
  #   substituteInPlace cmd/util.go \
  #     --replace-fail /bin/bash ${runtimeShell}
  # '';

  preBuild = ''
    make -C ui lint-js
  '';

  vendorHash = "sha256-zeiDMI0mR8z7+fmebfa3rNMvGvNnPyGbazWt1LgFY/4=";

  ldflags = [ "-X main.version=${version}" ];

  meta = with lib; {
    description = "AlertManager UI";
    # license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
