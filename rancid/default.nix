{
  stdenv,
  lib,
  fetchFromGitHub,
  writeShellScriptBin,
  autoreconfHook,
  libtool,
  makeBinaryWrapper ? null,
  makeWrapper,
  coreutils,
  expect,
  git,
  gnugrep,
  inetutils, # for telnet
  gnused,
  openssh,
  perl,
  runtimeShell,
}:

# we cannot use resholvePackage yet - the scripts are too hairy

let
  inherit (lib)
    concatMapStringsSep
    concatStringsSep
    makeBinPath
    mapAttrsToList
    replaceStrings
    ;

  # The installer executes ping to figure out how to call it and then sets the
  # full path to the binary.
  #
  # We need the wrapped binary, so use this dumb script that pretends all is OK
  # when run inside the builder and otherwise just calls the wrapped binary when
  # not. This assumes that the first way the installer calls ping (with -c 1) is
  # correct.
  wrappedPing = writeShellScriptBin "ping" ''
    test -x /run/wrappers/bin/ping || exit 0

    exec /run/wrappers/bin/ping "$@"
  '';

  # these are binaries, not scripts
  needsBin = {
    hlogin = makeBinPath [
      (placeholder "out")
      telnet'
    ];
    ulogin = makeBinPath [
      (placeholder "out")
      telnet'
    ];
    rancid-cvs = makeBinPath [ git ];
  };

  makeWrapper' = if makeBinaryWrapper == null then makeWrapper else makeBinaryWrapper;

  telnet' = inetutils;

  # https://sites.google.com/site/jrbinks/code/rancid/cmwrancid
  modules = {
    bin = [ ./modules/cmwlogin ];
    lib = [ ./modules/cmw.pm ];
    etc = [ ./modules/cmw ];
  };

in
stdenv.mkDerivation rec {
  pname = "rancid";
  version = "3.13";

  src = fetchFromGitHub {
    owner = "haussli";
    repo = "rancid";
    rev = "v" + replaceStrings [ "." ] [ "_" ] version;
    sha256 = "sha256-TAeOSwdDhP06OSV0en/hMVF3qWVwJUsiqt97rdgtAzE=";
  };

  postPatch = ''
    patchShebangs .

    substituteInPlace configure.ac \
      --replace-fail 'm4_esyscmd(configure.vers package_name),' ${pname}, \
      --replace-fail 'm4_esyscmd(configure.vers package_version),' ${version},

    substituteInPlace etc/rancid.conf.sample.in \
      --replace-fail @ENV_PATH@ ${
        makeBinPath [
          "/run/wrappers"
          (placeholder "out")
          coreutils
          git
          gnugrep
          gnused
          openssh
          perl
          runtimeShell
          telnet'
        ]
      }

    for f in bin/*.in; do \
      if grep -q /usr/bin/tail $f ; then
        substituteInPlace $f --replace-fail /usr/bin/tail ${coreutils}/bin/tail
      fi
    done

    substituteInPlace bin/par.c \
      --replace-fail '"sh"' '"${runtimeShell}"'

    substituteInPlace bin/rancid-run.in \
      --replace-fail '>$LOGDIR/$GROUP.`date +%Y%m%d.%H%M%S` 2>&1' ' ' \
      --replace-fail 'perl ' '${perl}/bin/perl ' \
      --replace-fail 'sh ' '${runtimeShell} ' \
      --replace-fail '"control_rancid ' '"${placeholder "out"}/bin/control_rancid '

    ${concatMapStringsSep "\n" (e: "cat ${e} >> etc/rancid.types.conf") modules.etc}
  '';

  enableParallelBuilding = true;

  nativeBuildInputs = [
    autoreconfHook
    libtool
    makeWrapper'
    wrappedPing
  ];

  buildInputs = [
    expect
    openssh
    perl
    telnet'
  ];

  preInstall = concatStringsSep "\n" (
    (map (e: "install -Dm0555 ${e} $out/bin/${builtins.baseNameOf e}") modules.bin)
    ++ (map (e: "install -Dm0444 ${e} $out/lib/rancid/${builtins.baseNameOf e}") modules.lib)
  );

  postInstall = concatStringsSep "\n" (
    mapAttrsToList (n: v: ''
      wrapProgram $out/bin/${n} \
        --argv0 ${n} \
        --prefix PATH : ${v}
    '') needsBin
  );

  meta = with lib; {
    description = ''
      RANCID monitors a device's configuration, including software and hardware and uses a VCS to maintain history of changes.
    '';
    license = licenses.bsd3;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
  };
}
