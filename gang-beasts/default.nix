{
  stdenv,
  lib,
  requireFile,
  autoPatchelfHook,
  symlinkJoin,
  writeShellApplication,
  unzip,
}:

let
  launcherName = "gang-beasts";

  launcher = writeShellApplication {
    name = launcherName;
    text = ''
      ${unwrapped}/opt/gang-beasts/"Gang Beasts.x86_64" "$@"
    '';
  };

  unwrapped = stdenv.mkDerivation (finalAttrs: {
    pname = "gang-beasts-unwrapped";
    version = "1.24";

    src = requireFile {
      name = "boneloaf_GangBeasts_${finalAttrs.version}_Linux.zip";
      hash = "sha256-zdyqmjfu8OWbZbZryoTZ5PaDaNyGjTQf4WDBbphyBmQ=";
      message = ''
        Download from ${finalAttrs.meta.homePage}

        nix-prefetch-url file://\$PWD/the_file_you_just_downloaded.zip
      '';
    };

    sourceRoot = "No-DRM-Linux/Builds/NoDRM/Linux/Release";

    unpackPhase = ''
      unzip -q $src ${finalAttrs.sourceRoot}/\*
    '';

    buildInputs = [ stdenv.cc.cc.lib ];

    nativeBuildInputs = [
      autoPatchelfHook
      unzip
    ];

    installPhase = ''
      dir=$out/opt/gang-beasts
      mkdir -p $dir
      mv * $dir
      chmod 555 $dir/*.x86_64
    '';

    meta = with lib; {
      description = "Gang Beasts for Linux";
      homePage = "https://gangbeasts.game/";
      license = licenses.free;
      maintainers = with maintainers; [ peterhoeg ];
      platforms = platforms.linux;
    };
  });

in
# unwrapped
symlinkJoin {
  name = "gang-beasts-${unwrapped.version}";
  paths = [
    unwrapped
    launcher
  ];
}
