{
  stdenv,
  stdenvNoCC,
  lib,
  fetchurl,
  fetchFromGitHub,
  cmake,
  unzip,
  autoPatchelfHook,
  makeBinaryWrapper,
  symlinkJoin,
  cjson,
  curl,
  freetype,
  glew,
  libGL,
  libjpeg_turbo,
  libpng,
  libtheora,
  libvorbis,
  lua5_4,
  minizip,
  openal,
  openssl,
  sqlite,
  SDL2,
  zlib,
  xorg,
  baseDir ? "/.local/share/etlegacy",
  withAssets ? true,
  withClient ? true,
}:

let
  inherit (lib)
    cmakeBool
    optional
    optionalString
    replaceStrings
    ;

  version = "2.80.2";

  mods = [
    (fetchurl {
      url = "https://mirror.etlegacy.com/geoip/GeoIP.dat.tar.gz";
      hash = "sha256-GqcCP1mdAfzneGH/Mx0QJM2SkY7fUnwvN/SjdzIYOis=";
    })

    (fetchurl {
      url = "https://mirror.etlegacy.com/omnibot/omnibot-linux-latest.tar.gz";
      hash = "sha256-hUnFaQtQAL5ujE5meRJwo+OSb37JFLCTYPFWffMOKX8=";
    })

    (fetchurl {
      url = "https://mirror.etlegacy.com/wolfadmin/wolfadmin.tar.gz";
      hash = "sha256-MWWbHZnNXJiEpmMX+AMQnmMaMV4RAoQuTk0puFeTl9E=";
    })
  ];

  cleanup = [
    "$out/share/etlegacy/*.sh"
    "$out/share/etlegacy/legacy/omni-bot/omnibot_et.so"
  ];

  etlegacy-assets = stdenvNoCC.mkDerivation (
    let
      version = "2.60b";
      version' = replaceStrings [ "." ] [ "" ] version;
    in
    {
      pname = "etlegacy-assets";
      inherit version;

      src = fetchurl {
        url = "https://cdn.splashdamage.com/downloads/games/wet/et${version'}.x86_full.zip";
        hash = "sha256-Ko/vjoVY7//K1li7mosS34dAQYs1FBQjUOujt2Qes+A=";
      };

      nativeBuildInputs = [ unzip ];

      buildCommand = ''
        dir=$out/share/etlegacy
        mkdir -p $dir
        unzip -q $src -d .
        bash ./et${version'}.x86_keygen_V03.run \
          --noexec --keep --target .
        cp -r etmain $dir/
      '';

      meta = with lib; {
        description = "ET:Legacy assets";
        license = licenses.unfree;
      };
    }
  );

in
rec {
  etlegacy-unwrapped = stdenv.mkDerivation rec {
    pname = "etlegacy-unwrapped";
    inherit version;

    src = fetchFromGitHub {
      owner = "etlegacy";
      repo = "etlegacy";
      rev = "v${version}";
      hash = "sha256-xCKsyjtB9FAYC/0/n0NGdR48P9LV+19PrFGVbF4Mon4=";
      # although we use dependencies from nixpkgs, etlegacy still expects the
      # structure to be set up, so instead of patching, we just fetch submodules
      fetchSubmodules = true;
    };

    postPatch =
      optionalString withClient ''
        substituteInPlace src/sys/sys_unix.c \
          --replace-fail '/.etlegacy' '${baseDir}'
      ''
      + ''
        mkdir -p build/legacy
      ''
      + lib.concatMapStringsSep "\n" (e: ''
        tar -xzf ${e} -C build/legacy
      '') mods;

    cmakeFlags = [
      "-Wno-dev"
      "-DOpenGL_GL_PREFERENCE=GLVND"
      (cmakeBool "ARM" (stdenv.isAarch32 || stdenv.isAarch64))
      (cmakeBool "BUILD_CLIENT" withClient)
      (cmakeBool "BUNDLED_LIBS" false)
      (cmakeBool "CROSS_COMPILE32" false)
      (cmakeBool "FEATURE_AUTOUPDATE" false)
      (cmakeBool "FEATURE_RENDERER2" true)
      (cmakeBool "INSTALL_EXTRA" true) # omnibot, geoip, wolfadmin
      "-DINSTALL_DEFAULT_BASEDIR=${placeholder "out"}/share/etlegacy"
      "-DINSTALL_DEFAULT_BINDIR=${placeholder "out"}/libexec"
      "-DINSTALL_DEFAULT_MODDIR=${placeholder "out"}/share/etlegacy"
    ];

    buildInputs =
      [
        cjson
        curl
        freetype
        glew
        libGL
        libjpeg_turbo
        libpng
        libtheora
        libvorbis
        lua5_4
        minizip
        openal
        openssl
        SDL2
        sqlite
        zlib
      ]
      ++ (with xorg; [
        libX11
        libpthreadstubs
      ]);

    # autoPatchelfHook is needed for the .so files included with the mods
    # which are not built from source
    nativeBuildInputs = [
      autoPatchelfHook
      cmake
    ];

    postInstall = lib.concatMapStringsSep "\n" (e: "rm -f ${e}") cleanup;

    meta = with lib; {
      description = "ET:Legacy";
      homepage = "https://www.etlegacy.com";
      license = licenses.gpl3Plus;
    };
  };

  etlegacy = symlinkJoin {
    name = "etlegacy${optionalString (!withClient) "-server"}-${version}";
    paths = [ etlegacy-unwrapped ] ++ optional withAssets etlegacy-assets;
    nativeBuildInputs = [ makeBinaryWrapper ];
    postBuild = ''
      mkdir -p $out/bin
      for f in ${optionalString withClient "etl"} etlded; do
        makeWrapper ${etlegacy-unwrapped}/libexec/$f $out/bin/$f \
          --argv0 $f \
          --add-flags "+set fs_basepath $out/share/etlegacy"
      done
    '';
  };
}
