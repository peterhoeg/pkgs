{
  stdenv,
  lib,
  fetchFromGitHub,
  cmake,
}:

stdenv.mkDerivation rec {
  pname = "cpu_features";
  version = "0.7.0";

  src = fetchFromGitHub {
    owner = "google";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-+lYYrSe+kmomHlBwY0hnNfq/kwNvX66F5xbiNLMBCDY=";
  };

  nativeBuildInputs = [ cmake ];

  # this is a statically linked library but we just want the binary that shows cpu details
  postInstall = ''
    rm -r $out/{include,lib}
  '';

  meta = with lib; {
    description = "A cross platform C99 library/binary to get cpu features at runtime";
    license = licenses.asl20;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
  };
}
