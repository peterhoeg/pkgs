{
  stdenv,
  lib,
  fetchurl,
  unzip,
  python3Packages,
}:

let
  pypkgs = python3Packages;

in
pypkgs.buildPythonApplication rec {
  pname = "kodi-netflix-auth";
  version = "1.1.4";

  format = "other";

  src = fetchurl {
    url = "https://www.dropbox.com/sh/ls3veptflvneub1/AADUXio4xjOcTndivDvBY8_Xa/NFAuthenticationKey_Linux_${version}.zip?dl=1";
    sha256 = "sha256-E4YPvNrbzxe3vdDpgfP1YRdmLNOEbnfr1mR7b+j8OfY=";
    name = "NFAuthenticationKey_Linux_${version}.zip";
  };

  postPatch = ''
    sed -i -e '1i#!/usr/bin/env python' *.py
  '';

  propagatedBuildInputs = with pypkgs; [
    pycryptodomex
    websocket-client
  ];

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    install -Dm555 -t $out/bin *.py
  '';

  meta = with lib; {
    description = "Kodi Netflix Authentication";
    license = licenses.free;
  };
}
