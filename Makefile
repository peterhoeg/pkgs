.PHONY: check clean dir flake pkgs

default: pkgs

check:
	@nix flake check

clean:
	@rm -f result*

flake:
	@nix flake update

dirs: pkgs

pkgs: clean
	@./generate.sh

include pkgs.mk
