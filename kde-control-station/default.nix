{
  stdenv,
  fetchFromGitHub,
  kdePackages,
}:

stdenv.mkDerivation {
  pname = "kde-control-station";
  version = "0-unstable-2024-08-29";

  src = fetchFromGitHub {
    owner = "EliverLara";
    repo = "kde-control-station";
    rev = "a1d0a4a0a5577d25d86c1e14e47f076cc9376434";
    hash = "sha256-gkfrY5x0TlerXNIrxaXaI8H970iTEClNDEH8Gq/2bjM=";
  };

  nativeBuildInputs = [ kdePackages.kpackage ];

  # --global still installs to $HOME/.local/share so we use --packageroot
  installPhase = ''
    runHook preInstall

    kpackagetool6 --install package --packageroot $out/share/plasma/plasmoids

    runHook postInstall
  '';
}
