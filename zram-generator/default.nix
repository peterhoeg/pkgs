{
  lib,
  rustPlatform,
  fetchFromGitHub,
  installShellFiles,
  ronn,
  systemd,
}:

rustPlatform.buildRustPackage rec {
  pname = "zram-generator";
  version = "1.1.2";

  src = fetchFromGitHub {
    owner = "systemd";
    repo = pname;
    rev = "v" + version;
    hash = "sha256-n+ZOWU+sPq9DcHgzQWTxxfMmiz239qdetXypqdy33cM=";
  };

  SYSTEMD_UTIL_DIR = "${placeholder "out"}/lib/systemd";
  SYSTEMD_SYSTEM_UNIT_DIR = "${SYSTEMD_UTIL_DIR}/system";
  SYSTEMD_SYSTEM_GENERATOR_DIR = "${SYSTEMD_UTIL_DIR}/system-generators";

  postPatch = ''
    cp ${cargoLock.lockFile} Cargo.lock

    sed -E -i Makefile \
      -e "s|^(SYSTEMD_UTIL_DIR).*|\1 = ${SYSTEMD_UTIL_DIR}|" \
      -e "s|^(SYSTEMD_SYSTEM_UNIT_DIR).*|\1 = ${SYSTEMD_SYSTEM_UNIT_DIR}|" \
      -e "s|^(SYSTEMD_SYSTEM_GENERATOR_DIR).*|\1 = ${SYSTEMD_SYSTEM_GENERATOR_DIR}|" \
      -e "s|$(DESTDIR)$(PREFIX)/share/doc|$out/share/doc/${pname}|"

    substituteInPlace Makefile \
      --replace-fail '$(DESTDIR)$(PREFIX)/share/doc' "$out/share/doc"
  '';

  cargoLock.lockFile = ./Cargo.lock;

  nativeBuildInputs = [
    installShellFiles
    ronn
  ];

  buildInputs = [ systemd ];

  postBuild = ''
    make man systemd-service
  '';

  postInstall = ''
    installManPage man/*.?
    install -Dpm444 units/systemd-zram-setup@.service -t ${SYSTEMD_SYSTEM_UNIT_DIR}
    install -Dpm444 zram-generator.conf.example -t $out/share/doc/${pname}

    install -Dm555 -t ${SYSTEMD_SYSTEM_GENERATOR_DIR} $out/bin/zram-generator
    rm -rf $out/bin
  '';

  meta = with lib; {
    description = "Systemd unit generator for zram devices";
    license = licenses.mit;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.linux;
    inherit (src.meta) homepage;
  };
}
