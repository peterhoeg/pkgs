{
  lib,
  fetchurl,
  perlPackages,
}:

perlPackages.buildPerlPackage rec {
  pname = "prename";
  version = "2.01";

  src = fetchurl {
    url = "mirror://cpan/authors/id/R/RM/RMBARKER/File-Rename-${version}.tar.gz";
    hash = "sha256-b0yV5VSjkCMIHLH/HJvL/p+hOLlz/sWMFNOruf5P17c=";
  };

  outputs = [ "out" ];

  postInstall = ''
    mv $out/bin/rename $out/bin/prename
    mv $out/share/man/man1/rename.1 $out/share/man/man1/prename.1
  '';

  meta = with lib; {
    description = "File renamer";
    license = licenses.free;
  };
}
