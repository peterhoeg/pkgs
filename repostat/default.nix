{
  lib,
  fetchFromGitHub,
  writeText,
  python3Packages,
  cacert,
  openssl,
  git,
}:

let
  pypkgs = python3Packages;

  # I have no idea why this isn't in the source distribution
  reqs = writeText "requirements.txt" ''
    Jinja2>=2.10.1
    pygit2>=1.6.1
    pytz>=2018.5
    pandas>=1.3.3
    tqdm>=4.45.0
  '';

in
pypkgs.buildPythonApplication rec {
  pname = "repostat";
  version = "2.2.0-unstable-2023-10-10";

  # format = "other";

  src = fetchFromGitHub {
    owner = "vifactor";
    repo = pname;
    # rev = "v${version}";
    rev = "4823c21c87d68f919d5144eb05722701cf23494e";
    hash = "sha256-W2lOxkBqQMX16QasagZIKADuBhO6ueJAGPAl4+cbI2E=";
  };

  postPatch = ''
    install -Dm444 ${reqs} ./${reqs.name}
  '';

  buildInputs = [
    cacert
    openssl
  ];

  propagatedBuildInputs = with pypkgs; [
    jinja2
    pandas
    pygit2
    pytz
    tqdm
  ];

  checkInputs = with pypkgs; [
    git
    pytestCheckHook
  ];

  disabledTests = [
    # expects our checkout to be a real repository
    "test_incomplete_signature_does_not_crash_gitdata_classes"
  ];

  meta = with lib; {
    description = "Replacement for gitstats";
    license = licenses.gpl3Only;
  };
}
