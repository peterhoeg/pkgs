{
  stdenv,
  lib,
  fetchFromGitHub,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "paq9a";
  version = "1.0";

  src = fetchFromGitHub {
    owner = "FS-make-simple";
    repo = "paq9a";
    rev = finalAttrs.version;
    hash = "sha256-6eMqkob1zngy0iX6Ghn0ial32lE/7PaTKUVmP+F7lYI=";
  };

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin paq9a
    install -Dm444 -t $out/share/doc/paq9a *.md LICENSE

    runHook postInstall
  '';

  meta = with lib; {
    description = "PAQ9A compression archiver";
    mainProgram = "paq9a";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.all;
    homepage = "https://www.mattmahoney.net/dc/";
  };
})
