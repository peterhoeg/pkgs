{
  stdenvNoCC,
  lib,
  fetchurl,
  _7zz,
  unshield,
}:

let
  mkDrv = stdenvNoCC.mkDerivation;

  ### NOTE:
  # before adding another one, check if it's in the Sierra Collection below

in
{
  leisure-suit-larry-collection = mkDrv (
    finalAttrs:
    let
      dir = "share/${finalAttrs.pname}";
      # no idea why the versions are mapped so strangely and lsl4 doesn't exist
      lslMap = [
        {
          from = 1;
          to = 1;
        }
        {
          from = 7;
          to = 2;
        }
        {
          from = 6;
          to = 3;
        }
        {
          from = 4;
          to = 5;
        }
        {
          from = 3;
          to = 6;
        }
      ];

    in
    {
      pname = "leisure-suit-larry-collection";
      version = "1.0";

      src = fetchurl {
        url = "https://archive.org/download/Sierra_Leisure_Suit_Larry_Collection_Win98_2006_Eng/Sierra-LeisureSuitLarryCollection-DosboxWin98.iso";
        hash = "sha256-vumaqO7BPExQt5B7J449ZA+y12yR6/IcCAdfuzeSTOM=";
      };

      nativeBuildInputs = [
        _7zz
        unshield
      ];

      buildCommand = ''
        mkdir -p $out/${finalAttrs.passthru.baseDir}
        7zz x $src
        unshield -d . x data1.cab

        ${lib.concatMapStringsSep "\n" (
          e: "mv Files${toString e.from} $out/${dir}/leisure-suit-larry-${toString e.to}"
        ) lslMap}
      '';

      passthru.baseDir = dir;
    }
  );

  police-quest-collection = mkDrv (finalAttrs: {
    pname = "police-quest-collection-data";
    version = "1.0";

    src = fetchurl {
      url = "https://archive.org/download/pqcoll_202402/PQCOLL.ISO";
      hash = "sha256-IqDgWDrQ6nmWk7ShuBFk5rDcVHs746eMK2PDuoPehS8=";
    };

    nativeBuildInputs = [
      _7zz
      unshield
    ];

    buildCommand = ''
      mkdir -p $out/${finalAttrs.passthru.baseDir}
      7zz x $src
      unshield -d . x data1.cab
      for i in 1 2 3 4; do
        mv Files$i $out/${finalAttrs.passthru.baseDir}/police-quest-$i
      done
    '';

    passthru.baseDir = "share/${finalAttrs.pname}";
  });

  # https://archive.org/details/sierra-collection-1988-1993
  sierra-collection = mkDrv (
    finalAttrs:
    let
      sierraMap = [
        {
          from = "King's Quest 1 (1990)";
          to = "kings-quest-1";
        }
        {
          from = "Quest for Glory 1 VGA";
          to = "quest-for-glory-1";
        }
        {
          from = "Quest for Glory 2";
          to = "quest-for-glory-2";
        }
        {
          from = "Quest for Glory 3";
          to = "quest-for-glory-3";
        }
        {
          from = "Quest for Glory 4";
          to = "quest-for-glory-4";
        }
        {
          from = "Space Quest 1 (Ingles)";
          to = "space-quest-1";
        }
        {
          from = "Space Quest 2";
          to = "space-quest-2";
        }
        {
          from = "Space Quest 3";
          to = "space-quest-3";
        }
        {
          from = "Space Quest 4 (Ingles)";
          to = "space-quest-4";
        }
        {
          from = "Space Quest 5 (Ingles)";
          to = "space-quest-5";
        }
      ];

    in
    {
      pname = "sierra-collection-data";
      version = "1.0";

      src = fetchurl {
        url = "https://archive.org/download/sierra-collection-1988-1993/SIERRA_COLLECTION_1988_1993.iso";
        hash = "sha256-XYwFbtJrvDn5XJmCTZSNWovcVnSfT4FiuqIeZfxSihs=";
      };

      nativeBuildInputs = [ _7zz ];

      buildCommand = ''
        mkdir -p $out/${finalAttrs.passthru.baseDir}
        7zz x $src

        ${lib.concatMapStringsSep "\n" (
          e: ''mv "${e.from}" "$out/${finalAttrs.passthru.baseDir}/${e.to}"''
        ) sierraMap}

        for e in BAT COM DRV EXE; do
          find $out -type f -name \*.$e -delete
        done
      '';

      passthru.baseDir = "share/${finalAttrs.pname}";
    }
  );
}
