{
  stdenv,
  lib,
  fetchurl,
}:

let
  baseDir = "share/wallpapers";

  urls = map (e: fetchurl { inherit (e) name url hash; }) [
    {
      name = "winnix";
      url = "";
      hash = "";
    }
  ];

in
stdenv.mkDerivation rec {
  pname = "additional-wallpapers";
  version = "20230523";

  dontUnpack = true;

  dontConfigure = true;

  dontBuild = true;

  installPhase =
    ''
      runHook preInstall

      mkdir -p $out/${baseDir}
    ''
    + lib.concatMapStringsSep "\n" (e: '''') [ ]
    + ''

      runHook postInstall
    '';

  passthru = {
    inherit baseDir;
    supportsDarkLight = false;
  };

  meta = with lib; {
    description = "Additional wallpapers";
    license = licenses.free;
  };
}
