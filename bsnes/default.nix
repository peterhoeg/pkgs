{
  lib,
  stdenv,
  fetchFromGitHub,
  pkg-config,
  wrapGAppsHook3,
  libX11,
  libXv,
  udev,
  SDL2,
  gtk3,
  gtksourceview3,
  alsa-lib,
  libao,
  openal,
  libpulseaudio,
}:

stdenv.mkDerivation {
  pname = "bsnes";
  version = "115.unstable-2024-09-23";

  src = fetchFromGitHub {
    owner = "bsnes-emu";
    repo = "bsnes";
    rev = "cdef244f38e68b9bb6341630621f8e7387b0ca10";
    hash = "sha256-cuQSqnjPeAqhd7s4/n8j7n+7F3vFdh1MuJCNO0FtYto=";
  };

  nativeBuildInputs = [ pkg-config ] ++ lib.optionals stdenv.hostPlatform.isLinux [ wrapGAppsHook3 ];

  buildInputs =
    [
      SDL2
      libao
    ]
    ++ lib.optionals stdenv.hostPlatform.isLinux [
      libX11
      libXv
      udev
      gtk3
      gtksourceview3
      alsa-lib
      openal
      libpulseaudio
    ];

  enableParallelBuilding = true;

  makeFlags =
    [
      "-C"
      "bsnes"
      "prefix=$(out)"
    ]
    ++ lib.optionals stdenv.hostPlatform.isLinux [ "hiro=gtk3" ]
    ++ lib.optionals stdenv.hostPlatform.isDarwin [ "hiro=cocoa" ];

  # https://github.com/bsnes-emu/bsnes/issues/107
  preFixup = lib.optionalString stdenv.hostPlatform.isLinux ''
    gappsWrapperArgs+=(
      --prefix GDK_BACKEND : x11
    )
  '';

  meta = with lib; {
    description = "bsnes";
    license = licenses.gpl3Only;
    maintainers = with maintainers; [ peterhoeg ];
    platforms = platforms.unix;
    mainProgram = "bsnes";
  };
}
