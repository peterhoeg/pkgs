{
  stdenv,
  fetchFromGitHub,
  coreutils,
  glibc,
  gnugrep,
  systemd,
  util-linux,
}:

# this can be dropped - it's the old version in bash. py3 is the future!

stdenv.mkDerivation rec {
  pname = "systemd-swap";
  version = "4.3.3";

  src = fetchFromGitHub {
    owner = "Nefelim4ag";
    repo = pname;
    rev = version;
    sha256 = "sha256-z7+ep8wut2qRXZzDR20n5WToXfXCtpXS9tzA7Gvv85U=";
  };

  postPatch = ''
    substituteInPlace systemd-swap.service \
      --replace-fail /usr/bin $out/bin

    sed -i systemd-swap \
      -e '5iPATH=$PATH:${
        stdenv.lib.makeBinPath [
          coreutils
          glibc
          gnugrep
          systemd
          util-linux
        ]
      }' \
      -e 's@ETC_SYSD=.*@ETC_SYSD=/etc/systemd@'
  '';

  dontConfigure = true;
  dontBuild = true;

  makeFlags = [
    "PREFIX=${placeholder "out"}"
    "bindir=${placeholder "out"}/bin"
    "datadir=${placeholder "out"}/share"
    "libdir=${placeholder "out"}/lib"
    "mandir=${placeholder "out"}/share/man"
  ];

  postInstall = ''
    docdir=$out/share/doc/${pname}
    mkdir -p $docdir
    mv $out/etc/systemd/*.conf $docdir
    rm -rf $out/etc

    install -Dm444 -t $docdir *.md

    substituteInPlace $out/bin/systemd-swap \
      --replace-fail /usr ""
  '';

  meta = with stdenv.lib; { };
}
