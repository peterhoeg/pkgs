{
  lib,
  fetchFromGitHub,
  python3Packages,
  glibc,
  kmod,
  util-linux,
}:

let
  binPath = lib.makeBinPath [
    glibc
    kmod
    util-linux
  ];

in
python3Packages.buildPythonApplication rec {
  pname = "systemd-swap";
  version = "4.4.0";

  disabled = python3Packages.pythonOlder "3.7";

  src = fetchFromGitHub {
    owner = "Nefelim4ag";
    repo = pname;
    rev = version;
    sha256 = "sha256-YJTPvsUQrzZTP6RLxQrJTS39WJR0ycQ34Erj8pA5g8M=";
  };

  # we need to look for the config file in /etc/systemd but we obviously cannot
  # install anything there
  postPatch = ''
    substituteInPlace Makefile \
      --replace-fail ' $(CNF_T) ' ' '
  '';

  format = "other";

  propagatedBuildInputs = with python3Packages; [
    systemd
    sysv_ipc
  ];

  makeWrapperArgs = [
    "--prefix PATH :"
    binPath
  ];

  makeFlags = [
    "PREFIX=${placeholder "out"}"
    "bindir=${placeholder "out"}/bin"
    "datarootdir=${placeholder "out"}/share"
    "libdir=${placeholder "out"}/lib"
    "mandir=${placeholder "out"}/share/man"
    "sysconfdir=/etc"
  ];

  postInstall = ''
    install -Dm444 -t $out/share/doc/${pname} *.md LICENSE
    rm -rf $out/var
  '';

  installCheckPhase = ''
    $out/bin/systemd-swap --help
  '';

  meta = with lib; {
    description = "Script to manage swap on: zswap, zram, files and block devices.";
    license = licenses.gpl3;
    platforms = platforms.linux;
  };
}
