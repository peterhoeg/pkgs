{
  lib,
  fetchFromGitHub,
  python3,
  git,
  writeText,
}:

# TODO: doesn't work

let
  pypkgs = python3.pkgs;

in
pypkgs.buildPythonApplication rec {
  pname = "niet";
  version = "2.4.0";

  # format = "other";

  src = fetchFromGitHub {
    owner = "openuado";
    repo = pname;
    rev = version;
    hash = "sha256-K6bHYgocD7yBaA1dj06aYxkZpUnWqnE4GH+ssloN+TI=";
  };

  propagatedBuildInputs = with pypkgs; [
    jmespath
    pyyaml
  ];

  # no tests from pypi
  doCheck = false;

  meta = with lib; {
    description = "Query YAML from shell scripts";
  };
}
