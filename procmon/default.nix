{
  lib,
  stdenv,
  fetchFromGitHub,
  cmake,
  ncurses,
  kernelPackages ? null,
}:

stdenv.mkDerivation rec {
  pname = "procmon";
  version = "1.0";

  src = fetchFromGitHub {
    owner = "microsoft";
    repo = "ProcMon-for-Linux";
    rev = version;
    hash = "sha256-9CcPjJEboDEqxOVRkA9vcZoy+mj+vIZezTW/hD/GEjg=";
  };

  nativeBuildInputs = [
    cmake
    # kernelPackages.bcc
  ];

  buildInputs = [ ncurses ];

  meta = with lib; {
    description = "ProcMon";
    # license = licenses.gpl3;
    maintainers = with maintainers; [ peterhoeg ];
    broken = true; # just needs work
  };
}
