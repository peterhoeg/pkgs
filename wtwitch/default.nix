{
  lib,
  stdenv,
  fetchFromGitHub,
  go-md2man,
  installShellFiles,
  resholve,
  bash,
  coreutils,
  curl,
  fzf,
  gawk,
  gnugrep,
  gnused,
  jq,
  libnotify,
  ncurses,
  procps,
  streamlink,
}:

# NOTE: wtwitch supports both mpv and vlc as players. We don't want to force
# either so they are both "faked" via fake.external which means if you don't
# have either, this program will not work. You will however, get an error
# message that explains that.

let
  inherit (lib) getBin getExe;

in
resholve.mkDerivation rec {
  pname = "wtwitch";
  version = "2.5.1";

  src = fetchFromGitHub {
    owner = "krathalan";
    repo = pname;
    rev = version;
    hash = "sha256-8M3qDg8LxeHKuA6zHoNv+8hxSsQSQlSayQKKUMfhtqI=";
  };

  solutions = {
    wtwitch = {
      scripts = [ "bin/wtwitch" ];
      interpreter = getExe bash;
      inputs = [
        coreutils
        curl
        fzf
        gawk
        gnugrep
        gnused
        jq
        libnotify
        ncurses
        procps
        streamlink
      ];
      execer = [
        "cannot:${getExe fzf}"
        "cannot:${getBin procps}/bin/pgrep"
        "cannot:${getExe streamlink}"
      ];
      keep = {
        source = [ "$ENVIRONMENT_FILE" ];
        "$userPlayer" = true;
      };
      fix = {
        "$DATE_CMD" = if stdenv.isLinux then [ "date" ] else [ "gdate" ];
      };
      fake.external = [
        "mpv"
        "pacman"
        "vlc"
      ];
    };

    # we *technically* don't need to do this as the completion plugin requires
    # wtwitch and jq which are obviously there for twitch to work in the first
    # place, but this way we are sure to catch it if a new dependency is
    # introduced.
    bash = {
      scripts = [ "share/bash-completion/completions/wtwitch.bash" ];
      interpreter = getExe bash;
      inputs = [
        "${placeholder "out"}/bin"
        jq
      ];
      execer = [ "cannot:${placeholder "out"}/bin/wtwitch" ];
    };
  };

  nativeBuildInputs = [
    go-md2man
    installShellFiles
  ];

  buildPhase = ''
    runHook preBuild

    go-md2man -in src/wtwitch.1.scd -out wtwitch.1

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin src/wtwitch

    installShellCompletion \
      --cmd wtwitch \
      --bash src/wtwitch-completion.bash \
      --zsh src/_wtwitch

    # this is the output of go-md2man
    installManPage wtwitch.?

    runHook postInstall
  '';

  meta = with lib; {
    description = "Terminal user interface for Twitch";
    license = licenses.gpl3Only;
  };
}
