{
  lib,
  appimageTools,
  requireFile,
  makeDesktopItem,
}:

let
  pname = "drsprinto";

  icon = ./drsprinto.png;

  desktopItems = [
    (makeDesktopItem {
      name = pname;
      inherit icon;
      desktopName = "Dr Sprinto";
      genericName = "Dr Sprinto";
      exec = "@out@/bin/${pname}";
      comment = "Compliance tool";
      categories = [ "System" ];
      startupNotify = false;
      keywords = [
        "system"
        "compliance"
        "network"
        "security"
      ];
    })
  ];

in
appimageTools.wrapType2 rec {
  inherit pname;
  version = "2.5.8";

  src = requireFile rec {
    name = "DrSprinto-${version}.AppImage";
    sha256 = "1v5ixp9ad5da3brzx0rp9h57bpv03wac00i4bxbg787fsgbk535a";
    message = ''
      Download from https://app.sprinto.com/

      nix-prefetch-url file://\$PWD/${name}
    '';
  };

  extraInstallCommands =
    ''
      mv $out/bin/${pname}-${version} $out/bin/${pname}
    ''
    + lib.concatMapStringsSep "\n " (e: ''
      install -Dm444 -t $out/share/applications ${e}/share/applications/*.desktop
    '') desktopItems
    + ''
      install -Dm444 ${icon} $out/share/icons/apps/${pname}.jpg;

      for f in $out/share/applications/*.desktop; do
        substituteInPlace $f --subst-var out
      done
    '';

  meta = with lib; {
    description = "Dr Sprinto compliance tool";
    license = licenses.free;
    hydraPlatforms = platforms.none;
  };
}
