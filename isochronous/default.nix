{
  stdenv,
  lib,
  fetchFromGitHub,
  openssl,
}:

stdenv.mkDerivation rec {
  pname = "isochronous";
  version = "unstable-2018-05-04";

  src = fetchFromGitHub {
    owner = "apenwarr";
    repo = pname;
    rev = "432914f991379ab04397002e6b15b232b07cd9f8";
    sha256 = "sha256-BNaJIiGmTn6CaX+iLJNEw0p78mrwJ/zXmWok8I6Cvl0=";
  };

  buildInputs = [ openssl ];

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin isoping isostream udpstress

    runHook postInstall
  '';

  enableParallelBuilding = true;

  meta = with lib; {
    description = "Precise timing related network testing tools: isoping, isostream, udpstress.";
    license = licenses.asl20;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
