{
  lib,
  python3,
  fetchFromGitHub,
  makeWrapper,
  androidenv,
}:

let
  pypkgs =
    (python3.override {
      packageOverrides = self: super: {
        folium = super.folium.overridePythonAttrs (old: rec {
          doCheck = false;
        });
      };
    }).pkgs;

in
pypkgs.buildPythonApplication rec {
  pname = "geowifi";
  version = "20220210";

  format = "other";

  src = fetchFromGitHub {
    owner = "GONZOsint";
    repo = pname;
    rev = "0061c7d35d198dd33d5cc74ef584f271f3a2337e";
    hash = "sha256-xElxxdOdNVLWauWpKsJ3q4m3Nnp61ef5UoIYdWZ7EXk=";
  };

  postPatch = ''
    sed -i utils/searcher.py \
      -e '2iimport os'

    substituteInPlace utils/searcher.py \
      --replace-fail "'utils/API.yaml'" "os.environ.get(\"GEOWIFI_API_YAML\", \"$out/share/geowifi/API.yaml\")"
  '';

  propagatedBuildInputs = with pypkgs; [
    folium
    google-api-python-client
    pyyaml
    requests
    rich
  ];

  nativeBuildInputs = [ makeWrapper ];

  installPhase = ''
    dir=$out/lib/${pypkgs.python.libPrefix}/site-packages
    mkdir -p $dir
    cp -r geowifi.py helpers utils $dir/

    install -Dm444 -t $out/share/geowifi $dir/utils/API.yaml

    makeWrapper ${pypkgs.python.interpreter} $out/bin/geowifi \
      --prefix PYTHONPATH : ${
        pypkgs.makePythonPath ([ (placeholder "out") ] ++ propagatedBuildInputs)
      } \
      --add-flags "$dir/geowifi.py"
  '';

  # pythonImportsCheck = [ (lib.replaceStrings [ "-" ] [ "_" ] pname) ];

  meta = with lib; {
    description = "Search WiFi geolocation data by BSSID and SSID on different public databases";
    license = licenses.gpl3Only;
    mainProgram = pname;
  };
}
