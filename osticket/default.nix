{
  stdenv,
  lib,
  fetchFromGitHub,
  writeShellScriptBin,
  php,
}:

let
  php' = php;

  fakeGit = writeShellScriptBin "git" ''
    exit 0
  '';

in

stdenv.mkDerivation rec {
  pname = "osticket";
  version = "1.16.3";

  src = fetchFromGitHub {
    owner = "osticket";
    repo = "osticket";
    rev = "v${version}";
    hash = "sha256-hh2fZXLRU4YUBXfhxQh4pz0nwckJL5j04M5vIUFrMGk=";
  };

  dontBuild = true;

  nativeBuildInputs = [
    fakeGit
    php'
  ];

  installPhase = ''
    runHook preInstall

    php manage.php deploy --setup $out/share/osticket/

    runHook preInstall
  '';

  meta = with lib; {
    description = "Ticketing/helpdesk system";
    license = licenses.free;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
